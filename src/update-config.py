"""
Update the main config file for Bluberry.
"""

__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import os
import json

import core.BluberryLogging

logger = core.getLogger("Update")

logger.info("Loading Bluberry config file")
with open('core/conf/conf.json', 'r') as f:
    conf = json.loads(f.read())

if 'NODE_ID' in os.environ:
    conf['local']['nodeId'] = os.environ['NODE_ID']
if 'DB' in os.environ:
    conf['local']['db'] = os.environ['DB']
if 'MSG' in os.environ:
    conf['local']['msg'] = os.environ['MSG']

if 'HOST_IP' in os.environ:
    conf['local']['docker-host'] = os.environ['HOST_IP']
if 'HOSTNAME' in os.environ:
    conf['local']['hostname'] = os.environ['HOSTNAME']

if 'NUM_NODES' in os.environ and 'NODE_PREFIX' in os.environ:
    for n in range(int(os.environ['NUM_NODES'])):
        n += 1
        node = f"{os.environ['NODE_PREFIX']}{n}"
        logger.debug(f"Collecting information for node {node}")

        if f"DB_IP_INT{n}" in os.environ:
            conf['nodes'][node]['db-host-int'] = os.environ[f"DB_IP_INT{n}"]
        if 'DB_PORT' in os.environ:
            conf['nodes'][node]['db-port-int'] = os.environ['DB_PORT']
        if f"MSG_IP_INT{n}" in os.environ:
            conf['nodes'][node]['msg-host-int'] = os.environ[f"MSG_IP_INT{n}"]
        if 'MSG_PORT' in os.environ:
            conf['nodes'][node]['msg-port-int'] = os.environ['MSG_PORT']
        # TODO: should this stay in?
        if f"MSG_IP_EXT{n}" in os.environ:
            conf['nodes'][node]['msg-host-virt'] = os.environ[f"MSG_IP_EXT{n}"]
        if 'MSG_PORT' in os.environ:
            conf['nodes'][node]['msg-port-virt'] = os.environ['MSG_PORT']
        if 'HOST_IP' in os.environ:
            conf['nodes'][node]['msg-host-ext'] = os.environ['HOST_IP']
        if f"MSG_PORT{n}" in os.environ:
            conf['nodes'][node]['msg-port-ext'] = os.environ[f"MSG_PORT{n}"]

logger.debug(f"Writing config file")
with open('core/conf/conf.json', 'w') as f:
    json.dump(conf, f, indent=4)

logger.debug(conf)

logger.info("Finished updating Bluberry config")

