__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g16@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u16@soton.ac.uk"]


import time
from typing import List, Set

import core.BluberryLogging
from core.IBluberryDBWriter import IBluberryDBWriter
from core.BluberryMessage import Message
from core.BluberryConf import Conf

from db.mysql.MySQLConnector import MySQLConnector
from db.mysql.models.Transaction import Transaction
from db.mysql.models.Statement import Statement
from db.mysql.constants import to_sql_string, filter_tables
from db.mysql.MySQLConnector import BLUBERRY_DB_NAME,\
                                    WRITER_STATUS_TABLE_NAME,\
                                    TRANSACTION_LOG_TABLE_NAME,\
                                    STATEMENT_LOG_TABLE_NAME,\
                                    ID_MAPPING_TABLE_NAME,\
                                    LOCAL_COUNT_TABLE_NAME
from db.mysql.ConflictManager import ConflictManager
from db.mysql.MySQLIDTranslationManager import MySQLIDTranslationManager


class BluberryMySQLWriter(IBluberryDBWriter):
    """
    MySQL Reader class for the Bluberry system. This class generates a list of updates that
    have occured in the database and packages them in messages to be sent to other nodes.
    """

    def __init__(self, conf: Conf):
        """
        config : `Conf` - The configuration for the node that this Reader is to run on
        """
        self.__logger = core.getLogger(self.__class__.__name__)
        self.__conf = conf
        self.__nodeId = conf.get('local', 'nodeId')
        self.__schema = self.__conf.get('db', 'connection', 'database')
        self.__mysql = MySQLConnector(conf)

        self.__all_tables = []
        for row in self.__mysql.execute(f"SHOW TABLES FROM {self.__schema}"):
            if row is not None and row[0] is not None and row[0] != WRITER_STATUS_TABLE_NAME:
                self.__all_tables.append(row[0])

        # Set up writer status table. It must be in the main database (not in the bluberry
        # database) because the reader needs to see updates to this.
        self.__mysql.execute(f"DROP TABLE IF EXISTS {self.__schema}."
                             f"{WRITER_STATUS_TABLE_NAME};")
        self.__mysql.execute(f"CREATE TABLE IF NOT EXISTS {self.__schema}."
                             f"{WRITER_STATUS_TABLE_NAME} (tables TEXT);")
        self.__mysql.execute(f"INSERT INTO {self.__schema}."
                             f"{WRITER_STATUS_TABLE_NAME} VALUES ('');")

        self.__conflict_manager = ConflictManager(self.__conf)
        self.__id_translation_manager = MySQLIDTranslationManager(self.__conf)

        self.__is_engaged = False

        self.__filter_mode = conf.get('db', 'sync', 'filtering')
        self.__listed_tables = conf.get('db', 'sync', 'tables')
        # TODO; check if this is needed
        # if self.__filter_mode == "whitelist":
        #     if WRITER_STATUS_TABLE_NAME not in self.__listed_tables:
        #         self.__listed_tables.append(WRITER_STATUS_TABLE_NAME)

        self.setup_db_modes()

        self.__logger.info(f"{self.__nodeId}: MySQL Writer initialised successfully")

    def prepare(self):
        """
        Ensure the writer is not already busy with some other work, and lock all the tables
        we need to ensure the transaction is correct.
        """
        if self.isEngaged():
            raise RuntimeError("Writer cannot prepare while already engaged")

        if self.__mysql.isConnected():
            self.__logger.warning("Already connected!")

        self.__is_engaged = True

        self.__mysql.connect()
        self.__mysql.execute(f"SET GLOBAL AUTOCOMMIT = {False};")
        tables = self.find_tables()
        self.__lock_tables(tables)
        self.__mysql.execute(f"SET GLOBAL SQL_MODE = '{self.__new_mode}';")

        if not self.__mysql.isConnected():
            self.__logger.warning("Connection lost during preparing - "
                                  "this might not work properly.")

    def runUpdates(self, updates: List[Message] = []):
        """
        Apply the updates provided to the database.

        updates : `List[Message]` - The updates from other nodes that are to be processed locally
        """
        self.__logger.info(f"{self.__nodeId}: Running {len(updates)} new update(s)")
        # TODO how do we handle errors? We need to work out what the recovery
        # process is for when these services fail
        for update in updates:
            self.handle_incoming_update(update)
        self.__logger.info(f"{self.__nodeId}: Update(s) complete")
        return True

    def finalise(self):
        """
        Unlock the tables and signal that the writer is no longer busy.
        """
        if not self.__mysql.isConnected():
            self.__logger.warning("Finalising outside a transaction - "
                                  "this might not work as expected")
        try:
            self.__mysql.execute(f"SET GLOBAL SQL_MODE = '{self.__original_mode}';")
            self.__logger.debug(f"MySQL is connected? {self.__mysql.isConnected()}")
            self.__unlock_all_tables()
            self.__logger.debug(f"MySQL is connected? {self.__mysql.isConnected()}")
            self.__mysql.commit()
        except Exception as e:
            self.__logger.error(e)
            self.__mysql.rollback()
        finally:
            self.__mysql.disconnect()
            self.__is_engaged = False

    def isEngaged(self) -> bool:
        """
        Returns : `bool` - If the writer is currently engaged with processing updates
        """
        return self.__is_engaged

    # #### MySQL database methods #### #

    def setup_db_modes(self):
        """
        Set the database to the correct modes for use in the Bluberry system.
        """
        self.__logger.debug(f"{self.__nodeId}: Capturing MySQL database modes")

        result = self.__mysql.execute("SHOW VARIABLES LIKE 'sql_mode';")
        data = result[0]
        self.__original_mode = data[1]
        self.__new_mode = self.__original_mode \
            .replace('NO_ZERO_IN_DATE', '') \
            .replace('NO_ZERO_DATE', '') \
            .replace(',,', ',') \
            .replace(',,', ',')

    def get_db_autocommit(self) -> bool:
        """
        Returns : `bool` - If the database is currently set to autocommit updates
        """
        result = self.__mysql.execute("SHOW VARIABLES LIKE 'autocommit';")
        if(result[0][0] == "autocommit"):
            return result[0][1] == "ON"
        else:
            raise RuntimeError("Variable 'autocommit' not found")

    def get_db_foreign_key_checks(self) -> bool:
        """
        Returns : `bool` - If the database is currently enforcing foreign key checks
        """
        result = self.__mysql.execute("SHOW VARIABLES LIKE 'foreign_key_checks';")
        if(result[0][0] == "foreign_key_checks"):
            return result[0][1] == "ON"
        else:
            raise RuntimeError("Variable 'foreign_key_checks' not found")

    def get_db_sql_mode(self) -> str:
        """
        Returns : `str` - The current mode of the database
        """
        result = self.__mysql.execute("SHOW VARIABLES LIKE 'sql_mode';")

        if(result[0][0] == "sql_mode"):
            return result[0][1]
        else:
            raise RuntimeError("Variable 'sql_mode' not found")

    # #### Update methods #### #

    def handle_incoming_update(self, update: Message):
        """
        Process the transaction contained in a Message and apply it to the database, ensuring
        to signal to the reader via the writer_status table that this update is to be ignored.
        The transaction is first checked for any conflicts with other records in the database.

        update : `Message` - The message containing the update `Transaction` as its payload
        """
        self.__logger.debug(f"{self.__nodeId}: Writer handling incoming transaction")

        transaction = Transaction.deserialise(update.getPayload())

        (transaction, write_transaction) =\
            self.__conflict_manager.get_conflictResolved_transaction(transaction)

        if write_transaction:
            self.__logger.debug(f"{self.__nodeId} Writing transaction to local database")
            tables = transaction.get_tables()
            statements = transaction.get_statements()
            # Update the writer status table to tell the reader on this node that the
            # updates came from us
            self.update_writer_status(tables)
            self.__mysql.connect()
            for i, statement in enumerate(statements):
                # Convert the statement from the global namespace into the local through
                # the ID translator
                statement = self.map_statement_to_local_namespace(statement)
                query = to_sql_string(statement)
                self.__logger.info(f"{self.__nodeId}: Writing statement {i + 1} / "
                                   f"{len(statements)} to database")
                self.__mysql.execute(query)

            self.__mysql.commit()
            self.__mysql.disconnect()
            # Update the writer status table to tell the reader on this node that
            # future updates can be read again as normal
            self.update_writer_status()
        else:
            self.__logger.debug(
                f"{self.__nodeId} Ignoring transaction due to conflict handling result")

    def map_statement_to_local_namespace(self, statement: Statement) -> Statement:
        """
        A helper function to convert a statement out of the global namespace that it is in
        when it arrives from another node into the local namespace of the database.

        statement : `Statement` - The incoming, globally namespaced statement

        Returns : `Statement` - The statement with the values translated into the local namespace
        """
        (local_before, local_after) =\
            self.__id_translation_manager.map_foreignUpdate_to_localValues(
                statement.get_before(), statement.get_after(), statement.get_type(),
                statement.get_table())

        statement.set_before(local_before)
        statement.set_after(local_after)

        return statement

    # #### Writer status methods #### #

    def update_writer_status(self, tables_to_ignore: Set[str] = set()):
        """
        Update the writer status table with the tables that have been updated by the writer,
        in order to signal to the reader which tables to ignore.

        tables_to_ignore : `Set[str]` - The table name to ignore
        """
        table_str = ",".join(sorted(tables_to_ignore))

        query = f"UPDATE `{self.__schema}`.`{WRITER_STATUS_TABLE_NAME}` "\
                f"SET tables = '{table_str}';"
        self.__mysql.execute(query)

    # #### Database methods #### #

    def find_tables(self) -> List[str]:
        """
        Find all the tables in the database that are relevant to the system based on the
        configured filtering rules.

        Returns : `List[str]` - The filtered tables in the database, sorted alphabetically
        """
        filtered_tables = filter_tables(
            self.__all_tables,
            self.__listed_tables,
            self.__filter_mode)

        return sorted(filtered_tables)

    def __get_locked_tables(self, database: str) -> List[str]:
        """
        Returns a list of the tables that have been locked by the writer in a database.

        database : `str` - The name of the database to lock tables

        Returns - `List[str]`
        """
        if not self.__mysql.isConnected():
            raise RuntimeError("Database is not connected but getting locked "
                               "tables needs to be done within a transaction.")

        query = f"SHOW OPEN TABLES IN `{self.__schema}` WHERE `in_use`=1;"
        result = self.__mysql.execute(query)

        table_names = []
        for _, table_name, *_ in result:
            table_names.append(table_name)
        filtered_tables = filter_tables(table_names, self.__listed_tables, self.__filter_mode)

        if WRITER_STATUS_TABLE_NAME in table_names:
            filtered_tables.append(WRITER_STATUS_TABLE_NAME)

        return filtered_tables

    def __lock_tables(self, tables_to_lock: List[str]):
        """
        Locks the given list of tables - throws an error if it wasn't possible to lock all
        of them.

        tables_to_lock : `List[str]`
        """
        if not self.__mysql.isConnected():
            raise RuntimeError("Database is not connected but locking "
                               "tables needs to be done within a transaction.")

        if WRITER_STATUS_TABLE_NAME not in tables_to_lock:
            tables_to_lock.append(WRITER_STATUS_TABLE_NAME)
        self.__logger.debug(f"{self.__nodeId}: Locking tables - {tables_to_lock}")

        all_tables_lock = ", ".join(map(lambda x: f"`{self.__schema}`.`{x}` WRITE",
                                        tables_to_lock))
        # Make sure to lock the two bluberry tables as well since we will be writing to them
        all_tables_lock += f", `{BLUBERRY_DB_NAME}`.`{ID_MAPPING_TABLE_NAME}` WRITE, "\
                           f"`{BLUBERRY_DB_NAME}`.`{LOCAL_COUNT_TABLE_NAME}` WRITE, "\
                           f"`{BLUBERRY_DB_NAME}`.`{STATEMENT_LOG_TABLE_NAME}` WRITE, "\
                           f"`{BLUBERRY_DB_NAME}`.`{TRANSACTION_LOG_TABLE_NAME}` WRITE;"
        # This must be done in one LOCK TABLES statement, otherwise the first locks are released
        self.__mysql.execute(f"LOCK TABLES {all_tables_lock}")

        num_locked = len(self.__get_locked_tables(self.__schema))
        if num_locked < len(tables_to_lock) - 1:
            raise RuntimeError(f"Locking all {len(tables_to_lock)} tables failed. Locked "
                               f"{num_locked} tables but could not acquire locks for "
                               f"{len(tables_to_lock) - num_locked} tables.")
        else:
            self.__logger.debug(f"Locked {num_locked} tables.")

    def __unlock_all_tables(self, retries: int = 3):
        """
        Unlock all the tables in the database locked by the writer. Requires retries as this
        method can fail silently if other writer operations are still ongoing when it is called.

        retries : `int` - Number of times to retry unlocking the tables before an error is raised
        """
        if not self.__mysql.isConnected():
            raise RuntimeError("Database is not connected but unlocking "
                               "tables needs to be done within a transaction.")

        self.__logger.debug(f"{self.__nodeId}: Unlocking tables")
        self.__mysql.execute("UNLOCK TABLES;")

        num_locked = len(self.__get_locked_tables(self.__schema))
        if num_locked > 0:
            if retries > 0:
                time.sleep(0.5)
                self.__unlock_all_tables(retries - 1)
            else:
                raise RuntimeError(f"Unlocking all tables failed. Could not acquire locks for "
                                   f"{num_locked} tables")

