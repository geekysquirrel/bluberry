__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import unittest
import json

import core.BluberryLogging
from db.mysql.models.Statement import Statement, StatementType, StatementConsentRequirement


class test_BluberryConflictStrategies(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")

    def tearDown(self):
        self.__logger.info(f"Finished test {self._testMethodName}")

    def test_init(self):
        s = Statement({}, {}, None, None, [])
        self.assertEqual(s.get_before(), {})
        self.assertEqual(s.get_after(), {})
        s = Statement(None, None, None, None, [])
        self.assertEqual(s.get_before(), {})
        self.assertEqual(s.get_after(), {})

    def test_converter(self):
        class Foo():
            pass
        s = Statement()
        s.set_related_afterValues(Foo())
        self.assertIn('Foo object', str(s))

    def test_deserialise_consent_requirement_normal(self):
        s1 = StatementConsentRequirement("mytable", ["individual1"], "OWNERTYPE")
        s2 = StatementConsentRequirement.deserialise(json.dumps(s1._asdict()))
        self.assertEqual(s1._asdict(), s2._asdict())

    def test_deserialise_consent_requirement_missing_value(self):
        self.assertRaises(RuntimeError, StatementConsentRequirement.deserialise, json.dumps({}))

    def test_consent_requirement_getters(self):
        s = StatementConsentRequirement("mytable", ["individual1"], "OWNERTYPE")
        self.assertEqual(s.get_consent_tag(), "mytable")
        self.assertEqual(s.get_consenting_individuals(), ["individual1"])
        self.assertEqual(s.get_consent_owner_type(), "OWNERTYPE")

    def test_consent_requirement_repr(self):
        s = StatementConsentRequirement("mytable", ["individual1"], "OWNERTYPE")
        self.assertEqual(s.__repr__(), "Consent Requirement: Type: OWNERTYPE, "
                         "Individual: ['individual1'], with consent_tag: mytable")

    def test_deserialise_normal(self):
        s1 = Statement({"num": 1}, {"num": 2}, StatementType.UPDATE, "`A`")
        s2 = Statement.deserialise(json.dumps(s1._asdict()))
        self.assertEqual(s1._asdict(), s2._asdict())

    def test_deserialise_missing_value(self):
        faulty_nobefore = {"after": {"num": 2},
                           "related_before": {}, "related_after": {},
                           "table": "`A`", "type": 1, "statement_consent_reqs": [],
                           "beforeHash": "", "afterHash": ""}
        self.assertRaises(RuntimeError, Statement.deserialise, json.dumps(faulty_nobefore))

    def test_deserialise_with_consent_requirement(self):
        s1 = Statement()
        scr = StatementConsentRequirement("mytable", ["individual1"], "OWNERTYPE")
        s1.get_consent_reqs().append(scr)
        s2 = Statement.deserialise(json.dumps(s1._asdict()))
        self.assertEqual(s1._asdict(), s2._asdict())

    def test_get_set_before(self):
        s = Statement()
        self.assertEqual(s.get_before(), {})
        s.set_before({'foo': 'bar'})
        self.assertEqual(s.get_before(), {'foo': 'bar'})

    def test_get_set_after(self):
        s = Statement()
        self.assertEqual(s.get_after(), {})
        s.set_after({'foo': 'bar'})
        self.assertEqual(s.get_after(), {'foo': 'bar'})

    def test_get_extended_state(self):
        s = Statement()
        self.assertEqual(s.get_extendedState_beforeStr(), '{}')
        self.assertEqual(s.get_extendedState_afterStr(), '{}')

    def test_add_related(self):
        s = Statement()
        self.assertEqual(s.get_related_beforeValues(), {})
        self.assertEqual(s.get_related_afterValues(), {})
        s.set_related_beforeValues({'foo': 'bar'})
        s.set_related_afterValues({'foo': 'bar'})
        self.assertEqual(s.get_related_afterValues_str(), '{"foo": "bar"}')
        self.assertEqual(s.get_related_beforeValues(), {'foo': 'bar'})
        self.assertEqual(s.get_related_afterValues(), {'foo': 'bar'})
        self.assertEqual(s.get_related_afterValues_str(), '{"foo": "bar"}')
        s.add_related_beforeValue({'foo': 'baz'})
        s.add_related_afterValue({'foo': 'baz'})
        self.assertEqual(s.get_related_beforeValues(), {'foo': 'baz'})
        self.assertEqual(s.get_related_afterValues(), {'foo': 'baz'})
        self.assertEqual(s.get_related_afterValues_str(), '{"foo": "baz"}')

    def test_get_set_hashes(self):
        s = Statement()
        self.assertEqual(s.get_beforeState_hash(), '')
        self.assertEqual(s.get_afterState_hash(), '')
        s.set_beforeState_hash('foo')
        s.set_afterState_hash('foo')
        self.assertEqual(s.get_beforeState_hash(), 'foo')
        self.assertEqual(s.get_afterState_hash(), 'foo')


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner())

