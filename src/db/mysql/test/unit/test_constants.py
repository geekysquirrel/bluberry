__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g16@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u16@soton.ac.uk"]


import unittest
import core.BluberryLogging

from db.mysql.models.Statement import Statement, StatementType
from db.mysql.constants import to_sql_string, convert_dict_to_sql_update_delete_str,\
                               convert_dict_to_sql_strs, filter_tables


class test_constants(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")

    def tearDown(self):
        self.__logger.info(f"Finished test {self._testMethodName}")

    def convert_dict_to_sql_strs(self):
        input_dict = {
            "num": 1,
            "foo": 2
        }

        result = convert_dict_to_sql_strs(input_dict)
        self.assertTupleEqual(result, ("(foo, num)", "('2', '1')"))

    def test_convert_dict_to_sql_strs_with_none(self):
        input_dict = {
            "num": None
        }

        result = convert_dict_to_sql_strs(input_dict)
        self.assertTupleEqual(result, ("(num)", "(NULL)"))

    def test_convert_dict_to_sql_strs_with_string(self):
        input_dict = {
            "foo": "bar"
        }

        result = convert_dict_to_sql_strs(input_dict)
        # TODO: not sure if that's what we should get...
        self.assertTupleEqual(result, ("(foo)", "('bar')"))

    def test_convert_dict_to_sql_update_delete_str_no_conditions(self):
        input_dict = {
            "num": 1
        }

        result = convert_dict_to_sql_update_delete_str(input_dict)
        self.assertEqual(result, "num = '1'")

    def test_convert_dict_to_sql_update_delete_str_with_none(self):
        input_dict = {
            "num": None
        }

        result = convert_dict_to_sql_update_delete_str(input_dict)
        self.assertEqual(result, "num = NULL")

    def test_convert_dict_to_sql_update_delete_str_multiple(self):
        input_dict = {
            "text": "one",
            "num": 1
        }

        result = convert_dict_to_sql_update_delete_str(input_dict)
        self.assertEqual(result, "num = '1', text = 'one'")

    def test_convert_dict_to_sql_update_delete_str_with_conditions(self):
        input_dict = {
            "num": 1,
            "text": "one"
        }

        result = convert_dict_to_sql_update_delete_str(input_dict, True)
        self.assertEqual(result, "num = '1' AND text = 'one'")

        result = convert_dict_to_sql_update_delete_str(input_dict, False)
        self.assertEqual(result, "num = '1', text = 'one'")

    def test_convert_dict_to_sql_update_delete_str_null_with_conditions(self):
        input_dict = {
            "num": 1,
            "text": None
        }

        result = convert_dict_to_sql_update_delete_str(input_dict, True)
        self.assertEqual(result, "num = '1' AND text IS NULL")

        result = convert_dict_to_sql_update_delete_str(input_dict, False)
        self.assertEqual(result, "num = '1', text = NULL")

    def test_to_sql_string_insert(self):
        statement = Statement(None, {"num": 1}, StatementType.INSERT, "`A`")
        result = to_sql_string(statement)

        self.assertEqual(result, "INSERT INTO `A` (num) VALUES ('1');")

    def test_to_sql_string_update(self):
        statement = Statement({"num": 1}, {"num": 2}, StatementType.UPDATE, "`A`")
        result = to_sql_string(statement)

        self.assertEqual(result, "UPDATE `A` SET num = '2' WHERE num = '1';")

    def test_to_sql_string_delete(self):
        statement = Statement({"num": 1}, None, StatementType.DELETE, "`A`")
        result = to_sql_string(statement)

        self.assertEqual(result, "DELETE FROM `A` WHERE num = '1';")

    def test_to_sql_string_raises_error(self):
        statement = Statement({"num": 1}, None, 4, "`A`")
        self.assertRaises(RuntimeError, to_sql_string, statement)

    def test_filter_tables_blacklist(self):
        found_tables = ["A", "B"]
        listed_tables = ["A"]

        result = filter_tables(found_tables, listed_tables, "blacklist")

        self.assertEqual(len(result), 1)
        self.assertListEqual(result, ["B"])

    def test_filter_tables_whitelist(self):
        found_tables = ["A", "B"]
        listed_tables = ["A"]

        result = filter_tables(found_tables, listed_tables, "whitelist")

        self.assertEqual(len(result), 1)
        self.assertListEqual(result, ["A"])

    def test_filter_tables_default(self):
        found_tables = ["A", "B"]
        listed_tables = ["A"]

        self.assertRaises(Exception, filter_tables, found_tables, listed_tables, "")


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner())

