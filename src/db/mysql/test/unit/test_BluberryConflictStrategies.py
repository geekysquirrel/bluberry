__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g20@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u20@soton.ac.uk"]


import unittest
import core.BluberryLogging

from db.mysql.strategies.Escalate import Escalate
from db.mysql.strategies.LastWins import LastWins
from db.mysql.strategies.FirstWins import FirstWins
from db.mysql.strategies.ABluberryConflictStrategy import ConflictResolutionType
from db.mysql.models.Statement import Statement


class test_BluberryConflictStrategies(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")
        # three conflicting statements with the current one being the middle one time-wise.
        cls.__conflicts_middle = [{
            "before_values": {"num": 1},
            "after_values": {"num": 2},
            "unix_timestamp": 0,
            "current": 0
        }, {
            "before_values": {"num": 3},
            "after_values": {"num": 4},
            "unix_timestamp": 1,
            "current": 1
        }, {
            "before_values": {"num": 5},
            "after_values": {"num": 6},
            "unix_timestamp": 2,
            "current": 0
        }]
        # simple conflict with the current statement being older
        cls.__conflicts_first = [{
            "before_values": {"num": 1},
            "after_values": {"num": 2},
            "unix_timestamp": 0,
            "current": 1
        }, {
            "before_values": {"num": 3},
            "after_values": {"num": 4},
            "unix_timestamp": 1,
            "current": 0
        }]
        # simple conflict with the current statement being newer
        cls.__conflicts_last = [{
            "before_values": {"num": 1},
            "after_values": {"num": 2},
            "unix_timestamp": 0,
            "current": 0
        }, {
            "before_values": {"num": 3},
            "after_values": {"num": 4},
            "unix_timestamp": 1,
            "current": 1
        }]

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")

    def tearDown(self):
        self.__logger.info(f"Finished test {self._testMethodName}")

    def test_escalation(self):
        strategy = Escalate()

        # resolve with no conflicts
        self.assertRaises(RuntimeError, strategy.resolve, [])

        # resolve with conflicts
        result = strategy.resolve(self.__conflicts_middle)
        self.assertTupleEqual(result, ({"num": 3}, {"num": 4}, ConflictResolutionType.ESCALATION))

        result = strategy.resolve(self.__conflicts_first)
        self.assertTupleEqual(result, ({"num": 1}, {"num": 2}, ConflictResolutionType.ESCALATION))

        result = strategy.resolve(self.__conflicts_last)
        self.assertTupleEqual(result, ({"num": 3}, {"num": 4}, ConflictResolutionType.ESCALATION))

    def test_firstwins(self):
        strategy = FirstWins()

        # resolve with no conflicts
        self.assertRaises(RuntimeError, strategy.resolve, [])

        # resolve with conflicts
        result = strategy.resolve(self.__conflicts_middle)
        self.assertEqual(result[2], ConflictResolutionType.ACCEPT_NEW)

        result = strategy.resolve(self.__conflicts_first)
        self.assertEqual(result[2], ConflictResolutionType.REJECT_NEW)

        result = strategy.resolve(self.__conflicts_last)
        self.assertEqual(result[2], ConflictResolutionType.ACCEPT_NEW)

    def test_lastwins(self):
        strategy = LastWins()

        # resolve with no conflicts
        self.assertRaises(RuntimeError, strategy.resolve, [])

        # resolve with conflicts
        result = strategy.resolve(self.__conflicts_middle)
        self.assertEqual(result[2], ConflictResolutionType.ACCEPT_NEW)

        result = strategy.resolve(self.__conflicts_first)
        self.assertEqual(result[2], ConflictResolutionType.ACCEPT_NEW)

        result = strategy.resolve(self.__conflicts_last)
        self.assertEqual(result[2], ConflictResolutionType.REJECT_NEW)


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner())

