__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g16@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u16@soton.ac.uk"]


import unittest
from pathlib import Path

import core.BluberryLogging
from core.BluberryConf import Conf

from db.mysql.MySQLConnector import MySQLConnector
from db.mysql.ConsentManager import ConsentManager
from db.mysql.models.Transaction import Transaction
from db.mysql.models.Statement import Statement, StatementConsentRequirement, StatementType
from db.mysql.MySQLIDTranslationManager import MySQLIDTranslationManager


class itest_ConsentManager(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        core.setLogLevel('MySQLConnector', 'warning')
        core.setLogLevel('MySQLIDTranslationManager', 'warning')
        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")
        cls.__conf = Conf('core/conf/conf.json')

        cls.__test_db = 'test'
        tmpconf = cls.__conf.getConf()
        tmpconf['db']['connection']['database'] = cls.__test_db
        # using prefix as defined in test consent records
        tmpconf['local']['networkPrefix'] = '3TC'
        # needed to create mappings for pre-existing data
        tmpconf['db']['sync']['static_tables'] = tmpconf['db']['sync']['tables']
        cls.__conf.setConf(tmpconf)

        cls.__mysql = MySQLConnector(cls.__conf)
        cls.__prefix = cls.__conf.get('local', 'networkPrefix')

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")

        # Ensure database is clean
        self.__mysql.connect(None)
        self.__mysql.execute(f"DROP DATABASE IF EXISTS {self.__test_db};")
        self.__mysql.execute(f"CREATE DATABASE {self.__test_db};")
        self.__mysql.execute(f"USE {self.__test_db};")
        self.__mysql.runScript("db/mysql/test/resources/setup.sql")
        self.__mysql.disconnect()
        self.__mysql.resetBluberryDatabase()

        self.__id_translation_manager = MySQLIDTranslationManager(self.__conf)
        self.consent_manager = ConsentManager(self.__conf)

    def tearDown(self):
        self.__mysql.execute("DROP DATABASE IF EXISTS test;")
        self.__logger.info(f"Finished test {self._testMethodName}")

    def test_init(self):
        conf = Conf('core/conf/conf.json')
        tmpconf = self.__conf.getConf()
        tmpconf['db']['consent']['record_path'] = '/i/dont/exist'
        conf.setConf(tmpconf)
        self.assertRaises(RuntimeError, ConsentManager, conf)

        # TODO: test if consent records have been read correctly

        # self.assertTrue("ANIMAL" in result)
        # self.assertTrue("VET" in result)
        # self.assertEqual(len(result["ANIMAL"]), 4)
        # self.assertEqual(len(result["VET"]), 2)

        # self.assertDictEqual(result["ANIMAL"][f"{self.__prefix}1"], {
        #     'id': f"{self.__prefix}1", 'location': {
        #         'animal_personal': ['global'],
        #         'animal_medical': []
        #     },
        #     'time': {
        #         'animal_personal': '1970-01-01_00-00-01Z',
        #         'animal_medical': True
        #     },
        #     'stakeholders': {
        #         'animal_personal': ['medicalProfessional', 'adminStaff'],
        #         'animal_medical': ['medicalProfessional']
        #     }
        # })

    def test_get_permitted_locations_global(self):
        consent_requirement = StatementConsentRequirement(
            "animal_personal", [f"{self.__prefix}1"], "ANIMAL")
        result = self.consent_manager.get_permitted_locations(consent_requirement)
        self.assertSetEqual(result, set(["node0", "node1"]))

    def test_get_permitted_locations_none(self):
        consent_requirement = StatementConsentRequirement(
            "vet_intellectual_property", [f"{self.__prefix}1"], "VET")
        result = self.consent_manager.get_permitted_locations(consent_requirement)
        self.assertSetEqual(result, set())

    def test_get_permitted_locations_no_record(self):
        consent_requirement = StatementConsentRequirement(
            "vet_intellectual_property", [f"{self.__prefix}3"], "VET")
        result = self.consent_manager.get_permitted_locations(consent_requirement)
        self.assertSetEqual(result, set())

    def test_get_statement_consent(self):
        # Insert test data and make sure the mapping is created
        self.__mysql.execute("INSERT INTO animals (species, name) VALUES (1, 'Joey');")
        self.__mysql.execute("INSERT INTO vets (techniques_available) VALUES ('X');")
        self.__id_translation_manager.write_new_mapping("animals", 1, f"{self.__prefix}1")
        self.__id_translation_manager.write_new_mapping("vets", 1, f"{self.__prefix}1")

        result = self.consent_manager.get_statement_consent(
            "`species`", {"id": 1, "name": "Koala", "description": ""},
                         {"id": 1, "name": "Koala", "description": ""})
        self.assertListEqual(result, [])

        result = self.consent_manager.get_statement_consent(
            "`animals`", {"id": 1, "species": 1, "name": "Joey"},
                         {"id": f"{self.__prefix}1", "species": 1, "name": "Joey"})

        self.assertEqual(len(result), 1)
        self.assertEqual(result[0].get_consent_tag(), "animal_personal")
        self.assertEqual(result[0].get_consenting_individuals(), [f"{self.__prefix}1"])
        self.assertEqual(result[0].get_consent_owner_type(), "ANIMAL")

        result = self.consent_manager.get_statement_consent(
            "`vets`", {"id": 1, "techniques_available": "surgery"},
                      {"id": f"{self.__prefix}1", "techniques_available": "surgery"})
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0].get_consent_tag(), "vet_intellectual_property")
        self.assertEqual(result[0].get_consenting_individuals(), [f"{self.__prefix}1"])
        self.assertEqual(result[0].get_consent_owner_type(), "VET")

    def test_map_append(self):
        dictionary = {}
        self.consent_manager.map_append(dictionary, "foo", "bar")

        self.assertTrue("foo" in dictionary)
        self.assertListEqual(dictionary["foo"], ["bar"])

        self.consent_manager.map_append(dictionary, "foo", "baz")
        self.assertListEqual(dictionary["foo"], ["bar", "baz"])

    def test_follow_consenting_individual_path_one_step_mapped(self):
        path_to_consenting_individual = [{
            "mapped_table": True,
            "mapped_table_name": "vet_visits",
            "source_column_name": "animal"}]
        record_values = {"animal": 1}

        result = self.consent_manager.follow_consenting_individual_path(
            path_to_consenting_individual, record_values)
        self.assertEqual(result, ("vet_visits", 1))

    def test_follow_consenting_individual_path_one_step_not_mapped(self):
        path_to_consenting_individual = [{
            "mapped_table": False, "source_table_name": "animals", "source_column_name": "id"}]
        record_values = {"id": 1}

        result = self.consent_manager.follow_consenting_individual_path(
            path_to_consenting_individual, record_values)
        self.assertTupleEqual(result, ("animals", 1))

    def test_follow_consenting_individual_path_multi_step(self):
        self.__mysql.connect()
        query = "INSERT INTO animals (species, name) VALUES (1, 'Harry');"
        self.__mysql.execute(query)
        query = "INSERT INTO vets VALUES (1, 'Surgery')"
        self.__mysql.execute(query)
        query = "INSERT INTO vet_visits VALUES (1, 1, 'Routine checkup', 1)"
        self.__mysql.execute(query)
        query = "INSERT INTO vet_prescriptions VALUES (1, 1, 'Cough medicine')"
        self.__mysql.execute(query)
        self.__mysql.disconnect()

        path_to_consenting_individual = [{
            "mapped_table": True,
            "source_table_name": "vet_prescriptions",
            "source_column_name": "vet_visit",
            "mapped_table_name": "vet_visits",
            "mapped_column_name": "id"
        }, {
            "mapped_table": True,
            "source_table_name": "vet_visits",
            "source_column_name": "animal",
            "mapped_table_name": "animals",
            "mapped_column_name": "id"
        }]
        record_values = {"id": 1, "vet_visit": 1, "animal": 1}

        result = self.consent_manager.follow_consenting_individual_path(
            path_to_consenting_individual, record_values)
        self.assertTupleEqual(result, ("animals", 1))

    def test_filter_by_consent_none_defined(self):
        statement = Statement({}, {}, StatementType.INSERT, "species", [])
        transaction = Transaction()
        transaction.add_statement(statement)

        result = self.consent_manager.filter_by_consent(transaction)
        self.assertListEqual(result, ["node0", "node1"])

    def test_filter_by_consent_global(self):
        consent_requirement = StatementConsentRequirement(
            "animal_personal", [f"{self.__prefix}1"], "ANIMAL")
        statement = Statement({}, {}, StatementType.INSERT, "animal", [consent_requirement])
        transaction = Transaction()
        transaction.add_statement(statement)

        result = self.consent_manager.filter_by_consent(transaction)
        self.assertListEqual(sorted(result), ["node0", "node1"])

    def test_filter_by_consent_node_defined(self):
        consent_requirement = StatementConsentRequirement("animal_personal",
                                                          [f"{self.__prefix}3"],
                                                          "ANIMAL")
        statement = Statement({}, {}, StatementType.INSERT, "animal", [consent_requirement])
        transaction = Transaction()
        transaction.add_statement(statement)

        result = self.consent_manager.filter_by_consent(transaction)
        self.assertListEqual(sorted(result), ["node0"])

    def test_filter_by_consent_no_consent(self):
        consent_requirement = StatementConsentRequirement(
            "vet_intellectual_property", [f"{self.__prefix}1"], "VET")
        statement = Statement({}, {}, StatementType.INSERT, "vets", [consent_requirement])
        transaction = Transaction()
        transaction.add_statement(statement)

        result = self.consent_manager.filter_by_consent(transaction)
        self.assertListEqual(result, [])

    def test_filter_by_consent_local(self):
        consent_requirement = StatementConsentRequirement(
            "vet_intellectual_property", [f"{self.__prefix}2"], "VET")
        statement = Statement({}, {}, StatementType.INSERT, "vets", [consent_requirement])
        transaction = Transaction()
        transaction.add_statement(statement)

        result = self.consent_manager.filter_by_consent(transaction)
        self.assertListEqual(result, ["node0"])

    def test_multiple_consenting_individuals(self):
        self.__id_translation_manager.write_new_mapping("animals", 1, f"{self.__prefix}1")

        a_req = StatementConsentRequirement("animal_personal", [f"{self.__prefix}1"], "ANIMAL")
        a_locs = self.consent_manager.get_permitted_locations(a_req)
        b_req = StatementConsentRequirement("animal_personal", [f"{self.__prefix}2"], "ANIMAL")
        b_locs = self.consent_manager.get_permitted_locations(b_req)

        multi_req = StatementConsentRequirement("animal_personal",
                                                [f"{self.__prefix}1", f"{self.__prefix}2"],
                                                "ANIMAL")
        multi_locations = self.consent_manager.get_permitted_locations(multi_req)

        self.assertEqual(a_locs.intersection(b_locs), multi_locations)

    def test_multiple_same_consenting_individual(self):
        self.__id_translation_manager.write_new_mapping("animals", 1, f"{self.__prefix}1")

        single_req = StatementConsentRequirement("animal_personal",
                                                [f"{self.__prefix}1"], "ANIMAL")
        single_loc = self.consent_manager.get_permitted_locations(single_req)
        multi_same_req = StatementConsentRequirement("animal_personal",
                                                     [f"{self.__prefix}1", f"{self.__prefix}1"],
                                                     "ANIMAL")
        multi_loc = self.consent_manager.get_permitted_locations(multi_same_req)

        self.assertEqual(single_loc, multi_loc)

    def test_multiple_path_to_consenting_individuals(self):

        locs_1 = set(self.consent_manager.get_individual_permitted_locations(
                 f"{self.__prefix}1", "ANIMAL", "animal_personal"))
        locs_2 = set(self.consent_manager.get_individual_permitted_locations(
                 f"{self.__prefix}2", "ANIMAL", "animal_personal"))
        locs_3 = set(self.consent_manager.get_individual_permitted_locations(
                 f"{self.__prefix}3", "ANIMAL", "animal_personal"))

        # insert animals and add mappings
        self.__mysql.execute("INSERT INTO animals (species, name) VALUES "
                             "(1, 'Joey'), (1, 'Bob'), (1, 'Kate'), (1, 'Baldrick');")
        self.__id_translation_manager.write_new_mapping("animals", 1, f"{self.__prefix}1")
        self.__id_translation_manager.write_new_mapping("animals", 2, f"{self.__prefix}2")
        self.__id_translation_manager.write_new_mapping("animals", 3, f"{self.__prefix}3")
        self.__id_translation_manager.write_new_mapping("animals", 4, f"{self.__prefix}4")

        # Test global and global --> gives global
        local_before = None
        local_after = {"id": 1, "animal_a": 1, "animal_b": 4}
        global_after = {"id": 1, "animal_a": f"{self.__prefix}1", "animal_b": f"{self.__prefix}4"}
        table = "`relationships`"

        multi_reqs = self.consent_manager.get_statement_consent(table, local_after, global_after)
        self.assertEqual(len(multi_reqs), 1)
        self.assertEqual(multi_reqs[0].get_consenting_individuals()[0], f"{self.__prefix}1")
        self.assertEqual(multi_reqs[0].get_consenting_individuals()[1], f"{self.__prefix}4")

        s_multi = Statement(local_before, local_after, StatementType.INSERT, table, multi_reqs)
        t = Transaction()
        t.add_statement(s_multi)

        # Test global and node0 --> gives node0
        local_after = {"id": 1, "animal_a": 1, "animal_b": 3}
        global_after = {"id": 1, "animal_a": f"{self.__prefix}1", "animal_b": f"{self.__prefix}3"}

        multi_reqs = self.consent_manager.get_statement_consent(table, local_after, global_after)
        self.assertEqual(len(multi_reqs), 1)
        self.assertEqual(multi_reqs[0].get_consenting_individuals()[0], f"{self.__prefix}1")
        self.assertEqual(multi_reqs[0].get_consenting_individuals()[1], f"{self.__prefix}3")

        s_multi = Statement(local_before, local_after, StatementType.INSERT, table, multi_reqs)
        t = Transaction()
        t.add_statement(s_multi)

        intersect_locs = locs_1.intersection(locs_3)
        self.assertEqual(sorted(self.consent_manager.filter_by_consent(t)),
                         sorted(intersect_locs))

        # Test node0 and [] --> gives set()
        local_after = {"id": 1, "animal_a": 2, "animal_b": 3}
        global_after = {"id": 1, "animal_a": f"{self.__prefix}2", "animal_b": f"{self.__prefix}3"}

        multi_reqs = self.consent_manager.get_statement_consent(table, local_after, global_after)
        self.assertEqual(len(multi_reqs), 1)
        self.assertEqual(multi_reqs[0].get_consenting_individuals()[0], f"{self.__prefix}2")
        self.assertEqual(multi_reqs[0].get_consenting_individuals()[1], f"{self.__prefix}3")

        s_multi = Statement(local_before, local_after, StatementType.INSERT, table, multi_reqs)
        t = Transaction()
        t.add_statement(s_multi)

        intersect_locs = locs_2.intersection(locs_3)
        self.assertEqual(sorted(self.consent_manager.filter_by_consent(t)),
                         sorted(intersect_locs))


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner())

