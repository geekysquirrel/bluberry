__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g16@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u16@soton.ac.uk"]


import unittest
from unittest.mock import MagicMock, patch
from pymysqlreplication.event import XidEvent
from pymysqlreplication.row_event import RowsEvent, WriteRowsEvent, UpdateRowsEvent,\
                                         DeleteRowsEvent

import core.BluberryLogging
from core.BluberryConf import Conf
from core.BluberryMessage import Message

from db.mysql.MySQLConnector import MySQLConnector
from db.mysql.MySQLIDTranslationManager import MySQLIDTranslationManager
from db.mysql.BluberryMySQLReader import BluberryMySQLReader
from db.mysql.models.Transaction import Transaction
from db.mysql.models.Statement import Statement, StatementType


class itest_BluberryMySQLReader(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        core.setLogLevel('MySQLConnector', 'warning')
        core.setLogLevel('MySQLIDTranslationManager', 'warning')
        core.setLogLevel('ConflictManager', 'warning')
        core.setLogLevel('ConsentManager', 'warning')

        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")
        cls.__conf = Conf('core/conf/conf.json')
        cls.__test_db = 'test'
        tmpconf = cls.__conf.getConf()
        tmpconf['db']['connection']['database'] = cls.__test_db
        # using prefix as defined in test consent records
        tmpconf['local']['networkPrefix'] = '3TC'
        cls.__conf.setConf(tmpconf)
        cls.__prefix = cls.__conf.get('local', 'networkPrefix')

        cls.__mysql = MySQLConnector(cls.__conf)

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")

        # Ensure database is clean
        self.__mysql.connect(None)
        self.__mysql.execute(f"DROP DATABASE IF EXISTS {self.__test_db};")
        self.__mysql.execute(f"CREATE DATABASE {self.__test_db};")
        self.__mysql.execute(f"USE {self.__test_db};")
        self.__mysql.runScript("db/mysql/test/resources/setup.sql")
        self.__mysql.disconnect()
        self.__mysql.resetBluberryDatabase()

        self.__id_translation_manager = MySQLIDTranslationManager(self.__conf)
        self.reader = BluberryMySQLReader(self.__conf)

        # Clear the binary log of the setup trace
        self.reader.create_stream_connection()
        self.reader.clear_event_log()

    def tearDown(self):
        self.reader.close_stream_connection()
        self.__logger.info(f"Finished test {self._testMethodName}")

    def test_get_row_events(self):
        self.reader.clear_event_log()
        query = "INSERT INTO A VALUES ('1');"
        self.__mysql.execute(query)

        result = []
        for event in self.reader.get_row_events():
            result.append(event)

        self.assertEqual(len(result), 4)
        self.assertEqual(result[1].table, "A")
        self.assertListEqual(result[2].rows, [{'values': {'num': 1}}])
        self.assertTrue(isinstance(result[3], XidEvent))

    def test_readUpdates_ignores_select(self):
        self.__mysql.execute("SELECT * FROM A;")
        result = self.reader.readUpdates()
        self.assertEqual(len(result), 0)

    def test_readUpdates_reads_insert(self):
        self.__mysql.execute("INSERT INTO A VALUES (1);")
        result = self.reader.readUpdates()
        self.assertEqual(len(result), 1)
        transaction = Transaction.deserialise(result[0].getPayload())
        statements = transaction.get_statements()
        self.assertEqual(len(statements), 1)
        self.assertEqual(statements[0].get_type(), StatementType.INSERT)

    def test_readUpdates_reads_insert_with_null(self):
        self.__mysql.execute("INSERT INTO A VALUES (NULL);")
        result = self.reader.readUpdates()
        self.assertEqual(len(result), 1)
        transaction = Transaction.deserialise(result[0].getPayload())
        statements = transaction.get_statements()
        self.assertEqual(len(statements), 1)

    def test_readUpdates_reads_update(self):
        self.__mysql.execute("INSERT INTO A VALUES (1);")
        self.reader.clear_event_log()
        self.__mysql.execute("UPDATE A SET num = 2 WHERE num = 1;")
        result = self.reader.readUpdates()
        self.assertEqual(len(result), 1)
        transaction = Transaction.deserialise(result[0].getPayload())
        statements = transaction.get_statements()
        self.assertEqual(len(statements), 1)
        self.assertEqual(statements[0].get_type(), StatementType.UPDATE)

    def test_readUpdates_reads_update_with_null(self):
        self.__mysql.execute("INSERT INTO A VALUES (1);")
        self.reader.clear_event_log()

        self.__mysql.execute("UPDATE A SET num = NULL WHERE num = 1;")
        result = self.reader.readUpdates()
        self.assertEqual(len(result), 1)
        transaction = Transaction.deserialise(result[0].getPayload())
        statements = transaction.get_statements()
        self.assertEqual(len(statements), 1)

    def test_readUpdates_reads_delete(self):
        self.__mysql.execute("INSERT INTO A VALUES (1);")
        self.reader.clear_event_log()

        self.__mysql.execute("DELETE FROM A WHERE num = 1;")
        result = self.reader.readUpdates()
        self.assertEqual(len(result), 1)
        transaction = Transaction.deserialise(result[0].getPayload())
        statements = transaction.get_statements()
        self.assertEqual(len(statements), 1)
        self.assertEqual(statements[0].get_type(), StatementType.DELETE)

    @patch('db.mysql.BluberryMySQLReader.BluberryMySQLReader.handle_writer_status_change')
    def test_handle_rows_event_ignores_writer_status(self, mock_handle_writer_status_change):
        event = MagicMock(WriteRowsEvent)
        event.schema = "test"
        event.table = "bluberry_writer_status"
        mock_transaction = MagicMock(Transaction)

        result = self.reader.handle_rows_event(event, mock_transaction)
        mock_handle_writer_status_change.assert_called()
        self.assertEqual(result, mock_transaction)

    def test_handle_rows_event_ignores_ignored_table(self):
        event = MagicMock(WriteRowsEvent)
        event.schema = "test"
        event.table = "bluberry_writer_status"
        event.rows = [{
            "values": {
                "tables": "A"
            }
        }]

        result = self.reader.handle_rows_event(event, Transaction())

        event = MagicMock(WriteRowsEvent)
        event.schema = "test"
        event.table = "A"
        event.rows = [{
            "values": {
                "num": 1
            }
        }]
        mock_transaction = MagicMock(Transaction)

        result = self.reader.handle_rows_event(event, mock_transaction)
        mock_transaction.add_statement.assert_not_called()
        self.assertEqual(result, mock_transaction)

    def test_handle_rows_event_ignores_unhandled_event(self):
        event = MagicMock(RowsEvent)
        event.schema = "test"
        event.table = "A"
        mock_transaction = MagicMock(Transaction)

        result = self.reader.handle_rows_event(event, mock_transaction)
        mock_transaction.add_statement.assert_not_called()
        self.assertEqual(result, mock_transaction)

    @patch('db.mysql.BluberryMySQLReader.BluberryMySQLReader.handle_write_rows_event')
    def test_handle_rows_event_handles_write_event(self, mock_write):
        event = MagicMock(WriteRowsEvent)
        event.schema = "test"
        event.table = "A"
        event.rows = [{"values": {"num": 1}}]
        mock_transaction = MagicMock(Transaction)

        self.reader.handle_rows_event(event, mock_transaction)
        mock_write.assert_called()

    @patch('db.mysql.BluberryMySQLReader.BluberryMySQLReader.handle_update_rows_event')
    def test_handle_rows_event_handles_update_event(self, mock_update):
        event = MagicMock(UpdateRowsEvent)
        event.schema = "test"
        event.table = "A"
        event.rows = [{"after_values": {"num": 2}}]
        mock_transaction = MagicMock(Transaction)

        self.reader.handle_rows_event(event, mock_transaction)
        mock_update.assert_called()

    @patch('db.mysql.BluberryMySQLReader.BluberryMySQLReader.handle_delete_rows_event')
    def test_handle_rows_event_handles_delete_event(self, mock_delete):
        event = MagicMock(DeleteRowsEvent)
        event.schema = "test"
        event.table = "A"
        event.rows = [{"values": {"num": 1}}]
        mock_transaction = MagicMock(Transaction)

        self.reader.handle_rows_event(event, mock_transaction)
        mock_delete.assert_called()

    def test_ignore_statement_on_table(self):
        # Test whitelist
        result = self.reader.ignore_statement_on_table("A", ["A", "B"], "whitelist")
        self.assertFalse(result)
        result = self.reader.ignore_statement_on_table("C", ["A", "B"], "whitelist")
        self.assertTrue(result)

        # Test blacklist
        result = self.reader.ignore_statement_on_table("A", ["A", "B"], "blacklist")
        self.assertTrue(result)
        result = self.reader.ignore_statement_on_table("C", ["A", "B"], "blacklist")
        self.assertFalse(result)

    def test_handle_writer_status_change(self):
        event = MagicMock(WriteRowsEvent)
        event.rows = [{
            "values": {
                "tables": ""
            }
        }]

        result = self.reader.handle_writer_status_change(event)
        self.assertSetEqual(result, set())

        event = MagicMock(WriteRowsEvent)
        event.rows = [{
            "values": {
                "tables": "`A`,`B`"
            }
        }]

        result = self.reader.handle_writer_status_change(event)
        self.assertSetEqual(result, set(["`A`", "`B`"]))

        event = MagicMock(UpdateRowsEvent)
        event.rows = [{
            "after_values": {
                "tables": "`C`"
            }
        }]

        result = self.reader.handle_writer_status_change(event)
        self.assertSetEqual(result, set(["`C`"]))

    def test_handle_writer_status_change_ignores_unhandled_event(self):
        event = MagicMock(RowsEvent)
        event.rows = [{
            "values": {
                "tables": "A"
            }
        }]

        result = self.reader.handle_writer_status_change(event)
        self.assertSetEqual(result, set())

    def test_handle_write_rows_event(self):

        self.__mysql.execute("INSERT INTO animals (species, name) VALUES (1, 'Joey');")
        self.__id_translation_manager.write_new_mapping("animals", 1, f"{self.__prefix}1")
        self.__mysql.execute("INSERT INTO vets (techniques_available) VALUES ('Surgery')")
        self.__id_translation_manager.write_new_mapping("vets", 1, f"{self.__prefix}1")

        event = MagicMock()
        event.rows = [{
            "values": {
                "id": 2,
                "animal": 1,
                "description": "illness",
                "vet": 1
            }
        }]
        event.schema = "test"
        event.table = "vet_visits"
        transaction = Transaction()

        result = self.reader.handle_write_rows_event(event, transaction)
        statements = result.get_statements()

        self.assertEqual(len(statements), 1)
        self.assertEqual(statements[0].get_before(), {})
        self.assertEqual(
            statements[0].get_after(), {
                # GLOBAL
                "id": f"{self.__prefix}1",
                "animal": f"{self.__prefix}1",
                "description": "illness",
                "vet": f"{self.__prefix}1"
            }
        )
        self.assertEqual(statements[0].get_type(), StatementType.INSERT)
        self.assertEqual(statements[0].get_table(), "vet_visits")

    def test_handle_update_rows_event(self):

        self.__mysql.execute("INSERT INTO animals (species, name) VALUES (1, 'Joey');")
        self.__id_translation_manager.write_new_mapping("animals", 1, f"{self.__prefix}1")

        event = MagicMock()
        event.rows = [{
            "before_values": {"id": 1, "species": 1, "name": "Joey"},
            "after_values": {"id": 1, "species": 1, "name": "Jojo"}
        }]
        event.schema = "test"
        event.table = "animals"
        transaction = Transaction()

        result = self.reader.handle_update_rows_event(event, transaction)
        statements = result.get_statements()

        self.assertEqual(len(statements), 1)
        self.assertEqual(statements[0].get_before(), {"id": f"{self.__prefix}1",
                                                      "species": 1, "name": "Joey"})
        self.assertEqual(statements[0].get_after(), {"id": f"{self.__prefix}1",
                                                     "species": 1, "name": "Jojo"})
        self.assertEqual(statements[0].get_type(), StatementType.UPDATE)
        self.assertEqual(statements[0].get_table(), "animals")

    def test_handle_delete_rows_event(self):

        # insert test data
        self.__mysql.execute("INSERT INTO vets (techniques_available) VALUES ('Surgery')")
        self.__id_translation_manager.write_new_mapping("vets", 1, f"{self.__prefix}1")

        self.__mysql.execute("DELETE FROM vets")
        event = MagicMock()
        event.rows = [{"values": {"id": 1}}]
        event.schema = "test"
        event.table = "vets"
        transaction = Transaction()

        result = self.reader.handle_delete_rows_event(event, transaction)
        statements = result.get_statements()

        self.assertEqual(len(statements), 1)
        self.assertEqual(statements[0].get_before(), {"id": f"{self.__prefix}1"})
        self.assertEqual(statements[0].get_after(), {})
        self.assertEqual(statements[0].get_type(), StatementType.DELETE)
        self.assertEqual(statements[0].get_table(), "vets")

    def test_create_database_update_message(self):
        statement = Statement(None, {"num": 1}, StatementType.INSERT, "A")
        transaction = Transaction()
        transaction.add_statement(statement)
        transaction.set_timestamp(1574868636)

        result = self.reader.create_database_update_message(transaction, "node0")

        self.assertTrue(isinstance(result, Message))
        self.assertEqual(result.getDestination(), "node0")

        payload = Transaction.deserialise(result.getPayload())
        self.assertEqual(payload.get_timestamp(), 1574868636)

        tables = sorted(payload.get_tables())
        self.assertListEqual(tables, ["A"])
        statements = payload.get_statements()
        self.assertEqual(len(statements), 1)
        self.assertEqual(statements[0].get_before(), {})
        self.assertEqual(statements[0].get_after(), {"num": 1})
        self.assertEqual(statements[0].get_type(), StatementType.INSERT)
        self.assertEqual(statements[0].get_table(), "A")


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner())

