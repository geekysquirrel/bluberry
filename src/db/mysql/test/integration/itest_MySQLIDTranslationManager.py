__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g16@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u16@soton.ac.uk"]


import unittest
import core.BluberryLogging
from core.BluberryConf import Conf

from db.mysql.MySQLConnector import BLUBERRY_DB_NAME,\
                                    ID_MAPPING_TABLE_NAME,\
                                    LOCAL_COUNT_TABLE_NAME
from db.mysql.MySQLIDTranslationManager import MySQLIDTranslationManager
from db.mysql.models.Statement import StatementType
from db.mysql.MySQLConnector import MySQLConnector


class itest_MySQLIDTranslationManager(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        core.setLogLevel('MySQLConnector', 'info')
        core.setLogLevel('BluberryMySQLIDTranslationManager', 'debug')
        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")

        cls.__conf = Conf('core/conf/conf.json')
        cls.__test_db = 'test'
        tmpconf = cls.__conf.getConf()
        tmpconf['db']['connection']['database'] = cls.__test_db
        cls.__conf.setConf(tmpconf)

        cls.__prefix = cls.__conf.get('local', 'networkPrefix')
        cls.__mysql = MySQLConnector(cls.__conf)

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")

        # Ensure database is clean
        self.__mysql.connect(None)
        self.__mysql.execute(f"DROP DATABASE IF EXISTS {self.__test_db};")
        self.__mysql.execute(f"CREATE DATABASE {self.__test_db};")
        self.__mysql.execute(f"USE {self.__test_db};")
        self.__mysql.runScript("db/mysql/test/resources/setup.sql")
        self.__mysql.disconnect()
        self.__mysql.resetBluberryDatabase()
        self.tm = MySQLIDTranslationManager(self.__conf)

    def tearDown(self):
        if self.__mysql.isConnected():
            self.__mysql.disconnect()
        self.__logger.info(f"Finished test {self._testMethodName}")

    def test_find_autoIncrement_tables(self):
        tables_with_autoIncr = self.tm.find_autoIncrement_tables()
        self.assertEqual(len(tables_with_autoIncr), 3)
        self.assertEqual(sorted(tables_with_autoIncr), ['animals', 'vet_visits', 'vets'])

    def test_getNetworkMapping_new_localInsert(self):
        network_id = self.tm.getNetworkMapping_new_localInsert('animals')
        self.assertEqual(network_id, f"{self.__prefix}1")
        network_id = self.tm.getNetworkMapping_new_localInsert('animals')
        self.assertEqual(network_id, f"{self.__prefix}2")
        network_id = self.tm.getNetworkMapping_new_localInsert('vets')
        self.assertEqual(network_id, f"{self.__prefix}1")

    def test_getLocalMapping_new_networkInsert(self):
        # doesn't INSERT.
        local_id = self.tm.getLocalMapping_new_networkInsert('animals')
        self.assertEqual(local_id, 1)

        query = "INSERT INTO animals (species, name) VALUES (1, 'Harry'), (1, 'Lily'), "\
                "(1, 'James'), (1, 'Matt');"
        self.__mysql.execute(query)
        local_id = self.tm.getLocalMapping_new_networkInsert(
            'animals')  # doesn't INSERT
        self.assertEqual(local_id, 5)
        local_id = self.tm.getLocalMapping_new_networkInsert(
            'vets')  # doesn't INSERT
        self.assertEqual(local_id, 1)

    def test_check_is_table_autoIncrementing(self):
        self.assertTrue(self.tm.is_table_autoIncrementing('animals'))
        self.assertTrue(self.tm.is_table_autoIncrementing('vets'))
        self.assertTrue(self.tm.is_table_autoIncrementing('vet_visits'))
        self.assertFalse(self.tm.is_table_autoIncrementing('vet_prescriptions'))
        self.assertFalse(self.tm.is_table_autoIncrementing('medicine_dosage'))
        self.assertFalse(self.tm.is_table_autoIncrementing('A'))

    def test_find_autoIncrement_fields(self):
        map_table_autoIncrementFieldName = self.tm.find_autoIncrement_fields()
        self.assertTrue(len(map_table_autoIncrementFieldName), 2)

        self.assertEqual(map_table_autoIncrementFieldName['animals'], 'id')
        self.assertEqual(map_table_autoIncrementFieldName['vets'], 'id')

    def test_find_foreignKeyFields_to_autoIncrementTables(self):
        map_table_foreignKeysToAutoIncrementedFields =\
            self.tm.find_foreignKeyFields_to_autoIncrementTables()
        self.assertTrue(len(map_table_foreignKeysToAutoIncrementedFields), 1)
        fk_autoIncF = map_table_foreignKeysToAutoIncrementedFields['vet_visits']
        self.assertTrue(len(fk_autoIncF), 2)

        self.assertEqual(fk_autoIncF[('vet_visits', 'vet')], 'vets')
        self.assertEqual(fk_autoIncF[('vet_visits', 'animal')], 'animals')

    def test_write_new_mapping(self):
        result = self.__mysql.execute(f"SELECT * FROM {BLUBERRY_DB_NAME}."
                                      f"{ID_MAPPING_TABLE_NAME};")
        self.assertEqual(len(result), 0)

        otherPrefix = '7AB'
        self.tm.write_new_mapping('animals', 14, f"{otherPrefix}6345")

        result = self.__mysql.execute(f"SELECT * FROM {BLUBERRY_DB_NAME}."
                                      f"{ID_MAPPING_TABLE_NAME};")
        self.assertEqual(len(result), 1)

        # TODO: assert other things

    def test_map_to_network_value(self):
        otherPrefix = '7AB'
        self.tm.write_new_mapping('animals', 13, f"{otherPrefix}3000309")
        network_val = self.tm.map_to_global_value("animals", 13)
        self.assertEqual(network_val, f"{otherPrefix}3000309")

    def test_map_to_local_value(self):
        otherPrefix = '7AB'
        self.tm.write_new_mapping('animals', 230061, f"{otherPrefix}3000309")
        local_id = self.tm.map_to_local_value("animals", f"{otherPrefix}3000309")
        self.assertEqual(local_id, 230061)

    def test_get_network_values(self):
        self.tm.write_new_mapping('animals', 10, f"{self.__prefix}18")
        self.tm.write_new_mapping('animals', 5, f"{self.__prefix}19")
        self.tm.write_new_mapping('vets', 155, f"{self.__prefix}20")
        self.tm.write_new_mapping('vets', 180, f"{self.__prefix}21")
        self.tm.write_new_mapping('vet_visits', 25, f"{self.__prefix}22")
        mapped_vals = self.tm.get_network_values('vet_visits', {
            "id": 25, "animal": 10, "description": "A lovely visit with a lovely animal",
            "vet": 180})
        self.assertEqual(mapped_vals,
                         {"id": f"{self.__prefix}22",
                          "animal": f"{self.__prefix}18",
                          "description": "A lovely visit with a lovely animal",
                          "vet": f"{self.__prefix}21"})

    def test_get_local_values(self):
        otherPrefix = '5FF'
        self.tm.write_new_mapping('animals', 1, f"{otherPrefix}102")
        self.tm.write_new_mapping('vets', 2, f"{otherPrefix}102")
        self.tm.write_new_mapping('animals', 5, f"{otherPrefix}390003")
        self.tm.write_new_mapping('vets', 3, f"{otherPrefix}103")
        self.tm.write_new_mapping('vet_visits', 101, f"{otherPrefix}39570")
        mapped_vals = self.tm.get_local_values(
            'vet_visits', {"id": f"{otherPrefix}39570",
                           "animal": f"{otherPrefix}390003",
                           "description": "Patient had large scar",
                           "vet": f"{otherPrefix}103"})
        self.assertEqual(mapped_vals, {"id": 101,
                                       "animal": 5,
                                       "description": "Patient had large scar",
                                       "vet": 3})

    def test_map_localUpdate_to_networkValues_handle_INSERT_autoIncr(self):
        after_vals = {"id": 1, "techniques_available": "very tidy"}
        s_type = StatementType.INSERT
        table = "vets"
        before_vals = None

        result = self.__mysql.execute(f"SELECT * FROM {BLUBERRY_DB_NAME}."
                                      f"{ID_MAPPING_TABLE_NAME};")
        self.assertEqual(len(result), 0)

        result = self.__mysql.execute(f"SELECT insert_count FROM {BLUBERRY_DB_NAME}."
                                      f"{LOCAL_COUNT_TABLE_NAME} WHERE table_name='{table}';")
        self.assertTrue(result)
        self.assertTrue(result[0])
        self.assertEqual(result[0][0], 0)

        (mapped_before, mapped_after) =\
            self.tm.map_localUpdate_to_networkValues(
                before_vals, after_vals, s_type, table)

        result = self.__mysql.execute(f"SELECT * FROM {BLUBERRY_DB_NAME}."
                                      f"{ID_MAPPING_TABLE_NAME};")
        self.assertEqual(len(result), 1)

        query = f"SELECT insert_count FROM {BLUBERRY_DB_NAME}."\
                f"{LOCAL_COUNT_TABLE_NAME} WHERE table_name='{table}';"
        result = self.__mysql.execute(query)

        self.assertTrue(result)
        self.assertTrue(result[0])
        self.assertEqual(result[0][0], 1)
        self.assertEqual(mapped_before, None)
        self.assertEqual(mapped_after, {"id": f"{self.__prefix}1",
                                        "techniques_available": "very tidy"})

    def test_map_localUpdate_to_networkValues_handle_DELETE_non_autoIncr(self):
        after_vals = None
        s_type = StatementType.DELETE
        table = "species"
        before_vals = {"id": 2, "name": "Weasel", "description": "furry predators"}

        result = self.__mysql.execute(f"SELECT * FROM {BLUBERRY_DB_NAME}."
                                      f"{ID_MAPPING_TABLE_NAME};")
        self.assertEqual(len(result), 0)

        query = f"SELECT insert_count FROM {BLUBERRY_DB_NAME}.{LOCAL_COUNT_TABLE_NAME} "\
                f"WHERE table_name='{table}';"
        result = self.__mysql.execute(query)
        self.assertTrue(result)
        self.assertTrue(result[0])
        self.assertEqual(result[0][0], 0)

        (mapped_before, mapped_after) =\
            self.tm.map_localUpdate_to_networkValues(
                before_vals, after_vals, s_type, table)

        result = self.__mysql.execute(f"SELECT * FROM {BLUBERRY_DB_NAME}."
                                      f"{ID_MAPPING_TABLE_NAME};")
        self.assertEqual(len(result), 0)

        query = f"SELECT insert_count FROM {BLUBERRY_DB_NAME}.{LOCAL_COUNT_TABLE_NAME} "\
                f"WHERE table_name='{table}';"
        result = self.__mysql.execute(query)

        self.assertTrue(result)
        self.assertTrue(result[0])
        self.assertEqual(result[0][0], 0)
        self.assertEqual(mapped_after, None)
        self.assertEqual(
            mapped_before, {
                "id": 2, "name": "Weasel", "description": "furry predators"})

    def test_map_foreignUpdate_to_localValues_INSERT(self):
        # to set the auto_increments on these tables
        # self.tm.write_localInsertIncrement('vet_visits', 10)
        animal_query = "INSERT INTO animals VALUES (10, 1, 'Harry');"
        vet_query = "INSERT INTO vets VALUES (180, 'one of the best all-rounders');"

        self.__mysql.connect()
        self.__mysql.execute(animal_query)
        self.__mysql.execute(vet_query)

        for i in range(14):
            vet_visit_query = "INSERT INTO vet_visits (animal, description, vet) "\
                              "VALUES (10, 'fine', 180)"
            self.__mysql.execute(vet_visit_query)
        self.__mysql.disconnect()

        self.tm.write_new_mapping('animals', 10, f"{self.__prefix}18")
        self.tm.write_new_mapping('animals', 5, f"{self.__prefix}19")  # dummy
        self.tm.write_new_mapping('vets', 155, f"{self.__prefix}20")  # dummy
        self.tm.write_new_mapping('vets', 180, f"{self.__prefix}18")
        self.tm.write_new_mapping('vet_visits', 25, f"{self.__prefix}18")  # dummy
        after_vals = {
            "id": f"{self.__prefix}2300",
            "animal": f"{self.__prefix}18",
            "description": "good health",
            "vet": f"{self.__prefix}18"}
        s_type = StatementType.INSERT
        table = "vet_visits"
        before_vals = None

        result = self.__mysql.execute(f"SELECT * FROM {BLUBERRY_DB_NAME}."
                                      f"{ID_MAPPING_TABLE_NAME};")
        self.assertEqual(len(result), 5)
        query = f"SELECT insert_count FROM {BLUBERRY_DB_NAME}.{LOCAL_COUNT_TABLE_NAME} "\
                f"WHERE table_name='{table}';"
        result = self.__mysql.execute(query)
        self.assertTrue(result)
        self.assertTrue(result[0])
        self.assertEqual(result[0][0], 0)

        (mapped_before, mapped_after) =\
            self.tm.map_foreignUpdate_to_localValues(
                before_vals, after_vals, s_type, table)

        result = self.__mysql.execute(f"SELECT * FROM {BLUBERRY_DB_NAME}."
                                      f"{ID_MAPPING_TABLE_NAME};")
        self.assertEqual(len(result), 6)

        query = f"SELECT insert_count FROM {BLUBERRY_DB_NAME}.{LOCAL_COUNT_TABLE_NAME} "\
                f"WHERE table_name='{table}';"
        result = self.__mysql.execute(query)

        # still 0, because this was not a local insert
        self.assertTrue(result)
        self.assertTrue(result[0])
        self.assertEqual(result[0][0], 0)
        self.assertEqual(mapped_after, {"id": 15, "animal": 10,
                                        "description": "good health", "vet": 180})
        self.assertEqual(mapped_before, None)

    def test_end_to_end_reader(self):
        # self.__mysql.execute("INSERT INTO vet_visits VALUES (25, 5, 'bad', 155);")
        # TODO: implement
        pass


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner())

