__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g16@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u16@soton.ac.uk"]


import unittest
from unittest.mock import patch, MagicMock

import core.BluberryLogging
from core.BluberryConf import Conf
from core.BluberryMessage import Message

from db.mysql.MySQLConnector import MySQLConnector
from db.mysql.MySQLConnector import WRITER_STATUS_TABLE_NAME
from db.mysql.BluberryMySQLWriter import BluberryMySQLWriter
from db.mysql.ConflictManager import ConflictManager
from db.mysql.MySQLIDTranslationManager import MySQLIDTranslationManager
from db.mysql.models.Transaction import Transaction
from db.mysql.models.Statement import Statement, StatementType


class itest_BluberryMySQLWriter(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        core.setLogLevel('BluberryMySQLWriter', 'debug')
        core.setLogLevel('MySQLConnector', 'warning')
        core.setLogLevel('MySQLIDTranslationManager', 'warning')
        core.setLogLevel('ConflictManager', 'warning')
        core.setLogLevel('ConsentManager', 'warning')

        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")
        cls.__conf = Conf('core/conf/conf.json')
        cls.__test_db = 'test'
        tmpconf = cls.__conf.getConf()
        tmpconf['db']['connection']['database'] = cls.__test_db
        cls.__conf.setConf(tmpconf)
        cls.__prefix = cls.__conf.get('local', 'networkPrefix')

        cls.__mysql = MySQLConnector(cls.__conf)

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")

        # Ensure database is clean
        self.__mysql.connect(None)
        self.__mysql.execute(f"DROP DATABASE IF EXISTS {self.__test_db};")
        self.__mysql.execute(f"CREATE DATABASE {self.__test_db};")
        self.__mysql.execute(f"USE {self.__test_db};")
        self.__mysql.runScript("db/mysql/test/resources/setup.sql")
        self.__mysql.disconnect()
        self.__mysql.resetBluberryDatabase()

        self.__id_translation_manager = MySQLIDTranslationManager(self.__conf)
        self.__conflict_manager = MagicMock(ConflictManager)
        self.writer = BluberryMySQLWriter(self.__conf)

    def tearDown(self):
        self.__mysql.connect(None)
        self.__mysql.execute(f"USE {self.__test_db};")
        self.__mysql.execute("UNLOCK TABLES;")
        self.__mysql.execute("USE bluberry;")
        self.__mysql.execute("UNLOCK TABLES;")
        self.__mysql.commit()
        self.__mysql.disconnect()
        self.__logger.info(f"Finished test {self._testMethodName}")

    def test_write_statement_writes_successfully(self):
        self.__mysql.execute("INSERT INTO A VALUES (1);")
        result = self.__mysql.execute("SELECT * FROM A;")

        self.assertEqual(len(result), 1)
        self.assertTupleEqual(result[0], (1,))

    @patch('db.mysql.BluberryMySQLWriter.BluberryMySQLWriter.handle_incoming_update')
    def test_runUpdates(self, mock_handle_update):
        update = Message()
        transaction = Transaction()
        statement = Statement({"num": 1}, {"num": 2}, StatementType.UPDATE, "`A`")
        transaction.add_statement(statement)
        transaction.set_timestamp(1574868636)

        update.setPayload(str(transaction))

        self.writer.runUpdates([update])
        self.assertEqual(len(mock_handle_update.mock_calls), 1)

    def test_prepare_finalise(self):

        self.assertFalse(self.writer.isEngaged())
        # TODO: check no tables are locked

        self.writer.prepare()
        self.assertRaises(RuntimeError, self.writer.prepare)

        autocommit = self.writer.get_db_autocommit()
        self.assertFalse(autocommit)
        sql_mode = self.writer.get_db_sql_mode()
        self.assertEqual(sql_mode, "")
        self.assertTrue(self.writer.isEngaged())
        # TODO: find a way to test this without creating a new connection
        # query = f"SHOW OPEN TABLES IN {self.__test_db} WHERE `in_use`=1;"
        # result = self.__mysql.execute(query)
        # some tables should be locked
        # self.assertEqual(len(result), 0)

        self.writer.finalise()

        sql_mode = self.writer.get_db_sql_mode()
        self.assertEqual(sql_mode, "")
        query = f"SHOW OPEN TABLES IN {self.__test_db} WHERE `in_use`=1;"
        result = self.__mysql.execute(query)
        self.assertEqual(len(result), 0)
        self.assertFalse(self.writer.isEngaged())

    @patch('db.mysql.BluberryMySQLWriter.BluberryMySQLWriter.update_writer_status')
    @patch('db.mysql.BluberryMySQLWriter.BluberryMySQLWriter.map_statement_to_local_namespace')
    def test_handle_incoming_update_handles_insert(self, mock_map, mock_update):

        update = Message()
        transaction = Transaction()
        statement = Statement({"num": 1}, {"num": 1}, StatementType.INSERT, "`A`")
        transaction.add_statement(statement)
        transaction.set_timestamp(1574868636)

        update.setPayload(str(transaction))

        # nothing to do here: whatever update it is, it will not raise a conflict
        mock_map.return_value = statement
        # TODO: find a better example

        self.writer.handle_incoming_update(update)
        self.assertEqual(len(mock_update.mock_calls), 2)

        # TODO: same test but with an unhandled conflict should not update anything

    def test_map_statement_to_local_namespace(self):
        self.__mysql.execute("INSERT INTO animals (species, name) VALUES (1, 'Joey');")
        self.__id_translation_manager.write_new_mapping("animals", 1, f"{self.__prefix}1")

        statement = Statement({'id': f"{self.__prefix}1", 'species': 1, 'name': 'Joey'},
                              {'id': f"{self.__prefix}1", 'species': 1, 'name': 'Joe'},
                              StatementType.UPDATE, 'animals')
        result = self.writer.map_statement_to_local_namespace(statement)
        self.assertDictEqual(result.get_before(), {'id': 1, 'species': 1, 'name': 'Joey'})
        self.assertDictEqual(result.get_after(), {'id': 1, 'species': 1, 'name': 'Joe'})

    def test_update_writer_status(self):
        tables = set(["A", "B"])
        self.writer.update_writer_status(tables)

        result = self.__mysql.execute(f"SELECT * FROM `{self.__test_db}`."
                                      f"`{WRITER_STATUS_TABLE_NAME}`;")
        self.assertTrue(result)
        self.assertTrue(result[0])
        self.assertEqual(result[0][0], 'A,B')

    def test_find_tables(self):
        # Here we are testing if we read from the config file correctly
        result = self.writer.find_tables()

        self.assertEqual(len(result), 8)


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner())

