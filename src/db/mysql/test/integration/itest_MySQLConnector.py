__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import unittest
import copy
import warnings
from time import time
from datetime import datetime

import core.BluberryLogging
from core.BluberryConf import Conf

from db.mysql.MySQLConnector import MySQLConnector, BLUBERRY_DB_NAME,\
                                    TRANSACTION_LOG_TABLE_NAME,\
                                    STATEMENT_LOG_TABLE_NAME,\
                                    ID_MAPPING_TABLE_NAME,\
                                    LOCAL_COUNT_TABLE_NAME


class itest_MySQLConnector(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        core.setLogLevel('MySQLConnector', 'debug')
        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")
        cls.__conf = Conf('core/conf/conf.json')
        # TODO: cheekily testing in the main bluberry db - should use test db!
        cls.__test_db = 'bluberry'
        tmpconf = cls.__conf.getConf()
        tmpconf['db']['connection']['database'] = cls.__test_db
        cls.__conf.setConf(tmpconf)

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")
        self.__mysql = MySQLConnector(copy.deepcopy(self.__conf))
        # make sure it really is empty
        self.__mysql.resetBluberryDatabase()

    def tearDown(self):
        if self.__mysql is not None:
            self.__mysql.disconnect()
        self.__logger.info(f"Finished test {self._testMethodName}")

    def test_init(self):
        self.__mysql = MySQLConnector(self.__conf, BLUBERRY_DB_NAME)
        self.assertEqual(self.__mysql.getDefaultSchema(), BLUBERRY_DB_NAME)

        self.__mysql = MySQLConnector(self.__conf, '')
        self.assertEqual(self.__mysql.getDefaultSchema(), BLUBERRY_DB_NAME)

    def test_connect(self):
        warnings.filterwarnings(action="ignore", message="unclosed", category=ResourceWarning)

        # default connection
        self.__mysql.connect()
        self.assertTrue(self.__mysql.isConnected())
        self.__mysql.disconnect()

        self.__mysql.connect(None)
        self.assertTrue(self.__mysql.isConnected())
        self.__mysql.disconnect()

        self.__mysql.connect('')
        self.assertTrue(self.__mysql.isConnected())
        self.__mysql.disconnect()

        self.__mysql.connect(BLUBERRY_DB_NAME)
        self.assertTrue(self.__mysql.isConnected())
        self.__mysql.disconnect()

        self.assertRaises(RuntimeError, self.__mysql.connect, 'idontexist')
        self.assertFalse(self.__mysql.isConnected())

        # alter connection conf and make it fail
        conf_original = copy.deepcopy(self.__conf.getConf())

        conf_broken = copy.deepcopy(conf_original)
        conf_broken['db']['connection']['host'] = 'inexistent.host'
        self.__conf.setConf(conf_broken)
        self.__mysql = MySQLConnector(self.__conf)
        self.assertRaises(RuntimeError, self.__mysql.connect)
        self.assertFalse(self.__mysql.isConnected())

        conf_broken = copy.deepcopy(conf_original)
        conf_broken['db']['connection']['user'] = 'idontexist'
        self.__conf.setConf(conf_broken)
        self.__mysql = MySQLConnector(self.__conf)
        self.assertRaises(RuntimeError, self.__mysql.connect)
        self.assertFalse(self.__mysql.isConnected())

        conf_broken = copy.deepcopy(conf_original)
        conf_broken['db']['connection']['password'] = 'wrongpw'
        self.__conf.setConf(conf_broken)
        self.__mysql = MySQLConnector(self.__conf)
        self.assertRaises(RuntimeError, self.__mysql.connect)
        self.assertFalse(self.__mysql.isConnected())

        conf_broken = copy.deepcopy(conf_original)
        conf_broken['db']['connection']['database'] = 'wrongdb'
        self.__conf.setConf(conf_broken)
        self.__mysql = MySQLConnector(self.__conf)
        self.assertRaises(RuntimeError, self.__mysql.connect)
        self.assertFalse(self.__mysql.isConnected())

        # db is optional and a connection can be made without specifying one
        conf_broken = copy.deepcopy(conf_original)
        conf_broken['db']['connection']['database'] = None
        self.__conf.setConf(conf_broken)
        self.__mysql = MySQLConnector(self.__conf)
        self.__mysql.connect()
        self.assertTrue(self.__mysql.isConnected())
        self.__mysql.disconnect()

        conf_broken = copy.deepcopy(conf_original)
        conf_broken['db']['connection']['port'] = '99'
        self.__conf.setConf(conf_broken)
        self.__mysql = MySQLConnector(self.__conf)
        self.assertRaises(RuntimeError, self.__mysql.connect)
        self.assertFalse(self.__mysql.isConnected())

        # try again with correct conf
        self.__conf.setConf(copy.deepcopy(conf_original))
        self.__mysql = MySQLConnector(self.__conf)
        self.__mysql.connect()
        self.assertTrue(self.__mysql.isConnected())

    def test_getCurrentSchema(self):
        self.assertEqual(self.__mysql.getCurrentSchema(), None)

        self.__mysql.connect(None)
        self.assertEqual(self.__mysql.getCurrentSchema(), None)
        self.__mysql.disconnect()

        self.__mysql.connect(BLUBERRY_DB_NAME)
        self.assertEqual(self.__mysql.getCurrentSchema(), BLUBERRY_DB_NAME)
        self.__mysql.disconnect()

        self.__mysql.connect()
        self.assertEqual(self.__mysql.getCurrentSchema(), BLUBERRY_DB_NAME)
        self.__mysql.disconnect()

        conf_original = copy.deepcopy(self.__conf.getConf())

        conf_nodb = copy.deepcopy(self.__conf.getConf())
        conf_nodb['db']['connection']['database'] = None
        self.__conf.setConf(conf_nodb)
        self.__mysql = MySQLConnector(self.__conf)
        self.__mysql.connect()
        self.assertEqual(self.__mysql.getCurrentSchema(), None)

        # restore conf
        self.__conf.setConf(copy.deepcopy(conf_original))

    def test_execute(self):
        self.assertRaises(RuntimeError, self.__mysql.execute, "USE `{BLUBERRY_DB_NAME}`;", False)
        result = self.__mysql.execute(f"USE `{BLUBERRY_DB_NAME}`;")
        self.assertIsNone(result)
        result = self.__mysql.execute(f"USE `{BLUBERRY_DB_NAME}`;", True)
        self.assertIsNone(result)
        self.__mysql.connect()
        result = self.__mysql.execute(f"USE `{BLUBERRY_DB_NAME}`;")
        self.assertIsNone(result)
        result = self.__mysql.execute("SELECT table_name FROM information_schema.tables "
                                f"WHERE table_schema = '{BLUBERRY_DB_NAME}';")
        self.assertIsNotNone(result)

    def test_commit(self):
        self.assertRaises(RuntimeError, self.__mysql.commit)
        self.__mysql.connect()
        self.__mysql.commit()

    def test_rollback(self):
        self.assertRaises(RuntimeError, self.__mysql.rollback)
        self.__mysql.connect()
        self.__mysql.rollback()

    def test_runScript(self):
        self.__mysql.runScript('db/mysql/test/resources/setup.sql')
        self.__mysql.connect()
        self.__mysql.runScript('db/mysql/test/resources/setup.sql')
        self.assertRaises(FileNotFoundError, self.__mysql.runScript, '/i/dont/exist.sql')
        # not an SQL script
        self.assertRaises(RuntimeError, self.__mysql.runScript,
                          'db/mysql/test/resources/conflict_config.json')
        # doing somethign forbidden, i.e. set a not null field to null
        self.assertRaises(RuntimeError, self.__mysql.runScript,
                          'db/mysql/test/resources/setup_faulty.sql')

    def test_resetBluberryDatabase(self):
        ts = datetime.utcfromtimestamp(time()).strftime("%Y-%m-%d %H:%M:%S.%f")
        self.__mysql.connect()
        self.__mysql.execute('SET FOREIGN_KEY_CHECKS = 0;')
        self.__mysql.execute(f"INSERT INTO {BLUBERRY_DB_NAME}.{TRANSACTION_LOG_TABLE_NAME} "
                             "(`origin_node`, `origin_timestamp`, `tables`, "
                             "`created_by_conflict_resolution`) "
                             f"VALUES ('1', '{ts}', 'foo', '0')")
        self.__mysql.execute(f"INSERT INTO {BLUBERRY_DB_NAME}.{STATEMENT_LOG_TABLE_NAME} "
                             "(`transaction_id`, `transaction_order`, `current`, "
                             "`created_by_conflict_resolution`) "
                             "VALUES ('1', '1', '1', '1')")
        self.__mysql.execute(f"INSERT INTO {BLUBERRY_DB_NAME}.{ID_MAPPING_TABLE_NAME} "
                             "(`table_name`, `local_id`, `network_id`) "
                             "VALUES ('foo', '1', 'bar')")
        self.__mysql.execute(f"INSERT INTO {BLUBERRY_DB_NAME}.{LOCAL_COUNT_TABLE_NAME} "
                             "(`table_name`, `insert_count`) "
                             "VALUES ('foo', '1')")
        self.__mysql.execute('SET FOREIGN_KEY_CHECKS = 1;')
        self.__mysql.commit()
        self.__mysql.disconnect()

        tables = [TRANSACTION_LOG_TABLE_NAME, STATEMENT_LOG_TABLE_NAME,
                  ID_MAPPING_TABLE_NAME, LOCAL_COUNT_TABLE_NAME]
        for table in tables:
            result = self.__mysql.execute(f"SELECT * FROM {BLUBERRY_DB_NAME}.{table}")
            self.assertTrue(result)
            self.assertEqual(len(result), 1)

        self.__mysql.resetBluberryDatabase()

        for table in tables:
            result = self.__mysql.execute(f"SELECT * FROM {BLUBERRY_DB_NAME}.{table}")
            self.assertFalse(result)
            self.assertEqual(len(result), 0)

    def test_disconnect(self):
        self.__mysql.disconnect()
        self.__mysql.connect()
        self.__mysql.disconnect()


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner())

