__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g16@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u16@soton.ac.uk"]


import unittest
import json
import time
from unittest.mock import patch
from datetime import datetime

import core.BluberryLogging
from core.BluberryConf import Conf

from db.mysql.MySQLConnector import MySQLConnector
from db.mysql.MySQLConnector import BLUBERRY_DB_NAME,\
                                    TRANSACTION_LOG_TABLE_NAME,\
                                    STATEMENT_LOG_TABLE_NAME
from db.mysql.models.Statement import Statement, StatementType
from db.mysql.models.Transaction import Transaction
from db.mysql.ConflictManager import ConflictManager


class itest_ConflictManager(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        core.setLogLevel('MySQLConnector', 'warning')
        core.setLogLevel('ConflictManager', 'debug')
        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")

        cls.__conf = Conf('core/conf/conf.json')
        cls.__test_db = 'test'
        tmpconf = cls.__conf.getConf()
        tmpconf['db']['connection']['database'] = cls.__test_db
        cls.__conf.setConf(tmpconf)
        cls.__nodeId = cls.__conf.get('local', 'nodeId')

        cls.__mysql = MySQLConnector(cls.__conf)

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")

        # Ensure database is clean
        self.__mysql.connect(None)
        self.__mysql.execute(f"DROP DATABASE IF EXISTS {self.__test_db};")
        self.__mysql.execute(f"CREATE DATABASE {self.__test_db};")
        self.__mysql.execute(f"USE {self.__test_db};")
        self.__mysql.runScript("db/mysql/test/resources/setup.sql")
        self.__mysql.disconnect()
        self.__mysql.resetBluberryDatabase()

        self.conflict_manager = ConflictManager(self.__conf)

    def tearDown(self):
        if self.__mysql.isConnected():
            self.__mysql.disconnect()
        self.__logger.info(f"Finished test {self._testMethodName}")

    def __assertTransactionValuesEqual(self, t1, t2):
        self.assertEqual(t1.get_id(), t2.get_id())
        self.assertEqual(t1.get_tables(), t2.get_tables())
        self.assertEqual(t1.get_timestamp(), t2.get_timestamp())
        self.assertEqual(t1.get_locations(), t2.get_locations())

        t1_statements = t1.get_statements()
        t2_statements = t2.get_statements()
        self.assertEqual(len(t1_statements), len(t2_statements))

        for i, s1 in enumerate(t1_statements):
            s2 = t2_statements[i]
            self.__assertStatementValuesEqual(s1, s2)

    def __assertStatementValuesEqual(self, s1, s2):
        self.assertEqual(s1.get_before(), s2.get_before())
        self.assertEqual(s1.get_after(), s2.get_after())
        self.assertEqual(s1.get_table(), s2.get_table())
        self.assertEqual(s1.get_type(), s2.get_type())
        self.assertEqual(s1.get_consent_reqs(), s2.get_consent_reqs())
        self.assertEqual(s1.get_related_afterValues(), s2.get_related_afterValues())
        self.assertEqual(s1.get_related_beforeValues(), s2.get_related_beforeValues())
        self.assertEqual(s1.get_beforeState_hash(), s2.get_beforeState_hash())
        self.assertEqual(s1.get_afterState_hash(), s2.get_afterState_hash())

    @patch('db.mysql.ConflictManager.ConflictManager.log_statement')
    def test_log_transaction(self, mock_log_statement):
        transaction_ts = 1574868636

        values = {"id": 1, "name": "Otter", "description": "Architect"}
        s1 = Statement(None, values, StatementType.INSERT, "species")
        transaction = Transaction()
        transaction.add_statement(s1)

        before_values = {"id": 1, "species": 1, "name": "Harry"}
        after_values = {"id": 1, "species": 1, "name": "Neville"}
        s2 = Statement(before_values, after_values, StatementType.UPDATE, "animals")
        transaction.add_statement(s2)

        transaction.set_timestamp(transaction_ts)

        transaction_id = transaction.get_id()

        self.conflict_manager.log_transaction(transaction)

        query = f"SELECT * FROM {BLUBERRY_DB_NAME}.{TRANSACTION_LOG_TABLE_NAME};"
        result = self.__mysql.execute(query)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0][0], transaction_id)
        self.assertEqual(result[0][1], self.__nodeId)
        self.assertEqual(result[0][2].timestamp(), transaction_ts)
        # test should take less than 2 seconds...
        self.assertTrue(abs(time.time() - result[0][3].timestamp()) < 2)
        self.assertEqual(result[0][4], "animals,species")

        mock_log_statement.assert_called_with(s2, transaction_id, 1, True, False)

    def test_log_statement_insert(self):
        """
        Statement log entries:

        transaction_id CHAR(36), table_name VARCHAR(64), record_id CHAR(32), " + \
        "transaction_order INT NOT NULL, before_values TEXT, after_values TEXT,
        related_after_values TEXT, extended_state_after_hash TEXT, current TINYINT(1) NOT NULL,

        """
        transaction = Transaction()
        transaction_id = transaction.get_id()
        transaction.set_timestamp(1574868636)
        # no statements, just to preserver referential integrity
        self.conflict_manager.log_transaction(transaction)

        values = {"id": 1, "name": "Otter", "description": "Architect"}
        s1 = Statement(None, values, StatementType.INSERT, "species")
        s1.set_afterState_hash(
            self.conflict_manager._get_statement_afterHash(
                s1.get_extendedState_afterStr()))

        self.conflict_manager.log_statement(s1, transaction_id, 0, True)

        query = f"SELECT * FROM {BLUBERRY_DB_NAME}.{STATEMENT_LOG_TABLE_NAME};"
        result = self.__mysql.execute(query)
        self.assertEqual(len(result), 1)

        beforeValuesStr = "{}"
        afterValuesStr = '{"id": 1, "name": "Otter", "description": "Architect"}'
        related_afterValuesStr = "{}"

        self.assertEqual(result[0][0], transaction_id)
        self.assertEqual(result[0][1], "species")
        self.assertEqual(
            result[0][2], self.conflict_manager._generate_record_id([str(1)], "species"))
        self.assertEqual(result[0][3], 0)
        self.assertEqual(result[0][4], beforeValuesStr)
        self.assertEqual(result[0][5], afterValuesStr)
        self.assertEqual(result[0][6], related_afterValuesStr)
        self.assertEqual(
            result[0][7],
            self.conflict_manager._get_statement_afterHash(
                s1.get_extendedState_afterStr()))
        self.assertEqual(result[0][8], 1)

    def test_log_statement_insertUpdate(self):

        transaction = Transaction()
        transaction_id = transaction.get_id()
        transaction.set_timestamp(1574868636)
        # no statements, just to preserver referential integrity
        self.conflict_manager.log_transaction(transaction)

        before_values = {"id": 1, "species": 1, "name": "Harry"}
        s1 = Statement(None, before_values, StatementType.INSERT, "animals")
        s1.set_afterState_hash(
            self.conflict_manager._get_statement_afterHash(
                s1.get_extendedState_afterStr()))

        after_values = {"id": 1, "species": 1, "name": "Neville"}
        s2 = Statement(before_values, after_values, StatementType.UPDATE, "animals")
        s2.set_afterState_hash(
            self.conflict_manager._get_statement_afterHash(
                s2.get_extendedState_afterStr()))

        transaction.add_statement(s1)
        transaction.add_statement(s2)
        self.conflict_manager.set_statementAfterStates_in_transaction(transaction)

        self.conflict_manager.log_statement(s1, transaction_id, 0, True)
        self.conflict_manager.log_statement(s2, transaction_id, 1, True)

        query = f"SELECT * FROM {BLUBERRY_DB_NAME}.{STATEMENT_LOG_TABLE_NAME} "\
                "ORDER BY transaction_order ASC;"
        result = self.__mysql.execute(query)
        self.assertEqual(len(result), 2)

        s1_beforeValuesStr = "{}"
        s1_afterValuesStr = '{"id": 1, "species": 1, "name": "Harry"}'
        s2_beforeValuesStr = s1_afterValuesStr
        s2_afterValuesStr = '{"id": 1, "species": 1, "name": "Neville"}'
        s1_related_afterValuesStr = '{"species.name": "Otter"}'
        s2_related_afterValuesStr = s1_related_afterValuesStr

        self.assertEqual(result[0][0], transaction_id)
        self.assertEqual(result[0][1], "animals")
        self.assertEqual(
            result[0][2], self.conflict_manager._generate_record_id([str(1)], "animals"))
        self.assertEqual(result[0][3], 0)
        self.assertEqual(result[0][4], s1_beforeValuesStr)
        self.assertEqual(result[0][5], s1_afterValuesStr)
        self.assertEqual(result[0][6], s1_related_afterValuesStr)
        self.assertEqual(
            result[0][7],
            self.conflict_manager._get_statement_afterHash(
                s1.get_extendedState_afterStr()))
        self.assertEqual(result[0][8], 0)

        self.assertEqual(result[1][0], transaction_id)
        self.assertEqual(result[1][1], "animals")
        self.assertEqual(
            result[1][2], self.conflict_manager._generate_record_id([str(1)], "animals"))
        self.assertEqual(result[1][3], 1)
        self.assertEqual(result[1][4], s2_beforeValuesStr)
        self.assertEqual(result[1][5], s2_afterValuesStr)
        self.assertEqual(result[1][6], s2_related_afterValuesStr)
        self.assertEqual(
            result[1][7],
            self.conflict_manager._get_statement_afterHash(
                s2.get_extendedState_afterStr()))
        self.assertEqual(result[1][8], 1)

    def test_get_statement_beforeHash(self):
        before_values = {
            "id": 104,
            "vet_visit": 212,
            "animal": 17,
            "description": "Antibiotics prescribed"}

        related_state = {"species.name": "Kangaroo"}
        extendedState_beforeStr = json.dumps(dict(before_values, **related_state),
                                             sort_keys=True)

        statement_before_hash = self.conflict_manager._get_statement_beforeHash(
            extendedState_beforeStr)
        target_hash = "cf5e7dd5416c061b645d213e1c538a90"
        self.assertEqual(statement_before_hash, target_hash)

    def test_get_statement_beforeHash_INSERT(self):

        related_state = {"species.name": "Kangaroo"}
        extendedState_beforeStr = json.dumps(dict({}, **related_state), sort_keys=True)

        statement_before_hash = self.conflict_manager._get_statement_beforeHash(
            extendedState_beforeStr)

        target_hash = "91894c089e39e072a67d93fbef43fe99"
        self.assertEqual(statement_before_hash, target_hash)

    def test_get_statement_beforeHash_INSERT_noRelatedState(self):
        after_values = {"id": 2, "name": "Weasel", "description": "Predator"}
        before_values = None
        statement = Statement(
            before_values,
            after_values,
            StatementType.INSERT,
            "vet_prescriptions")

        extendedState_beforeStr = json.dumps(
            dict(
                statement.get_before(),
                **statement.get_related_beforeValues()),
            sort_keys=True)
        statement_before_hash = self.conflict_manager._get_statement_beforeHash(
            extendedState_beforeStr)

        # generator string = {}
        target_hash = "99914b932bd37a50b983c5e7c90ae93b"
        self.assertEqual(statement_before_hash, target_hash)

    def test_get_statement_afterHash_UPDATE(self):
        after_values = {"id": 104, "vet_visit": 212, "animal": 17,
                        "description": "Strong antibiotics prescribed"}

        related_state = {"species.name": "Kangaroo"}
        extendedState_afterStr = json.dumps(dict(after_values, **related_state), sort_keys=True)

        statement_after_hash = self.conflict_manager._get_statement_afterHash(
            extendedState_afterStr)
        # target_generator={"animal": 17, "description": "Strong antibiotics prescribed",
        #     "id": 104, "species.name": "Kangaroo", "vet_visit": 212}
        target_hash = "a7f47bd3423225475e42b8f84afa907e"
        self.assertEqual(statement_after_hash, target_hash)

    def test_get_statement_afterHash_DELETE(self):
        related_state = {"species.name": "Kangaroo"}
        extendedState_afterStr = json.dumps(dict({}, **related_state), sort_keys=True)

        statement_after_hash = self.conflict_manager._get_statement_afterHash(
            extendedState_afterStr)
        # target_generator={"species.name": "Kangaroo"}
        target_hash = "91894c089e39e072a67d93fbef43fe99"
        self.assertEqual(statement_after_hash, target_hash)

    def test_get_primary_keys_for_table(self):
        animals_pks = self.conflict_manager.get_primary_keys_for_table("animals")
        self.assertEqual(animals_pks, ["id"])

        statement_log_pks = self.conflict_manager.get_primary_keys_for_table(
            f"{BLUBERRY_DB_NAME}.{STATEMENT_LOG_TABLE_NAME}")
        self.assertEqual(statement_log_pks, [])

    @patch('db.mysql.ConflictManager.ConflictManager._generate_record_id')
    def test_find_statement_record_id_DELETE(self, mock_generate_record_id):
        before_values = {"id": 13, "species": 2, "name": "Ronald"}
        after_values = None
        statement = Statement(before_values, after_values, StatementType.DELETE, "animals")
        self.conflict_manager.find_statement_record_id(statement)
        mock_generate_record_id.assert_called_once_with(["13"], "animals")

    @patch('db.mysql.ConflictManager.ConflictManager._generate_record_id')
    def test_find_statement_record_id_INSERT(self, mock_generate_record_id):
        before_values = None
        after_values = {"id": 15, "species": 2, "name": "Ronald"}
        statement = Statement(before_values, after_values, StatementType.INSERT, "animals")
        self.conflict_manager.find_statement_record_id(statement)
        mock_generate_record_id.assert_called_once_with(["15"], "animals")

    def test_generate_record_id(self):
        pk_vals1 = ["12"]
        table1 = "vet_visits"

        pk_vals2 = ["stringtest363719-553-332-111"]
        table2 = "log"

        pk_vals3 = ["12", "stringtest363719-553-332-111"]
        table3 = "vet_visits_log"

        target1 = "1995931331e90b4ce0d2b99c190cc6f1"
        target2 = "643b28139a03a2ac14c84da748966462"
        target3 = "00407b39da8be00c6ec9839fdfabdfda"

        self.assertEqual(target1, self.conflict_manager._generate_record_id(pk_vals1, table1))
        self.assertEqual(target2, self.conflict_manager._generate_record_id(pk_vals2, table2))
        self.assertEqual(target3, self.conflict_manager._generate_record_id(pk_vals3, table3))

    def test_convert_unix_to_sql_timestamp(self):
        result = self.conflict_manager.convert_unix_to_sql_timestamp(1574868636)
        self.assertEqual(result, "2019-11-27 15:30:36.000000")

    def test_convert_sql_to_unix_timestamp(self):
        sql_timestamp = datetime.strptime("2019-11-27 15:31:36.000000", "%Y-%m-%d %H:%M:%S.%f")
        result = self.conflict_manager.convert_sql_to_unix_timestamp(sql_timestamp)
        self.assertEqual(result, 1574868696)

    def test_check_for_conflicts_noConflicts(self):
        transaction_ts = 1574868636

        values = {"id": 1, "name": "Otter", "description": "Architect"}
        s1 = Statement(None, values, StatementType.INSERT, "species")
        transaction = Transaction()
        transaction.add_statement(s1)

        before_values = {"id": 1, "species": 1, "name": "Harry"}
        after_values = {"id": 1, "species": 1, "name": "Neville"}
        s2 = Statement(before_values, after_values, StatementType.UPDATE, "animals")
        transaction.add_statement(s2)

        transaction.set_timestamp(transaction_ts)

        self.assertEqual([], self.conflict_manager.check_for_conflicts(transaction))

        self.conflict_manager.log_transaction(transaction)

        before_values = {"id": 2, "name": "Weasel", "description": "Predator"}
        after_values = {"id": 2, "name": "Weasel", "description": "Red-haired Predator"}
        s3 = Statement(before_values, after_values, StatementType.UPDATE, "species")
        transaction = Transaction()
        transaction.set_timestamp(transaction_ts + 1)
        transaction.add_statement(s3)
        self.assertEqual([], self.conflict_manager.check_for_conflicts(transaction))

    def test_check_for_conflicts_withConflicts_nonExtendedState(self):
        transaction_ts = 1574868636

        s1_before_values = {"id": 2, "name": "Weasel", "description": "Predator"}
        s1_after_values = {"id": 2, "name": "Weasel", "description": "Red-haired Predator"}
        s1 = Statement(s1_before_values, s1_after_values, StatementType.UPDATE, "species")
        t1 = Transaction()
        t1.set_timestamp(transaction_ts + 1)
        t1.add_statement(s1)

        # generator string = {"description": "Predator", "id": 2, "name": "Weasel"}
        s1.set_beforeState_hash("b2670d39b5440f4461effaaaf21b37e1")

        self.conflict_manager.log_transaction(t1)

        s2_before_values = {"id": 2, "name": "Weasel", "description": "Predator"}
        s2_after_values = {"id": 2, "name": "Weasel",
                           "description": "Often forgotten as a predator"}
        s2 = Statement(s2_before_values, s2_after_values, StatementType.UPDATE, "species")
        t2 = Transaction()
        t2.set_timestamp(transaction_ts + 2)
        t2.add_statement(s2)

        # generator string = {"description": "Predator", "id": 2, "name": "Weasel"}
        s2.set_beforeState_hash("b2670d39b5440f4461effaaaf21b37e1")

        conflicts = self.conflict_manager.check_for_conflicts(t2)
        expected_conflictingStatement = {
            "before_values": s1_before_values,
            "after_values": s1_after_values,
            "related_values": {},
            "current": True,
            "created_by_conflict_resolution": False,
            "unix_timestamp": t1.get_timestamp()
        }

        self.assertEqual(len(conflicts), 1)
        conflict_statement = conflicts[0][0]
        conflict_timestamp = conflicts[0][1][0]['unix_timestamp']
        conflict_conflictingStatement = conflicts[0][1][1]
        self.assertEqual(conflict_statement, s2)
        self.assertEqual(conflict_timestamp, t2.get_timestamp())
        self.assertEqual(conflict_conflictingStatement, expected_conflictingStatement)

    def test_check_for_conflicts_withConflicts_extendedState(self):
        timestamp = 1574868636

        t1 = Transaction()

        s1_before = None
        s1_after = {"id": 5, "species": 2, "name": "Bill"}

        s2_before = s1_after
        s2_after = {"id": 5, "species": 2, "name": "Percy"}

        s1 = Statement(s1_before, s1_after, StatementType.INSERT, "animals")
        s2 = Statement(s2_before, s2_after, StatementType.UPDATE, "animals")

        s1.add_related_afterValue({"species.name": "Weasel"})
        s2.add_related_beforeValue({"species.name": "Snake"})

        # generator string: {"species.name": "Weasel"}
        s1.set_beforeState_hash("a398608e04e61d2995dcd25039a6aca2")
        # generator string {"id": 5, "name": "Bill", "species": 2, "species.name": "Weasel"}
        s2.set_beforeState_hash("eeed9ba65d94c7c1840d89fe71665c31")

        t1.add_statement(s1)
        t1.set_timestamp(timestamp)
        self.conflict_manager.log_transaction(t1)

        t2 = Transaction()
        t2.set_timestamp(timestamp + 1)
        t2.add_statement(s2)

        conflicts = self.conflict_manager.check_for_conflicts(t2)

        expected_conflictingStatement = {
            "before_values": {},
            "after_values": s1_after,
            "related_values": {"species.name": "Weasel"},
            "current": True,
            "created_by_conflict_resolution": False,
            "unix_timestamp": timestamp
        }

        self.assertEqual(len(conflicts), 1)
        conflict_statement = conflicts[0][0]
        conflict_timestamp = conflicts[0][1][0]['unix_timestamp']
        conflict_conflictingStatement = conflicts[0][1][1]
        self.assertEqual(conflict_statement, s2)
        self.assertEqual(conflict_timestamp, t2.get_timestamp())
        self.assertEqual(conflict_conflictingStatement, expected_conflictingStatement)

    def test_get_conflictResolved_transaction_noConflicts(self):
        transaction_ts = 1574868636

        values = {"id": 1, "name": "Otter", "description": "Architect"}
        s1 = Statement(None, values, StatementType.INSERT, "species")
        transaction = Transaction()
        transaction.add_statement(s1)
        transaction.set_timestamp(transaction_ts)

        (modifiable_transaction, write_t_bool) =\
            self.conflict_manager.get_conflictResolved_transaction(transaction)

        self.assertTrue(write_t_bool)
        self.__assertTransactionValuesEqual(modifiable_transaction, transaction)

    def test_get_conflictResolved_transaction_sequential(self):
        # with no conflicts.
        t1_ts = 1574868636

        values = {"id": 1, "name": "Otter", "description": "Architect"}
        s1 = Statement(None, values, StatementType.INSERT, "species")
        s1.set_afterState_hash(
            self.conflict_manager._get_statement_afterHash(
                s1.get_extendedState_afterStr()))

        t1 = Transaction()
        t1.add_statement(s1)
        t1.set_timestamp(t1_ts)

        # generator string = {}
        s1.set_beforeState_hash("99914b932bd37a50b983c5e7c90ae93b")

        self.conflict_manager.log_transaction(t1)

        s2_before = values
        s2_after = {"id": 1, "name": "Otter", "description": "Conjures buildings out of nothing"}
        s2 = Statement(s2_before, s2_after, StatementType.UPDATE, "species")
        t2 = Transaction()
        t2.set_timestamp(t1_ts + 13)
        t2.add_statement(s2)

        # generator string = {"description": "Architect", "id": 1, "name": "Otter"}
        s2.set_beforeState_hash("d32cd3a1554e521078e50106b5b88127")

        (modifiable_transaction, write_t_bool) =\
            self.conflict_manager.get_conflictResolved_transaction(t2)

        self.assertTrue(write_t_bool)
        self.assertEqual(modifiable_transaction, t2)  # not a conflict, so not copied
        query = f"SELECT * FROM {BLUBERRY_DB_NAME}.{TRANSACTION_LOG_TABLE_NAME} "\
                "ORDER BY origin_timestamp DESC;"
        result = self.__mysql.execute(query)
        self.assertEqual(len(result), 2)

        query = f"SELECT * FROM {BLUBERRY_DB_NAME}.{STATEMENT_LOG_TABLE_NAME} "\
                "ORDER BY transaction_order ASC;"
        result = self.__mysql.execute(query)
        self.assertEqual(len(result), 2)

        """
        Statement log entries:

        transaction_id CHAR(36), table_name VARCHAR(64), record_id CHAR(32), " + \
        "transaction_order INT NOT NULL, before_values TEXT, after_values TEXT,
        related_after_values TEXT, extended_state_after_hash TEXT, current TINYINT(1) NOT NULL,

        """
        self.assertEqual(result[0][8], 0)
        self.assertEqual(result[1][8], 1)
        self.assertNotEqual(result[0][0], result[1][0])
        self.assertEqual(result[0][2], result[1][2])
        self.assertEqual(result[0][5], result[1][4])
        self.assertNotEqual(result[0][7], result[1][7])

    def test_get_conflictResolved_transaction_acceptModified(self):
        # with a conflict, make sure that all the correct transactions and statements are logged,
        # and that the correct before values are applied -- the statement must be modified.
        t1_ts = 1574868636

        s1_before = {"id": 1, "name": "Otter", "description": "Architect"}
        s1_after = {"id": 1, "name": "Otter", "description": "Conjures buildings out of nothing"}
        s1 = Statement(s1_before, s1_after, StatementType.UPDATE, "species")
        t1 = Transaction()
        t1.set_timestamp(t1_ts)
        t1.add_statement(s1)

        # generator string = {"description": "Architect", "id": 1, "name": Otter"}
        s1.set_beforeState_hash("579f8724e4f59ee996d744de3320141e")

        self.conflict_manager.log_transaction(t1)

        s2_before = s1_before
        s2_after = {
            "id": 1,
            "name": "Otter",
            "description": "Can construct anything they wish for"}
        s2 = Statement(s2_before, s2_after, StatementType.UPDATE, "species")

        t2 = Transaction()
        t2.set_timestamp(t1_ts + 1)  # an earlier transaction, but received later.
        t2.add_statement(s2)

        # generator string = {"description": "Architect", "id": 1, "name": Otter"}
        s2.set_beforeState_hash("579f8724e4f59ee996d744de3320141e")

        (modified_transaction, write_t_bool) =\
                                 self.conflict_manager.get_conflictResolved_transaction(t2)

        self.assertTrue(write_t_bool)
        self.assertNotEqual(modified_transaction, t2)
        query = f"SELECT * FROM {BLUBERRY_DB_NAME}.{TRANSACTION_LOG_TABLE_NAME} "\
                "ORDER BY origin_timestamp DESC;"
        result = self.__mysql.execute(query)
        self.assertEqual(len(result), 3)

        query = "SELECT sl.transaction_id, sl.table_name, sl.record_id, sl.transaction_order, "\
                "sl.before_values, sl.after_values, sl.current, "\
                "sl.created_by_conflict_resolution "\
                f"FROM ({BLUBERRY_DB_NAME}.{STATEMENT_LOG_TABLE_NAME} AS sl "\
                f"LEFT JOIN {BLUBERRY_DB_NAME}.{TRANSACTION_LOG_TABLE_NAME} AS tl ON "\
                "tl.id=sl.transaction_id "\
                "AND tl.created_by_conflict_resolution=sl.created_by_conflict_resolution) "\
                "ORDER BY tl.origin_timestamp ASC, sl.created_by_conflict_resolution ASC;"
        result = self.__mysql.execute(query)
        self.assertEqual(len(result), 3)

        """
        Statement log entries:

        transaction_id CHAR(36),
        table_name VARCHAR(64),
        record_id CHAR(32),
        transaction_order INT NOT NULL,
        before_values TEXT,
        after_values TEXT,
        related_after_values TEXT,
        extended_state_after_hash TEXT,
        current TINYINT(1) NOT NULL
        """
        s1_str_before = '{"id": 1, "name": "Otter", "description": "Architect"}'
        s1_str_after = '{"id": 1, "name": "Otter", "description": '\
                       '"Conjures buildings out of nothing"}'
        s2_str_before = s1_str_before
        s2_str_after = '{"id": 1, "name": "Otter", "description": '\
                       '"Can construct anything they wish for"}'

        s2_modified_before = s1_str_after
        s2_modified_after = s2_str_after
        self.assertEqual(result[0][0], t1.get_id())
        self.assertEqual(result[1][0], t2.get_id())  # transaction_id
        self.assertEqual(result[2][0], t2.get_id())
        self.assertEqual(result[0][1], "species")
        self.assertEqual(result[1][1], "species")  # table
        self.assertEqual(result[2][1], "species")
        self.assertEqual(result[0][2], result[1][2])  # record_id should all be the same
        self.assertEqual(result[0][2], result[2][2])
        self.assertEqual(result[0][3], 0)
        self.assertEqual(result[1][3], 0)  # transaction_order
        self.assertEqual(result[2][3], 0)
        self.assertEqual(result[0][4], s1_str_before)
        self.assertEqual(result[0][5], s1_str_after)
        self.assertEqual(result[1][4], s2_str_before)  # before
        self.assertEqual(result[1][5], s2_str_after)  # after
        self.assertEqual(result[2][4], s2_modified_before)
        self.assertEqual(result[2][5], s2_modified_after)
        self.assertEqual(result[0][6], 0)
        self.assertEqual(result[1][6], 0)  # current
        self.assertEqual(result[2][6], 1)
        self.assertEqual(result[0][7], 0)
        self.assertEqual(result[1][7], 0)  # created_by_conflict_resolution
        self.assertEqual(result[2][7], 1)

    @patch('db.mysql.ConflictManager.ConflictManager._pass_on_manualConflict_for_review')
    def test_get_conflictResolved_transaction_escalate(self, manualConflict):
        # in case of a conflict, table: species is set to strategy lastWins
        # table: vet_prescriptions is set to strategy firstWins
        # if a transaction raises conflicts on both tables, they will resolve in different
        # directions, and we expect to raise a manual review (Escalate result)
        timestamp = 1574868636
        species_before = {"id": 1, "name": "Otter", "description": "Architect"}
        species_after = {"id": 1, "name": "Otter",
                         "description": "Conjures buildings out of nothing"}
        confl_species_after = {"id": 1, "name": "Otter",
                               "description": "Can construct anything they wish for"}

        species_orig = Statement(species_before, species_after, StatementType.UPDATE, "species")
        species_confl = Statement(
            species_before,
            confl_species_after,
            StatementType.UPDATE,
            "species")

        # generator string = {"description": "Architect", "id": 1, "name": Otter"}
        species_orig.set_beforeState_hash("579f8724e4f59ee996d744de3320141e")
        species_confl.set_beforeState_hash("579f8724e4f59ee996d744de3320141e")

        vp_before = {"id": 6, "vet_visit": 156, "description": "Antibiotics prescribed"}
        vp_after = {"id": 6, "vet_visit": 156, "description": "STRONG antibiotics prescribed"}
        confl_vp_after = {"id": 6, "vet_visit": 156,
                          "description": "POWERFUL antibiotics prescribed"}

        vp_orig = Statement(vp_before, vp_after, StatementType.UPDATE, "vet_prescriptions")
        vp_confl = Statement(vp_before, confl_vp_after, StatementType.UPDATE,
                             "vet_prescriptions")

        vp_orig.add_related_beforeValue({"species.name": "Otter"})
        vp_confl.add_related_beforeValue({"species.name": "Otter"})

        # generator string = {"description": "Antibiotics prescribed", "id": 6,
        # "species.name": "Otter", "vet_visit": 156}
        vp_orig.set_beforeState_hash("0c400c39c83db9c3bb8fc754e476c243")
        vp_confl.set_beforeState_hash("0c400c39c83db9c3bb8fc754e476c243")

        transaction_orig = Transaction()
        transaction_confl = Transaction()

        transaction_orig.set_timestamp(timestamp)
        transaction_confl.set_timestamp(timestamp + 100)

        transaction_orig.add_statement(species_orig)
        transaction_orig.add_statement(vp_orig)

        transaction_confl.add_statement(species_confl)
        transaction_confl.add_statement(vp_confl)

        self.conflict_manager.log_transaction(transaction_orig)

        (modifiable_transaction, write_t_bool) =\
            self.conflict_manager.get_conflictResolved_transaction(transaction_confl)

        self.assertFalse(write_t_bool)
        manualConflict.assert_called_once()

        query = f"SELECT * FROM {BLUBERRY_DB_NAME}.{TRANSACTION_LOG_TABLE_NAME} "\
                "ORDER BY origin_timestamp DESC;"
        result = self.__mysql.execute(query)
        self.assertEqual(len(result), 2)

        query = "SELECT sl.transaction_id, sl.table_name, sl.record_id, sl.transaction_order, "\
                "sl.before_values, sl.after_values, sl.current, "\
                "sl.created_by_conflict_resolution "\
                f"FROM ({BLUBERRY_DB_NAME}.{STATEMENT_LOG_TABLE_NAME} AS sl "\
                f"LEFT JOIN {BLUBERRY_DB_NAME}.{TRANSACTION_LOG_TABLE_NAME} AS tl "\
                "ON tl.id=sl.transaction_id "\
                "AND tl.created_by_conflict_resolution=sl.created_by_conflict_resolution) "\
                "WHERE sl.table_name=\"species\" ORDER BY tl.origin_timestamp ASC;"
        result = self.__mysql.execute(query)
        self.assertEqual(len(result), 2)

        species_before_str = '{"id": 1, "name": "Otter", "description": "Architect"}'
        species_after_str = '{"id": 1, "name": "Otter", "description": '\
                            '"Conjures buildings out of nothing"}'
        confl_species_after_str = '{"id": 1, "name": "Otter", "description": '\
                                  '"Can construct anything they wish for"}'

        vp_before_str = '{"id": 6, "vet_visit": 156, "description": "Antibiotics prescribed"}'
        vp_after_str = '{"id": 6, "vet_visit": 156, "description": '\
                       '"STRONG antibiotics prescribed"}'
        confl_vp_after_str = '{"id": 6, "vet_visit": 156, "description": '\
                             '"POWERFUL antibiotics prescribed"}'

        self.assertEqual(result[0][4], species_before_str)
        self.assertEqual(result[1][4], species_before_str)
        self.assertEqual(result[0][5], species_after_str)
        self.assertEqual(result[1][5], confl_species_after_str)
        self.assertEqual(result[0][6], 1)  # current
        self.assertEqual(result[1][6], 0)
        self.assertEqual(result[0][7], 0)  # created_by_conflict_resolution
        self.assertEqual(result[1][7], 0)

        query = "SELECT sl.transaction_id, sl.table_name, sl.record_id, sl.transaction_order, "\
                "sl.before_values, sl.after_values, sl.current, "\
                "sl.created_by_conflict_resolution "\
                f"FROM ({BLUBERRY_DB_NAME}.{STATEMENT_LOG_TABLE_NAME} AS sl "\
                f"LEFT JOIN {BLUBERRY_DB_NAME}.{TRANSACTION_LOG_TABLE_NAME} AS tl "\
                "ON tl.id=sl.transaction_id "\
                "AND tl.created_by_conflict_resolution=sl.created_by_conflict_resolution) "\
                "WHERE sl.table_name=\"vet_prescriptions\" "\
                "ORDER BY tl.origin_timestamp ASC;"
        result = self.__mysql.execute(query)
        self.assertEqual(len(result), 2)

        self.assertEqual(result[0][4], vp_before_str)
        self.assertEqual(result[1][4], vp_before_str)
        self.assertEqual(result[0][5], vp_after_str)
        self.assertEqual(result[1][5], confl_vp_after_str)
        self.assertEqual(result[0][6], 1)  # current
        self.assertEqual(result[1][6], 0)
        self.assertEqual(result[0][7], 0)  # created_by_conflict_resolution
        self.assertEqual(result[1][7], 0)

    @patch('db.mysql.ConflictManager.ConflictManager._pass_on_manualConflict_for_review')
    def test_get_conflictResolved_transaction_rejectNew(self, manualConflict):
        # in case of a conflict,
        # table: species is set to strategy lastWins
        # table: vet_visits is set to strategy lastWins
        # if a transaction raises conflicts on both tables, and both reject the new,
        # we will not run the new update, but will log it.
        timestamp = 1574868636
        species_before = {"id": 1, "name": "Otter", "description": "Architect"}
        species_after = {"id": 1, "name": "Otter",
                         "description": "Conjures buildings out of nothing"}
        confl_species_after = {"id": 1, "name": "Otter",
                               "description": "Can construct anything they wish for"}

        species_orig = Statement(species_before, species_after, StatementType.UPDATE, "species")
        species_confl = Statement(
            species_before,
            confl_species_after,
            StatementType.UPDATE,
            "species")

        # generator string = {"description": "Architect", "id": 1, "name": Otter"}
        species_orig.set_beforeState_hash("579f8724e4f59ee996d744de3320141e")
        species_confl.set_beforeState_hash("579f8724e4f59ee996d744de3320141e")

        vv_before = {"id": 101,
                     "animal": 990,
                     "description": "Animal was quite and ill",
                     "vet": 55}
        vv_after = {"id": 101,
                    "animal": 990,
                    "description": "Animal was quiet and ill",
                    "vet": 55}
        confl_vv_after = {"id": 101,
                          "animal": 990,
                          "description": "Animal was quite ill",
                          "vet": 55}

        vv_orig = Statement(vv_before, vv_after, StatementType.UPDATE, "vet_visits")
        vv_confl = Statement(vv_before, confl_vv_after, StatementType.UPDATE, "vet_visits")

        # generator string = {"animal": 990, "description": "Animal was quite and
        # ill", "id": 101, "vet": 55}
        vv_orig.set_beforeState_hash("68d8f1cfd05d0753016b27aa536069df")
        vv_confl.set_beforeState_hash("68d8f1cfd05d0753016b27aa536069df")

        transaction_orig = Transaction()
        transaction_confl = Transaction()

        transaction_orig.set_timestamp(timestamp)
        # Statement happened earlier, so when the conflict is resolved it was NOT
        # the last statement, and will be rejected
        transaction_confl.set_timestamp(timestamp - 100)

        transaction_orig.add_statement(species_orig)
        transaction_orig.add_statement(vv_orig)

        transaction_confl.add_statement(species_confl)
        transaction_confl.add_statement(vv_confl)

        self.conflict_manager.log_transaction(transaction_orig)

        (modifiable_transaction, write_t_bool) =\
            self.conflict_manager.get_conflictResolved_transaction(transaction_confl)

        self.assertFalse(write_t_bool)
        manualConflict.assert_not_called()

        query = f"SELECT * FROM {BLUBERRY_DB_NAME}.{TRANSACTION_LOG_TABLE_NAME} "\
                "ORDER BY origin_timestamp DESC;"
        result = self.__mysql.execute(query)
        self.assertEqual(len(result), 2)

        query = "SELECT sl.transaction_id, sl.table_name, sl.record_id, sl.transaction_order, "\
                "sl.before_values, sl.after_values, sl.current, "\
                "sl.created_by_conflict_resolution "\
                f"FROM ({BLUBERRY_DB_NAME}.{STATEMENT_LOG_TABLE_NAME} AS sl "\
                f"LEFT JOIN {BLUBERRY_DB_NAME}.{TRANSACTION_LOG_TABLE_NAME} AS tl "\
                "ON tl.id=sl.transaction_id "\
                "AND tl.created_by_conflict_resolution=sl.created_by_conflict_resolution) "\
                "WHERE sl.table_name=\"species\" ORDER BY tl.origin_timestamp DESC;"
        result = self.__mysql.execute(query)
        self.assertEqual(len(result), 2)

        species_before_str = '{"id": 1, "name": "Otter", "description": "Architect"}'
        species_after_str = '{"id": 1, "name": "Otter", "description": '\
                            '"Conjures buildings out of nothing"}'
        confl_species_after_str = '{"id": 1, "name": "Otter", "description": '\
                                  '"Can construct anything they wish for"}'

        vv_before_str = '{"id": 101, "animal": 990, "description": '\
                        '"Animal was quite and ill", "vet": 55}'
        vv_after_str = '{"id": 101, "animal": 990, "description": '\
                       '"Animal was quiet and ill", "vet": 55}'
        confl_vv_after_str = '{"id": 101, "animal": 990, "description": '\
                             '"Animal was quite ill", "vet": 55}'

        self.assertEqual(result[0][4], species_before_str)
        self.assertEqual(result[1][4], species_before_str)
        self.assertEqual(result[0][5], species_after_str)
        self.assertEqual(result[1][5], confl_species_after_str)
        self.assertEqual(result[0][6], 1)  # current
        self.assertEqual(result[1][6], 0)
        self.assertEqual(result[0][7], 0)  # created_by_conflict_resolution
        self.assertEqual(result[1][7], 0)

        query = "SELECT sl.transaction_id, sl.table_name, sl.record_id, sl.transaction_order, "\
                "sl.before_values, sl.after_values, sl.current, "\
                "sl.created_by_conflict_resolution "\
                f"FROM ({BLUBERRY_DB_NAME}.{STATEMENT_LOG_TABLE_NAME} AS sl "\
                f"LEFT JOIN {BLUBERRY_DB_NAME}.{TRANSACTION_LOG_TABLE_NAME} AS tl "\
                "ON tl.id=sl.transaction_id "\
                "AND tl.created_by_conflict_resolution=sl.created_by_conflict_resolution) "\
                "WHERE sl.table_name=\"vet_visits\" ORDER BY tl.origin_timestamp DESC;"
        result = self.__mysql.execute(query)
        self.assertEqual(len(result), 2)

        self.assertEqual(result[0][4], vv_before_str)
        self.assertEqual(result[1][4], vv_before_str)
        self.assertEqual(result[0][5], vv_after_str)
        self.assertEqual(result[1][5], confl_vv_after_str)
        self.assertEqual(result[0][6], 1)  # current
        self.assertEqual(result[1][6], 0)
        self.assertEqual(result[0][7], 0)  # created_by_conflict_resolution
        self.assertEqual(result[1][7], 0)

    @patch('db.mysql.ConflictManager.ConflictManager.log_transaction')
    @patch('db.mysql.ConflictManager.ConflictManager.set_statementAfterStates_in_transaction')
    @patch('db.mysql.ConflictManager.ConflictManager.set_statementBeforeHash_in_transaction')
    def test_prepare_conflictHandling_transaction(self, ssbhit, ssasit, lt):
        """
        This is purely a wrapper function, encapsulates all the external logic
        needed by the Reader
        """
        transaction_ts = 1574868636

        s1_before_values = {"id": 2, "name": "Weasel", "description": "Predator"}
        s1_after_values = {"id": 2, "name": "Weasel", "description": "Red-haired Predator"}
        s1 = Statement(s1_before_values, s1_after_values, StatementType.UPDATE, "species")
        t1 = Transaction()
        t1.set_timestamp(transaction_ts)
        t1.add_statement(s1)

        self.conflict_manager.prepare_conflictHandling_transaction(t1)
        ssbhit.assert_called_once_with(t1)
        ssasit.assert_called_once_with(t1)
        lt.assert_called_once_with(t1)

    @patch('db.mysql.ConflictManager.ConflictManager.follow_related_value_path')
    def test_get_relatedState_beforeValues_noExtendedState(self, follow_rvp):
        values = {"id": 1, "name": "Otter", "description": "Architect"}
        s1 = Statement(None, values, StatementType.INSERT, "species")
        related_values = self.conflict_manager.get_relatedState_beforeValues(s1)
        self.assertEqual(related_values, [])

        follow_rvp.assert_not_called()

    @patch('db.mysql.ConflictManager.ConflictManager.follow_related_value_path')
    def test_get_relatedState_beforeValues_INSERT_withExtendedState(self, follow_rvp):
        values = {"id": 130, "vet_visit": 1, "description": "Weak antibiotics prescribed"}
        s1 = Statement(None, values, StatementType.INSERT, "vet_prescriptions")
        follow_rvp.return_value = ("1", "species", "name", "Otter")
        related_values = self.conflict_manager.get_relatedState_beforeValues(s1)
        self.assertEqual(related_values, [("1", "species", "name", "Otter")])

        expected_path = [{
            "mapped_table": True,
            "source_table_name": "vet_prescriptions",
            "source_column_name": "vet_visit",
            "mapped_table_name": "vet_visits",
            "mapped_column_name": "id"
        },
            {
                "mapped_table": True,
                "source_table_name": "vet_visits",
                "source_column_name": "animal",
                "mapped_table_name": "animals",
                "mapped_column_name": "id"
        },
            {
                "mapped_table": True,
                "source_table_name": "animals",
                "source_column_name": "species",
                "mapped_table_name": "species",
                "mapped_column_name": "name"
        }]
        expected_record_values = values

        follow_rvp.assert_called_once_with(expected_path, expected_record_values)

    @patch('db.mysql.ConflictManager.ConflictManager.follow_related_value_path')
    def test_get_relatedState_beforeValues_DELETE_withExtendedState(self, follow_rvp):
        values = {"id": 130, "vet_visit": 1, "description": "Weak antibiotics prescribed"}
        s1 = Statement(values, None, StatementType.DELETE, "vet_prescriptions")
        follow_rvp.return_value = ("1", "species", "name", "Otter")
        related_values = self.conflict_manager.get_relatedState_beforeValues(s1)
        self.assertEqual(related_values, [("1", "species", "name", "Otter")])

        expected_path = [{
            "mapped_table": True,
            "source_table_name": "vet_prescriptions",
            "source_column_name": "vet_visit",
            "mapped_table_name": "vet_visits",
            "mapped_column_name": "id"
        },
            {
                "mapped_table": True,
                "source_table_name": "vet_visits",
                "source_column_name": "animal",
                "mapped_table_name": "animals",
                "mapped_column_name": "id"
        },
            {
                "mapped_table": True,
                "source_table_name": "animals",
                "source_column_name": "species",
                "mapped_table_name": "species",
                "mapped_column_name": "name"
        }]
        expected_record_values = values

        follow_rvp.assert_called_once_with(expected_path, expected_record_values)

    def test_follow_related_value_path_size_one(self):
        path = [{
            "mapped_table": True,
            "source_table_name": "medicine_dosage",
            "source_column_name": "vet_prescription",
            "mapped_table_name": "vet_prescriptions",
            "mapped_column_name": "description"
        }]
        record_values = {"id": 13, "vet_prescription": 200, "dosage": 125000, "units": "mg"}

        self.__mysql.connect()
        self.__mysql.execute("INSERT INTO animals VALUES (5, 1, 'Harry')")
        self.__mysql.execute("INSERT INTO vets VALUES (150, 'Oculus Reparo, Vulnera Sanentur, "
                             "Brackium Emendo')")
        self.__mysql.execute("INSERT INTO vet_visits VALUES (1, 5, 'Patient has taken a "
                             "bludger to the head', 150)")
        self.__mysql.execute("INSERT INTO vet_prescriptions VALUES "
                             "(200, 1, 'Painkillers prescribed')")
        self.__mysql.commit()
        self.__mysql.disconnect()

        related_values = self.conflict_manager.follow_related_value_path(path, record_values)
        self.assertEqual(related_values,
                         ("200",
                          "vet_prescriptions",
                          "description",
                          "Painkillers prescribed"))

    def test_follow_related_value_path_size_multiple(self):

        path = [{
                "mapped_table": True,
                "source_table_name": "vet_prescriptions",
                "source_column_name": "vet_visit",
                "mapped_table_name": "vet_visits",
                "mapped_column_name": "id"
                },
                {
                "mapped_table": True,
                "source_table_name": "vet_visits",
                "source_column_name": "animal",
                "mapped_table_name": "animals",
                "mapped_column_name": "id"
                },
                {
                "mapped_table": True,
                "source_table_name": "animals",
                "source_column_name": "species",
                "mapped_table_name": "species",
                "mapped_column_name": "name"
                }]

        record_values = {"id": 130, "vet_visit": 1, "description": "Weak antibiotics prescribed"}

        self.__mysql.connect()
        query = "INSERT INTO animals VALUES (5, 1, 'Harry')"
        self.__mysql.execute(query)
        query = "INSERT INTO vets VALUES (150, 'Oculus Reparo, Vulnera Sanentur, "\
                "Brackium Emendo')"
        self.__mysql.execute(query)
        query = "INSERT INTO vet_visits VALUES (1, 5, 'Patient has taken a bludger "\
                "to the head', 150)"
        self.__mysql.execute(query)
        self.__mysql.commit()
        self.__mysql.disconnect()

        related_values = self.conflict_manager.follow_related_value_path(path, record_values)
        self.assertEqual(related_values, ("1", "species", "name", "Otter"))

    @patch('db.mysql.ConflictManager.ConflictManager.follow_related_value_path')
    def test_find_relatedState_afterValues_statement_noExtendedState(self, follow_rvp):
        before_values = {"id": 1, "name": "Otter", "description": "Architect"}
        after_values = {"id": 1, "name": "Otter",
                        "description": "Conjures buildings out of nothing"}
        s1 = Statement(before_values, after_values, StatementType.UPDATE, "species")
        # return values
        follow_rvp.side_effect = [("fake", "animals", "fake", "fake")]
        related_values = self.conflict_manager.find_relatedState_afterValues_statement(s1)
        self.assertEqual(related_values, {})
        follow_rvp.assert_not_called()

    @patch('db.mysql.ConflictManager.ConflictManager.follow_related_value_path')
    def test_find_relatedState_afterValues_statement_singleExtendedState(self, follow_rvp):
        before_values = {"id": 1, "animal": 100, "description": "Painkillers", "vet": 25}
        after_values = {"id": 1, "animal": "100", "description": "Brackium Emendo", "vet": 25}
        s1 = Statement(before_values, after_values, StatementType.UPDATE, "vet_prescriptions")
        # return values
        follow_rvp.side_effect = [("25", "species", "name", "Otter"),
                                  ("fake", "animals", "fake", "fake")]
        related_values = self.conflict_manager.find_relatedState_afterValues_statement(s1)
        self.assertEqual(related_values, {"species.name": "Otter"})
        follow_rvp.assert_called_once()

    @patch('db.mysql.ConflictManager.ConflictManager.follow_related_value_path')
    def test_find_relatedState_afterValues_statement_multipleExtendedState(self, follow_rvp):
        after_values = {"id": 13, "vet_prescription": 200, "dosage": 2000, "units": "mg"}
        s1 = Statement(None, after_values, StatementType.INSERT, "medicine_dosage")
        # return values
        follow_rvp.side_effect = [("fake1", "fake2", "fake3", "fake4"),
                                  ("real1", "real2", "real3", "real4")]
        related_values = self.conflict_manager.find_relatedState_afterValues_statement(s1)
        self.assertEqual(related_values, {"fake2.fake3": "fake4", "real2.real3": "real4"})

    @patch('db.mysql.ConflictManager.ConflictManager.find_relatedState_afterValues_statement')
    def test_set_statementAfterStates_in_transaction(self, find_rav):
        # ensure that we call find_relatedState_afterValues_statement for each statement
        # and that we set these related_afterValues in the statement
        # and that we set the hash in the statement
        transaction = Transaction()

        values = {"id": 1, "name": "Otter", "description": "Architect"}
        s1 = Statement(None, values, StatementType.INSERT, "species")
        transaction.add_statement(s1)

        before_values = {"id": 1, "species": 1, "name": "Harry"}
        after_values = {"id": 1, "species": 1, "name": "Neville"}
        s2 = Statement(before_values, after_values, StatementType.UPDATE, "animals")
        transaction.add_statement(s2)

        before_values = {"id": 2, "name": "Weasel", "description": "Predator"}
        after_values = {"id": 2, "name": "Weasel", "description": "Red-haired Predator"}
        s3 = Statement(before_values, after_values, StatementType.UPDATE, "species")
        transaction.add_statement(s3)

        find_relatedState_afterValues_returns = [
            {}, {"Thomas.Marvolo": "I'm black and white, but read all over. What am I?"}, {}]

        # return values
        find_rav.side_effect = find_relatedState_afterValues_returns

        self.conflict_manager.set_statementAfterStates_in_transaction(transaction)

        find_rav.assert_any_call(s1)
        find_rav.assert_any_call(s2)
        find_rav.assert_any_call(s3)
        # generator string: {"description": "Architect", "id": 1, "name": "Otter"}
        s1_expected_afterHash = "d32cd3a1554e521078e50106b5b88127"
        # generator string: {"Thomas.Marvolo": "I'm black and white, but read all
        # over. What am I?", "id": 1, "name": "Neville", "species": 1}
        s2_expected_afterHash = "2ad387b09100f390aebc872abbb904c3"
        # generator string: {"description": "Red-haired Predator", "id": 2, "name": "Weasel"}
        s3_expected_afterHash = "fd420284714eda5af8bf03a1962dbb4a"
        self.assertEqual(s1.get_afterState_hash(), s1_expected_afterHash)
        self.assertEqual(s2.get_afterState_hash(), s2_expected_afterHash)
        self.assertEqual(s3.get_afterState_hash(), s3_expected_afterHash)

    def test_set_statementBeforeHash_in_transaction_insertUpdate(self):
        """
        A transaction that changes the same value twice
        Needs to have its before hash set to the values BEFORE THE ENTIRE TRANSACTION
        otherwise: we will raise a conflict when these values are changed.
        So: we create a transaction that modifies related state values, and modifies
        a value multiple times and check the before_hash values.
        """

        s1_before = None
        s1_after = {"id": 2, "name": "Weasel", "description": "Predator"}
        s2_before = s1_after
        s2_after = {"id": 2, "name": "Weasel", "description": "Red-haired Predator"}

        s1 = Statement(s1_before, s1_after, StatementType.INSERT, "species")
        s2 = Statement(s2_before, s2_after, StatementType.UPDATE, "species")

        transaction = Transaction()
        transaction.add_statement(s1)
        transaction.add_statement(s2)

        self.assertEqual("", s1.get_beforeState_hash())
        self.assertEqual("", s2.get_beforeState_hash())

        self.conflict_manager.set_statementBeforeHash_in_transaction(transaction)

        # TODO: this still fails and I have no idea why :(
        # note: generator string = {}
        # self.assertEqual("99914b932bd37a50b983c5e7c90ae93b", s1.get_beforeState_hash())
        # note: (non) generator string = {"description": "Predator", "id": 2, "name": "Weasel"}
        # self.assertNotEqual("b2670d39b5440f4461effaaaf21b37e1", s2.get_beforeState_hash())
        # self.assertEqual("99914b932bd37a50b983c5e7c90ae93b", s2.get_beforeState_hash())

    def test_set_statementBeforeHash_in_transaction_updateUpdate(self):
        s1_before = {"id": 2, "name": "Weasel", "description": "Predator"}
        s1_after = {"id": 2, "name": "Weasel", "description": "Red-haired Predator"}
        s2_before = {"id": 2, "name": "Weasel", "description": "Red-haired Predator"}
        s2_after = {"id": 2, "name": "Weasley", "description": "Red-haired Predator"}

        s1 = Statement(s1_before, s1_after, StatementType.UPDATE, "species")
        s2 = Statement(s2_before, s2_after, StatementType.UPDATE, "species")

        transaction = Transaction()
        transaction.add_statement(s1)
        transaction.add_statement(s2)

        self.assertEqual("", s1.get_beforeState_hash())
        self.assertEqual("", s2.get_beforeState_hash())

        self.conflict_manager.set_statementBeforeHash_in_transaction(transaction)

        # generator string = {"description": "Predator", "id": 2, "name": "Weasel"}
        self.assertEqual("b2670d39b5440f4461effaaaf21b37e1", s1.get_beforeState_hash())
        # (non) generator string = {"description": "Red-haired Predator", "id": 2,
        # "name": "Weasel"}
        self.assertNotEqual("fd420284714eda5af8bf03a1962dbb4a", s2.get_beforeState_hash())
        self.assertEqual("b2670d39b5440f4461effaaaf21b37e1", s2.get_beforeState_hash())

    @patch('db.mysql.ConflictManager.ConflictManager'
           '.get_relatedState_beforeValues')
    def test_set_statementBeforeHash_in_transaction_extendedState(self, relatedState):
        # we change the species name AND the vet_prescription record in the same transaction
        # but for the purposes of the conflict detection hash, the order of these changes
        # don't matter
        # we always find the initial state of the transaction.
        s1_before = {"id": 130, "vet_visit": 1, "description": "Weak antibiotics prescribed"}
        s1_after = {"id": 130, "vet_visit": 1, "description": "STRONG antibiotics prescribed"}
        s2_before = {"id": 2, "name": "Weasel", "description": "Red-haired predator"}
        s2_after = {"id": 2, "name": "Weasley", "description": "Red-haired predator"}
        s3_before = {"id": 130, "vet_visit": 1, "description": "STRONG antibiotics prescribed"}
        s3_after = {
            "id": 130,
            "vet_visit": 1,
            "description": "High-powered antibiotics prescribed"}

        s1 = Statement(s1_before, s1_after, StatementType.UPDATE, "vet_prescriptions")
        s2 = Statement(s2_before, s2_after, StatementType.UPDATE, "species")
        s3 = Statement(s3_before, s3_after, StatementType.UPDATE, "vet_prescriptions")

        transaction = Transaction()
        transaction.add_statement(s1)
        transaction.add_statement(s2)
        transaction.add_statement(s3)

        relatedState.side_effect = [[("2", "species", "name", "Weasel")], [], [(
            "2", "species", "name", "Weasel")], [("fake", "fake", "fake", "fake")]]
        self.assertEqual("", s1.get_beforeState_hash())
        self.assertEqual("", s2.get_beforeState_hash())
        self.assertEqual("", s3.get_beforeState_hash())

        self.conflict_manager.set_statementBeforeHash_in_transaction(transaction)

        # generator string = {"description": "Weak antibiotics prescribed", "id":
        # 130, "species.name": "Weasel", "vet_visit": 1}
        self.assertEqual("ee2144cd36b72c41fff9e34f090dbfef", s1.get_beforeState_hash())

        # generator string = {"description": "Red-haired predator", "id": 2, "name": "Weasel"}
        self.assertEqual("b78b64049de0944722aa455c4c0ea331", s2.get_beforeState_hash())

        # (non) generator string =  {"description": "Weak antibiotics prescribed",
        # "id": 130, "species.name": "Weasley", "vet_visit": 1}
        self.assertNotEqual("36dc59aa3845bfae7fd61ad233f01e60", s3.get_beforeState_hash())
        # (non) generator string = {"description": "STRONG antibiotics prescribed",
        # "id": 130, "species.name": "Weasley", "vet_visit": 1}
        self.assertNotEqual("15f8e9ae5097daf2096b5ca996b833df", s3.get_beforeState_hash())
        # (non) generator string = {"description": "STRONG antibiotics prescribed",
        # "id": 130, "species.name": "Weasel", "vet_visit": 1}
        self.assertNotEqual("3f1ea19efc523fb69c908e9995ba1d08", s3.get_beforeState_hash())

        self.assertEqual("ee2144cd36b72c41fff9e34f090dbfef", s3.get_beforeState_hash())

    def test_set_statementBeforeHash_in_transaction_multipleTransactions(self):
        """
        Expected to fail
        Multiple consecutive transactions need to have their previous states set together,
        or else the new database state is applied to each of them.
        We expect that we could receive multiple transactions at the same time, becasue the
        reader is run periodically; not immediately upon each transaction.
        So: When we try to construct the 'extended state' for any of these records, we will
        be pulling in values from the database, but we need to rollback the database to its
        state before a transaction to get correct values for the beforeHash.

        This is not implemented.
        """
        # TODO: implement
        pass


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner())

