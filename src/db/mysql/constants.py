__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g20@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u20@soton.ac.uk"]


from typing import List
from db.mysql.models.Statement import Statement, StatementType


def filter_tables(tables: List[str], listed_tables: List[str], filter_mode: str) -> List[str]:
    """
    Filter a list of tables based on a given filter mode.

    tables : `List[str]` - The list of tables to filter\n
    listed_tables : `List[str]` - The list of tables to filter against\n
    filter_mode : `str` - The mode to run the filtering in, either 'whitelist' or 'blacklist'

    Returns : `List[str]` - the tables that pass the filter, sorted alphabetically
    """
    filtered_tables = []
    for table_name in tables:
        if filter_mode == "blacklist":
            if table_name not in listed_tables:
                filtered_tables.append(table_name)
        elif filter_mode == "whitelist":
            if table_name in listed_tables:
                filtered_tables.append(table_name)
        else:
            raise RuntimeError(f"Unknown filter mode: {filter_mode}")

    return sorted(filtered_tables)


def to_sql_string(statement: Statement) -> str:
    """
    Convert the row-based representation of the statement to an SQL query for execution
    on the database.

    Returns : `str` - SQL string that represents to the input statement
    """
    table = statement.get_table()
    s_type = statement.get_type()
    if s_type == StatementType.INSERT:
        (keys_str, values_str) = convert_dict_to_sql_strs(statement.get_after())
        return f"INSERT INTO {table} {keys_str} VALUES {values_str};"
    elif s_type == StatementType.UPDATE:
        before_str = convert_dict_to_sql_update_delete_str(statement.get_before(),
                                                           conditions=True)
        after_str = convert_dict_to_sql_update_delete_str(statement.get_after())
        return f"UPDATE {table} SET {after_str} WHERE {before_str};"
    elif s_type == StatementType.DELETE:
        before_str = convert_dict_to_sql_update_delete_str(statement.get_before(),
                                                           conditions=True)
        return f"DELETE FROM {table} WHERE {before_str};"
    else:
        raise RuntimeError(f"Unknown StatementType {s_type}")


def convert_dict_to_sql_strs(input_dict: dict) -> (str, str):
    """
    Convert a dict of row based representation to a string of the keys and the values.

    input_dict : `dict` - The row values of the statement to generate

    Returns : `Tuple[str, str]` - A tuple of the keys and values of the row formatted for SQL
    """
    sorted_keys = []
    sorted_values = []
    for key, value in sorted(input_dict.items()):
        sorted_keys.append(key)
        if value is None:
            sorted_values.append("NULL")
        else:
            if isinstance(value, str):
                value = value.replace("'", "''")
            sorted_values.append("'%s'" % value)

    keys_str = f"({', '.join(sorted_keys)})"
    values_str = f"({', '.join(sorted_values)})"

    return (keys_str, values_str)


def convert_dict_to_sql_update_delete_str(input_dict: dict, conditions=False) -> str:
    """
    Convert a dict of row based representation to a string of the keys and the values,
    with the option for conditional joining which is needed in
    UPDATE and DELETE queries

    input_dict : `dict` - The row values of the statement to generate

    Returns : `str` - Formatted SQL string
    """
    strings = []
    for key, value in sorted(input_dict.items()):
        if value is None:
            if conditions:
                strings.append(f"{key} IS NULL")
            else:
                strings.append(f"{key} = NULL")
        else:
            strings.append(f"{key} = '{value}'")

    if conditions:
        output_string = " AND ".join(strings)
    else:
        output_string = ", ".join(strings)

    return output_string

