__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g16@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u16@soton.ac.uk"]

from typing import List, Set, Generator
from pymysqlreplication import BinLogStreamReader
from pymysqlreplication.event import QueryEvent, XidEvent, RotateEvent
from pymysqlreplication.row_event import BinLogEvent, RowsEvent, WriteRowsEvent,\
                                         UpdateRowsEvent, DeleteRowsEvent

import core.BluberryLogging
from core.BluberryConf import Conf
from core.BluberryMessage import Message
from core.IBluberryDBReader import IBluberryDBReader

from db.mysql.MySQLConnector import MySQLConnector
from db.mysql.models.Transaction import Transaction
from db.mysql.models.Statement import Statement, StatementType
from db.mysql.ConsentManager import ConsentManager
from db.mysql.ConflictManager import ConflictManager
from db.mysql.MySQLIDTranslationManager import MySQLIDTranslationManager
from db.mysql.MySQLConnector import TRANSACTION_LOG_TABLE_NAME,\
                                    STATEMENT_LOG_TABLE_NAME,\
                                    WRITER_STATUS_TABLE_NAME


class BluberryMySQLReader(IBluberryDBReader):
    """
    MySQL Reader class for the Bluberry system. This class generates a list of updates that
    have occured in the database and packages them in messages to be sent to other nodes.
    """

    def __init__(self, conf: Conf):
        """
        conf : `Conf` - The configuration for the node that this Reader is to run on
        """
        self.__logger = core.getLogger(self.__class__.__name__)
        self.__conf = conf
        self.__nodeId = conf.get('local', 'nodeId')
        self.__mysql = MySQLConnector(conf)
        self.__db_config = conf.get('db', 'connection')

        self.__stream_connection: BinLogStreamReader = None
        self.__furthest_log_position = -1
        self.__use_auto_log_position = True
        self.__log_file = None

        self.__ignored_tables = set()

        self.create_stream_connection()
        # TODO cache the log position and remove this line to avoid losing updates
        self.clear_event_log()
        self.close_stream_connection()

        self.__listed_tables = conf.get('db', 'sync', 'tables')
        self.__filter_mode = conf.get('db', 'sync', 'filtering')
        if self.__filter_mode == "blacklist":
            if TRANSACTION_LOG_TABLE_NAME not in self.__listed_tables:
                self.__listed_tables.append(TRANSACTION_LOG_TABLE_NAME)
            if STATEMENT_LOG_TABLE_NAME not in self.__listed_tables:
                self.__listed_tables.append(STATEMENT_LOG_TABLE_NAME)

        self.__conflict_manager = ConflictManager(self.__conf)
        self.__id_translation_manager = MySQLIDTranslationManager(self.__conf)
        self.__consent_manager = ConsentManager(self.__conf)

        self.__logger.info(f"{self.__nodeId}: MySQL Reader initialised successfully")

    def readUpdates(self) -> List[Message]:
        """
        Reads the updates from the database binary log and returns a message for each
        completed transaction

        Returns : `List[Message]`
        """
        self.__logger.debug(f"{self.__nodeId}: Reading new updates from MySQL binary log stream")
        current_transaction = None
        outgoing_messages = []
        self.create_stream_connection()
        for event in self.get_row_events():
            process_transaction = False

            # treat differently depending on the type
            if isinstance(event, QueryEvent) and event.query == "BEGIN":
                # "START TRANSACTION"
                self.__logger.debug('start transaction')
                current_transaction = Transaction()
            elif isinstance(event, RowsEvent):
                # "normal" row update
                if current_transaction is not None:
                    self.__logger.debug('ongoing transaction')
                    current_transaction = self.handle_rows_event(event, current_transaction)
                else:
                    self.__logger.debug('row update outside transaction')
                    current_transaction = self.handle_rows_event(event, Transaction())
                    process_transaction = True
            elif isinstance(event, XidEvent):
                # "COMMIT"
                self.__logger.debug('commit transaction')
                process_transaction = True
            elif isinstance(event, RotateEvent):
                # TODO update log file here
                pass
            else:
                # Unknown event - ignore
                pass

            # only use this transaction if the tables are configured for synchronisation
            if (process_transaction
                    and current_transaction is not None
                    and not current_transaction.isEmpty()):

                self.__logger.debug('checking transaction tables')
                ignore_transaction = False
                for t in current_transaction.get_tables():
                    if t in self.__ignored_tables:
                        ignore_transaction = True
                        self.__logger.debug('ignoring transaction')
                        currentTransaction = None
                        break

                # now process the transactions
                if not ignore_transaction:
                    self.__logger.debug('processing transaction')
                    current_transaction.set_timestamp(event.timestamp)
                    # Pass this transaction through the conflict handler to make sure it is
                    # correctly logged, and extra states are found
                    self.__conflict_manager.prepare_conflictHandling_transaction(
                        current_transaction)
                    # Get the permitted location for this transaction from the consent manager
                    permitted_locations = self.__consent_manager.filter_by_consent(
                        current_transaction)
                    # If there are permitted locations, add them to the transaction and
                    # package it up to be sent out
                    if len(permitted_locations) > 0:
                        current_transaction.set_locations(permitted_locations)
                        for location in permitted_locations:
                            # Don't send messages to ourselves
                            if location == self.__nodeId:
                                continue
                            message = self.create_database_update_message(
                                current_transaction, location)
                            outgoing_messages.append(message)
                            currentTransaction = None
                    else:
                        self.__logger.debug('No permitted locations found - ignoring update')

        self.close_stream_connection()
        self.__logger.info(f"{self.__nodeId}: Found {len(outgoing_messages)} new update(s) "
                           "to be sent")
        return outgoing_messages

    def hasUpdates(self) -> bool:
        result = self.get_row_events(false)
        return result is not None

    # #### MySQL connection methods #### #

    def create_stream_connection(self):
        """
        Open a stream connection to the MySQL database binary log using the database
        connection settings in the node's config file.
        """
        if (self.__stream_connection is None):
            self.__logger.debug(f"{self.__nodeId}: Opening MySQL stream connection")
            # If we are opening the stream for the first time, we let it join automatically
            if self.__use_auto_log_position:
                self.__use_auto_log_position = False
                if self.__db_config is not None:
                    self.__stream_connection = BinLogStreamReader(
                        connection_settings=self.__db_config, server_id=105)
            # Otherwise we can join from the last place we read so that we don't
            # double read updates
            else:
                self.__stream_connection = BinLogStreamReader(
                    connection_settings=self.__db_config,
                    server_id=105,
                    log_pos=self.__furthest_log_position,
                    resume_stream=True,
                    log_file=self.__log_file)
            self.__logger.debug(f"{self.__nodeId}: MySQL stream connection established")
        else:
            self.__logger.debug(f"{self.__nodeId}: MySQL stream connection already open")

    def close_stream_connection(self):
        """
        Close the connection to the MySQL binary log
        """
        self.__logger.debug(f"{self.__nodeId}: Closing MySQL stream connection")
        if self.__stream_connection is not None:
            self.__stream_connection.close()
            self.__stream_connection = None
            self.__logger.debug(f"{self.__nodeId}: MySQL stream connection closed")

    # #### Event methods #### #

    def get_row_events(self, consume_updates: bool = True) -> Generator[BinLogEvent, None, None]:
        """
        A generator that reads events from the binary log and yields them from the returned
        iterator.

        consume_updates : `bool` - Flag for whether the reader will set its log position when
                          reading updates

        Yields : `BinLogEvent`
        """
        if self.__stream_connection is not None:
            for bin_log_event in self.__stream_connection:
                event_log_position = bin_log_event.packet.log_pos

                if event_log_position > self.__furthest_log_position:
                    if consume_updates:
                        self.__furthest_log_position = event_log_position
                    yield bin_log_event
            # If we are not consuming these updates, we need to reset our log position
            # back to where it was before we read
            if not consume_updates:
                self.__stream_connection.log_pos = self.__furthest_log_position

            self.__log_file = self.__stream_connection.log_file
        else:
            self.__logger.warning("Could not get row events - stream connection not established.")

    def clear_event_log(self):
        """
        Helper method to clear the log of updates by iterating out the generator
        """
        self.__logger.debug(f"{self.__nodeId}: Clearing MySQL binary log event stream")
        for _ in self.get_row_events():
            pass

    def handle_rows_event(self, event: RowsEvent, transaction: Transaction) -> Transaction:
        """
        Takes in a row event and processes it according to the target table and the event type.
        Events on the writer status table are passed off, and updates to the transaction log
        and statment log are ignored. The events that are left are either ignored if they
        concern tables that the writer has flagged as having been synchronized, and all other
        events are handled by their respective handlers and the mutated transaction is returned.

        event : `RowsEvent` - Incoming event from the binary log\n
        transaction : `Transaction` - The current transaction object that this event is part of
        """
        self.__logger.debug(f"{self.__nodeId}: Handling RowEvent on table `{event.table}`")
        # If the event is on the writer status table, we need to update our ignored tables
        if event.table == WRITER_STATUS_TABLE_NAME:
            self.__ignored_tables = self.handle_writer_status_change(event)
            return transaction
        # If the event is on a table we are not configured to watch on, ignore it
        elif self.ignore_statement_on_table(event.table, self.__listed_tables,
                                            self.__filter_mode):
            self.__logger.debug(f"{self.__nodeId}: Ignoring event on table {event.table}"
                                "To include the table, add it to db-conf.json sync/tables.")
            return transaction

        # If the event is on a table that the writer status has told us to ignore, we ignore it
        if event.table in self.__ignored_tables:
            self.__logger.debug(
                f"{self.__nodeId}: Table in list to ignore from writer_status, ignoring")
            return transaction

        self.__logger.info(f"{self.__nodeId}: Handling {type(event)} on table `{event.table}`")
        if isinstance(event, WriteRowsEvent):
            return self.handle_write_rows_event(event, transaction)
        elif isinstance(event, UpdateRowsEvent):
            return self.handle_update_rows_event(event, transaction)
        elif isinstance(event, DeleteRowsEvent):
            return self.handle_delete_rows_event(event, transaction)

        self.__logger.warning(f"Unhandled row event {event}")
        return transaction

    def ignore_statement_on_table(
            self, table: str, listed_tables: List[str], filter_mode: str) -> bool:
        """
        Applies the filter mode to the table on the listed tables and returns if this table
        is to be read.

        table : `str`\n
        listed_tables : `List[str]`\n
        filter_mode : `str` - Currently 'whitelist' or 'blacklist'

        Returns : `bool`
        """
        return (filter_mode == "whitelist" and table not in listed_tables) or \
               (filter_mode == "blacklist" and table in listed_tables)

    def handle_writer_status_change(self, event: RowsEvent) -> Set[str]:
        """
        Handles the writes to the writer status log. These need to be seen by the reader so
        that the affected tables are ignored, but not added to
        the outgoing updates as they are not part of the synchronization.

        event : `RowsEvent` - the event on the writer status table

        Returns : `Set[str]` - The set of table names to ignore
        """
        # Ignored tables are stored as a CSV string in the table so we need to split into a Set
        ignored_tables_str = ""
        ignored_tables = set()

        if isinstance(event, WriteRowsEvent):
            ignored_tables_str = event.rows[0]["values"]["tables"]
        elif isinstance(event, UpdateRowsEvent):
            ignored_tables_str = event.rows[0]["after_values"]["tables"]

        if len(ignored_tables_str) > 0:
            ignored_tables = set(ignored_tables_str.split(","))

        self.__logger.debug(
            f"{self.__nodeId}: New writer_status ignored tables - {ignored_tables}")
        return ignored_tables

    def handle_write_rows_event(self, event: WriteRowsEvent,
                                transaction: Transaction) -> Transaction:
        """
        Handles an event that writes to a table that is not being ignored. The new row values
        are contained in a Statement object and the consent is also computed, before being
        added to the current transaction. This catches an INSERT query.

        event : `WriteRowsEvent`\n
        transaction : `Transaction` - The current transaction object that this event is part of

        Returns : `Transaction` - The updated transaction with this event added
        """
        self.__logger.info(f"{self.__nodeId}: Handling INSERT statement on table {event.table}")

        for row in event.rows:
            values = row["values"]
            # Translate the local values from the event into global values for
            # communication to other nodes
            (global_before_values, global_after_values) =\
                self.__id_translation_manager.map_localUpdate_to_networkValues(
                    None, values, StatementType.INSERT, event.table)
            # Generate the consent statement for this INSERT, making sure to use global values
            statement_consent = self.__consent_manager.get_statement_consent(
                event.table, values, global_after_values)
            statement = Statement(
                global_before_values,
                global_after_values,
                StatementType.INSERT,
                event.table,
                statement_consent)
            transaction.add_statement(statement)
        return transaction

    def handle_update_rows_event(self, event: UpdateRowsEvent,
                                 transaction: Transaction) -> Transaction:
        """
        Handles an event that updates a table that is not being ignored. The before and
        after row values are contained in a Statement object and the consent is also computed,
        before being added to the current transaction. This catches an UPDATE query.

        event : `UpdateRowsEvent`\n
        transaction : `Transaction` - The current transaction object that this event is part of

        Returns : `Transaction` - The updated transaction with this event added
        """
        self.__logger.info(f"{self.__nodeId}: Handling UPDATE statement on table {event.table}")

        for row in event.rows:
            before_values = row["before_values"]
            after_values = row["after_values"]
            # Translate the local values from the event into global values for
            # communication to other nodes
            (global_before_values, global_after_values) =\
                self.__id_translation_manager.map_localUpdate_to_networkValues(
                    before_values, after_values, StatementType.UPDATE, event.table)
            # Generate the consent statement for this UPDATE, making sure to use global values
            statement_consent = self.__consent_manager.get_statement_consent(
                event.table, after_values, global_after_values)
            statement = Statement(
                global_before_values,
                global_after_values,
                StatementType.UPDATE,
                event.table,
                statement_consent)
            transaction.add_statement(statement)
        return transaction

    def handle_delete_rows_event(self, event: DeleteRowsEvent,
                                 transaction: Transaction) -> Transaction:
        """
        Handles an event that deletes from a table that is not being ignored. The old row values
        are contained in a Statement object and the consent is also computed, before being
        added to the current transaction. This catches a DELETE query.

        event : `DeleteRowsEvent`\n
        transaction : `Transaction` - The current transaction object that this event is part of

        Returns : `Transaction` - The updated transaction with this event added
        """
        self.__logger.info(f"{self.__nodeId}: Handling DELETE statement on table {event.table}")

        for row in event.rows:
            values = row["values"]
            # Translate the local values from the event into global values for
            # communication to other nodes
            (global_before_values, global_after_values) =\
                self.__id_translation_manager.map_localUpdate_to_networkValues(
                    values, None, StatementType.DELETE, event.table)
            # Generate the consent statement for this DELETE, making sure to use global values
            statement_consent = self.__consent_manager.get_statement_consent(
                event.table, values, global_before_values)
            statement = Statement(
                global_before_values,
                global_after_values,
                StatementType.DELETE,
                event.table,
                statement_consent)
            transaction.add_statement(statement)
        return transaction

    def create_database_update_message(
            self, transaction: Transaction, destination: str) -> Message:
        """
        Converts a Transaction object, which holds the list of statements for the new local
        updates read by the reader, into a Message that can be sent out to the rest of the
        system.

        transaction : `Transaction` - The transaction object to send in the message\n
        destination : `str` - The nodeId of the node to send this message to

        Returns : `Message` - The Bluberry Message to send
        """
        message = Message()
        message.setOriginNode(self.__nodeId)
        message.setDestination(destination)
        message.setPayload(str(transaction))
        return message

