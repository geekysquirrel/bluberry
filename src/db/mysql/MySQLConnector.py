"""
A lower-level tool to manage a connection to a MySQL database.
"""

__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import mysql.connector
import os
from typing import List, Tuple
from pathlib import Path

import core.BluberryLogging
from core.BluberryConf import Conf

# tables in bluberry's own db
# TODO: this should be the same as in the init script - perhaps get it from config?
BLUBERRY_DB_NAME = 'bluberry'
TRANSACTION_LOG_TABLE_NAME = "transaction_log"
STATEMENT_LOG_TABLE_NAME = "statement_log"
ID_MAPPING_TABLE_NAME = "network_id_mapping"
LOCAL_COUNT_TABLE_NAME = "local_insert_count"
# table in the main db
WRITER_STATUS_TABLE_NAME = "bluberry_writer_status"


class MySQLConnector():

    def __init__(self, conf: Conf, schema: str = ''):
        """
        Connect to a database as configured in the db-config.json

        :param conf: A configuration object, containing a db/connection section
        :param schema: A default schema for this connector, overriding anything that's
                       defined in the conf object
        """
        self.__logger = core.getLogger(self.__class__.__name__)
        self.__conf = conf
        # use the default schema if none (not None!) was passed
        if schema == '':
            self.__defaultSchema = self.__conf.get('db', 'connection', 'database')
        else:
            self.__defaultSchema = schema

        self.__connection = None
        self.__cursor = None

        self.__logger.debug(f"Initialised MySQL database; default schema: {self.__defaultSchema}")

    def connect(self, schema: str = ''):
        """
        Connect to a database as configured in the db-config.json
        """
        # use a schema for this connection only, overriding the one defined in the constructor
        if schema == '':
            db = self.__defaultSchema
        else:
            db = schema
        self.__logger.debug(f"Connecting to MySQL database; schema: {db}")

        try:
            self.__connection = mysql.connector.connect(
                # TODO: figure out why the IP doesn't work but the alias does
                # host=conf.get('nodes', conf.get('local', 'nodeId'), 'msg-host-int'),
                host=self.__conf.get('db', 'connection', 'host'),
                # TODO: figure out how to give the bluberry user sufficient permissions
                # across all schemas
                user=self.__conf.get('db', 'connection', 'user'),
                password=self.__conf.get('db', 'connection', 'password'),
                # this is optional; passing in None will still connect but not select a db
                database=db,
                # TODO: figure out why this doesn't work although it should be the same
                # port=conf.get('nodes', conf.get('local', 'nodeId'), 'msg-port-int')
                port=self.__conf.get('db', 'connection', 'port')
            )
            self.__cursor = self.__connection.cursor(buffered=True)
            self.__logger.debug(f"Connection established")
        except Exception as e:
            self.__logger.error(f"Could not connect to MySQL database:\n{e}")
            self.disconnect()
            raise(RuntimeError(f"Could not connect to MySQL database:\n{e}")) from e

    def getDefaultSchema(self) -> str:
        return self.__defaultSchema

    def getCurrentSchema(self) -> str:
        """
        Get the currently selected database.

        :returns: The name of the database or None if not connected to one.
        """
        if self.isConnected():
            result = self.execute('SELECT DATABASE();')
            if result and result[0] and result[0][0]:
                return result[0][0]
        return None

    def execute(self, sql: str, connect: bool = True) -> List[Tuple]:
        """
        Execute a SQL query against the currently configured database.

        :param sql: A single SQL query
        :param connect: Whether to establish a connection for this query or not
                        (=using an existing one)
        :returns: The result of the query or None if there wasn't a result
        """
        # TODO: make this secure, see https://bobby-tables.com/python
        closeConnection = False
        if not self.isConnected():
            if connect:
                self.connect()
                closeConnection = True
            else:
                raise(RuntimeError("Could not execute sql; not connected to database."))
        else:
            # silently use existing connection
            pass

        try:
            self.__logger.debug(f"Executing sql:\n{sql}")
            self.__cursor.execute(sql)
            self.__logger.debug('Done.')
        except mysql.connector.errors.ProgrammingError as e:
            self.__logger.error(f"Could not execute query:\n{sql}")
            self.__logger.error(e)

        result = None
        try:
            result = self.__cursor.fetchall()
            self.__logger.debug(f"Fetched {len(result)} "
                                f"row{('s' if len(result)>1 or len(result)==0 else '')}")
        except mysql.connector.errors.InterfaceError:
            # If it doesn't have a result, it's most likely an update/indert/delete
            # so if we want to propagate the result into the database, we need to commit
            # However, we only do this if the query is stand-alone, i.e. the database
            # was not connected prior to the query.
            if closeConnection:
                self.commit()
        finally:
            if closeConnection:
                self.disconnect()

        return result

    def commit(self):
        """
        Commits the current (possibly implicit) transaction.
        """
        if not self.isConnected():
            raise(RuntimeError("Could not commit; not connected to database."))
        self.__connection.commit()

    def rollback(self):
        """
        Rolls back the current (possibly implicit) transaction.
        """
        if not self.isConnected():
            raise(RuntimeError("Could not rollback; not connected to database."))
        self.__connection.rollback()

    def runScript(self, scriptFile: str):
        """
        Runs a script on the connected database.

        :param scriptFile: The file to run, its location being relative to the main bluberry
                           directory, e.g. "db/mysql/conf/some-script.sql"
        """

        # convert relative path to absolute path
        if not scriptFile.startswith('/'):
            path = Path(f"{self.__conf.get('local', 'basedir')}{os.sep}{scriptFile}")
        else:
            path = Path(scriptFile)

        self.__logger.info(f"Running script {path}")
        wasConnected = self.isConnected()
        if not wasConnected:
            self.connect()
        with open(path) as f:
            try:
                for result in self.__cursor.execute(f.read(), multi=True):
                    self.__logger.debug(result)
                    pass
            except mysql.connector.errors.ProgrammingError as e:
                self.__logger.error(e)
                raise(RuntimeError("Syntax error in SQL script")) from e
            except Exception as e:
                self.__logger.error(e)
                raise(RuntimeError("Could not execute script")) from e
            finally:
                self.commit()
        f.close()
        if not wasConnected:
            self.disconnect()

    def resetBluberryDatabase(self, tables: List = [TRANSACTION_LOG_TABLE_NAME,
                                                    STATEMENT_LOG_TABLE_NAME,
                                                    ID_MAPPING_TABLE_NAME,
                                                    LOCAL_COUNT_TABLE_NAME]):
        """
        Resets all tables in the Bluberry database.
        This means not to truncate (i.e. drop and create again) so that
        the auto_increment values are reset too.

        :param tables: all tables in the bluberry database to be reset.
                       resets all tables if not given.
        """
        self.connect(f"{BLUBERRY_DB_NAME}")
        self.execute('SET FOREIGN_KEY_CHECKS = 0;')
        for table in tables:
            self.execute(f"TRUNCATE TABLE {BLUBERRY_DB_NAME}.{table};")
        self.execute('SET FOREIGN_KEY_CHECKS = 1;')
        self.commit()
        self.disconnect()

    def disconnect(self):
        """
        Disconnects from the currently connected database.
        """
        if self.isConnected():
            self.__logger.debug("Disconnecting from MySQL database")
            self.__connection.close()
            self.__cursor = None
            self.__connection = None
        else:
            self.__logger.debug("Already disconnected")

    def isConnected(self) -> bool:
        """
        Checks whether the connector is currently connected to the DBMS.
        """
        return self.__connection is not None

