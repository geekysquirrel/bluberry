__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = ["jde1g16@soton.ac.uk", "ms6n15@soton.ac.uk", "mfw1g15@soton.ac.uk",
             "ei2u16@soton.ac.uk"]

import os
import json
from pathlib import Path
from typing import List, Set, Tuple

import core.BluberryLogging
from core.BluberryConf import Conf
from db.mysql.MySQLConnector import MySQLConnector
from db.mysql.models.Transaction import Transaction
from db.mysql.models.TableConsentRequirement import TableConsentRequirement
from db.mysql.models.Statement import StatementConsentRequirement
from db.mysql.MySQLIDTranslationManager import MySQLIDTranslationManager


class ConsentManager():
    """
    Class that contains all the logic for handling the consent system. The main public API
    methods are get_statement_consent, which computes the consent rules for a given database
    statement, and filter_by_consent which computes which nodes a transcation can be sent to.
    Ultimately, this entire consent system can be restructured to a seperate module in the
    Bluberry system.
    """

    def __init__(self, conf: Conf):
        """
        confg : `Conf` - The configuration object for this node\n
        """
        self.__logger = core.getLogger(self.__class__.__name__)
        self.__conf = conf
        self.__nodeId = conf.get('local', 'nodeId')
        self.__mysql = MySQLConnector(conf)

        self.__id_translation_manager = MySQLIDTranslationManager(self.__conf)

        consent_config_file_path = Path(f"{self.__conf.get('local', 'basedir')}{os.sep}"
                                        f"{self.__conf.get('db', 'consent', 'config_path')}")
        if not consent_config_file_path.exists():
            raise RuntimeError(f"No such configuration file {consent_config_file_path}")

        with open(consent_config_file_path, "r") as config_file:
            consent_config = json.load(config_file)
        config_file.close()

        consent_records = Path(f"{self.__conf.get('local', 'basedir')}{os.sep}"
                               f"{self.__conf.get('db', 'consent', 'record_path')}")
        if not consent_records.is_dir():
            raise RuntimeError(f"No such consent records folder {consent_records}")

        self.__locationMappings = consent_config["locationMapping"]

        map_tables_to_consentRequirements = {}  # one_to_many
        map_consentTags_to_consentRequirements = {}  # one_to_many
        map_consentOwnerTypes_to_consentTags = {}  # one_to_many
        map_consentTags_to_consentOwnerType = {}  # one_to_one

        consent_tags = consent_config["dataTypes"]
        tag_mappings = consent_config["dataTypeMapping"]
        consent_owner_types = consent_config["dataTypeOwnership"]
        for tag_name in consent_tags:
            for table_name, consent_info in tag_mappings[tag_name].items():
                list_path_to_consenting_individuals =\
                    consent_info["list_path_to_consenting_individuals"]
                consentRequirement: TableConsentRequirement = {
                    "tag_name": tag_name,
                    "table": table_name,
                    "sensitive_fields": consent_info["sensitive_fields"],
                    "list_path_to_consenting_individuals": list_path_to_consenting_individuals}

                self.map_append(
                    map_tables_to_consentRequirements, table_name, consentRequirement)

                self.map_append(
                    map_consentTags_to_consentRequirements, tag_name, consentRequirement)

            consent_owner_type = consent_owner_types[tag_name]
            map_consentTags_to_consentOwnerType[tag_name] = consent_owner_type
            self.map_append(map_consentOwnerTypes_to_consentTags, consent_owner_type, tag_name)

        self.__tables_to_consentRequirements_map = map_tables_to_consentRequirements
        self.__map_consentTags_to_consentRequirements = map_consentTags_to_consentRequirements
        self.__map_consentOwnerTypes_to_consentTags = map_consentOwnerTypes_to_consentTags
        self.__consentTags_to_ownerType_map = map_consentTags_to_consentOwnerType

        self.__consent_records = self.__read_consent_files(consent_records)
        self.__logger.info(f"{self.__nodeId}: Consent Manager initialised successfully")

    def __read_consent_files(self, file_path: Path) -> dict:
        """
        Reads the individual consent records into the manager from the json files.

        file_path : `Path` - Path to the folder holding the consent records

        Returns : `dict` - The consent records loaded into a dictionary, keyed by type
                  and individual ID
        """
        self.__logger.debug(f"{self.__nodeId}: Reading consent records from {file_path}")

        if file_path.exists() is False:
            raise RuntimeError(f"No such consent records folder {file_path}")
        consent_records = {}
        # For each type e.g ANIMAL
        for path in file_path.iterdir():
            consent_owner_type = path.name
            consent_records[consent_owner_type] = {}
            consent_owner_types_path = file_path.joinpath(consent_owner_type)
            # For each individial record
            for owner in consent_owner_types_path.iterdir():
                consent_owner_path = consent_owner_types_path.joinpath(owner)
                with open(str(owner.resolve())) as consent_record:
                    consent_json = json.load(consent_record)
                    individual_id = consent_json["id"]
                    consent_records[consent_owner_type][individual_id] = consent_json
                consent_record.close()
        return consent_records

    def get_individual_permitted_locations(self, individual: any,
            consent_owner_type: any, consent_tag: str) -> List[str]:
        """
        individual : `any` - The individual ID to search consent records for\n
        consent_owner_type : `any` - The type of individual that this ID relates to\n
        consent_tag : `str` - The consent tag relating to the record that we wish to find
                      consent for

        Returns : `List[str]` - The list of nodes that an individual has consented to send
                  this tag
        """
        if individual not in self.__consent_records[consent_owner_type]:
            return set()  # by default

        individual_consent_record = self.__consent_records[consent_owner_type][individual]
        data_choice: List[str] = individual_consent_record["location"][consent_tag]

        for choice in data_choice:
            if choice in self.__locationMappings:
                data_choice.remove(choice)
                data_choice.extend(self.__locationMappings[choice])
        return data_choice

    def get_permitted_locations(self, consent_requirement: StatementConsentRequirement)\
                                -> Set[str]:
        """
        Returns a set of the locations permited for a given statment consent requirement
        to be sent to. By default, an empty set is returned to ensure privacy by default.

        consent_requirement : `StatementConsentRequirement` -

        Returns : `Set[str]`
        """
        consenting_individuals = consent_requirement.get_consenting_individuals()
        consent_owner_type = consent_requirement.get_consent_owner_type()
        data_choices = []
        for individual in consenting_individuals:
            data_choices.append(self.get_individual_permitted_locations(
                individual, consent_owner_type, consent_requirement.get_consent_tag()))
        if len(data_choices) == 0:
            return set()
        else:
            # return intersection of all lists in data_choices (permitted_locations for all
            # individuals)
            return set(data_choices[0]).intersection(*data_choices)

    def get_statement_consent(self, table: str, local_values: dict, global_values: dict)\
                              -> List[StatementConsentRequirement]:
        """
        Computes the list of statement consent requirements for a given row update on a table.

        table : `str`\n
        local_values : `dict`\n
        global_values : `dict`

        Returns : `List[StatementConsentRequirement]`
        """
        table_name = table.replace("`", "")  # remove sql escape strings

        if table_name not in self.__tables_to_consentRequirements_map:
            self.__logger.info(f"{self.__nodeId}: Table {table} has no consent requirements")
            return []

        table_consent_reqs = self.__tables_to_consentRequirements_map[table_name]

        statement_consent_reqs = []
        for requirement in table_consent_reqs:
            consent_tag = requirement["tag_name"]
            list_path_to_consenting_individuals =\
                requirement["list_path_to_consenting_individuals"]
            global_consenting_individuals = []
            # We need to explore each path since a table may have multiple links to other tables
            for path_to_consenting_individual in list_path_to_consenting_individuals:
                consent_owner_type = self.__consentTags_to_ownerType_map[consent_tag]
                table_name, local_consenting_individual_id =\
                    self.follow_consenting_individual_path(
                        path_to_consenting_individual, local_values)
                # map consenting_individual from local namespace to global namespace
                global_consenting_individual_id =\
                    self.__id_translation_manager.map_localValue_to_globalValue(
                        table_name, local_consenting_individual_id)
                global_consenting_individuals.append(global_consenting_individual_id)
            consent_req = StatementConsentRequirement(
                consent_tag, global_consenting_individuals, consent_owner_type)
            statement_consent_reqs.append(consent_req)

        return statement_consent_reqs

    def map_append(self, dictionary: dict, key: str, value):
        """
        For key, if doesn't yet exist: create list then add val, otherwise just add val.

        dictionary : `dict`\n
        key : `str`\n
        value : `Any`
        """
        if key not in dictionary:
            dictionary[key] = []
        dictionary[key].append(value)

    def follow_consenting_individual_path(self, path_to_consenting_individual: List[dict],
                                          record_values: dict) -> Tuple[str, any]:
        """
        Traverses the consent path to compute the end individual involved in the given
        consent scenario.

        For the first record, we have record_values passed in.
        for all subsequent records we have to fetch the new 'record_values' from sql.
        we will only fetch the single record that we do need, from the next mapping.
        if "mapped_table" is false,
        this can only happen in the first and only element in the path
        then we simply grab the id field from record_values.

        IF "mapped_tables" is True, but the length is only one,
        then once again we don't need to read from SQL, and we can simply
        grab the value of the 'source_column_name', and this is the same value as
        the eventual target - so we don't have to grab anything from the DB.

        If the length is greater than one, "mapped_tables" must be True always,
        and we have to access the DB to find the new record values.
        this is looped logic, with a special case for the final element of the list.

        path_to_consenting_individual : `List[dict]`\n
        record_values : `dict`

        Returns : `Tuple[str, any]`
        """
        path_length = len(path_to_consenting_individual)
        if path_length == 1:
            path_step = path_to_consenting_individual[0]
            if path_step["mapped_table"]:
                # then the single key needed to this path is stored in source_column_name
                return (path_step["mapped_table_name"],
                        record_values[path_step["source_column_name"]])
            else:
                # the single key needed for this path is stored in "source_column_name"
                return (path_step["source_table_name"],
                        record_values[path_step["source_column_name"]])
                # return record_values[path_to_consenting_individual[0]["source_column_name"]]

        else:
            for i in range(path_length):
                path_step = path_to_consenting_individual[i]

                if i == 0:
                    # we already have record_values, so don't need to fetch from the DB
                    source_table_name = path_step["source_table_name"]
                    fk_column_name = path_step["source_column_name"]
                    # the mapped_table_name value is unused, but required in the config
                    # for clarity
                    mapped_column_name = path_step["mapped_column_name"]
                    mapped_value = record_values[fk_column_name]
                else:
                    source_table_name = path_step["source_table_name"]
                    fk_column_name = path_step["source_column_name"]

                    query = f"SELECT {fk_column_name} FROM `{source_table_name}` "\
                            f"WHERE {mapped_column_name} = {mapped_value};"
                    results = self.__mysql.execute(query)

                    if results and results[0] and results[0][0]:
                        mapped_value = results[0][0]
                    else:
                        mappedValue = None
                    # and NOW, update the mapped_column_name - to be used in the next loop
                    # if required
                    mapped_table_name = path_step["mapped_table_name"]
                    mapped_column_name = path_step["mapped_column_name"]
            return (mapped_table_name, mapped_value)
    # FIXME invert the logic to force global to be specified

    def filter_by_consent(self, transaction: Transaction) -> List[str]:
        """
        Takes a transaction and computes the locations to which it is consented to be sent.
        If there are no consent requirements for a statement, global is returned.

        transaction : `Transaction`

        Returns : `List[str]`
        """
        consent_failures = []
        permitted_locations = []
        statements = transaction.get_statements()
        for statement in statements:
            for consent_req in statement.get_consent_reqs():
                permitted = self.get_permitted_locations(consent_req)
                if len(permitted) == 0:
                    consent_failures.append(consent_req)
                self.__logger.info(
                    f"{self.__nodeId}: Statement concerning individual(s)"
                    f" {consent_req.get_consenting_individuals()} permitted "
                    f"to {list(permitted)}")
                permitted_locations.append(permitted)

        locations = []
        if len(permitted_locations) == 0 and len(consent_failures) == 0:
            locations = self.__locationMappings["global"]
        else:
            intersection = set.intersection(*permitted_locations)
            locations = list(intersection)

        self.__logger.info(f"{self.__nodeId}: Under consent constraints, "
                           f"transaction is permitted to {locations}")
        return locations

