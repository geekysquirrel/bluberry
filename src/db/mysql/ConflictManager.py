__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g16@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u16@soton.ac.uk"]

import json
import hashlib
import copy
import os

from time import time
from typing import List, Tuple
from datetime import datetime
from pathlib import Path
from enum import IntEnum

import core.BluberryLogging
from core.BluberryConf import Conf
from db.mysql.MySQLConnector import MySQLConnector
from db.mysql.models.Transaction import Transaction
from db.mysql.models.Statement import Statement, StatementType, ConflictingStatement
from db.mysql.constants import filter_tables
from db.mysql.MySQLConnector import BLUBERRY_DB_NAME,\
                                    TRANSACTION_LOG_TABLE_NAME,\
                                    STATEMENT_LOG_TABLE_NAME,\
                                    WRITER_STATUS_TABLE_NAME
from db.mysql.strategies.Escalate import Escalate
from db.mysql.strategies.LastWins import LastWins
from db.mysql.strategies.FirstWins import FirstWins
from db.mysql.strategies.ABluberryConflictStrategy import ABluberryConflictStrategy,\
                                                          ConflictResolutionType


def converter(o: any) -> str:
    """
    Used to serialise objects that do not natively support being JSON stringified

    o : `any`

    Returns : `str`
    """
    return o.__str__()


def choose_strategy(strategy_name: str) -> ABluberryConflictStrategy:
    if strategy_name == "escalate":
        return Escalate()
    elif strategy_name == "lastWins":
        return LastWins()
    elif strategy_name == "firstWins":
        return FirstWins()
    raise RuntimeError(f"Unsupported Strategy {strategy_name}")


class TransactionConflictResult(IntEnum):
    TRIVIAL_ACCEPT = 1
    MODIFIED_ACCEPT = 2
    REJECT = 3
    ESCALATE = 4


class ConflictManager():

    def __init__(self, conf: Conf):
        """
        conf : `Conf` - The configuration for the node that this Reader is to run on
        """
        self.__logger = core.getLogger(self.__class__.__name__)
        self.__conf = conf
        self.__db_name = self.__conf.get('db', 'connection', 'database')
        self.__nodeId = self.__conf.get('local', 'nodeId')
        self.__mysql = MySQLConnector(conf)
        self.__conflict_config = self.read_conflict_config_file()

        rows = self.__mysql.execute(f"SHOW TABLES FROM {self.__db_name}")
        table_names = []
        for table_name, *_ in rows:
            table_names.append(table_name)

        if WRITER_STATUS_TABLE_NAME in table_names:
            table_names.remove(WRITER_STATUS_TABLE_NAME)

        self.__all_tables = filter_tables(
            table_names,
            self.__conf.get('db', 'sync', 'tables'),
            self.__conf.get('db', 'sync', 'filtering'))

        self.__table_to_conflictResolutionStrategy_map =\
            self.create_map_table_to_conflictResolutionStrategy()

        self.__map_table_to_columns = self.create_map_table_to_columns()

    def read_conflict_config_file(self) -> dict:
        """
        Reads the conflict configuration json file.

        Returns : `dict` - A dictionary with the conflict configuration loaded
        """
        self.__logger.debug(f"{self.__nodeId}: Loading conflict configuration into "
                            "ConflictManager")
        conflict_config_file_path = Path(f"{self.__conf.get('local', 'basedir')}{os.sep}"
                                         f"{self.__conf.get('db', 'conflict', 'config_path')}")

        if conflict_config_file_path.exists() is False:
            raise RuntimeError(f"No such configuration file {conflict_config_file_path}")

        with open(str(conflict_config_file_path.resolve()), "r") as conflict_config_file:
            conflict_config = json.load(conflict_config_file)
        conflict_config_file.close()
        return conflict_config

    def create_map_table_to_conflictResolutionStrategy(self) -> dict:
        """
        Creates a mapping from table names to the conflict resolution strategies to use.

        Returns : `dict`
        """
        table_to_conflictResolutionStrategy_map = {}
        for table in self.__all_tables:
            table_to_conflictResolutionStrategy_map[table] = {
                "strategy": (self.__conflict_config["tableConflictSetup"][table]
                            ["conflictStrategy"]),
                "list_paths_to_related_values": (self.__conflict_config["tableConflictSetup"]
                                                [table]["list_paths_to_related_values"]),
                "timewindows": None}
        return table_to_conflictResolutionStrategy_map

    def create_map_table_to_columns(self) -> dict:
        """
        Create a map of each table to all of its columns.

        Returns : `dict`
        """
        table_to_columns_map = {}
        for table in self.__all_tables:
            query = f"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.columns "\
                    f"WHERE TABLE_SCHEMA = '{self.__db_name}' AND "\
                    f"TABLE_NAME = '{table}';"
            columns = self.__mysql.execute(query)
            table_to_columns_map[table] = [x[0] for x in columns]
        return table_to_columns_map

    def log_transaction(self, transaction: Transaction, current: bool = True,
                        created_by_conflict_resolution: bool = False):
        """
        Write the current transaction to the transaction_log, and write all of the
        statements to the statement_log.

        transaction : `Transaction`\n
        current : `bool` - If the statements in this transaction represent the current
                  state of the database
        created_by_conflict_handling : `bool` - If this transaction was created by conflict
                                       handling, not a node.
        """

        if created_by_conflict_resolution:
            created_by_conflict_resolution_val = 1
        else:
            created_by_conflict_resolution_val = 0

        current_timestamp_str = self.convert_unix_to_sql_timestamp(time())
        origin_timestamp_str = self.convert_unix_to_sql_timestamp(transaction.get_timestamp())

        tables_string = ",".join(sorted(transaction.get_tables()))

        query = f"INSERT INTO `{BLUBERRY_DB_NAME}`.`{TRANSACTION_LOG_TABLE_NAME}` "\
                f"(id, origin_node, origin_timestamp, received_timestamp, tables,"\
                f" created_by_conflict_resolution) VALUES "\
                f"('{transaction.get_id()}', '{self.__nodeId}', '{origin_timestamp_str}', "\
                f"'{current_timestamp_str}', '{tables_string}',"\
                f" '{created_by_conflict_resolution_val}');"
        self.__mysql.execute(query)

        transaction_id = transaction.get_id()
        for i, statement in enumerate(transaction.get_statements()):
            self.log_statement(
                statement,
                transaction_id,
                i,
                current,
                created_by_conflict_resolution)

    def log_statement(self, statement: Statement, transaction_id: str, transaction_order: int,
                      current: bool = True, created_by_conflict_resolution: bool = False):
        """
        Write a statement into the statement_log;
        Enables conflict detection.

        statement : `Statement`\n
        transaction_id : `str` - The UUID of the transaction that is used as the foreign key
                         in the statement log\n
        transaction_order : `int` - The position in the transaction of this statement\n
        current : `bool` - If statement represents the current state of the database
        created_by_conflict_handling : `bool` - If this transaction was created by conflict
                                       handling, not a node.
        """
        if current:
            current_val = 1
        else:
            current_val = 0

        if created_by_conflict_resolution:
            created_by_conflict_resolution_val = 1
        else:
            created_by_conflict_resolution_val = 0

        table = statement.get_table()
        before_values = statement.get_before()
        after_values = statement.get_after()
        before_str = json.dumps(before_values, default=converter)
        after_str = json.dumps(after_values, default=converter)
        related_afterStr = statement.get_related_afterValues_str()
        extendedState_afterHash = statement.get_afterState_hash()

        record_id = self.find_statement_record_id(statement)

        self.__mysql.connect()
        # We need to set any other statements that relate to this record to not be
        # current, if this is current; otherwise assume state is ok
        if current:
            query = f"UPDATE `{BLUBERRY_DB_NAME}`.`{STATEMENT_LOG_TABLE_NAME}` "\
                    f"SET current = 0 WHERE record_id = '{record_id}' AND current=1;"
            self.__mysql.execute(query)
            query = f"DELETE FROM `{BLUBERRY_DB_NAME}`.`{STATEMENT_LOG_TABLE_NAME}` "\
                    f"WHERE record_id = '{record_id}' AND created_by_conflict_resolution=1;"
            self.__mysql.execute(query)

        query = f"INSERT INTO `{BLUBERRY_DB_NAME}`.`{STATEMENT_LOG_TABLE_NAME}` "\
                f"(transaction_id, table_name, record_id, transaction_order, before_values, "\
                f"after_values, related_after_values, extended_state_after_hash, current, "\
                f"created_by_conflict_resolution) VALUES ('{transaction_id}', '{table}', "\
                f"'{record_id}', '{transaction_order}', "\
                f"'{before_str}', '{after_str}', '{related_afterStr}', "\
                f"'{extendedState_afterHash}', {current_val}, "\
                f"{created_by_conflict_resolution_val});"
        self.__mysql.execute(query)
        self.__mysql.commit()
        self.__mysql.disconnect()

    def _get_statement_hash(self, extendedState: str) -> str:
        """
        Get a repeatable hash of the entire state of a record

        extendedState : `str` - The stringified dict of all values and extended state values
                        for a statement.

        Returns : `str` - the MD5 digest of the input
        """
        hashDigest = hashlib.md5(extendedState.encode("utf-8")).hexdigest()
        self.__logger.debug(f"{self.__nodeId}: For {extendedState}, am returning a digest: "
                            f"{hashDigest}")
        return hashDigest

    def _get_statement_afterHash(self, extendedState_afterStr: str) -> str:
        """
        Get a repeatable hash of the extended after state of a record

        extendedState_afterStr : `str` - The stringified dict of all values and extended state
                                 values for a statement

        Returns : `str` - the MD5 digest of the input
        """
        self.__logger.debug(f"{self.__nodeId}: Creating after hash from input "
                            f"{extendedState_afterStr}")
        return self._get_statement_hash(extendedState_afterStr)

    def _get_statement_beforeHash(self, extendedState_beforeStr: str) -> str:
        """
        Get a repeatable hash of the extended before state of a record

        extendedState_beforeStr : `str` - The stringified dict of all values and extended
                                  state values for a statement.

        Returns : `str` - the MD5 digest of the input
        """
        self.__logger.debug(f"{self.__nodeId}: Creating before hash from input "
                            f"{extendedState_beforeStr}")
        return self._get_statement_hash(extendedState_beforeStr)

    def get_primary_keys_for_table(self, table: str) -> List[str]:
        """
        Find the primary key column name(s) for a table.

        table : `str`

        Returns : `List[str]` - All the column names that are primary keys in the given table,
                  sorted alphabetically
        """
        query = f"SELECT DISTINCT COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE "\
                f"WHERE TABLE_NAME = '{table}' AND CONSTRAINT_NAME = 'PRIMARY';"
        result = self.__mysql.execute(query)

        if (len(result) == 0):
            self.__logger.warning(f"Table: {table} has NO primary keys. Updates on this table "
                                  "are unsupported.")
        elif (len(result) > 1):
            self.__logger.warning(f"Table: {table} has MULTIPLE primary keys. updates on this "
                                  "table are unsupported.")
        return sorted(map(lambda x: x[0], result))

    def find_statement_record_id(self, statement: Statement) -> str:
        """
        Given a statement, return the record_id - a unique identifier for this record,
        which allows you to link together multiple different changes to the same record.

        statement : `Statement`

        Returns : `str` - The record id of the row that the statement relates to
        """
        table = statement.get_table()
        primary_keys = self.get_primary_keys_for_table(table)
        statement_type = statement.get_type()
        if statement_type == StatementType.INSERT:
            record_values = statement.get_after()
        elif statement_type == StatementType.UPDATE:
            record_values = statement.get_before()
        elif statement_type == StatementType.DELETE:
            record_values = statement.get_before()

        pk_values_list = ([str(record_values[key]) for key in primary_keys])
        return self._generate_record_id(pk_values_list, table)

    def _generate_record_id(self, pk_values_list: List[str], table_name: str) -> str:
        """
        Generates a MD5 hash of the primary key values for a row in the given table which can
        be used to uniquely identify a record.

        pk_values_list : `List[str]` - The primary key values of the record\n
        table_name : `str`

        Returns : `str` - A record ID, MD5 hashed
        """
        key_str = ",".join(pk_values_list)
        string_to_encode = f"{key_str},{table_name}"
        record_id = hashlib.md5(string_to_encode.encode("utf-8")).hexdigest()

        return record_id

    def convert_unix_to_sql_timestamp(self, unix_ts: int) -> str:
        """
        unix_ts : int - The UNX timestamp to convert

        Returns : `str` - A formatted timestamp string for use in SQL
        """
        return datetime.utcfromtimestamp(unix_ts).strftime("%Y-%m-%d %H:%M:%S.%f")

    def convert_sql_to_unix_timestamp(self, sql_datetime: datetime) -> int:
        """
        sql_datetime : `datetime` - Python datetime object to convert

        Returns : `int` - UNIX timestamp for use in the application
        """
        return int(sql_datetime.timestamp())

    def check_for_conflicts(
            self, transaction: Transaction) ->\
                List[Tuple[Statement, List[ConflictingStatement]]]:
        """
        For each statement in a transaction, identify whether it conflicts with any existing
        statements, or not, and return them

        transaction : `Transaction`

        Return : `List[Tuple[Statement, List[ConflictingStatement]]]` - A list of
                 Conflicts, each containing the statement, and all previously
                 existing Conflicting Statements.
        """
        conflicts = []
        has_conflicts = False
        for statement in transaction.get_statements():
            conflicting_statements = self.find_conflicting_statements(statement,
                                                                      transaction.get_timestamp())
            if len(conflicting_statements) > 0:
                has_conflicts = True
            conflicts.append((statement, conflicting_statements))

        if has_conflicts:
            return conflicts
        else:
            return []

    def find_conflicting_statements(self, statement: Statement, timestamp: int)\
                                   -> List[ConflictingStatement]:
        """
        Find which statements in the statement log do conflict with the given statement by
        checking hash equality.
        If they are unequal, return a list of ConflictingStatement objects, otherwise return [].
        Perfectly sequential statements are not conflicting; non-sequential statements are
        conflicting.
        This notion of conflict also takes into account the extended state of records.

        statement : `Statement` - the new statement
        timestamp: `int` - the timestamp of the transaction that the new statement is in

        Returns : `List[ConflictingStatement]`
        """

        if statement.get_type() == StatementType.INSERT:
            self.__logger.debug(f"{self.__nodeId}: INSERT statements cannot raise conflicts")
            # should never have an existing record.
            # TODO: what about inserts that specify existing IDs?
            return []

        record_id = self.find_statement_record_id(statement)

        query = f"SELECT extended_state_after_hash "\
                f"FROM `{BLUBERRY_DB_NAME}`.`{STATEMENT_LOG_TABLE_NAME}` WHERE "\
                f"record_id = '{record_id}' AND current = 1 ORDER BY transaction_order DESC;"
        result = self.__mysql.execute(query)

        if (len(result) == 0):
            # there are no other statements on this record.
            return []

        currentStatement_afterHash = result[0][0]

        newStatement_beforeHash = statement.get_beforeState_hash()

        self.__logger.info(f"Finding conflicting statements; new_statement_beforeHash = "
                           f"{newStatement_beforeHash} \n Current Statement after hash is "
                           f"{currentStatement_afterHash}\n")
        if newStatement_beforeHash == currentStatement_afterHash:
            # this is a sequential action, no conflict.
            return []
        else:
            # There is a conflict.
            sl = f"`{BLUBERRY_DB_NAME}`.`{STATEMENT_LOG_TABLE_NAME}`"
            tl = f"`{BLUBERRY_DB_NAME}`.`{TRANSACTION_LOG_TABLE_NAME}`"
            query = f"SELECT {sl}.current, {sl}.before_values, {sl}.after_values, "\
                    f"{sl}.related_after_values, {sl}.created_by_conflict_resolution, "\
                    f"{tl}.origin_timestamp FROM ({sl} "\
                    f" JOIN {tl} ON {sl}.transaction_id = {tl}.id) "\
                    f"WHERE ({sl}.record_id = '{record_id}' "\
                    f"AND {sl}.created_by_conflict_resolution=0) OR "\
                    f"({sl}.record_id='{record_id}' AND {sl}.current=1) ORDER BY {sl}.current;"

            rows = self.__mysql.execute(query)
            conflicting_statements = []
            # append the statement itself
            conflicting_statements.append({
                "before_values": statement.get_before(),
                "after_values": statement.get_after(),
                "related_values": statement.get_related_afterValues(),
                "current": False,
                "created_by_conflict_resolution": False,
                "unix_timestamp": timestamp
            })
            for result in rows:
                current = result[0]
                before_values = json.loads(result[1])
                after_values = json.loads(result[2])
                related_after_values = json.loads(result[3])
                created_by_conflict_resolution = result[4]
                timestamp = self.convert_sql_to_unix_timestamp(result[5])

                conflicting_statement: ConflictingStatement = {
                    "before_values": before_values,
                    "after_values": after_values,
                    "related_values": related_after_values,
                    "current": bool(int(current)),
                    "created_by_conflict_resolution": bool(int(created_by_conflict_resolution)),
                    "unix_timestamp": timestamp
                }
                conflicting_statements.append(conflicting_statement)
            return conflicting_statements

    def _handle_conflicting_transaction(self,
                                        transaction: Transaction,
                                        conflicts: List[Tuple[Statement,
                                                        List[ConflictingStatement]]])\
                                        -> Tuple[Transaction, TransactionConflictResult]:
        """
        Resolves conflicts between a transaction and a list of conflicts by applying the
        configured resolution strategies.

        transaction : `Transaction` - An incoming transaction, to be written if permitted by
                      conflict handling.\n
        conflicts : `List[Tuple[Statement, List[ConflictingStatement]]]` - The
                    return value from check_for_conflicts.

        Returns : `Tuple[Transaction, TransactionConflictResult]` - The new transaction and
                  the result of the conflict resolution
        """
        # for each statement that has conflicts,
        # find the resolution of those conflicts.
        # if it is OUR statement (or some contextual business decision) in every case:,
        # we accept the transaction with ACCEPT_NEW or MODIFIED_ACCEPT as appropriate,
        # and the new transaction returned can be written.
        # else:
        # return ESCALATE, or REJECT
        # if there is any escalate in the individual conflict strategy
        # resolutions, always return Escalate.

        resolution_types = []
        before_after_statements = []

        # only need to 'handle' a conflict on the LAST occurrence of each record
        # that is modified. the order of handling conflicts makes no difference,
        # SO - we go through them in reverse order,
        # and choose not run if the same record comes up.
        records_seen = []
        for (statement, conflicting_statements) in conflicts[::-1]:
            record_id = self.find_statement_record_id(statement)
            if record_id in records_seen:
                continue
            records_seen.append(record_id)

            if len(conflicting_statements) == 0:
                # no conflict, before, after and statement are all as existing.
                before_after_statements.append(
                    (statement.get_before(), statement.get_after(), statement))
                continue

            statement_table = statement.get_table()
            table_resolution_strategy = self.__table_to_conflictResolutionStrategy_map[
                statement_table]["strategy"]

            strategy = choose_strategy(table_resolution_strategy)
            self.__logger.info(f"{self.__nodeId}: Resolving conflict with strategy "
                               f"{type(strategy)}")
            new_before, new_after, resolution_type = strategy.resolve(conflicting_statements)
            self.__logger.info(f"{self.__nodeId}: Conflict resolved with decision: "
                               f"{resolution_type.name}")
            resolution_types.append(resolution_type)
            # the statement will be modified to use this new set of before and after values.
            before_after_statements.append((new_before, new_after, statement))

        # The values to return depends on the resolution types, we generalise this into
        # failure: Rejection,
        # escalate: immediately stop and return ESCALATE
        # accept_or_modified: either accepted as is, or modified and then accepted.
        seen_failure = False
        seen_success_or_modified = False
        self.__logger.info(f"Resolution types seen are: {resolution_types}")
        for i, r_type in enumerate(resolution_types):
            if r_type == ConflictResolutionType.ESCALATION:
                self.__logger.info(f"{self.__nodeId}: Handling on conflict {i + 1} "
                                   "resolved to ESCALATION")
                # if there is any escalation result - escalate.
                return (transaction, TransactionConflictResult.ESCALATE)

            elif r_type == ConflictResolutionType.REJECT_NEW:
                self.__logger.info(f"{self.__nodeId}: Handling on conflict {i + 1} "
                                   "resolved to REJECT_NEW")
                seen_failure = True
            elif r_type == ConflictResolutionType.MODIFIED_ACCEPT:
                self.__logger.info(f"{self.__nodeId}: Handling on conflict {i + 1} "
                                   "resolved to MODIFIED_ACCEPT")
                seen_success_or_modified = True

            elif r_type == ConflictResolutionType.ACCEPT_NEW:
                self.__logger.info(f"{self.__nodeId}: Handling on conflict {i + 1} "
                                   "resolved to ACCEPT")
                seen_success_or_modified = True
            else:
                self.__logger.error(f"self.__nodeId: ERROR: Handling on conflict {i + 1} "
                                    f"failed to resolve, did not find an expected "
                                    f"ConflictResolutionType.\nResolutionTypes are "
                                    f"{resolution_types}, transaction is {transaction}")
                return (transaction, TransactionConflictResult.ESCALATE)

        if seen_failure:
            if (seen_success_or_modified):
                # some statements could be accepted (with modifications), and others
                # rejected - no clear consensus
                self.__logger.debug(f"{self.__nodeId}: Conflict handling for a transaction "
                                    "has a MIXTURE of accepted and rejected statements, "
                                    "set for ESCALATION")
                return (transaction, TransactionConflictResult.ESCALATE)
            else:
                self.__logger.debug(f"{self.__nodeId}: Conflict handling for a transaction "
                                    "has REJECTED all new statements.")
                return (transaction, TransactionConflictResult.REJECT)
        else:
            # all either success or modified
            self.__logger.debug(f"{self.__nodeId}: Conflict handling has resolved a "
                                "transaction, modified some statements, and accepted "
                                "the new result")
            self._modify_statements_in_conflictResolution(before_after_statements)
            self.set_statementAfterStates_in_transaction(transaction)
            return (transaction, TransactionConflictResult.MODIFIED_ACCEPT)

    def _modify_statements_in_conflictResolution(
            self, before_after_statements: Tuple[dict, dict, Statement]):
        """
        Ensure that the statement that is to be run has the right before values so that
        it can run.
        These values are created in `handle_conflicting_transactions()`
        Modifies the statements in input in-place.

        before_after_statements : `Tuple[dict, dict, Statement]` - A tuple of before values,
                                  after_values, and the statement
        """
        self.__logger.warning("Called modify statements in conflictResolution with "
                              f"{before_after_statements}")
        for bas in before_after_statements:
            bas[2].set_before(bas[0])
            bas[2].set_after(bas[1])

    def get_conflictResolved_transaction(
            self, orig_transaction: Transaction) -> Tuple[Transaction, bool]:
        """
        Openly accessible method, called by the writer when it receives a new foreign transaction.

        transaction : `Transaction`

        Returns : `Tuple[Transaction, bool]` - A potentially modified transaction, and a flag
                   to decide whether to run this transaction or not
        """
        modifiable_transaction = copy.deepcopy(orig_transaction)
        conflicts = self.check_for_conflicts(modifiable_transaction)

        if len(conflicts) == 0:
            self.__logger.info(f"{self.__nodeId}: Conflict handling complete, "
                               "no conflicts with new transaction")
            self.log_transaction(orig_transaction, current=True)
            return (orig_transaction, True)

        (modifiable_transaction, conflictResult) = self._handle_conflicting_transaction(
            modifiable_transaction, conflicts)

        if conflictResult == TransactionConflictResult.MODIFIED_ACCEPT:
            self.__logger.info(f"{self.__nodeId}: Conflict handling complete, "
                               "transaction accepted in modified form")
            self.log_transaction(orig_transaction, current=False)
            self.log_transaction(
                modifiable_transaction,
                current=True,
                created_by_conflict_resolution=True)
            return (modifiable_transaction, True)
        elif conflictResult == TransactionConflictResult.REJECT:
            self.__logger.info(f"{self.__nodeId}: Conflict handling complete, "
                               "transaction rejected")
            self.log_transaction(orig_transaction, current=False)
            return (orig_transaction, False)
        elif conflictResult == TransactionConflictResult.ESCALATE:
            self.__logger.info(f"{self.__nodeId}: Conflict handling paused, "
                               "waiting for manual review")
            self.log_transaction(orig_transaction, current=False)
            # For now we do nothing here.
            self._pass_on_manualConflict_for_review()
            return (orig_transaction, False)

    def _pass_on_manualConflict_for_review(self):
        # TODO pass in the necessary information here, and link this with
        # necessary method in the Core.
        pass

    def get_relatedState_beforeValues(self, statement) -> List[Tuple[str, str, str, str]]:
        """
        A record is linked to multiple related values (extra state). This method finds the
        related state as it is in the database for a given statement

        statement :  `Statement` - The statement to compute the values for

        Returns : `List[Tuple[str, str, str, str]]` - A list of the full before state values;
                  (Primary_Key of target record, Target table, Target column, Target value)
        """
        table = statement.get_table()
        list_paths_to_related_values = self.__table_to_conflictResolutionStrategy_map[
            table]["list_paths_to_related_values"]
        related_values = []
        for path in list_paths_to_related_values:
            if statement.get_type() == StatementType.UPDATE \
               or statement.get_type() == StatementType.INSERT:
                record_values = statement.get_after()
            elif statement.get_type() == StatementType.DELETE:
                record_values = statement.get_before()

            related_values.append(self.follow_related_value_path(path, record_values))

        return related_values

    def find_relatedState_afterValues_statement(self, statement) -> dict:
        """
        A record is linked to multiple related values (extended state). This method finds
        the extra state from AFTER a transaction was executed, given a statement.

        statement :  `Statement` - The statement to compute the values for

        Returns : `dict` - A dictionary of the after state values for this record,
                  in format: {table_name.column_name: VALUE}
        """
        table = statement.get_table()
        list_paths_to_related_values = self.__table_to_conflictResolutionStrategy_map[
            table]["list_paths_to_related_values"]
        related_values = {}
        for path in list_paths_to_related_values:
            if statement.get_type() == StatementType.UPDATE\
               or statement.get_type() == StatementType.INSERT:
                record_values = statement.get_after()
            elif statement.get_type() == StatementType.DELETE:
                record_values = statement.get_before()

            relatedValueInfo = self.follow_related_value_path(path, record_values)
            relatedValue_key = relatedValueInfo[1] + "." + relatedValueInfo[2]
            relatedValue_value = relatedValueInfo[3]
            related_values.update({relatedValue_key: relatedValue_value})

        return related_values

    def follow_related_value_path(self, path_to_related_value: List[dict], record_values: dict)\
                                 -> Tuple[str, str, str, str]:
        """
        Traverses the configured path to compute related values given an initial record.

        For the first record, we have record_values passed in.
        for all subsequent records we have to fetch the new 'record_values' from sql.
        we will only fetch the single record that we do need, from the next mapping.
        if "mapped_table" is false,
        this can only happen in the first and only element in the path
        then we simply grab the id field from record_values

        IF "mapped_tables" is True, but the length is only one,
        then once again we don't need to read from SQL, and we can simply
        grab the value of the 'source_column_name', and this is the same value as
        the eventual target - so we don't have to grab anything from the DB.

        If the length is greater than one, "mapped_tables" must be True always,
        and we have to access the DB to find the new record values.
        this is looped logic, with a special case for the final element of the list.

        path_to_consenting_individual : `List[dict]`\n
        record_values : `dict`

        Returns : `Tuple[str, str, str, Tuple]` - A tuple of ((Primary_Key of target record,
                  Target table, Target column, Target value))
        """

        path_length = len(path_to_related_value)
        if path_length == 1:
            path_step = path_to_related_value[0]
            if path_step["mapped_table"]:
                # then the single key needed to this path is stored in source_column_name

                source_table_name = path_step["source_table_name"]
                source_column_name = path_step["source_column_name"]
                # the mapped_table_name value is unused, but required in the config for clarity
                mapped_column_name = path_step["mapped_column_name"]
                mapped_table_name = path_step["mapped_table_name"]
                mapped_value = record_values[source_column_name]

                # and also we need to get the pk_values_dict for the record that thsi ends up on.
                primary_keys = self.get_primary_keys_for_table(mapped_table_name)
                primary_keys_str = ", ".join(primary_keys)

                query = f"SELECT {mapped_column_name} FROM {mapped_table_name} "\
                        f"WHERE {primary_keys_str}={mapped_value}"
                context_value = self.__mysql.execute(query)

                return (str(mapped_value), mapped_table_name,
                        mapped_column_name, context_value[0][0])
            else:
                raise RuntimeError("Invalid conflict config reached: if length of path "
                                   f"is 1, mapped_table must be true, {path_to_related_value}")

        else:
            for i in range(path_length):
                path_step = path_to_related_value[i]

                if i == 0:
                    # we already have record_values, so don't need to fetch from the DB
                    source_table_name = path_step["source_table_name"]
                    source_column_name = path_step["source_column_name"]
                    # the mapped_table_name value is unused, but required in the config
                    # for clarity
                    mapped_column_name = path_step["mapped_column_name"]
                    mapped_value = record_values[source_column_name]
                else:
                    source_table_name = path_step["source_table_name"]
                    source_column_name = path_step["source_column_name"]

                    query = f"SELECT {source_column_name} FROM {source_table_name} "\
                            f"WHERE {mapped_column_name} = {mapped_value};"
                    results = self.__mysql.execute(query)

                    try:
                        mapped_value = results[0][0]
                    except BaseException:
                        raise RuntimeError("Error in follow_related_value_path; "
                                           "Unable to find a result for query. "
                                           "Possibly because of a DELETE")

                    # and NOW, update the mapped_column_name - to be used in the next loop if
                    # required
                    mapped_table_name = path_step["mapped_table_name"]
                    mapped_column_name = path_step["mapped_column_name"]

                    primary_key_str = self.get_primary_keys_for_table(mapped_table_name)[0]
                    query = f"SELECT {mapped_column_name} FROM {mapped_table_name} "\
                            f"WHERE {primary_key_str}={mapped_value}"
                    context_value = self.__mysql.execute(query)

            return (str(mapped_value), mapped_table_name,
                    mapped_column_name, str(context_value[0][0]))

    def __map_key_key_append_map(self, map_a: dict, key_1: any, key_2: any):
        """
        Take in a map_a, a key_1, and a key_2, such that eventually map_a[key_1][key_2] "
        "returns (another) unspecified dictionary/map.
        Create those structures if needed, or otherwise append values to the existing maps.

        map_a : `dict`\n
        key_1 : `any`\n
        key_2 : `any`
        """
        if key_1 not in map_a:
            map_a[key_1] = {}
        if key_2 not in map_a[key_1]:
            map_a[key_1][key_2] = {}

    def set_statementBeforeHash_in_transaction(self, transaction: Transaction):
        """
        Aims to set the beforeHash value in each statement to the value before the entire
        transaction.

        When a node RECEIVES a new (foreign) transaction, it needs to compare it with the
        existing state for conflict.
        This is done by use of a Hash comparison on existing (extended) state.
        When a transaction is sent by the SENDING node it must have a value (hash) that
        represents the state that update was made in.
        This must be equal to the state AS INITIALLY WITHIN THE TRANSACTION - as this is how
        conflicts are defined.

        So, when the sending node creates this before_hash, it needs to reconstruct the
        state before the transaction in order to attach
        this to each statement.

        This method finds the correct beforeHash, and attaches it to each statement.

        transaction : `Transaction`
        """

        # read the statements in inverse order.
        # for each statement, set the before_state for this record (or its related value) to
        # the values for that record in _before_ / path,
        # and if this gets overwritten - it gets overwritten by something that preceded it:
        # so is closer to the actual true before state.
        # so, when reading values from the database in this loop - only write in
        # if there is nothing for that (table, record_key) pair.

        before_states = {}
        # before_states is a map from tables to:
        # a map from record_ids to:
        # a map from columns to their values, in their state from prior to the transaction

        extra_states_keys = {}
        # extra_states_keys contains a map from statements to:
        # a list containing:
        # source_table, record_id, column_id to be used to access the extra state values

        for statement in transaction.get_statements()[::-1]:
            table = statement.get_table()
            record_id = self.find_statement_record_id(statement)

            self.__map_key_key_append_map(before_states, table, record_id)

            if statement.get_type() == StatementType.INSERT:
                # there are on before values.
                # need to set the values to "" for each column there should be in this table...
                for c in self.__map_table_to_columns[table]:
                    before_states[table][record_id] = {}
            else:
                # overwrite anything that might exist with the before values from the statement.
                before_states[table][record_id] = copy.deepcopy(statement.get_before())

            # and now the related_values to this statement, these are only softly added.
            # If anything exists for the entire record,
            # we don't overwrite that.
            extra_state_prev_values = self.get_relatedState_beforeValues(statement)

            extra_states_keys[statement] = []
            for xsp_value in extra_state_prev_values:
                source_table = xsp_value[1]
                source_column = xsp_value[2]
                extra_state_value = xsp_value[3][0][0]
                pk_value = xsp_value[0]

                pk_value_list = [str(pk_value)]
                # generate the record id for this record referenced through extra state.
                record_id = self._generate_record_id(pk_value_list, source_table)

                extra_states_keys[statement].append((source_table, record_id, source_column))
                self.__map_key_key_append_map(before_states, source_table, record_id)
                if source_column in before_states[source_table][record_id]:
                    # then don't add this value
                    # it either exists from a 'statement.get_before()' result,
                    # which (takes precedence),
                    # or it exists because the db value (same as now) has already been written,
                    # so it makes no difference.
                    continue
                before_states[source_table][record_id][source_column] = extra_state_value

        # we now have a before_states dict with the prev states for each record that we need
        # to collect before_state from.
        # and we have a extra_states_keys dict with the keys to use to access
        # this, given a statement.

        # so, we now can loop through the statement and insert before_extra_records as
        # appropriate.

        for statement in transaction.get_statements():
            for key in extra_states_keys[statement]:
                extendedValue_key = key[0] + "." + key[2]
                extendedValue_value = before_states[key[0]][key[1]][key[2]]
                statement.add_related_beforeValue({extendedValue_key: extendedValue_value})

            table = statement.get_table()
            record_id = self.find_statement_record_id(statement)
            new_before = before_states[table][record_id]

            statement_extendedState_beforeStr = json.dumps(
                dict(new_before, **statement.get_related_beforeValues()), sort_keys=True)

            statement.set_beforeState_hash(
                self._get_statement_beforeHash(statement_extendedState_beforeStr))

    def set_statementAfterStates_in_transaction(self, transaction: Transaction):
        """
        For each statement, set the related_values and the afterState_hash based on values
        from the database + the statements.

        transaction : `Transaction` - The transaction that holds the statements to set values for
        """
        for statement in transaction.get_statements():
            related_values = self.find_relatedState_afterValues_statement(statement)
            statement.set_related_afterValues(related_values)
            extendedState_afterValues = statement.get_extendedState_afterStr()
            statement.set_afterState_hash(
                self._get_statement_afterHash(extendedState_afterValues))

    def prepare_conflictHandling_transaction(self, transaction: Transaction):
        """
        Used by a sending node. Sets the before and after states for each statement in a
        transaction, and then logs the transaction to that node's transaction log

        transaction: `Transaction` - The newly read transaction.
        """
        self.set_statementBeforeHash_in_transaction(transaction)
        self.set_statementAfterStates_in_transaction(transaction)
        self.log_transaction(transaction)

