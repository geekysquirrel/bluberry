__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = ["jde1g16@soton.ac.uk", "ms6n15@soton.ac.uk", "mfw1g15@soton.ac.uk",
             "ei2u16@soton.ac.uk"]

import abc
from enum import IntEnum
from typing import List, Tuple

from db.mysql.models.Statement import ConflictingStatement


class ConflictResolutionType(IntEnum):
    """
    A result of a conflict resolution decision
    """
    ACCEPT_NEW = 1  # Used when the new input is accepted as is
    REJECT_NEW = 2  # Used when the original record is accepted as is
    MODIFIED_ACCEPT = 3  # Used when the new input is modified, and accepted
    ESCALATION = 4  # Used when no automatic decision can be made


class ABluberryConflictStrategy(abc.ABC):
    """
    The abstract class sketches out a way for defining a stategy to resolve a conflict.
    Implementing classes must override the resolve method to provide the results of a resolution.
    """

    def __init__(self):
        pass

    @abc.abstractmethod
    def resolve(self, conflicting_statements: List[ConflictingStatement])\
                -> Tuple[dict, dict, ConflictResolutionType]:
        """
        Resolves a conflict, choosing from a list of conflicting statements.

        conflicting_statements : `List[ConflictinStatement]` - The statements in the database
                                  that conflict with the new update

        Returns : `Tuple[dict, dict, ConflictResolutionType]` A tuple of the new before and
                   after values (the "winning statement"), and the type of the resolution.
        """

    def getCurrent(self, conflicting_statements: List[ConflictingStatement])\
                   -> ConflictingStatement:
        """
        Returns the statement that refers to the current state of the database.

        conflicting_statements : `ListConflictingStatement]`

        Returns : `ConflictingStatement`
        """
        for cs in conflicting_statements:
            if cs["current"] == 1:
                return cs

        raise RuntimeError("No current value found conflict strategy")

