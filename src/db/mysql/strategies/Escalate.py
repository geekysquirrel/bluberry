__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g16@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u16@soton.ac.uk"]


from typing import List, Tuple

from db.mysql.models.Statement import ConflictingStatement
from db.mysql.strategies.ABluberryConflictStrategy import ABluberryConflictStrategy,\
                                                          ConflictResolutionType


class Escalate(ABluberryConflictStrategy):
    """
    An example strategy class that implements a business logic decision to resolve cases any case.
    Here, the conflict is always escalated to manual review.
    """

    def __init__(self):
        super().__init__()

    def resolve(self, conflicting_statements: List[ConflictingStatement])\
               -> Tuple[dict, dict, ConflictResolutionType]:

        cs = self.getCurrent(conflicting_statements)
        return (cs["before_values"], cs["after_values"], ConflictResolutionType.ESCALATION)

