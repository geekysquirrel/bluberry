__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g16@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u16@soton.ac.uk"]


from typing import List, Tuple

import core.BluberryLogging
from db.mysql.models.Statement import ConflictingStatement
from db.mysql.strategies.ABluberryConflictStrategy import ABluberryConflictStrategy,\
                                                          ConflictResolutionType


class FirstWins(ABluberryConflictStrategy):
    """
    An example strategy class that implements a business logic decision to resolve cases.
    Here, the new statement is accepted if was earlier than the current conflicting statement.
    """

    def __init__(self):
        super().__init__()
        self.__logger = core.getLogger(self.__class__.__name__)

    def resolve(self, conflicting_statements: List[ConflictingStatement])\
               -> Tuple[dict, dict, ConflictResolutionType]:

        current_statement = self.getCurrent(conflicting_statements)
        min_timestamp = current_statement['unix_timestamp']
        winning_statement = None
        for cs in conflicting_statements:
            if min_timestamp > cs['unix_timestamp']:
                min_timestamp = cs['unix_timestamp']
                winning_statement = cs

        if min_timestamp < current_statement["unix_timestamp"]:
            # the current statement was later
            return (current_statement["after_values"],
                    winning_statement["after_values"],
                    ConflictResolutionType.ACCEPT_NEW)
        else:
            # the current statement was earlier
            return (current_statement["before_values"],
                    current_statement["after_values"],
                    ConflictResolutionType.REJECT_NEW)

