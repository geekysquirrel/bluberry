#!/bin/bash

echo "Waiting for MySQL server to start..."
while ! mysqladmin ping -h"localhost" --silent; do \
    sleep 2; echo '.'; \
done
echo "MySQL server is running, waiting for entrypoint script to do its magic."
# This will start ther server, create the user as per ENV vars in the docker-compose file
# and do other stuff before restarting the mysql server using the appropriate user (i.e. not root).

echo "Checking MySQL is up, running and configured"
# Checking whether we can log in. If we can, this means the entrypoint script has set up
# the root user and we can proceed with the bluberry setup.
RESULT=1
while [[ "$RESULT" != *0 ]]; do
    echo "Can't log in as root yet"
    sleep 1;
    # The last bit echos the return value which we expect to be 0.
    # We can't check for the entire string as it also contains the result of the query
    RESULT=` mysql --user=root --password=password --execute "SHOW DATABASES;"; echo $?`
done
echo " Databases and return value: $RESULT"

echo "Granting privileges to bluberry MySQL user"
# extra space before mysql command to avoid saving password in bash history
` mysql -h localhost --user="root" --password="password" < /conf/grant-privileges.sql`
echo "Done."

# Assuming if the above worked, this works too
echo "Setting up bluberry database"
` mysql -h localhost --user="bluberry" --password="password" < /conf/create-tables.sql`
echo "Done."
