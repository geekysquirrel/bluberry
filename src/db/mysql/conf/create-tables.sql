/* # TODO: hardcoded names need to match constants.py */
USE bluberry;

SET AUTOCOMMIT = 1;
SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "";
SET GLOBAL SQL_MODE = "";

CREATE TABLE IF NOT EXISTS transaction_log (id CHAR(36), origin_node TEXT NOT NULL, origin_timestamp TIMESTAMP NOT NULL, received_timestamp TIMESTAMP NULL DEFAULT NULL, tables TEXT NOT NULL, created_by_conflict_resolution TINYINT(1) NOT NULL, PRIMARY KEY(id, created_by_conflict_resolution));
CREATE TABLE IF NOT EXISTS statement_log (transaction_id CHAR(36), table_name VARCHAR(64), record_id CHAR(32), transaction_order INT NOT NULL, before_values TEXT, after_values TEXT, related_after_values TEXT, extended_state_after_hash TEXT, current TINYINT(1) NOT NULL, created_by_conflict_resolution TINYINT(1) NOT NULL, FOREIGN KEY (transaction_id, created_by_conflict_resolution) REFERENCES transaction_log(id, created_by_conflict_resolution));
CREATE TABLE IF NOT EXISTS network_id_mapping (table_name VARCHAR(64) NOT NULL, local_id INT NOT NULL, network_id VARCHAR(18) NOT NULL, PRIMARY KEY(table_name, local_id, network_id));
CREATE TABLE IF NOT EXISTS local_insert_count (table_name VARCHAR(64) NOT NULL, insert_count INT NOT NULL, PRIMARY KEY(table_name));

SET FOREIGN_KEY_CHECKS = 1;
SET AUTOCOMMIT = 0;
