#!/bin/bash

# Run setup in the background - it will wait until the mysql server is up.
bash /conf/setup.sh &
/usr/local/bin/docker-entrypoint.sh mysqld --character-set-server=utf8 --collation-server=utf8_general_ci
