__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g16@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u16@soton.ac.uk"]


from typing import List, Tuple, Union

import core.BluberryLogging
from core.BluberryConf import Conf
from db.mysql.MySQLConnector import MySQLConnector
from db.mysql.MySQLConnector import BLUBERRY_DB_NAME,\
                                    ID_MAPPING_TABLE_NAME,\
                                    LOCAL_COUNT_TABLE_NAME,\
                                    WRITER_STATUS_TABLE_NAME
from db.mysql.models.Statement import StatementType


class MySQLIDTranslationManager():
    """
    Handles the translation of local values created by SQL AUTOINCREMENT into a globally
    unique ID that can be sent to other nodes to avoid conflicts.
    """

    def __init__(self, conf: Conf):
        """
        conf : `dict` - The configuration for the node that the translator is to run on
        """
        self.__logger = core.getLogger(self.__class__.__name__)

        self.__conf = conf
        self.__nodeId = conf.get('local', 'nodeId')
        self.__schema = conf.get('db', 'connection', 'database')
        self.__mysql = MySQLConnector(conf)

        self.__all_tables = []
        for row in self.__mysql.execute(f"SHOW TABLES FROM {self.__schema}"):
            if row is not None and row[0] is not None and row[0] != WRITER_STATUS_TABLE_NAME:
                self.__all_tables.append(row[0])
        self.__tables_with_autoIncr = self.find_autoIncrement_tables()

        self.setup_insertCount()

        self.__network_prefix = self.__conf.get('local', 'networkPrefix')
        self.__map_tupleOf_table_localID_to_network_id = {}

        self.__map_table_autoIncrementFieldName = self.find_autoIncrement_fields()

        self.__table_foreignKeysToAutoIncrementedFields_map =\
            self.find_foreignKeyFields_to_autoIncrementTables()

        self.__tables_with_existing_data = self.__conf.get('db', 'sync', 'static_tables')
        self.create_self_id_mapping_for_initial_data()
        self.__logger.info(f"{self.__nodeId}: MySQL ID Translation Manager initialised "
                           "successfully")

    def write_new_mapping(self, table: str, local_id: int, network_id: str):
        """
        Writes a new mapping entry into the autoincrement translation table.

        table : `str` - The table that the update is on
        local_id : `int` - The locally assigned value from the autoincrement
        network_id : `str` - The globally unique value this record has been assigned
        """
        self.__logger.debug(f"{self.__nodeId}: Writing new autoincrement mapping for table "
                           f"{table}: {local_id} (local) -> {network_id} (global)")

        # this is required because the following is a transaction.
        # the data will not be available to the calling method if it is running a
        # long transaction and this call is part of it
        wasConnected = False
        if self.__mysql.isConnected():
            wasConnected = True
            # TODO: commit?
            self.__mysql.disconnect()

        query = f"INSERT INTO `{BLUBERRY_DB_NAME}`.`{ID_MAPPING_TABLE_NAME}` "\
                f"(table_name, local_id, network_id) VALUES ('{table}', '{local_id}', "\
                f"'{network_id}');"
        self.__mysql.execute(query)

        if wasConnected:
            self.__mysql.connect()

    def create_self_id_mapping_for_initial_data(self):
        """
        Create a self_id mapping from local_id to network_id where network_id = local_id
        in order to allow syncing of tables with static pre-existing information that applies
        on multiple nodes.
        """
        self.__mysql.connect()
        for table in self.__tables_with_existing_data:
            query = f"SELECT column_name FROM INFORMATION_SCHEMA.key_column_usage "\
                    f"WHERE table_schema = '{self.__schema}' AND "\
                    f"constraint_name = 'PRIMARY' and table_name = '{table}';"
            result = self.__mysql.execute(query)

            if result and result[0]:
                primary_key_column = result[0][0]

                query = f"SELECT {primary_key_column} FROM `{table}`;"
                table_primaryKeys = self.__mysql.execute(query)

                query = f"SELECT local_id FROM `{BLUBERRY_DB_NAME}`."\
                        f"`{ID_MAPPING_TABLE_NAME}"\
                        f"` WHERE table_name='{table}';"
                already_mapped_primaryKeys = self.__mysql.execute(query)

                for key in table_primaryKeys:
                    if key not in already_mapped_primaryKeys:
                        self.write_new_mapping(table, key[0], key[0])

        self.__mysql.disconnect()

    def setup_insertCount(self):
        """
        Check for local_insert_count table in database
        If exists, pull values out from there
        Else: create the table.

        Store values in memory in self.__tableWithAutoIncr_to_insertCount_map
        """
        self.__mysql.connect()
        result = self.__mysql.execute(f"SELECT table_name, insert_count "
                                      f"FROM `{BLUBERRY_DB_NAME}`.`{LOCAL_COUNT_TABLE_NAME}`;")

        self.__tableWithAutoIncr_to_insertCount_map = {}
        if result is None:
            raise(RuntimeError(f"{self.__nodeId}: Table `{BLUBERRY_DB_NAME}`."
                               f"`{LOCAL_COUNT_TABLE_NAME}` doesn't exist."))
        elif len(result) == 0:
            self.__logger.debug(f"Setting up `{BLUBERRY_DB_NAME}`.`{LOCAL_COUNT_TABLE_NAME}`")
            for table in self.__all_tables:
                query = f"INSERT INTO `{BLUBERRY_DB_NAME}`.`{LOCAL_COUNT_TABLE_NAME}` "\
                        f"VALUES ('{table}', 0)"
                self.__mysql.execute(query)
            # do it again to get result
            result = self.__mysql.execute(f"SELECT table_name, insert_count FROM "
                                          f"`{BLUBERRY_DB_NAME}`.`{LOCAL_COUNT_TABLE_NAME}`;")
        else:
            self.__logger.debug(f"{self.__nodeId}: Table `{BLUBERRY_DB_NAME}`."
                                f"`{LOCAL_COUNT_TABLE_NAME}` exists, reading cached setup")

        if result is not None:
            for r in result:
                self.__logger.debug(f"{self.__nodeId}: Setting insert count "
                                    f"for {r[0]} to {r[1]}")
                self.__tableWithAutoIncr_to_insertCount_map[r[0]] = r[1]

        self.__mysql.commit()
        self.__mysql.disconnect()

    def find_autoIncrement_tables(self) -> List[str]:
        """
        Finds all the tables in the database that have an autoincrementing column.

        Returns : `List[str]`
        """
        # MySQL 8 bug:
        # Need to set this, otherwise getting auto_increment tables doesn't work
        # If it continues to not work, perhaps use "set global" or "set persist"
        self.__mysql.execute('SET INFORMATION_SCHEMA_STATS_EXPIRY = 0;')
        self.__mysql.execute('SET GLOBAL INFORMATION_SCHEMA_STATS_EXPIRY = 0;')
        # this query however works in MySQL 8 where the AUTO_INCREMENT column is not reliable
        rows = self.__mysql.execute(f"SELECT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                                    f"WHERE TABLE_SCHEMA='{self.__schema}' AND "
                                    "EXTRA = 'auto_increment';")
        tables_with_autoIncr = []
        for result in rows:
            tables_with_autoIncr.append(result[0])

        return tables_with_autoIncr

    def write_localInsertIncrement(self, table: str, value: int, initial=False):
        """
        Writes the given value to the local insert count table on the row for the given table.

        table : `str`
        value : `int`
        intitial : `bool` - If this is first time writing to the count for the given table
        """
        self.__logger.debug(f"{self.__nodeId}: Setting local insert count of table "
                            f"`{table}` to {value}")
        if initial:
            query = f"INSERT INTO `{BLUBERRY_DB_NAME}`.`{LOCAL_COUNT_TABLE_NAME}` "\
                    f"(table_name, insert_count) VALUES ('{table}', {value});"
        else:
            query = f"UPDATE `{BLUBERRY_DB_NAME}`.`{LOCAL_COUNT_TABLE_NAME}` "\
                    f"SET insert_count = '{value}' WHERE table_name = '{table}';"
        self.__mysql.execute(query)

    def getNetworkMapping_new_localInsert(self, table: str) -> str:
        """
        Creates a new global ID for a local insert to map to.

        table : `str` - The table that is being inserted into

        Returns : `str` - A new globally unique ID for this node and table
        """
        count = 0
        if table in self.__tableWithAutoIncr_to_insertCount_map:
            count = self.__tableWithAutoIncr_to_insertCount_map[table]
            new_count = count + 1
            self.write_localInsertIncrement(table, new_count)
            self.__tableWithAutoIncr_to_insertCount_map[table] = new_count

            network_id = self.__network_prefix + str(new_count)

            return network_id
        else:
            self.__logger.warning(f"Table {table} not found.")

        return None

    def getLocalMapping_new_networkInsert(self, table: str) -> int:
        """
        Finds the next autoincrement value will be for an inserted record on a given table.

        table : `str`

        Returns : `int`
        """
        return self.get_table_nextAutoIncrement(table)

    def is_table_autoIncrementing(self, table: str) -> bool:
        """
        Checks if the given table has an autoincrementing column.

        Returns : `bool`
        """
        return table in self.__tables_with_autoIncr

    def find_autoIncrement_fields(self) -> dict:
        """
        Generates a map from table name to its autoincrementing column name.

        Returns : `dict`
        """
        map_table_autoIncrementFieldName = {}

        query = "SELECT table_name, column_name FROM INFORMATION_SCHEMA.columns "\
                f"WHERE table_schema = '{self.__schema}' "\
                "AND extra like 'auto_increment';"
        rows = self.__mysql.execute(query)
        for result in rows:
            if result[0] in map_table_autoIncrementFieldName:
                raise RuntimeError(f"{self.__nodeId}: System does not support multiple "
                                    f"auto_increments per table, as found on table {result[0]} "
                                    f"only supporting translation for column {result[1]}")
            map_table_autoIncrementFieldName[result[0]] = result[1]
        return map_table_autoIncrementFieldName

    def find_table_if_foreignKey_goesTo_autoIncrementTables(
            self, table_name: str, column_name: str) -> str:
        """
        Recurses through the foreign key column of a table to find if there are any links to
        an autoincrementing columns that need to be translated.

        table_name : `str`
        column_name : `str`

        Returns : `str` - The name of the table, or None if there are no autoincremeting links
        """
        query = "SELECT referenced_table_name, referenced_column_name "\
                "FROM INFORMATION_SCHEMA.key_column_usage "\
                f"WHERE referenced_table_schema = '{self.__schema}' "\
                f"AND table_name = '{table_name}' AND column_name = '{column_name}';"
        rows = self.__mysql.execute(query)
        if len(rows) > 1:
            raise RuntimeError(f"")
        if len(rows) == 0:
            return None
        for new_table, new_column in rows:
            if new_table not in self.__tables_with_autoIncr:
                # this foreign key goes to a table that doesn't have any autoIncr values,
                # does not need to be mapped by the IDTranslationManager, unless it FKs to
                # an autoIncrColumn, so recurse.
                return self.find_table_if_foreignKey_goesTo_autoIncrementTables(
                    new_table, new_column)
            elif self.__map_table_autoIncrementFieldName[new_table] != new_column:
                # this foreign key goes to a column within the autoIncrementingTable that is
                # NOT the autoIncrementing column
                # check if THIS column FKs to an autoIncrColumn
                return self.find_table_if_foreignKey_goesTo_autoIncrementTables(
                    new_table, new_column)
            else:
                # This is a FK from some table,column to some
                # autoIncrementingTable.autoIncrementingColumn
                # return the target table name.
                return new_table

    def find_foreignKeyFields_to_autoIncrementTables(self):
        """
        Return a list of tuples, giving, for any table: the Foreign Keys from that table that
        go to auto_incrementing fields[those that will be network-mapped], mapped to the table
        they go to.

        Returns : `dict`
        """
        table_foreignKeysToAutoIncrementedFields_map = {}

        # find all foreign keys that go to the database., find the
        # tablename.column_name of each of those foreign keys.
        query = f"SELECT table_name, column_name FROM INFORMATION_SCHEMA.key_column_usage "\
                f"WHERE referenced_table_schema='{self.__schema}';"
        rows = self.__mysql.execute(query)

        for t in self.__all_tables:
            table_foreignKeysToAutoIncrementedFields_map[t] = {}

        for new_table, new_column in rows:
            fk_target_autoIncrementTable =\
                self.find_table_if_foreignKey_goesTo_autoIncrementTables(new_table, new_column)
            if fk_target_autoIncrementTable is None:
                # fk does not go to, or chain to, an auto increment table.
                continue
            else:
                # fk, on table new_table, identifiable by (new_table, new_column) goes to
                # (or eventually chains to) the fk_target_autoIncrementTable.
                table_foreignKeysToAutoIncrementedFields_map[new_table][(
                    new_table, new_column)] = fk_target_autoIncrementTable

        return table_foreignKeysToAutoIncrementedFields_map

    def map_to_global_value(self, table: str, local_id: int) -> str:
        """
        Returns the global value that maps to the local value on the given table.

        table : `str`
        local_id : `int`

        Returns : `str`
        """
        self.__logger.info(f"map_to_global_value with table: {table} and local_id: {local_id}")

        result = self.__mysql.execute(f"SELECT * FROM {BLUBERRY_DB_NAME}.{ID_MAPPING_TABLE_NAME}")
        self.__logger.debug(result)
        query = f"SELECT network_id FROM `{BLUBERRY_DB_NAME}`.`{ID_MAPPING_TABLE_NAME}` "\
                f"WHERE table_name = '{table}' AND local_id = '{local_id}';"
        result = self.__mysql.execute(query)

        if result and result[0] and result[0][0]:
            network_id = result[0][0]
            self.__logger.debug(f"{self.__nodeId}: Local ({table}, {local_id}) translates "
                                f"to {network_id}")
            return network_id
        else:
            self.__logger.warning(f"Could not map to global value for table {table} "
                                  f"and local ID {local_id}")
        return None

    def map_to_local_value(self, table: str, network_id: str) -> int:
        """
        Returns the local value that maps to the global value on the given table.

        table : `str`
        network_id : `str`

        Returns : `int`
        """
        query = f"SELECT local_id FROM `{BLUBERRY_DB_NAME}`.`{ID_MAPPING_TABLE_NAME}` "\
                f"WHERE table_name = '{table}' AND network_id = '{network_id}';"
        result = self.__mysql.execute(query)

        if result and result[0] and result[0][0]:
            local_id = result[0][0]
            self.__logger.debug(f"{self.__nodeId}: Global ID {network_id} in table {table} "
                                f"translates to local ID {local_id}")
        else:
            self.__logger.warning("Could not get local ID for "
                                  f"network ID {network_id} in table {table}")
            local_id = None

        return local_id

    def get_network_values(self, table: str, vals: dict) -> dict:
        """
        Given a dictionary of values for record on a given table, convert the values to
        the global namespace.

        table : `str`
        vals : `dict`

        Returns : `dict`
        """
        global_vals = vals.copy()
        # first check if this table is auto_incrementing.
        if self.is_table_autoIncrementing(table):
            table_id = self.__map_table_autoIncrementFieldName[table]
            network_id_val = self.map_to_global_value(table, vals[table_id])
            global_vals[table_id] = network_id_val

        # then, go through for each foreign key from this table, and map the
        # values in those columns
        map_fk_table = self.__table_foreignKeysToAutoIncrementedFields_map[table]
        for autoinc_fk in map_fk_table.keys():
            fk_target_table = map_fk_table[autoinc_fk]
            local_fk_value = vals[autoinc_fk[1]]
            if local_fk_value is None:
                network_fk_val = None
            else:
                network_fk_val = self.map_to_global_value(fk_target_table, local_fk_value)
            global_vals[autoinc_fk[1]] = network_fk_val
        return global_vals

    def get_local_values(self, table: str, vals: dict) -> dict:
        """
        Given a dictionary of values for record on a given table, convert the values to
        the local namespace.

        table : `str`
        vals : `dict`

        Returns : `dict`
        """
        local_vals = vals.copy()
        # first check if this table is auto_incrementing.
        if self.is_table_autoIncrementing(table):
            table_id = self.__map_table_autoIncrementFieldName[table]
            local_id_val = self.map_to_local_value(table, vals[table_id])
            local_vals[table_id] = local_id_val

        # then, go through for each foreign key from this table, and map the
        # values in those columns
        map_fk_table = self.__table_foreignKeysToAutoIncrementedFields_map[table]
        for autoinc_fk in map_fk_table.keys():
            fk_target_table = map_fk_table[autoinc_fk]
            network_fk_value = vals[autoinc_fk[1]]
            if network_fk_value is None:
                local_fk_value = None
            else:
                local_fk_value = self.map_to_local_value(fk_target_table, network_fk_value)
            local_vals[autoinc_fk[1]] = local_fk_value
        return local_vals

    def get_table_nextAutoIncrement(self, table: str) -> int:
        """
        Finds the next autoincrement value will be for an inserted record on a given table.

        table : `str`

        Returns : `int`
        """
        query = "SELECT auto_increment FROM sys.schema_auto_increment_columns "\
                f"WHERE table_schema='{self.__schema}' AND table_name='{table}';"
        result = self.__mysql.execute(query)

        if result and result[0] and len(result[0]) > 0:
            # MySQL 8.0 hack: if the table is empty, the value will be null
            # so returning 1 as the next ID
            if not result[0][0]:
                return 1
            else:
                return result[0][0]
        else:
            self.__logger.warning(f"Could not get next auto increment for table {table} "
                                  f"in schema {self.__schema}.")
            return 0

    def map_localUpdate_to_networkValues(self,
                                         before_values: dict,
                                         after_values: dict,
                                         statement_type: StatementType,
                                         table: str) -> Tuple[dict, dict]:
        """
        Given a set of before and after values for a given statement type on a table,
        returns a tuple of the values mapped into the global namespace.

        before_values : `dict`
        after_values : `dict`
        statement_type : `StatementType`
        table : `str`

        Returns : `Tuple[dict, dict]`
        """
        self.__logger.info(f"{self.__nodeId}: Translating incoming local update on table {table}"
                           "from local to global namespace")
        if statement_type == StatementType.INSERT:
            # before_values is None, after_values has new values.
            # is this an INSERT on an auto_incrementing field?
            if self.is_table_autoIncrementing(table):
                self.__logger.debug(f"{self.__nodeId}: Insert on autoincrementing table, "
                                    "computing new mapping")
                network_id = self.getNetworkMapping_new_localInsert(table)
                autoIncColumn = self.__map_table_autoIncrementFieldName[table]
                local_id = after_values[autoIncColumn]
                self.write_new_mapping(table, local_id, network_id)
            else:
                self.__logger.debug(f"{self.__nodeId}: Insert not on autoincrementing table, "
                                    "no new mapping needed")
            network_after_values = self.get_network_values(table, after_values)
            return (None, network_after_values)
        elif statement_type == StatementType.UPDATE:
            network_before_values = self.get_network_values(table, before_values)
            network_after_values = self.get_network_values(table, after_values)
            return (network_before_values, network_after_values)
        elif statement_type == StatementType.DELETE:
            network_before_values = self.get_network_values(table, before_values)
            return (network_before_values, None)

    def map_foreignUpdate_to_localValues(self,
                                         before_values: dict,
                                         after_values: dict,
                                         statement_type: StatementType,
                                         table: str) -> Tuple[dict,
                                                              dict]:
        """
        Given a set of before and after values for a given statement type on a table, returns
        a tuple of the values mapped into the global namespace.

        before_values : `dict`
        after_values : `dict`
        statement_type : `StatementType`
        table : `str`

        Returns : `Tuple[dict, dict]`
        """
        self.__logger.info(f"{self.__nodeId}: Translating incoming foreign update "
                           f"on table {table} from global to local namespace")
        if statement_type == StatementType.INSERT:
            # before_values is None, after_values has new values.
            # is this an INSERT on an auto_incrementing field?
            if self.is_table_autoIncrementing(table):
                self.__logger.debug(f"{self.__nodeId}: Insert on autoincrementing table, "
                                    "computing new mapping")
                local_id = self.getLocalMapping_new_networkInsert(table)
                autoIncColumn = self.__map_table_autoIncrementFieldName[table]
                network_id = after_values[autoIncColumn]
                self.write_new_mapping(table, local_id, network_id)
            else:
                self.__logger.debug(
                    f"{self.__nodeId}: Insert not on autoincrementing table, "
                    "no new mapping needed")
            local_after_values = self.get_local_values(table, after_values)
            return (None, local_after_values)

        elif statement_type == StatementType.UPDATE:
            local_before_values = self.get_local_values(table, before_values)
            local_after_values = self.get_local_values(table, after_values)
            return (local_before_values, local_after_values)

        elif statement_type == StatementType.DELETE:
            local_before_values = self.get_local_values(table, before_values)
            return (local_before_values, None)

        return (None, None)

    def map_localValue_to_globalValue(self, table_name: str, local_value: int) -> Union[int, str]:
        """
        Returns the mapped global value for the given local value if the given table has an
        autoincrementing column, otherwise just returns the local value.

        table_name : `str`
        local_value : `int`

        Returns : `Union[int, str]`
        """
        if table_name in self.__tables_with_autoIncr:
            global_value = self.map_to_global_value(table_name, local_value)
            return global_value
        else:
            return local_value

