__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g16@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u16@soton.ac.uk"]

from typing import TypedDict, List


class TableConsentRequirement(TypedDict):
    """
    Typed dictionary for consent requirements imposed on a table
    """
    tag_name: str
    table: str
    sensitive_fields: List[str]
    list_path_to_consenting_individuals: List
