__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = [
    "jde1g16@soton.ac.uk",
    "ms6n15@soton.ac.uk",
    "mfw1g15@soton.ac.uk",
    "ei2u16@soton.ac.uk"]

import json
from uuid import uuid4
from typing import List, Set

from db.mysql.models.Statement import Statement


class Transaction():
    """
    Holds a database transaction - the list of statements, the tables that are affected,
    a timestamp and the locations that it is permitted to be sent to.
    """

    def __init__(self):
        self.__id = str(uuid4())
        self.__statements: List[Statement] = []
        self.__tables: Set[str] = set()
        self.__timestamp: int = None
        self.__locations: List[str] = []

    def __str__(self):
        return json.dumps(self._asdict())

    def _asdict(self):
        return {
            "id": self.__id,
            "statements": [str(statement) for statement in self.__statements],
            "tables": list(self.__tables),
            "timestamp": self.__timestamp,
            "locations": self.__locations
        }

    @staticmethod
    def deserialise(json_string: str):
        """
        Transform a transaction as a JSON string back into a python object.

        json_string : `str`

        Returns : `Transaction`
        """
        transaction_dict = json.loads(json_string)

        requiredFields = [
            "id",
            'statements',
            'tables',
            'timestamp',
            "locations"]
        missingFields = []
        for field in requiredFields:
            if field not in transaction_dict:
                missingFields.append(field)
        if len(missingFields) > 0:
            raise RuntimeError(
                f"Incomplete JSON statement, missing {missingFields}:{statement_dict}")

        transaction = Transaction()
        for statement_dict in transaction_dict["statements"]:
            statement = Statement.deserialise(statement_dict)
            transaction.add_statement(statement)
        transaction.set_timestamp(int(transaction_dict["timestamp"]))
        transaction.override_id(transaction_dict["id"])
        transaction.set_locations(transaction_dict["locations"])

        return transaction

    def add_statement(self, statement: Statement):
        """
        Adds a statement to the transaction.

        statement : `Statement`
        """
        self.__statements.append(statement)
        if statement.get_table() is not None:
            self.__tables.add(statement.get_table())

    def set_tables(self, tables: List[str]):
        """
        Sets the tables that this transaction touches. Not normally needed since this is
        automatically set by adding statements.

        tables : `List[str]`
        """
        self.__tables = tables

    def set_timestamp(self, timestamp: int):
        """
        Sets the timestamp of the transaction.

        timestamp: `int` - Unix timestamp
        """
        self.__timestamp = timestamp

    def set_locations(self, locations: List[str]):
        """
        Sets the locations (nodes) that the transaction is permitted to be sent to

        locations : `List[str]`
        """
        self.__locations = locations

    def isEmpty(self) -> bool:
        """
        Returns : `bool` - If the transaction has no statements
        """
        return len(self.__statements) == 0

    def get_id(self) -> str:
        """
        Returns `id` : UUID4 for this transaction
        """
        return self.__id

    def get_statements(self) -> List[Statement]:
        """
        Returns : `List[Statement]` - Statements in the transaction
        """
        return self.__statements

    def get_tables(self) -> Set[str]:
        """
        Returns : `List[str]` - Tables that the statements in this transaction affect
        """
        return self.__tables

    def get_timestamp(self) -> int:
        """
        Returns : `int` - Unix timestamp of this transaction
        """
        return self.__timestamp

    def get_locations(self) -> List[str]:
        """
        Returns : `List[str]` - Locations that this transaction is permitted to be
        synchromnized to
        """
        return self.__locations

    def override_id(self, id: str):
        """
        Used to override the automatically generated UUID for a transaction when it is
        being deserialised

        id : `str` - UUID to assign to this transaction
        """
        self.__id = id

