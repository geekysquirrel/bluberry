__author__ = ["James Errington", "Maanasa Srikrishna", "Matt Warren", "Evelina Ivanova"]
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = ["jde1g16@soton.ac.uk",
             "ms6n15@soton.ac.uk",
             "mfw1g15@soton.ac.uk",
             "ei2u16@soton.ac.uk"]


import json
from enum import IntEnum
from typing import List, TypedDict


class StatementType(IntEnum):
    UPDATE = 1
    INSERT = 2
    DELETE = 3


class ConflictingStatement(TypedDict):
    """
    Typed dictionary for a conflicting statement
    """
    before_values: dict
    after_values: dict
    related_values: dict
    current: bool
    created_by_conflict_handling: bool
    unix_timestamp: int


class StatementConsentRequirement:
    """
    Identifies the type of consent required, and the individual from which that consent should
    be provided.
    """

    def __init__(self, consent_tag: str, consenting_individuals: List[str],
                 consent_owner_type: str):
        """
        consent_tag : `str`\n
        consenting_individuals : `List[str]`\n
        consent_owner_type : `str`
        """
        self.__consent_tag = consent_tag
        self.__consenting_individuals = consenting_individuals
        self.__consent_owner_type = consent_owner_type

    def __str__(self):
        return json.dumps(self._asdict())

    def _asdict(self):
        return {
            "consent_tag": self.__consent_tag,
            "consenting_individuals": [str(ci) for ci in self.__consenting_individuals],
            "consent_owner_type": self.__consent_owner_type
        }

    @staticmethod
    def deserialise(json_string: str):
        """
        Transform a statement consent requirement as a JSON string back into a python object.

        json_string : `str`

        Returns : `StatementConsentRequirement`
        """
        statement_dict = json.loads(json_string)

        requiredFields = [
            "consent_tag",
            "consenting_individuals",
            "consent_owner_type"
        ]
        missingFields = []
        for field in requiredFields:
            if field not in statement_dict:
                missingFields.append(field)
        if len(missingFields) > 0:
            raise RuntimeError(f"Incomplete JSON statement, missing {missingFields}:"
                               f"{statement_dict}")

        consenting_individuals = []
        for ci in statement_dict["consenting_individuals"]:
            consenting_individuals.append(ci)

        return StatementConsentRequirement(statement_dict["consent_tag"],
                                           consenting_individuals,
                                           statement_dict["consent_owner_type"])

    def get_consent_tag(self) -> str:
        """
        Returns : `str` - The consent tag of the requirement
        """
        return self.__consent_tag

    def get_consenting_individuals(self) -> List[str]:
        """
        Returns : `List[str]` - The individuals that have consent rules for this tag
        """
        return self.__consenting_individuals

    def get_consent_owner_type(self) -> str:
        """
        Returns : `str` - The type of owner of this requirement
        """
        return self.__consent_owner_type

    def __repr__(self):
        return "Consent Requirement: Type: %s, Individual: %s, with consent_tag: %s" % (
            self.__consent_owner_type, self.__consenting_individuals, self.__consent_tag)


class Statement:
    """
    Represents a SQL query for INSERT, UPDATE, or DELETE. Holds the row values of the operation,
    the type, the table being acted upon and a list of consent requirements for the data that is
    affected by the statement.
    """

    def __init__(self,
                 before: dict = {},
                 after: dict = {},
                 s_type: StatementType = None,
                 table: str = None,
                 statement_consent_reqs: List[StatementConsentRequirement] = []):
        """
        before : `dict` - the row of the database before the statement was run\n
        after : `dict` - the row of the database after the statement was run\n
        s_type : `StatementType` - The type of update that this statement encodes\n
        table : `str` - The table that this statement updates\n
        statement_consent_reqs : `List[StatementConsentRequirement]` - The list of consent
                                 requirements that relate to the data in this statement
        """
        if before is None:
            self.__before = {}
        else:
            self.__before = before
        if after is None:
            self.__after = {}
        else:
            self.__after = after
        self.__related_before = {}
        self.__related_after = {}
        self.__table = table
        self.__type = s_type
        self.__statement_consent_reqs = statement_consent_reqs
        self.__beforeHash = ""
        self.__afterHash = ""

    def converter(self, o: any) -> str:
        """
        Used to serialise objects that do not natively support being JSON stringified

        o : `any`

        Returns : `str`
        """
        return o.__str__()

    def __str__(self):
        return json.dumps(self._asdict(), default=self.converter)

    def _asdict(self) -> dict:
        return {
            "before": self.__before,
            "after": self.__after,
            "related_before": self.__related_before,
            "related_after": self.__related_after,
            "table": self.__table,
            "type": self.__type,
            "statement_consent_reqs": [str(req) for req in self.__statement_consent_reqs],
            "beforeHash": self.__beforeHash,
            "afterHash": self.__afterHash
        }

    @staticmethod
    def deserialise(json_string: str):
        """
        Transform a statement as a JSON string back into a python object.

        json_string : `str`

        Returns : `Statement`
        """
        statement_dict = json.loads(json_string)

        requiredFields = [
            "before",
            "after",
            "related_before",
            "related_after",
            "table",
            "type",
            "statement_consent_reqs",
            "beforeHash",
            "afterHash"]
        missingFields = []
        for field in requiredFields:
            if field not in statement_dict:
                missingFields.append(field)
        if len(missingFields) > 0:
            raise RuntimeError(f"Incomplete JSON statement, missing {missingFields}:"
                               f"{statement_dict}")

        requirements = []
        for requirement_str in statement_dict["statement_consent_reqs"]:
            requirements.append(StatementConsentRequirement.deserialise(requirement_str))

        statement = Statement(statement_dict["before"],
                              statement_dict["after"],
                              statement_dict["type"],
                              statement_dict["table"],
                              requirements)

        statement.set_beforeState_hash(statement_dict["beforeHash"])
        statement.set_afterState_hash(statement_dict["afterHash"])
        statement.set_related_beforeValues(statement_dict["related_before"])
        statement.set_related_afterValues(statement_dict["related_after"])

        return statement

    def get_before(self) -> dict:
        """
        Returns : `dict` - The before values of the statement
        """
        return self.__before

    def get_after(self) -> dict:
        """
        Returns : `dict` - The after values of the statement
        """
        return self.__after

    def get_table(self) -> str:
        """
        Returns : `str` - The table this statement updates
        """
        return self.__table

    def get_type(self) -> StatementType:
        """
        Returns : `StatementType` - The type of the statement
        """
        return self.__type

    def get_consent_reqs(self) -> List[StatementConsentRequirement]:
        """
        Returns : `List[StatementConsentRequirement]` - The consent requirements of the statement
        """
        return self.__statement_consent_reqs

    def set_before(self, new_before: dict):
        """
        new_before : `dict` - The before values to set in the statement
        """
        self.__before = new_before

    def set_after(self, new_after: dict):
        """
        new_after : `dict` - The after values to set in the statement
        """
        self.__after = new_after

    def set_related_beforeValues(self, related_before):
        self.__related_before = related_before

    def set_related_afterValues(self, related_after):
        self.__related_after = related_after

    def add_related_beforeValue(self, new_related_before):
        for key in new_related_before:
            self.__related_before[key] = new_related_before[key]

    def add_related_afterValue(self, new_related_after):
        for key in new_related_after:
            self.__related_after[key] = new_related_after[key]

    def get_related_afterValues(self):
        return self.__related_after

    def get_related_afterValues_str(self):
        return json.dumps(self.__related_after, sort_keys=True, default=self.converter)

    def get_related_beforeValues(self) -> dict:
        """
        Returns : 'dict' - the related values before the start of this entire transaction,
                  used for conflict detection.
        """
        return self.__related_before

    def get_extendedState_beforeStr(self):
        return json.dumps(dict(self.__before, **self.__related_before), sort_keys=True,
                               default=self.converter)

    def get_extendedState_afterStr(self):
        return json.dumps(dict(self.__after, **self.__related_after), sort_keys=True,
                               default=self.converter)

    def get_beforeState_hash(self) -> str:
        """
        Returns : `str` - The hash of the state before this statement
        """
        return self.__beforeHash

    def get_afterState_hash(self) -> str:
        """
        Returns : `str` - The hash of the state after this statement
        """
        return self.__afterHash

    def set_beforeState_hash(self, hash_val: str):
        """
        hash_val : `str` - The hash of the state before this statement
        """
        self.__beforeHash = hash_val

    def set_afterState_hash(self, hash_val: str):
        """
        hash_val : `str` - The hash of the state after this statement
        """
        self.__afterHash = hash_val

