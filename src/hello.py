"""
Hello, world!
"""

__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"

import core.BluberryLogging


logger = core.getLogger("Hello")

logger.debug("Hello, debug!")
logger.info("Hello, info!")
logger.warning("Hello, warning!")
logger.error("Hello, error!")
logger.critical("Hello, critical!")

