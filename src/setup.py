"""
Setup script for Bluberry.
This script generates all files necessary to set up a network group of Bluberry
nodes.
"""

__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2019, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import os
import shutil
import json
import secrets
import string
import random
from Crypto.PublicKey import RSA

import core.BluberryLogging

SEED_CONF = 'core/conf/seedConf.json'

logger = core.getLogger("Setup")
logger.info("Starting Bluberry setup")

# Load seed config
with open(SEED_CONF, 'r') as f:
    seedconf = json.loads(f.read())

# Set up directory
keyDir = os.environ['SETUP_DIR']
if os.path.exists(keyDir):
    shutil.rmtree(keyDir)

if not os.path.exists(keyDir):
    os.makedirs(keyDir)

# Generate general (group/broadcast) key
key = RSA.generate(2048)
with open(f"{keyDir}/broadcast_public.pem", "wb") as public,\
     open(f"{keyDir}/broadcast_private.pem", "wb") as private:
    private.write(key.export_key())
    public.write(key.publickey().export_key())

num_nodes = int(os.environ['NUM_NODES'])
node_prefix = os.environ['NODE_PREFIX']

# Generate unique, random node prefixes for all nodes
prefixes = []
while len(prefixes) < num_nodes:
    code = ''.join(random.choice(string.ascii_uppercase + string.digits) for l in range(3))
    if code not in prefixes:
        prefixes.append(code)

# Generate key files for nodes
logger.info(f"Generating keys & passwords")
nodeDirs = {}
passwords = {}
for n in range(num_nodes):
    # we want to start at 1, not 0
    node = f"{node_prefix}{n+1}"
    logger.debug(f"Generating keys for node {node}")
    nodeDir = f"{keyDir}/{node}"
    nodeDirs[node] = nodeDir
    if not os.path.exists(nodeDir):
        os.makedirs(nodeDir)
    if not os.path.exists(nodeDir + '/keys'):
        os.makedirs(nodeDir + '/keys')

    key = RSA.generate(2048)

    with open(f"{nodeDir}/keys/{node}_public.pem", "wb") as public,\
          open(f"{nodeDir}/keys/{node}_private.pem", "wb") as private:
        private.write(key.export_key())
        public.write(key.publickey().export_key())

    # Generate passwords for messaging
    alphabet = string.ascii_letters + string.digits + string.punctuation
    # for a 20-character password
    password = ''.join(secrets.choice(alphabet) for i in range(20))
    passwords[node] = password

# Need another loop as this loop requires all key files to exist
logger.info(f"Setting up node config")
for n in range(num_nodes):
    node = f"{node_prefix}{n+1}"
    logger.debug(f"Aggregating keys for node {node}")
    nodeDir = nodeDirs[node]

    # Copy broadcast key
    shutil.copy(f"{keyDir}/broadcast_public.pem",
                f"{nodeDirs[node]}/keys/broadcast_public.pem")
    shutil.copy(f"{keyDir}/broadcast_private.pem",
                f"{nodeDirs[node]}/keys/broadcast_private.pem")

    # "local" config
    seedconf['local']['nodeId'] = node
    seedconf['local']['networkPrefix'] = prefixes[n]
    oldDb = seedconf['local']['db']
    seedconf['local']['db'] = os.environ['DB']
    oldMsg = seedconf['local']['msg']
    seedconf['local']['msg'] = os.environ['MSG']
    seedconf['local']['basedir'] = os.environ['BB_WORKDIR']

    # replace with actual path to conf
    seedconf['local']['dbconf'] = seedconf['local']['dbconf']\
                                  .replace(oldDb, os.environ['DB'], 1)
    seedconf['local']['msgconf'] = seedconf['local']['msgconf']\
                                  .replace(oldMsg, os.environ['MSG'], 1)

    for otherNode in nodeDirs:
        if node != otherNode:
            # Copy all public keys
            shutil.copy(f"{nodeDirs[otherNode]}/keys/{otherNode}_public.pem",
                        f"{nodeDirs[node]}/keys/{otherNode}_public.pem")

        # generate conf file using key paths
        seedconf['nodes'][otherNode] = {}
        seedconf['nodes'][otherNode]['publicKey'] =\
            f"core/conf/keys/{otherNode}_public.pem"
        if node == otherNode:
            seedconf['nodes'][otherNode]['privateKey'] =\
                f"core/conf/keys/{otherNode}_private.pem"

        # store passwords
        seedconf['nodes'][otherNode]['password'] = passwords[otherNode]
        # TODO: obviously it's not ideal to have everybody's password here, but
        # they are needed to set up the messaging auth. This means they cannot
        # be generated inside the messaging container as we need to report them
        # back to the bluberry config so they can actually be used.
        # From a security point of view it's not too bad as messages are
        # encrypted so nothing gets leaked and they still need to be signed by
        # the recipient using their private key which others don't have.
        # The msg module setup script should delete these after using, with the
        # exception of its own key.

    logger.debug(f"Writing config file for node {node}")
    with open(f"{nodeDirs[node]}/conf.json", 'w') as f:
        json.dump(seedconf, f, indent=4)

logger.info("Finished Bluberry setup")

