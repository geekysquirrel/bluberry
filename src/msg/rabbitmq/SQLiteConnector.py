__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import sqlite3
from typing import List

import core.BluberryLogging
from core.BluberryMessage import Message

RECEIVED_TABLE = 'received'
REACHED_DESTINATION_TABLE = 'reached_destination'


class SQLiteConnector():
    """
    A SQLite interface for managing simple tables.
    """

    def __init__(self, dbfile: str):
        """
        The constructor for the SQLiteConnector

        Args:
            dbfile : str
                The path of the sqlite database file. Will be created if it doesn't exist.
        """
        self.__logger = core.getLogger(self.__class__.__name__)

        self.__db = dbfile

        self.__update(f"CREATE TABLE IF NOT EXISTS {RECEIVED_TABLE} ("
                      "origin      TEXT NOT NULL, "
                      "destination TEXT NOT NULL, "
                      "msgid       TEXT NOT NULL, "
                      "PRIMARY KEY (origin, msgid)"
                      ");")

        self.__update("CREATE TABLE IF NOT EXISTS "
                      f"{REACHED_DESTINATION_TABLE} ("
                      "origin      TEXT NOT NULL, "
                      "destination TEXT NOT NULL, "
                      "msgid       TEXT NOT NULL, "
                      "PRIMARY KEY (origin, msgid)"
                      ");")

    # Public methods ##########################################################

    def clear(self):
        """
        Delete all data from the database
        """
        self.__update(f"DELETE FROM {RECEIVED_TABLE}")
        self.__update(f"DELETE FROM {REACHED_DESTINATION_TABLE}")

    def markReceived(self, messages: List[Message] = []):
        """
        Mark a message a received locally by storing its ID
        in the sqlite database.

        Args:
            messages : List[Message]
                The messages we want to mark as having been received.
        """
        self.__mark(RECEIVED_TABLE, messages)

    def markReachedDestination(self, messages: List[Message] = []):
        """
        Mark a message a receivedby its final destination by storing its ID
        in the sqlite database.

        Args:
            messages : List[Message]
                The messages we want to mark as having reached their destination.
        """
        self.__mark(REACHED_DESTINATION_TABLE, messages)
        # TODO: check if the signature is correct

    def isReceived(self, msg: Message):
        """
        Check whether a given message has been received by this node.

        Args:
            msg : Message
                The message we want to check.
        """
        return self.__contains(RECEIVED_TABLE, msg)

    def reachedDestination(self, msg: Message):
        """
        Check whether a given message has been received by its recipient.

        Args:
            msg : Message
                The message we want to check.
        """
        return self.__contains(REACHED_DESTINATION_TABLE, msg)

    def getDb(self):
        return self.__db

    # Private methods #########################################################

    def __mark(self, table: str, messages: List[Message]):
        """
        Mark a message in a given table.

        Args:
            table: str
                The table in which to mark the messages
            messages : List[Message]
                The messages we want to mark.
        """
        marked = 0
        skipped = 0

        for msg in messages:
            sql = f"INSERT INTO {table} "\
                  "(origin, destination, msgid) VALUES (?, ?, ?)"
            args = [msg.getOriginNode(), msg.getDestination(), msg.getId()]

            try:
                self.__update(sql, args)
                marked += 1
            except sqlite3.IntegrityError:
                skipped += 1

        self.__logger.debug(f"Marked {marked} messages as received in table "
                            f"{table}, skipped {skipped} already marked messages.")

    def __contains(self, table: str, msg: Message):
        """
        Check whether the table contains the message

        Args:
            table: str
                The table in which to mark the message
            messages : Message
                The message we want to mark.
        """
        result = self.__query(f"SELECT * FROM {table} "
                              "WHERE origin=? and msgid=?",
                              [msg.getOriginNode(), msg.getId()])

        if (result is not None and len(result) > 0
                and result[0][0] == msg.getOriginNode()
                and result[0][1] == msg.getDestination()
                and result[0][2] == msg.getId()):
            return True
        return False

    def __query(self, sql: str, args: List[str] = []):
        """
        Execute a (read-only) query against the database.

        Args:
            sql: str
                The SQL query string
            args : List[str]
                Any arguments to replace variables in the `sql` argument. This is to
                prevent SQL injections and will escape properly.
        """
        conn = sqlite3.connect(self.__db)
        c = conn.cursor()
        c.execute(sql, tuple(args))
        result = c.fetchall()
        conn.close()
        return result

    def __update(self, sql: str, args: List[str] = []):
        """
        Execute an update against the database.

        Args:
            sql: str
                The SQL query string
            args : List[str]
                Any arguments to replace variables in the `sql` argument. This is to
                prevent SQL injections and will escape properly.
        """
        conn = sqlite3.connect(self.__db)
        try:
            conn.cursor().execute(sql, tuple(args))
            conn.commit()
        except sqlite3.IntegrityError as e:
            conn.rollback()
            raise(e)
        finally:
            conn.close()

