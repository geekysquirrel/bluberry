#!/bin/bash

echo "Enabling web-ui"
rabbitmq-plugins enable rabbitmq_management

# Wait for RabbitMQ startup
for (( ; ; )) ; do
  sleep 1
  rabbitmqctl -q node_health_check &> /dev/null
  if [ $? -eq 0 ] ; then
    echo "$0 `date` rabbitmq is now running"
    break
  else
    echo "$0 `date` waiting for rabbitmq startup"
  fi
done

# Create default user
( rabbitmqctl add_user pika $PASSWORD 2>/dev/null; \
rabbitmqctl set_user_tags pika administrator; \
rabbitmqctl set_permissions -p / pika ".*" ".*" ".*"; )

if [ $NUM_NODES -gt 0 ] ; then

    echo "Setting up RabbitMQ users for $NUM_NODES nodes (this node: $THIS_NODE)"

    n=1 ; while [ $n -le $NUM_NODES ] ; do

        # Create Rabbitmq user
        # TODO: we may want to have different passwords here
        rabbitmqctl add_user $NODE_PREFIX$n $PASSWORD 2>/dev/null

        if [ $NODE_PREFIX$n = $THIS_NODE ]
        then
            echo "Setting admin permissions for $THIS_NODE"
            # The host node has all permissions
            rabbitmqctl set_user_tags $NODE_PREFIX$n administrator
            rabbitmqctl set_permissions -p / $NODE_PREFIX$n ".*" ".*" ".*"
        else
            echo "Setting user permissions for $NODE_PREFIX$n"
            # All other nodes can read everybody's queue but only delete from their own
            rabbitmqctl set_user_tags $NODE_PREFIX$n user
            # permissions are (in that order):
            # - configure (configure permission required for reading queue length)
            # - write (write/delete only for own queue and management queue)
            # - read (can read queues for any node)
            rabbitmqctl set_permissions -p / $NODE_PREFIX$n \
                        "^($NODE_PREFIX[0-9]+|bluberry-management|amq.default)$" \
                        "^($NODE_PREFIX$n|bluberry-management|amq.default)$" \
                        "^($NODE_PREFIX[0-9]+|bluberry-management|amq.default)$"
        fi

        ((n = n + 1)) ;
    done
fi

