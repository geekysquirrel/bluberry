#!/bin/bash

# Run setup in the background - it will wait until the rabbit server is up.
bash /conf/setup.sh &
/usr/local/bin/docker-entrypoint.sh rabbitmq-server
