__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import unittest
from datetime import datetime
from typing import List
from unittest.mock import PropertyMock, patch

import core.BluberryLogging
from core.BluberryConf import Conf
from core.BluberryMessage import Message
from msg.rabbitmq.RabbitMQConnector import RabbitMQConnector

BB_MANAGEMENT = 'bluberry-management'


class itest_BluberryRabbitMQConnector(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")
        cls.__conf = Conf('core/conf/conf.json')
        # use master user/pw to get r/w access to all queues on all nodes
        # TODO: get master password from elsewhere
        cls.rootRabbit = RabbitMQConnector(cls.__conf, 'virt', 'pika', 'pika')

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")

        # Make sure queues are empty prior to test
        for node in self.__conf.get('nodes'):
            self.rootRabbit.clearQueue(node, BB_MANAGEMENT)
            self.rootRabbit.clearQueue(node, 'foo')
            for queue in self.__conf.get('nodes'):
                self.rootRabbit.clearQueue(node, queue)

        self.rabbit = RabbitMQConnector(self.__conf, 'virt')

    def tearDown(self):
        self.__logger.info(f"Finished test {self._testMethodName}")

    def createMessage(self, origin: str, destination: str, payload: str) -> Message:
        msg = Message()
        msg.setOriginNode(origin)
        msg.setDestination(destination)
        msg.setOriginTimestamp(datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%fZ'))
        msg.setPayload(payload)
        return msg

    def sendTestMessages(self, node: str, qname: str, messages: List[str]) -> List[Message]:
        msgObjects = []
        for m in messages:
            msgObjects.append(self.createMessage(node, qname, m))

        if len(msgObjects) > 0:
            self.rootRabbit.publish(node, msgObjects)
        else:
            self.__logger.warn('No messages to send')

        return msgObjects

    @patch.object(RabbitMQConnector, '_RabbitMQConnector__channel', new_callable=PropertyMock)
    def test_getMessages_exception(self, mockChannel):
        # make the method throw an exception
        # since it's a PropertyMock it needs to be called using its getter
        mockChannel().basic_get.side_effect = ValueError('Mock error')
        mockChannel().queue_declare.method.message_count = 1
        self.assertRaises(RuntimeError, self.rabbit.getMessages, 'node1', 'node3')

    def test_getMessages_int_self(self):
        self.rabbit = RabbitMQConnector(self.__conf, 'int')
        self.sendTestMessages('node1', 'node3', ['test13'])
        result = self.rabbit.getMessages('node1', 'node3')
        self.assertEqual(len(result), 1)

    def test_getMessages_int_foreign(self):
        self.rabbit = RabbitMQConnector(self.__conf, 'int')
        self.assertRaises(RuntimeError, self.rabbit.getMessages, 'node2', 'node3')

    def test_getMessages_ext_self(self):
        self.rabbit = RabbitMQConnector(self.__conf, 'ext')
        self.sendTestMessages('node1', 'node3', ['test13'])
        result = self.rabbit.getMessages('node1', 'node3')
        self.assertEqual(len(result), 1)

    def test_getMessages_ext_foreign(self):
        self.rabbit = RabbitMQConnector(self.__conf, 'ext')
        self.sendTestMessages('node2', 'node3', ['test23'])
        result = self.rabbit.getMessages('node2', 'node3')
        self.assertEqual(len(result), 1)

    def test_getMessages_virt_self(self):
        self.rabbit = RabbitMQConnector(self.__conf, 'virt')
        self.sendTestMessages('node1', 'node3', ['test13'])
        result = self.rabbit.getMessages('node1', 'node3')
        self.assertEqual(len(result), 1)

    def test_getMessages_virt_foreign(self):
        self.rabbit = RabbitMQConnector(self.__conf, 'virt')
        self.sendTestMessages('node2', 'node3', ['test23'])
        result = self.rabbit.getMessages('node2', 'node3')
        self.assertEqual(len(result), 1)

    def test_getMessages_wrong_username(self):
        self.rabbit = RabbitMQConnector(self.__conf, 'virt', 'foo', 'pika')
        self.assertRaises(RuntimeError, self.rabbit.getMessages, 'node1', 'node3')

    def test_getMessages_wrong_password(self):
        self.rabbit = RabbitMQConnector(self.__conf, 'virt', 'pika', 'foo')
        self.assertRaises(RuntimeError, self.rabbit.getMessages, 'node1', 'node3')

    def test_getMessages_wrong_node(self):
        self.assertRaises(RuntimeError, self.rabbit.getMessages, 'foo', 'node1')

    def test_getMessages_wrong_queue(self):
        self.assertRaises(RuntimeError, self.rabbit.getMessages, 'node1', 'foo')

    def test_getMessages_from_self_empty(self):
        result = self.rabbit.getMessages('node1', 'node3')
        self.assertEqual(result, [])

    def test_getMessages_from_self_not_empty_dont_delete_implicit(self):
        self.sendTestMessages('node1', 'node3', ['test13'])
        result = self.rabbit.getMessages('node1', 'node3')
        self.assertEqual(len(result), 1)

    def test_getMessages_from_self_not_empty_dont_delete_explicit(self):
        self.sendTestMessages('node1', 'node3', ['test13'])
        result = self.rabbit.getMessages('node1', 'node3', False)
        self.assertEqual(len(result), 1)

    def test_getMessages_from_self_not_empty_delete(self):
        self.sendTestMessages('node1', 'node3', ['test13'])
        result = self.rabbit.getMessages('node1', 'node3', True)
        self.assertEqual(len(result), 1)
        result = self.rabbit.getMessages('node1', 'node3')
        self.assertEqual(result, [])

    def test_getMessages_from_other_node_for_foreign_empty(self):
        result = self.rabbit.getMessages('node2', 'node3')
        self.assertEqual(result, [])

    def test_getMessages_from_other_node_for_foreign_dont_delete_implicit(self):
        self.sendTestMessages('node2', 'node3', ['test23'])
        result = self.rabbit.getMessages('node2', 'node3')
        self.assertEqual(len(result), 1)

    def test_getMessages_from_other_node_for_foreign_dont_delete_explicit(self):
        self.sendTestMessages('node2', 'node3', ['test23'])
        result = self.rabbit.getMessages('node2', 'node3', False)
        self.assertEqual(len(result), 1)

    def test_getMessages_from_other_node_for_foreign_delete(self):
        self.sendTestMessages('node2', 'node3', ['test23'])
        result = self.rabbit.getMessages('node2', 'node3', True)
        self.assertEqual(len(result), 1)
        result = self.rabbit.getMessages('node2', 'node3')
        # delete should not have worked as this is not for us
        self.assertEqual(len(result), 1)

    def test_getMessages_from_other_node_for_self_empty(self):
        result = self.rabbit.getMessages('node2', 'node1')
        self.assertEqual(result, [])

    def test_getMessages_from_other_node_for_self_dont_delete_implicit(self):
        self.sendTestMessages('node2', 'node1', ['test21'])
        result = self.rabbit.getMessages('node2', 'node1')
        self.assertEqual(len(result), 1)

    def test_getMessages_from_other_node_for_self_dont_delete_explicit(self):
        self.sendTestMessages('node2', 'node1', ['test21'])
        result = self.rabbit.getMessages('node2', 'node1', False)
        self.assertEqual(len(result), 1)

    def test_getMessages_from_other_node_for_self_delete(self):
        self.sendTestMessages('node2', 'node1', ['test21'])
        result = self.rabbit.getMessages('node2', 'node1', True)
        self.assertEqual(len(result), 1)
        result = self.rabbit.getMessages('node2', 'node1')
        self.assertEqual(result, [])

    def test_publish(self):
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 0)
        messages = [self.createMessage('node2', 'node3', 'test23a'),
                    self.createMessage('node2', 'node3', 'test23b'),
                    self.createMessage('node2', 'node3', 'test23c')]
        self.rabbit.publish('node1', messages)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 3)

    def test_publish_override_queue(self):
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 0)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'foo'), 0)
        messages = [self.createMessage('node2', 'node3', 'test23a'),
                    self.createMessage('node2', 'node3', 'test23b'),
                    self.createMessage('node2', 'node3', 'test23c')]
        self.rabbit.publish('node1', messages, 'foo')
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 0)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'foo'), 3)

    def test_publish_filter_self_to_self(self):
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node1'), 0)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node2'), 0)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 0)
        messages = [self.createMessage('node1', 'node1', 'test11'),
                    self.createMessage('node1', 'node2', 'test12'),
                    self.createMessage('node2', 'node3', 'test23')]
        self.rabbit.publish('node1', messages)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node1'), 0)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node2'), 1)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 1)

    @patch.object(RabbitMQConnector, '_RabbitMQConnector__channel', new_callable=PropertyMock)
    def test_publish_exception(self, mockChannel):
        mockChannel().basic_publish.side_effect = ValueError('Mock error')
        messages = [self.createMessage('node2', 'node3', 'test23')]
        self.assertRaises(RuntimeError, self.rabbit.publish, 'node1', messages)

    def test_dropMessages_empty(self):
        result = self.rabbit.dropMessages([])
        self.assertEqual(result, [])

    def test_dropMessages_non_existent(self):
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 0)
        messages = [self.createMessage('node1', 'node3', 'test13a'),
                    self.createMessage('node1', 'node3', 'test13b'),
                    self.createMessage('node1', 'node3', 'test13c')]
        result = self.rabbit.dropMessages(messages)
        self.assertEqual(result, [])

    def test_dropMessages_existing(self):
        messages1 = self.sendTestMessages('node1', 'node2', ['test12a', 'test12b', 'test12c'])
        self.sendTestMessages('node1', 'node3', ['test13a', 'test13b', 'test13c'])
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node2'), 3)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 3)
        result = self.rabbit.dropMessages(messages1)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node2'), 0)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 3)
        self.assertEqual(result, messages1)

    def test_dropMessages_foreign(self):
        messages = self.sendTestMessages('node3', 'node2', ['test1', 'test2', 'test3'])
        self.assertEqual(self.rabbit.getQueueSize('node3', 'node2'), 3)
        result = self.rabbit.dropMessages(messages)
        # cannot drop messages from foreign node
        self.assertEqual(self.rabbit.getQueueSize('node3', 'node2'), 3)
        self.assertEqual(result, [])

    def test_dropMessages_faulty(self):
        messages = [self.createMessage('node1', 'node2', 'foo'),
                    self.createMessage('node1', 'node3', 'bar'),
                    self.createMessage('node1', 'node1', 'foobar'),
                    self.createMessage('node1', 'foo', 'x'),
                    self.createMessage('foo', 'bar', 'x')]
        result = self.rabbit.dropMessages(messages)
        self.assertEqual(result, [])

    def test_getQueueSize_empty(self):
        result = self.rabbit.getQueueSize('node2', 'node3')
        self.assertEqual(result, 0)

    def test_getQueueSize_not_empty(self):
        self.sendTestMessages('node1', 'node3', ['test31a', 'test31b', 'test31c'])
        result = self.rabbit.getQueueSize('node1', 'node3')
        self.assertEqual(result, 3)

    def test_getQueueSize_no_permission(self):
        result = self.rabbit.getQueueSize('node2', BB_MANAGEMENT)
        self.assertEqual(result, 0)

    @patch.object(RabbitMQConnector, '_RabbitMQConnector__channel', new_callable=PropertyMock)
    def test_getQueueSize_exception(self, mockChannel):
        # make the method throw an exception
        mockChannel().queue_declare.side_effect = ValueError('Mock error')
        self.assertRaises(RuntimeError, self.rabbit.getQueueSize, 'node1', 'node3')

    def test_clearQueue(self):
        self.sendTestMessages('node1', 'node3', ['test31a', 'test31b', 'test31c'])
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 3)
        # try again - should still be the same
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 3)
        self.rabbit.clearQueue('node1', 'node3')
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 0)

    @patch.object(RabbitMQConnector, '_RabbitMQConnector__channel', new_callable=PropertyMock)
    def test_clearQueue_exception(self, mockChannel):
        # make the method throw an exception
        mockChannel().queue_delete.side_effect = ValueError('Mock error')
        # ensure the other parts of the code don't also throw an exception
        mockChannel().queue_declare.method.message_count.return_value = 99
        self.assertRaises(RuntimeError, self.rabbit.clearQueue, 'node1', 'node3')


if __name__ == "__main__":
    unittest.main()

