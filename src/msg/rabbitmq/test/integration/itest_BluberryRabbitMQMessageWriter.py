__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2018, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import unittest
import sqlite3
from datetime import datetime

import core.BluberryLogging
from core.BluberryConf import Conf
from core.BluberryMessage import Message
from msg.rabbitmq.SQLiteConnector import SQLiteConnector
from msg.rabbitmq.RabbitMQConnector import RabbitMQConnector
from msg.rabbitmq.BluberryRabbitMQMessageWriter\
     import BluberryRabbitMQMessageWriter

BB_MANAGEMENT = 'bluberry-management'
RECEIVED_TABLE = 'received'


class itest_BluberryRabbitMQMessageWriter(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        core.setLogLevel('BluberryConf', 'warning')
        core.setLogLevel('BluberryRabbitMQMessageWriter', 'debug')
        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")
        cls.__conf = Conf('core/conf/conf.json')
        cls.__db = SQLiteConnector("bluberry_rabbitmq_"
                                   f"{cls.__conf.get('local', 'nodeId')}.sqlite")
        # use master user/pw to get r/w access to all queues on all nodes
        # TODO: get master password from elsewhere
        cls.rootRabbit = RabbitMQConnector(cls.__conf, 'virt', 'pika', 'pika')

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")
        self.rabbit = RabbitMQConnector(self.__conf, 'virt')
        self.w = BluberryRabbitMQMessageWriter(self.__conf)

    def tearDown(self):
        for node in self.__conf.get('nodes'):
            self.rootRabbit.clearQueue(node, BB_MANAGEMENT)
            for queue in self.__conf.get('nodes'):
                self.rootRabbit.clearQueue(node, queue)
        self.w = None
        self.__logger.info(f"Finished test {self._testMethodName}")

    def createMessage(self, origin: str, destination: str, payload: str) -> Message:
        msg = Message()
        msg.setOriginNode(origin)
        msg.setDestination(destination)
        msg.setOriginTimestamp(datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%fZ'))
        msg.setPayload(payload)
        return msg

    def test_publish_one(self):
        self.rabbit.clearQueue('node1', 'node2')
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node2'), 0)
        messages = [self.createMessage('node1', 'node2', 'test12')]
        self.w.publish(messages)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node2'), 1)

    def test_publish_self_to_self(self):
        # should be dropped
        self.rabbit.clearQueue('node1', 'node1')
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node1'), 0)
        messages = [self.createMessage('node1', 'node1', 'test11')]
        self.w.publish(messages)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node1'), 0)

    def test_publish_foreign(self):
        self.rabbit.clearQueue('node1', 'node3')
        self.rabbit.clearQueue('node2', 'node3')
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 0)
        self.assertEqual(self.rabbit.getQueueSize('node2', 'node3'), 0)
        messages = [self.createMessage('node2', 'node3', 'test23')]
        self.w.publish(messages)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 1)
        self.assertEqual(self.rabbit.getQueueSize('node2', 'node3'), 0)

    def test_publish_multiple(self):
        self.rabbit.clearQueue('node1', 'node2')
        self.rabbit.clearQueue('node1', 'node3')
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node2'), 0)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 0)
        messages = [self.createMessage('node1', 'node2', 'test12'),
                    self.createMessage('node1', 'node3', 'test131'),
                    self.createMessage('node1', 'node3', 'test132')]
        self.w.publish(messages)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node2'), 1)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 2)

    def test_discard(self):
        self.rabbit.clearQueue('node1', 'node2')
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node2'), 0)
        messages = [self.createMessage('node3', 'node2', 'test32')]
        self.rabbit.publish('node1', messages)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node2'), 1)
        self.w.discard(messages)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node2'), 0)

    def test_notify_self(self):
        # has no effect
        self.rabbit.clearQueue('node1', BB_MANAGEMENT)
        self.assertEqual(self.rabbit.getQueueSize('node1', BB_MANAGEMENT), 0)
        messages = [self.createMessage('node2', 'node3', 'test23')]
        self.w.notify('node1', messages)
        self.assertEqual(self.rabbit.getQueueSize('node1', BB_MANAGEMENT), 0)

    def test_notify_foreign(self):
        # Notifying other node will queue message
        self.rootRabbit.clearQueue('node2', BB_MANAGEMENT)
        self.assertEqual(self.rootRabbit.getQueueSize('node2', BB_MANAGEMENT), 0)
        messages = [self.createMessage('node1', 'node3', 'test13'),
                    self.createMessage('node2', 'node3', 'test23'),
                    self.createMessage('node3', 'node2', 'test32')]
        self.w.notify('node2', messages)
        self.assertEqual(self.rootRabbit.getQueueSize('node2', BB_MANAGEMENT), 3)

    def test_reset(self):
        self.w.reset()
        # check the database really *is* clear
        conn = sqlite3.connect(self.__db.getDb())
        c = conn.cursor()
        c.execute(f"SELECT COUNT(*) FROM {RECEIVED_TABLE}")
        num_received = c.fetchone()[0]
        conn.close()
        self.assertEqual(num_received, 0)


if __name__ == "__main__":
    unittest.main()

