__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import unittest
from datetime import datetime
from typing import List

import core.BluberryLogging
from core.BluberryConf import Conf
from core.BluberryMessage import Message
from msg.rabbitmq.RabbitMQConnector import RabbitMQConnector
from msg.rabbitmq.SQLiteConnector import SQLiteConnector
from msg.rabbitmq.BluberryRabbitMQMessageReader\
     import BluberryRabbitMQMessageReader

BB_MANAGEMENT = 'bluberry-management'


class itest_BluberryRabbitMQMessageReader(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.__conf = Conf('core/conf/conf.json')
        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")
        # use master user/pw to get r/w access to all queues on all nodes
        # TODO: get master password from elsewhere
        cls.rootRabbit = RabbitMQConnector(cls.__conf, 'virt', 'pika', 'pika')

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")
        self.rabbit = RabbitMQConnector(self.__conf, 'virt')
        self.db = SQLiteConnector("bluberry_rabbitmq_"
                  f"{self.__conf.get('local','nodeId')}.sqlite")
        self.r = BluberryRabbitMQMessageReader(self.__conf)
        self.r.reset()

        # Make sure queues are empty prior to test
        for node in self.__conf.get('nodes'):
            self.rabbit.clearQueue(node, BB_MANAGEMENT)
            for queue in self.__conf.get('nodes'):
                self.rabbit.clearQueue(node, queue)

    def tearDown(self):
        self.r = None
        self.__logger.info(f"Finished test {self._testMethodName}")

    def sendTestMessages(self, node: str, qname: str, messages: List[str]):
        msgObjects = []
        for m in messages:
            msgObjects.append(self.createMessage(node, qname, m))

        if len(msgObjects) > 0:
            self.rootRabbit.publish(node, msgObjects)
        else:
            self.__logger.warn('No messages to send')

    def createMessage(self, origin: str, destination: str, payload: str)\
                     -> Message:
        msg = Message()
        msg.setOriginNode(origin)
        msg.setDestination(destination)
        msg.setOriginTimestamp(datetime.now()
                               .strftime('%Y-%m-%dT%H:%M:%S.%fZ'))
        msg.setPayload(payload)
        return msg

    def test_readMessages(self):

        self.sendTestMessages('node1', 'node2', ['test12'])
        self.sendTestMessages('node1', 'node3', ['test13'])
        self.sendTestMessages('node2', 'node1', ['test21'])
        self.sendTestMessages('node2', 'node3', ['test23'])
        self.sendTestMessages('node3', 'node1', ['test31'])
        self.sendTestMessages('node3', 'node2', ['test32'])
        oldmsg = self.createMessage('node2', 'node3', 'old msg')
        self.rootRabbit.publish('node2', [oldmsg])
        self.db.markReachedDestination([oldmsg])

        messages = self.r.readMessages()
        mgmt = self.r.peek('node2', BB_MANAGEMENT)

        # We should get all messages from the other two nodes only.
        self.assertEqual(len(messages), 5)
        for msg in messages:
            # check no messages from the node itself have been read
            self.assertNotEqual(msg.getOriginNode(), 'node1')
            # check no messages from another node to that same node exist
            self.assertNotEqual(msg.getOriginNode(), msg.getDestination())

            # Make sure messages that have reached their final destination are
            # copied onto the internal management queue
            if msg.getId() == oldmsg.getId():
                found = False
                for mmsg in mgmt:
                    if mmsg.getId() == msg.getId():
                        found = True
                        break
                self.assertTrue(found)

        # Check previously received messages are filtered out
        messages = self.r.readMessages()
        self.assertEqual(len(messages), 0)

    def test_checkNotifications(self):
        messages = [self.createMessage('node2', 'node3', 'test23a'),
                    self.createMessage('node2', 'node3', 'test23b'),
                    self.createMessage('node2', 'node3', 'test23c')]
        self.rabbit.publish('node1', messages)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 3)
        self.rabbit.publish('node1', messages[0:2], BB_MANAGEMENT)
        self.assertEqual(self.rabbit.getQueueSize('node1', BB_MANAGEMENT), 2)
        self.r.checkNotifications()
        self.assertEqual(self.rabbit.getQueueSize('node1', BB_MANAGEMENT), 0)
        self.assertEqual(self.rabbit.getQueueSize('node1', 'node3'), 1)

    def test_peek(self):
        self.rabbit.clearQueue('node3', 'node2')
        self.sendTestMessages('node3', 'node2',
                              ['pika', 'pika pikaaa', 'pikachu'])
        messages = self.r.peek('node3', 'node2')
        self.assertEqual(len(messages), 3)
        # repeating this as peek should not reduce the number of messages
        messages = self.r.peek('node3', 'node2')
        self.assertEqual(len(messages), 3)

    def test_pop(self):
        self.rabbit.clearQueue('node2', 'node1')
        self.sendTestMessages('node2', 'node1',
                              ['pika', 'pika pikaaa', 'pikachu'])
        messages = self.r.pop('node2', 'node1')
        self.assertEqual(len(messages), 3)
        # repeating this as pop should have reduced the number of messages
        messages = self.r.pop('node2', 'node1')
        self.assertEqual(len(messages), 0)

        # try to pop from queue for node other than self (should fail) due to
        # lack of write permissions
        self.sendTestMessages('node3', 'node2', ['test32'])
        messages = self.r.pop('node3', 'node2')
        self.assertEqual(len(messages), 1)
        messages = self.r.pop('node3', 'node2')
        self.assertEqual(len(messages), 1)


if __name__ == "__main__":
    unittest.main()

