"""
BluberryRabbitMQMessageWriter test file
"""

__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2018, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import unittest
import os
import sqlite3
from datetime import datetime

import core.BluberryLogging
from core.BluberryMessage import Message
from msg.rabbitmq.SQLiteConnector import SQLiteConnector

TEST_DB_FILE = 'test-sqliteconnector.db'
RECEIVED_TABLE = 'received'


class test_SQLiteConnector(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        core.setLogLevel('BluberryConf', 'warning')
        core.setLogLevel('SQLiteConnector', 'debug')

        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")
        self.db = SQLiteConnector(TEST_DB_FILE)

    def tearDown(self):
        os.remove(TEST_DB_FILE)
        self.db = None
        self.__logger.info(f"Finished test {self._testMethodName}")

    def createMessage(self, origin: str, destination: str) -> Message:
        msg = Message()
        msg.setOriginNode(origin)
        msg.setDestination(destination)
        msg.setOriginTimestamp(datetime.now()
                               .strftime('%Y-%m-%dT%H:%M:%S.%fZ'))
        msg.setPayload('test')
        return msg

    def getNumReceived(self):
        conn = sqlite3.connect(self.db.getDb())
        c = conn.cursor()
        c.execute(f"SELECT COUNT(*) FROM {RECEIVED_TABLE}")
        num_received = c.fetchone()[0]
        conn.close()
        return num_received

    def test_clear(self):

        messages = [self.createMessage('node1', 'node2'),
                    self.createMessage('node1', 'node3'),
                    self.createMessage('node1', 'node2')]
        self.db.markReceived(messages)
        self.assertNotEqual(self.getNumReceived(), 0)
        self.db.clear()
        self.assertEqual(self.getNumReceived(), 0)

    def test_markReceived(self):

        self.assertEqual(self.getNumReceived(), 0)
        # receiving one message on a virgin db
        msg = self.createMessage('node2', 'node1')
        self.db.markReceived([msg])
        self.assertEqual(self.getNumReceived(), 1)

        # receive same message again
        self.db.markReceived([msg])
        self.assertEqual(self.getNumReceived(), 1)

        # receive messages with the same source/dest
        messages = [self.createMessage('node1', 'node2'),
                    self.createMessage('node1', 'node3'),
                    self.createMessage('node1', 'node2')]
        self.db.markReceived(messages)
        self.assertEqual(self.getNumReceived(), 4)

        # receive some duplicate messages some new
        messages = [self.createMessage('node3', 'node1'),
                    msg,
                    self.createMessage('node3', 'node2')]
        self.db.markReceived(messages)
        self.assertEqual(self.getNumReceived(), 6)

    def test_markReachedDestination(self):
        # TODO: implement
        pass

    def test_isReceived(self):
        msg = self.createMessage('node1', 'node2')
        self.assertFalse(self.db.isReceived(msg))
        self.db.markReceived([msg])
        self.assertTrue(self.db.isReceived(msg))

    def test_reachedDestination(self):
        msg = self.createMessage('node1', 'node2')
        self.assertFalse(self.db.reachedDestination(msg))
        self.db.markReachedDestination([msg])
        self.assertTrue(self.db.reachedDestination(msg))

    def test_getDb(self):
        self.assertEqual(self.db.getDb(), TEST_DB_FILE)


if __name__ == "__main__":
    unittest.main()

