__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2019, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import logging
from typing import List

import core.BluberryLogging
from core.IBluberryMessageReader import IBluberryMessageReader
from core.BluberryMessage import Message
from core.BluberryConf import Conf
from msg.rabbitmq.RabbitMQConnector import RabbitMQConnector
from msg.rabbitmq.SQLiteConnector import SQLiteConnector


logging.basicConfig(level=logging.WARNING)
logging.getLogger("pika").setLevel(logging.CRITICAL)

BB_MANAGEMENT = 'bluberry-management'


class BluberryRabbitMQMessageReader(IBluberryMessageReader):
    """
    BluberryDB message reader for the RabbitMQ messaging layer
    """

    def __init__(self, conf: Conf):
        self.__logger = core.getLogger(self.__class__.__name__)
        self.__conf = conf
        self.__rabbit = RabbitMQConnector(conf)
        self.__db = SQLiteConnector("bluberry_rabbitmq_"
                                    f"{self.__conf.get('local','nodeId')}.sqlite")

    # Methods implementing IBluberryMessageReader ############################

    def readMessages(self) -> List[Message]:
        self.__logger.info(f"{self.__conf.get('local','nodeId')} reading "
                           "foreign messages")
        messages = []

        for n in self.__conf.get('nodes'):
            # No need to get messages from the node itself!
            if n != self.__conf.get('local', 'nodeId'):
                self.__logger.debug(f"Getting messages from node {n}")
                messages.extend(self.__getAllMessages(n))
            else:
                self.__logger.debug(f"Skipping messages from node "
                                    f"{self.__conf.get('local', 'nodeId')}'s "
                                    "own message queue.")

        return messages

    def checkNotifications(self):
        messages = self.pop(self.__conf.get('local', 'nodeId'), BB_MANAGEMENT)
        self.__db.markReachedDestination(messages)
        self.__rabbit.dropMessages(messages)

    # Implementation-specific methods #########################################

    def reset(self):
        """
        Reset the message tracking, meaning no messages have yet been received.
        """
        self.__db.clear()

    def peek(self, node: str, queue: str) -> List[Message]:
        """
        Get messages from a given queue on a given node but don't delete them
        after reading.

        Args:
            node : str
                The node whose messages to access.
            queue : str
                The queue on that node that should be read.

        Returns:
            An (ordered) list of messages from that queue on that node.
        """
        return self.__rabbit.getMessages(node, queue, False)

    def pop(self, node: str, queue: str) -> List[Message]:
        """
        Get messages from a given queue on a given node but do delete them
        after reading.

        Args:
            node : str
                The node whose messages to access.
            queue : str
                The queue on that node that should be read.

        Returns:
            An (ordered) list of messages from that queue on that node.
        """
        return self.__rabbit.getMessages(node, queue, True)

    # Private methods #########################################################

    def __getAllMessages(self, node: str) -> List[Message]:
        """
        Get all messages from the given node.

        Args:
            node : str
                The node whose messages to access.

        Returns:
            An (ordered) list of messages from that queue on that node.
        """

        messages = []

        for n in self.__conf.get('nodes'):
            # skip the queue for the node itself - it will always be empty
            if n == node:
                continue
            if n == self.__conf.get('local', 'nodeId'):
                # consume messages for which we are the recipient
                messages.extend(self.pop(node, n))
            else:
                # just look at messages for others
                foreignMessages = self.peek(node, n)
                # keep all received messages for now
                messages.extend(foreignMessages)

        newMessages = []
        oldMessages = []
        toBeDeleted = []
        for msg in messages:

            if self.__db.reachedDestination(msg):
                self.__logger.debug(f"Message {msg.getId()} from node {msg.getOriginNode()}"
                                    f" to node {msg.getDestination()}, received from node "
                                    f"{node} has already been received by its final "
                                    "destination. It will be removed from all queues.")
                toBeDeleted.append(msg)

            if self.__db.isReceived(msg):
                oldMessages.append(msg.getId())
            else:
                self.__logger.debug(f"Received new message {msg.getId()} from "
                                    f"node {msg.getOriginNode()}.")
                newMessages.append(msg)

        self.__logger.debug(f"Skipped {len(oldMessages)} old messages.")

        # notify the node about already received messages
        self.__rabbit.publish(node, toBeDeleted, BB_MANAGEMENT)

        self.__db.markReceived(newMessages)

        newMessages.sort()
        return newMessages

