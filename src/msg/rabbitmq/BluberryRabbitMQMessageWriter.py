__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2019, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


from typing import List

import core.BluberryLogging
from core.IBluberryMessageWriter import IBluberryMessageWriter
from core.BluberryMessage import Message
from core.BluberryConf import Conf
from msg.rabbitmq.RabbitMQConnector import RabbitMQConnector
from msg.rabbitmq.SQLiteConnector import SQLiteConnector

BB_MANAGEMENT = 'bluberry-management'


class BluberryRabbitMQMessageWriter(IBluberryMessageWriter):
    """
    BluberryDB message writer for the RabbitMQ messaging layer.
    """

    def __init__(self, conf: Conf):
        self.__logger = core.getLogger(self.__class__.__name__)
        self.__conf = conf
        self.__rabbit = RabbitMQConnector(conf)
        self.__db = SQLiteConnector("bluberry_rabbitmq_"
                                    f"{self.__conf.get('local', 'nodeId')}.sqlite")

    # Methods implementing IBluberryMessageWriter ##########################

    def publish(self, messages: List[Message] = []):
        # Send to itself (i.e. locally) on a queue named after the recipient.
        self.__rabbit.publish(self.__conf.get('local', 'nodeId'), messages)

    def discard(self, messages: List[Message] = []):
        self.__rabbit.dropMessages(messages)

    def notify(self, node: str, messages: List[Message] = []):
        if node != self.__conf.get('local', 'nodeId'):
            self.__rabbit.publish(node, messages, BB_MANAGEMENT)
        else:
            self.__logger.warning(f"Ignoring message to {node}'s own management queue.")

    # Implementation-specific methods #########################################

    def reset(self):
        """
        Reset the message tracking, meaning no messages have yet been received.
        """
        self.__db.clear()

