__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import pika
import traceback
import logging
import copy
from typing import List

import core.BluberryLogging
from core.BluberryMessage import Message
from core.BluberryConf import Conf


# mute RabbitMQ logger
logging.basicConfig(level=logging.WARNING)
logging.getLogger("pika").setLevel(logging.CRITICAL)


class RabbitMQConnector():
    """
    A lower-level tool to access (RW) RabbitMQ queues

    Attributes:
        __connection: pika.BlockingConnection
            The pika connection object that interfaces with RabbitMQ
        __channel: pika.adapters.blocking_connection.BlockingChannel
            The channel used to communicate with the queue
        __currentlyConnectedNode: str
            The ID od the node to who's message queue we're currently connected
    """

    __connection = None
    __channel = None
    __currentlyConnectedNode = None

    def __init__(self,
                 conf: Conf,
                 networkType: str = 'ext',
                 masterUser: str = None,
                 masterPassword: str = None):
        """
        The constructor for the RabbitMQConnector

        Args:
            conf: Conf
                The configuration object containing the connection details
            networkType: str
                The network type over which the different nodes are connected.
                available modes:
                `int`: internal network, i.e. all services running on the same machine
                `ext`: external network, such as the internet
                `virt`: a virtualised network, e.g. using docker
            masterUser: str
                the username for the RabbitMQ service
            masterPassword: str
                the password for the username
        """
        self.__logger = core.getLogger(self.__class__.__name__)
        self.__conf = conf
        self.__networkType = networkType
        if masterUser is not None and masterPassword is not None:
            tmpconf = copy.deepcopy(conf.getConf())
            tmpconf['msg']['password'] = masterPassword
            self.__conf.setConf(tmpconf)
            self.__rabbitUser = masterUser
            self.__rabbitPassword = masterPassword
        else:
            self.__rabbitUser = self.__conf.get('local', 'nodeId')
            self.__rabbitPassword = self.__conf.get('msg', 'password')

        self.__me = self.__conf.get('local', 'nodeId')

    def __connect(self, node: str):
        """
        Connect to a RabbitMQ service

        Args:
            node: str
                The ID of the node to which to connect

        Raises:
            RuntimeError: if authentication or connection fails
        """
        # only ever use the stack-internal network to connect to own MQ
        if node == self.__me:
            nwtype = 'int'
        else:
            nwtype = self.__networkType

        host = self.__conf.get('nodes', node, 'msg-host-' + nwtype)
        port = self.__conf.get('nodes', node, 'msg-port-' + nwtype)

        try:
            credentials = pika.PlainCredentials(username=self.__rabbitUser,
                                                password=self.__rabbitPassword)
            self.__connection = pika.BlockingConnection(
                                    pika.ConnectionParameters(host=host,
                                                              port=port,
                                                              credentials=credentials))
            self.__channel = self.__connection.channel()
            self.__currentlyConnectedNode = node
            # self.__logger.debug(f"Node {self.__me} connected to "
            #                     f"node {node} at {host}:{port} on network \"{nwtype}\".")

        except (pika.exceptions.ProbableAuthenticationError) as e:
            raise RuntimeError(f"Authentication failed for user {self.__rabbitUser} "
                                f"at node {node}.") from e

        except Exception as e:
            self.__logger.error(e.__class__.__name__)
            self.__logger.error(e)
            traceback.print_tb(e.__traceback__)

            self.__disconnect()

            raise RuntimeError(f"Node {self.__me} could not connect to "
                               f"node {node} at {host}:{port} on {nwtype} network.") from e

    def __disconnect(self):
        """
        Disconnect from the currently connected RabbitMQ service
        """
        # self.__logger.debug(f"Node {self.__me} disconnecting from node "
        #                     f"{self.__currentlyConnectedNode}.")
        if self.__connection is not None and self.__connection.is_open:
            self.__connection.close()
        self.__connection = None
        self.__channel = None
        self.__currentlyConnectedNode = None

    def getMessages(self, node: str, qname: str, delete: bool = False) -> List[Message]:
        """
        Get all messages from the given node's given queue.
        Optionally delete messages from the queue after retrieving them.

        Args:
            node: str
                the node whose messages to access
            qname: str
                the queue on that node that should be read
            delete: bool
                whether to delete the messages from the queue after reading

        Raises:
            RuntimeError: if messages cannot be retrieved

        Returns:
            List[Message]: An (ordered) list of messages from that queue on that node
        """

        self.__logger.debug(f"Node {self.__me} getting messages from queue {qname} "
                            f"on node {node}. Delete? {delete}")
        messages = []

        self.__connect(node)
        try:
            num_msg = int(self.__channel.queue_declare(queue=qname).method.message_count)

            if num_msg > 0:
                self.__logger.debug(f"{num_msg} messages in "
                                    f"queue {qname} on node {node}.")
            else:
                self.__logger.debug(f"Queue {qname} on node {node} is empty.")

            while num_msg > 0:
                method, header, body = self.__channel.basic_get(queue=qname,
                                                                auto_ack=False)
                num_msg = method.message_count
                msg = Message()
                msg.deserialise(body)
                messages.append(msg)

                # allow delete only for
                # - queues for that node on foreign nodes or
                # - any queue on the node itself
                if delete and (qname == self.__me or node == self.__me):
                    self.__logger.debug(f"Node {self.__me} acknowledging (=deleting) message "
                                        f"{msg.getId()} from queue {qname} on node {node}.")
                    self.__channel.basic_ack(delivery_tag=method.delivery_tag)

            if len(messages) > 0:
                self.__logger.debug(f"Read {len(messages)} messages "
                                    f"from queue {qname} on node {node}.")

        except Exception as e:
            self.__logger.error(e)
            traceback.print_tb(e.__traceback__)
            raise RuntimeError(f"Node {self.__me} could not get messages from queue "
                               f"{qname} on node {node}.") from e

        finally:
            self.__disconnect()

        return messages

    def publish(self, node: str, messages: List[Message], overrideQueue: str = None):
        """
        Send messages to the specified node.
        No need to specify the recipient/queue name as an argument as it's
        already contained in the message object.
        Permissions need to be held at the node for the current node's user.

        Args:
            node: str
                where to send the messages
            messages: List[Message]
                the messages to send
            overrideQueue: str
                optional parameter to specify the queue to which all messages should be published

        Raises:
            RuntimeError: if messages cannot be sent
        """
        lastQueue = None
        self.__connect(node)
        try:
            for msg in messages:
                qname = msg.getDestination()
                if overrideQueue is not None:
                    qname = overrideQueue
                lastQueue = qname

                # skip messages from self to self
                if node == self.__me and qname == self.__me:
                    self.__logger.debug('Skipping message from self to self.')
                    continue

                self.__logger.debug(f"Node {self.__me} sending message '{msg.getId()}' "
                                    f"from node {msg.getOriginNode()} "
                                    f"to node {msg.getDestination()} "
                                    f"to queue {qname} on node {node}...")

                self.__channel.queue_declare(queue=qname)
                self.__channel.basic_publish(exchange='',
                                             routing_key=qname,
                                             body=msg.serialise())
                self.__logger.debug('Message sent successfully.')

        except Exception as e:
            self.__logger.error('Error sending message(s).')
            raise RuntimeError(f"Node {self.__me} could not publish messages to "
                               f"node {node}. Last queue: {lastQueue}.") from e

        finally:
            self.__disconnect()

    def dropMessages(self, messages: List[Message]) -> List[Message]:
        """
        Delete the messages from the local queue.

        Args:
            messages: str
                the messages to be deleted

        Raises:
            Exception: if messages cannot be deleted

        Returns:
            List[Message]: The messages that were actually dropped
        """
        # Pre-order messages by queue
        msgByQ = {}
        for msg in messages:
            if msg.getDestination() not in msgByQ:
                msgByQ[msg.getDestination()] = []
            msgByQ[msg.getDestination()].append(msg)

        self.__connect(self.__me)
        droppedMessages = []
        try:
            numDropped = 0
            for qname in msgByQ:
                while True:
                    method, header, body = self.__channel.basic_get(queue=qname,
                                                                    auto_ack=False)
                    readMsg = Message()
                    readMsg.deserialise(body)

                    for msg in msgByQ[qname]:
                        if readMsg.getId() == msg.getId():
                            self.__logger.debug(f"Dropping message {msg.getId()}")
                            self.__channel.basic_ack(delivery_tag=method.delivery_tag)
                            droppedMessages.append(msg)
                            numDropped += 1

            self.__logger.debug(f"Dropped {numDropped} messages "
                                f"from node {self.__me}.")

        except Exception as e:
            self.__logger.error(f"Node {self.__me} could not drop messages from "
                                f"its own message queue.")
            self.__logger.error(e)
            traceback.print_tb(e.__traceback__)

        finally:
            self.__disconnect()

        return droppedMessages

    def getQueueSize(self, node: str, qname: str) -> int:
        """
        Get the number of messages on the given queue at the given node.

        Args:
            node: str
                the node on which the queue resides
            qname: str
                the name of the queue

        Raises:
            RuntimeError: if queue size cannot be retrieved

        Returns:
            int: the number of messages on that queue
        """
        num = 0
        self.__connect(node)
        try:
            queue = self.__channel.queue_declare(queue=qname)
            num = queue.method.message_count

        except Exception as e:
            raise RuntimeError(f"Could not get queue size for queue {qname} on node {node}.")\
                  from e

        finally:
            self.__disconnect()

        return num

    def clearQueue(self, node: str, qname: str):
        """
        Delete all messages from the given queue at the given node.

        Args:
            node: str
                the node on which the queue resides
            qname: str
                the name of the queue

        Raises:
            RuntimeError: if queue cannot be cleared
        """
        self.__connect(node)
        try:
            num = int(self.__channel.queue_declare(queue=qname).method.message_count)
            if num > 0:
                self.__logger.debug(f"Deleting {num} messages from queue {qname} on node {node}")
                # purge *and* delete here as the queue may exist but be broken
                self.__channel.queue_purge(queue=qname)
                self.__channel.queue_delete(queue=qname)

        except Exception as e:
            self.__logger.error(e)
            traceback.print_tb(e.__traceback__)
            raise RuntimeError(f"Could not clear queue {qname} on node {node}.") from e

        finally:
            self.__disconnect()

