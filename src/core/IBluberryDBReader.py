__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2019, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import abc
from typing import List
from core.BluberryMessage import Message


class IBluberryDBReader(abc.ABC):
    """
    BluberryDB reader interface:
    This defines the interface for the BluberryDB reader, which can read database
    changes for potentially any DBMS. This module is a high level class, that may
    accesses the database directly or indirectly.
    The reader is configured on a per database server basis, *not* on a
    per-application basis. This means this reader will parse all operations on a
    database without filtering by host, user or schema; this is the job of the
    BluberryDBReaderService.
    """

    @abc.abstractmethod
    def readUpdates(self) -> List[Message]:
        """
        Read updates from the database. Mark updates as read so they're not
        re-read the next time this method is called.

        Returns:
            List[Message]: the messages retrieved from the database
        """

    @abc.abstractmethod
    def hasUpdates(self) -> bool:
        """
        Check whether there are any new updates in the database.

        Returns:
            bool: whether there are any updates in the database
        """

