__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2018, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import abc
from typing import List

from core.BluberryMessage import Message


class IBluberryDBWriter(abc.ABC):
    """
    Bluberry DB writer interface:
    This defines the interface for the Bluberry DB writer, which can execute
    a database query for potentially any DBMS. This module accesses the database
    directly.
    """

    @abc.abstractmethod
    def prepare(self):
        """
        This method should do "what needs to be done" before any queries can be
        executed.
        Depending on the database, this may involve acquiring a lock to prevent
        other processes from interrupting. It could also mean to "remove
        defenses", such as (foreign key) constraints that would prevent the
        queries from being executed in an arbitrary order.
        It should also set the status of this object to "engaged".
        """

    @abc.abstractmethod
    def runUpdates(self, updates: List[Message]) -> bool:
        """
        This method should execute all updates on the database. It should be
        called after calling the prepare() method.

        Args:
            updates : List[Message]
                The updates to be executed against the database.

        Returns:
            bool: whether the updates were executed successfully
        """

    @abc.abstractmethod
    def finalise(self):
        """
        This method needs to be called following a round of updates. It should
        implement everything required to make sure the database is "restored"
        in terms of constraints that were removed during the prepare() method.
        It should also set the status of this object to "disengaged".
        """

    @abc.abstractmethod
    def isEngaged(self) -> bool:
        """
        This gives the status of the DBWriter.

        Returns:
            bool: true if the DB is currently engaged and cannot start another activity
        """

