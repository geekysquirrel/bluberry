__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2018, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import pkgutil
import importlib
import traceback
import rsa
import os
from threading import Lock
from typing import List
from pathlib import Path
from core.BluberryMessage import Message
from core.BluberryConf import Conf

import core.BluberryLogging
from .IBluberryDBReader import IBluberryDBReader
from .IBluberryDBWriter import IBluberryDBWriter
from .IBluberryMessageReader import IBluberryMessageReader
from .IBluberryMessageWriter import IBluberryMessageWriter
from .ABluberryThread import ABluberryThread


class Bluberry:
    """
    An asynchronous, distributed, consent-based, probably-not-consistent,
    schema-agnostic database synchronisation layer.
    This class represents the Bluberry core and handles the synchronisation.

    Attributes:
        __conf : core.BluberryConf.Conf
            This node's configuration

        __nodeId : str
            The id of this node

        __interval : int
            The interval in which to execute actions on the database and messaging modules

        __lock : threading.Lock
            A lock to make sure threads only do things when they're supposed to.

        __db : BluberryDBManagerService
            The implementation-agnostic database service

        __msg : BluberryMessageManagerService
            The implementation-agnostic messaging service
    """
    __db = None
    __msg = None

    def __init__(self, conf: Conf):
        """
        The constructor sets up the instance with the configuration object passed to it.
        It uses reflection to find the currently configured database and messaging modules,
        loads them and instatiates the relevant services.

        Args:
            conf : core.BluberryConf.Conf
                This node's configuration
        """

        self.__logger = core.getLogger(self.__class__.__name__)
        self.__logger.info("Setting up Bluberry")

        self.__conf = conf
        self.__nodeId = conf.get('local', 'nodeId')
        self.__interval = conf.get('local', 'interval')

        self.__lock = Lock()

        # import all classes from the main db/msg packages
        if conf.get('local', 'db') is not None:
            try:
                dbPackage = importlib.import_module(f"db.{conf.get('local', 'db')}")
                for importer, modname, ispkg in pkgutil.iter_modules(dbPackage.__path__):
                    if not ispkg:
                        importlib.import_module(f"db.{conf.get('local', 'db')}.{modname}")
            except ModuleNotFoundError as e:
                self.__logger.warning(f"Could not load module {conf.get('local', 'db')}")

        if conf.get('local', 'msg') is not None:
            try:
                msgPackage = importlib.import_module(f"msg.{conf.get('local', 'msg')}")
                for importer, modname, ispkg in pkgutil.iter_modules(msgPackage.__path__):
                    if not ispkg:
                        importlib.import_module(f"msg.{conf.get('local', 'msg')}.{modname}")
            except ModuleNotFoundError as e:
                self.__logger.warning(f"Could not load module {conf.get('local', 'msg')}")

        # Find the implementations for the interfaces based on the config (greedy)
        dbr, dbw, msgr, msgw = None, None, None, None
        if conf.get('local', 'db') is not None:
            for dbReaderImpl in IBluberryDBReader.__subclasses__():
                if dbReaderImpl.__module__.startswith('db.' + conf.get('local', 'db')):
                    module = importlib.import_module(dbReaderImpl.__module__)
                    class_ = getattr(module, dbReaderImpl.__name__)
                    dbr = class_(self.__conf)
                    break
            for dbWriterImpl in IBluberryDBWriter.__subclasses__():
                if dbWriterImpl.__module__.startswith('db.' + conf.get('local', 'db')):
                    module = importlib.import_module(dbWriterImpl.__module__)
                    class_ = getattr(module, dbWriterImpl.__name__)
                    dbw = class_(self.__conf)
                    break
        if conf.get('local', 'msg') is not None:
            for msgReaderImpl in IBluberryMessageReader.__subclasses__():
                if msgReaderImpl.__module__.startswith('msg.' + conf.get('local', 'msg')):
                    module = importlib.import_module(msgReaderImpl.__module__)
                    class_ = getattr(module, msgReaderImpl.__name__)
                    msgr = class_(self.__conf)
                    break
            for msgWriterImpl in IBluberryMessageWriter.__subclasses__():
                if msgWriterImpl.__module__.startswith('msg.' + conf.get('local', 'msg')):
                    module = importlib.import_module(msgWriterImpl.__module__)
                    class_ = getattr(module, msgWriterImpl.__name__)
                    msgw = class_(self.__conf)
                    break

        if dbr is not None and dbw is not None:
            self.__db = BluberryDBManagerService(self, dbr, dbw)

        if msgr is not None and msgw is not None:
            self.__msg = BluberryMessageManagerService(self, msgr, msgw)

        self.__logger.info(f"Created new Bluberry object with db service {self.__db} and "
                           f"msg service {self.__msg}.")

    def startServices(self):
        """
        Start the database and messaging services if they exist.
        """
        if self.__db is None:
            self.__logger.error('No database service was found.')
        else:
            self.__logger.info("Starting database service using module "
                               f"db.{self.__conf.get('local', 'db')}")
            self.__db.start()

        if self.__msg is None:
            self.__logger.error('No messaging service was found.')
        else:
            self.__logger.info("Starting messaging service using module "
                               f"msg.{self.__conf.get('local', 'msg')}")
            self.__msg.start()

    def stop(self):
        """
        Start the database and messaging services if they exist.
        """
        self.__logger.info("Stopping Bluberry services")
        if self.__db is not None:
            self.__db.stop()
        if self.__msg is not None:
            self.__msg.stop()

    def isRunning(self) -> bool:
        """
        Check whether at least one of the services is still running.

        Returns:
            bool: True if still running, False if not.
        """
        dbRunning = False
        if self.__db is not None:
            dbRunning = self.__db.isRunning()
        msgRunning = False
        if self.__msg is not None:
            msgRunning = self.__msg.isRunning()
        return dbRunning or msgRunning

    # Callback methods ########################################################

    def receivedDBUpdates(self, messages: List[Message]):
        """
        This callback is invoked by the `BluberryDBManagerService` when it has found new
        updates to the local database. It encrypts the messages it received for the
        designated recipient and then calls the `BluberryMessageManagerService` to send
        them off.

        Args:
            messages : typing.List[Message]
                The messages as read from the `BluberryDBManagerService`'s `IBluberryDBReader`
        """
        finished = False
        outbox = []
        while not finished:
            self.__lock.acquire()
            try:
                self.__logger.info('Locking for db updates')
                for msg in messages:

                    # load own private key
                    privateKey = None
                    pkpath = Path(f"{self.__conf.get('local', 'basedir')}{os.sep}"
                                  f"{self.__conf.get('nodes', self.__nodeId, 'privateKey')}")
                    with open(pkpath, "rb") as pk:
                        privateKey = rsa.PrivateKey.load_pkcs1(pk.read())

                    # load destination's public key
                    publicKey = None
                    pkpath = Path(f"{self.__conf.get('local', 'basedir')}{os.sep}"
                        f"{self.__conf.get('nodes', msg.getDestination(), 'publicKey')}")
                    with open(pkpath, "rb") as pk:
                        publicKey = rsa.PublicKey.load_pkcs1_openssl_pem(pk.read())

                    if not msg.isEncrypted():
                        msg.encrypt(publicKey, privateKey)
                    else:
                        self.__logger.error('Message {msg.getId()} is already encrypted')
                    outbox.append(msg)
                if self.__msg is not None:
                    self.__msg.publishMessages(outbox)
                else:
                    self.__logger.error("Could not publish messages - "
                                 "no messaging service is configured.")
                finished = True
            # TODO: which exceptions to catch here?
            except Exception as e:
                self.__logger.error("Could not process DB updates:")
                self.__logger.error(e)
                traceback.print_tb(e.__traceback__)
            finally:
                self.__logger.debug('Unlocking')
                self.__lock.release()

    def receivedMessages(self, messages: List[Message]):
        """
        This callback is invoked by the `BluberryMessageManagerService` when it has found new
        remote updates on other nodes. It checks the messages are valid, decrypts them and
        either invokes the `BluberryDBManagerService` if they are for this node or acknowledges
        it received them and re-queues them on its own message queue for other nodes to pick up.

        Args:
            messages : typing.List[Message]
                The messages as read from the `BluberryMessageManagerService`'s
                `IBluberryMessageReader`
        """
        finished = False
        inbox = []
        outbox = []
        while not finished:
            self.__lock.acquire()
            try:
                self.__logger.info('Locking for msg updates')
                for msg in messages:
                    self.__logger.debug(f"Processing message {msg}")

                    # load own private key
                    privateKey = None
                    pkpath = Path(f"{self.__conf.get('local', 'basedir')}{os.sep}"
                                  f"{self.__conf.get('nodes', self.__nodeId, 'privateKey')}")
                    with open(pkpath, "rb") as pk:
                        privateKey = rsa.PrivateKey.load_pkcs1(pk.read())

                    # load sender's public key
                    publicKey_origin = None
                    pkpath = Path(f"{self.__conf.get('local', 'basedir')}{os.sep}"
                        f"{self.__conf.get('nodes', msg.getOriginNode(), 'publicKey')}")
                    with open(pkpath, "rb") as pk:
                        publicKey_origin = rsa.PublicKey.load_pkcs1_openssl_pem(pk.read())

                    # load destination's public key
                    publicKey_destination = None
                    pkpath = Path(f"{self.__conf.get('local', 'basedir')}{os.sep}"
                        f"{self.__conf.get('nodes', msg.getDestination(), 'publicKey')}")
                    with open(pkpath, "rb") as pk:
                        publicKey_destination = rsa.PublicKey.load_pkcs1_openssl_pem(pk.read())

                    # Message for us!
                    if msg.getDestination() == self.__nodeId:
                        if msg.isEncrypted():
                            msg.decrypt(privateKey, publicKey_origin)
                        inbox.append(msg)
                        # don't requeue
                    else:
                        if not msg.reachedDestination(publicKey_destination):
                            msg.receive(self.__nodeId, privateKey)
                            outbox.append(msg)
                        else:
                            self.__logger.debug("The message has already been received by "
                                                f"{msg.getDestination()}, discarding.")
                if self.__db is not None:
                    self.__db.writeUpdates(inbox)
                else:
                    self.__logger.error("Could not write messages - "
                                 "no database service is configured.")

                if self.__msg is not None:
                    self.__msg.publishMessages(outbox)
                else:
                    self.__logger.error("Could not requeue messages - "
                                 "no messaging service is configured.")
                finished = True
            # TODO: which exceptions to catch here?
            except Exception as e:
                self.__logger.error("Could not process messaging updates:")
                self.__logger.error(e)
                traceback.print_tb(e.__traceback__)
            finally:
                self.__logger.debug('Unlocking')
                self.__lock.release()

    # Invoke the db/msg threads ###############################################

    def invokeDB(self):
        """
        Manually triggers its `BluberryDBManagerService` to check for new local updates.
        """
        if self.__db is not None:
            self.__db.doit()
        else:
            self.__logger.warning("Could not invoke database service - it's not set")

    def invokeMsg(self):
        """
        Manually triggers its `BluberryDBManagerService` to check for new remote messages.
        """
        if self.__msg is not None:
            self.__msg.doit()
        else:
            self.__logger.warning("Could not invoke messaging service - it's not set")

    # Getters/Setters #########################################################

    def getConf(self):
        return self.__conf


# defined here instead of as independent class to avoid circular dependency
class BluberryDBManagerService(ABluberryThread):
    """
    This defines the service responsible for reading changes from the database and
    writing them back. To access the databse, it uses `core.IBluberryDBReader` and
    `core.IBluberryDBWriter` implementations, that should only be accessed via their
    public interfaces and be database agnostic at this level.
    """

    def __init__(self,
                 bluberry: Bluberry,
                 reader: IBluberryDBReader,
                 writer: IBluberryDBWriter):
        """
        The constructor links the DB service to its utility members and its "parent"
        `core.Bluberry` object so callback methods can be invoked.

        Args:
            bluberry : core.Bluberry
                The parent object

            reader : core.IBluberryDBReader
                The database reader

            writer : core.IBluberryDBWriter
                The database writer
        """
        self.__bb = bluberry
        conf = bluberry.getConf()

        ABluberryThread.__init__(self,
                                 f"DBManager-{conf.get('local', 'nodeId')}",
                                 conf.get('local', 'interval'))

        self.__logger = core.getLogger(self.__class__.__name__)

        if not reader:
            raise RuntimeError('Please specify a database reader')

        if not writer:
            raise RuntimeError('Please specify a database writer')

        self.__reader = reader
        self.__writer = writer

    # Methods implementing ABluberryThread ################################

    def conditionalAction(self):
        super().conditionalAction()
        self.__bb.receivedDBUpdates(self.__reader.readUpdates())

    def unconditionalAction(self):
        super().unconditionalAction()

    # Own methods #########################################################

    def writeUpdates(self, updates: List[Message]):
        """
        Writes remote messages to the local database.

        Args:
            updates : typing.List[Message]
                The remote messages as read from the `BluberryMessageManagerService`'s
                `core.IBluberryMessageReader`
        """
        self.__writer.runUpdates(updates)


class BluberryMessageManagerService(ABluberryThread):
    """
    This represents the interface between the messaging module and the core module. It is a
    service, that runs alongside the core and listens for messages. Upon receiving a message,
    it passes it on to the core or messaging module respectively.
    """

    def __init__(self, bluberry: Bluberry,
                 reader: IBluberryMessageReader,
                 writer: IBluberryMessageWriter):
        """
        The constructor links the messaging service to its utility members and its "parent"
        `core.Bluberry` object so callback methods can be invoked.

        Args:
            bluberry : core.Bluberry
                The parent object

            reader : core.IBluberryMessageReader
                The message reader

            writer : core.IBluberryMessageWriter
                The message writer
        """

        self.__bb = bluberry
        conf = bluberry.getConf()

        ABluberryThread.__init__(self,
                                 'MessageManager-'
                                 + conf.get('local', 'nodeId'),
                                 conf.get('local', 'interval'))

        self.__logger = core.getLogger(self.__class__.__name__)

        if not reader:
            raise RuntimeError('Please specify a message reader')

        if not writer:
            raise RuntimeError('Please specify a message writer')

        self.__reader = reader
        self.__writer = writer

    # Methods implementing ABluberryThread ################################

    def conditionalAction(self):
        super().conditionalAction()
        self.__bb.receivedMessages(self.__reader.readMessages())
        self._busy = False

    def unconditionalAction(self):
        super().unconditionalAction()
        self._busy = False

    # Own methods #########################################################

    def publishMessages(self, messages: List[Message]):
        """
        Publishes local database updates to the messaging middleware for other nodes.

        Args:
            messages : `typing.List`[`core.BluberryMessage.Message`]
                The local messages as read from the `BluberryDBManagerService`'s
                `core.IBluberryDBReader`
        """
        self.__writer.publish(messages)

