__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2018, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import abc
from typing import List
from core.BluberryMessage import Message


class IBluberryMessageReader(abc.ABC):
    """
    Bluberry message reader interface:
    This defines an interface to pick up serialised messages from a remote place,
    transforming them into objects that can be handled by Bluberry's
    core controller. These messages will contain encrypted fields but this
    interface will not deal with that but hand them on to the calling
    messaging/core module as they are instead.
    """

    @abc.abstractmethod
    def readMessages(self) -> List[Message]:
        """
        This method should retrieve all messages (not just messages intended
        for the node on which it is running) from all nodes (except itself).
        For messages intended for itself, it should delete them from the node
        (pop) whereas messages for other nodes should be read but left intact
        (peek). If a message read via "peek" is found to have been received by
        the designated recipient, the host node of this message should be
        notified using the IBluberryMessagePublisher's notify(...) method.
        The returned messages should be using the Bluberry data model.

        Returns:
            List[Message]: the messages retrieved from the database
        """

    @abc.abstractmethod
    def checkNotifications(self):
        """
        This method should check its own notifications locally, possibly
        from an internal management message queue or other data store,
        and process them. This involves removing messages it has been notified
        that have been received by their final recipient from its own queues
        and storing that state internally so other nodes can be notified.
        """
