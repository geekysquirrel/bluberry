__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2018, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import threading
import time
import abc
import datetime

import core.BluberryLogging


class ABluberryThread(abc.ABC, threading.Thread):
    """
    BluberryDB thread abstract class:
    This defines a basic thread class, which does not deliver any actual
    functionality on its own. Instead, it only provides methods to
    start/stop/pause/resume the thread but its main loop is empty.
    It's supposed to be subclassed to actually do something.
    """

    def __init__(self, name: str, interval: float):

        threading.Thread.__init__(self)
        self.__name = name
        self.__logger = core.getLogger(f"ABluberryThread ({self.__name})")
        self.__interval = interval
        self.__paused = False
        self.__stop = True
        self.__lastConditionalAction = None
        self.__lastUnconditionalAction = None

    # Main ("inheritable") methods ############################################

    def run(self):
        """
        The main loop of the thread. Started using threading.Thread.run()
        """
        self.__logger.info(f"Starting thread {self.__name}")

        self.__stop = False

        while self.__interval is not None and not self.__stop:
            self.__logger.debug(f"Thread {self.__name} running...")
            self.doit()
            time.sleep(self.__interval)

        self.__logger.info(f"Thread {self.__name} stopped")

    def doit(self):
        """
        Manual handle to make a thread "do its thing" just once.
        """
        self.unconditionalAction()

        # this might happen if the unconditional action takes a long time
        if self.__stop:
            return

        if not self.__paused:
            self.conditionalAction()

    def stop(self):
        """
        Stops the thread and causes it to exit the main loop
        """
        self.__logger.info(f"Stopping thread {self.__name}")
        self.__stop = True

    def pause(self):
        """
        After this has been called, the main loop is paused or sent to "offline"
        mode, meaning conditionalAction() is not executed but
        unconditionalAction() is.
        """
        self.__logger.info(f"Pausing thread {self.__name}")
        self.__paused = True

    def resume(self):
        """
        After this has been called on a paused thread, it is unpaused or sent back
        to "online" mode and will execute conditionalAction() as well as
        unconditionalAction().
        """
        self.__logger.info(f"Resuming thread {self.__name}")
        self.__paused = False

    # Getters #################################################################

    def getName(self) -> str:
        return self.__name

    def isRunning(self) -> bool:
        return not self.__stop

    def isPaused(self) -> bool:
        return self.__paused

    def getInterval(self) -> float:
        return self.__interval

    def getLastConditionalAction(self) -> datetime:
        return self.__lastConditionalAction

    def getLastUnconditionalAction(self) -> datetime:
        return self.__lastUnconditionalAction

    # Setters #################################################################

    def setInterval(self, interval: float):
        """
        Set an interval for the thread to do its action.

        Args:
            interval : float
                The number of seconds (or parts thereof) the thread should wait
                before repeating its loop.
        """
        self.__interval = interval
        self.__logger.debug(f'Interval set to {interval}s for thread '
                            f'{self.__name}')

    # Abstract methods ########################################################

    @abc.abstractmethod
    def conditionalAction(self):
        """
        This action will be executed in the main loop, provided the thread is
        not paused. Implementing classes need to remember to call this via
        super().conditionalAction().
        """
        self.__lastConditionalAction = datetime.datetime.now()

    @abc.abstractmethod
    def unconditionalAction(self):
        """
        This action will be executed in the main loop independently of whether
        the thread is paused or not. This an be useful for "offline" activities
        that can be executed in any case. Implementing classes need to remember
        to call this via super().unconditionalAction().
        """
        self.__lastUnconditionalAction = datetime.datetime.now()

