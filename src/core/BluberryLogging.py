__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2018, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"

import logging
import coloredlogs


coloredlogs.install(
    isatty=True,
    fmt='%(asctime)s.%(msecs)03d [%(levelname)-8s] %'
        '(filename)s:%(lineno)d - %(message)s',
    datefmt='%H:%M:%S',
    level='DEBUG',
    field_styles={
        'asctime': {'color': 34},
        'filename': {'color': 105},
        'lineno': {'color': 105, 'bold': True},
        'levelname': {'color': 228, 'bold': True},
        'message': {'color': 'white'}
    },
    level_styles={
        'debug': {'color': 240},
        'info': {'color': 44},
        'warning': {'color': 214},
        'error': {'color': 9},
        'critical': {'color': 255, 'background': 'red', 'bold': True}
    }
)


def getLogger(name: str) -> logging.Logger:
    """
    Get hold of a logger from the logging framework.

    Args:
        name : str
            The name of the logger. If referring to an existing logger, that logger will be
            returned rather than a new one created.
    """
    return logging.getLogger(name)


def setLogLevel(name: str, level: str) -> logging.Logger:
    """
    Set the minimum sensitivity for a logger.

    Args:
        name : str
            The name of the logger for which to set the level.
        level: str
            The new sensitivity level. You can either use the case-insensitive level names
            or their integer values:
            (debug: 10, info: 20, warning: 30, error: 40, critical: 50)
    """
    newLevel = level
    if isinstance(level, str):
        newLevel = level.upper()

    # Levels' integer values: 0, 10, 20, 30, 40, 50
    # https://docs.python.org/3/library/logging.html#logging-levels
    if newLevel == 'DEBUG':
        newLevel = logging.DEBUG
    elif newLevel == 'INFO':
        newLevel = logging.INFO
    elif newLevel == 'WARNING' or newLevel == 'WARN':
        newLevel = logging.WARNING
    elif newLevel == 'ERROR':
        newLevel = logging.ERROR
    elif newLevel == 'CRITICAL':
        newLevel = logging.CRITICAL
    elif isinstance(newLevel, int):
        getLogger('BluberryLogging').debug(f"Used integer {level} to set logging level.")
    else:
        getLogger('BluberryLogging').warning(f"Invalid log level {level}; please use one of: "
                                             "debug|info|warning|error|critical")

    logging.getLogger(name).setLevel(newLevel)

