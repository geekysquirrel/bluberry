"""
Bluberry integration test file
"""

__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import unittest
import time
import os
import copy
from datetime import datetime
from typing import List

import core.BluberryLogging
from core.Bluberry import Bluberry, BluberryDBManagerService, BluberryMessageManagerService
from core.IBluberryDBReader import IBluberryDBReader
from core.IBluberryMessageReader import IBluberryMessageReader
from core.BluberryMessage import Message
from core.BluberryConf import Conf

# TODO: perhaps we should not use implementation-specific stuff here - or perhaps
# this class is not the place for a full demo?
from db.mysql.MySQLConnector import MySQLConnector


class itest_Bluberry(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        core.setLogLevel('Bluberry', 'debug')
        core.setLogLevel('MySQLConnector', 'warning')
        core.setLogLevel('ConsentManager', 'warning')
        core.setLogLevel('ConflictManager', 'warning')
        core.setLogLevel('BluberryMySQLReader', 'info')
        core.setLogLevel('BluberryMySQLWriter', 'info')
        core.setLogLevel('MySQLIDTranslationManager', 'warning')
        core.setLogLevel('RabbitMQConnector', 'info')
        core.setLogLevel('SQLiteConnector', 'info')
        # TODO: make parent logger for db/msg module
        # TODO: make method for setting default log level

        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")
        cls.__conf = Conf('core/conf/conf.json')
        cls.__test_db = 'bluberry_test'
        tmpconf = cls.__conf.getConf()
        tmpconf['db']['connection']['database'] = cls.__test_db
        cls.__conf.setConf(tmpconf)

    @classmethod
    def tearDownClass(cls):
        # make sure the threads have finished
        time.sleep(cls.__conf.get('local', 'interval') * 1.5)

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")
        self.__mysql = MySQLConnector(self.__conf)

        # Ensure database is clean
        self.__mysql.connect(None)
        self.__mysql.execute(f"DROP DATABASE IF EXISTS {self.__test_db};")
        self.__mysql.execute(f"CREATE DATABASE {self.__test_db};")
        self.__mysql.execute(f"USE {self.__test_db};")
        self.__mysql.runScript(f"{self.__conf.get('local', 'basedir')}{os.sep}"
                               "db/mysql/test/resources/setup.sql")
        self.__mysql.disconnect()
        self.__mysql.resetBluberryDatabase()

        self.__bb = None

    def tearDown(self):
        if self.__bb is not None:
            while self.__bb.isRunning():
                self.__bb.stop()
            self.__bb = None
        if self.__mysql is not None:
            self.__mysql.disconnect()
        self.__logger.info(f"Finished test {self._testMethodName}")

    def createMessage(self, origin: str, destination: str, payload: str)\
                     -> Message:
        msg = Message()
        msg.setOriginNode(origin)
        msg.setDestination(destination)
        msg.setOriginTimestamp(datetime.now()
                               .strftime('%Y-%m-%dT%H:%M:%S.%fZ'))
        msg.setPayload(payload)
        return msg

    def test_init_and_start(self):
        # no db services configured
        conf = copy.deepcopy(self.__conf)
        tmpconf = copy.deepcopy(self.__conf.getConf())
        tmpconf['local']['db'] = None
        conf.setConf(tmpconf)
        self.__bb = Bluberry(conf)
        self.assertIsNone(self.__bb.getConf().get('local', 'db'))
        with self.assertLogs('Bluberry', level='ERROR') as cm:
            core.setLogLevel('Bluberry', 'error')
            self.__bb.startServices()
            core.setLogLevel('Bluberry', 'debug')
        self.assertIn('ERROR:Bluberry:No database service was found.', cm.output)
        self.__bb.stop()

        # no msg services configured
        conf = copy.deepcopy(self.__conf)
        tmpconf = copy.deepcopy(self.__conf.getConf())
        tmpconf['local']['msg'] = None
        conf.setConf(tmpconf)
        self.__bb = Bluberry(conf)
        self.assertIsNone(self.__bb.getConf().get('local', 'msg'))
        with self.assertLogs('Bluberry', level='ERROR') as cm:
            core.setLogLevel('Bluberry', 'error')
            self.__bb.startServices()
            core.setLogLevel('Bluberry', 'debug')
        self.assertIn('ERROR:Bluberry:No messaging service was found.', cm.output)
        self.__bb.stop()

    def test_missing_reader_writer_in_services(self):
        self.__bb = Bluberry(self.__conf)

        # DB module incomplete
        self.assertRaises(RuntimeError, BluberryDBManagerService, self.__bb, None, None)

        class TestDBReader(IBluberryDBReader):

            def readUpdates(self) -> List[Message]:
                pass

            def hasUpdates(self) -> bool:
                pass

        self.assertRaises(RuntimeError, BluberryDBManagerService, self.__bb, TestDBReader(), None)

        # msg module incomplete
        self.assertRaises(RuntimeError, BluberryMessageManagerService, self.__bb, None, None)

        class TestMessageReader(IBluberryMessageReader):

            def readMessages(self) -> List[Message]:
                pass

            def checkNotifications(self):
                pass

        self.assertRaises(RuntimeError, BluberryMessageManagerService, self.__bb,
                          TestMessageReader(), None)

    def test_receivedDBUpdates(self):
        """
        This test pretends to have received local updates from the db component.
        It triggers the Bluberry method to send them to the other nodes.
        """
        self.__bb = Bluberry(self.__conf)
        self.__bb.startServices()
        messages = [self.createMessage('node1', 'node2', 'update1'),
                    self.createMessage('node1', 'node2', 'update2'),
                    self.createMessage('node1', 'node3', 'update1')]
        self.__bb.receivedDBUpdates(messages)
        # TODO: - check the three messages have been sent to the correct queue
        #       - simulate message being picked up by removing it from the queue
        #       - simulate notification from node2 that the msg for node3 has been received
        #       - check messages are gone

    def test_receivedMessages(self):
        """
        This test pretends to have received foreign messages from the msg component.
        The messages are:
            INSERT INTO animals (id, species, name) VALUES (4, 1, 'Harry P. Otter');
            UPDATE animals SET name='Harry Potter' WHERE id=4;
        It triggers the Bluberry method to pass them to the db component.
        """
        self.__bb = Bluberry(self.__conf)
        self.__bb.startServices()

        msg1 = Message()
        msg1.deserialise('{"id": "dfdd878d-9d8f-436c-8f33-91fc5183a79c", "originNode": '
        '"node2", "destination": "node1", "receivedBy": {}, "signatureInner": null, '
        '"signatureOuter": null, "encryptedKey": null, "nonce": null, "tag": null, '
        '"originTimestamp": 1584724573, "payload": "{\\"id\\": '
        '\\"6564a70b-df00-4b30-bb66-ec8d9f65cb32\\", \\"statements\\": '
        '[\\"{\\\\\\"before\\\\\\": {}, \\\\\\"after\\\\\\": {\\\\\\"id\\\\\\": '
        '\\\\\\"3TC1\\\\\\", \\\\\\"species\\\\\\": 1, \\\\\\"name\\\\\\": '
        '\\\\\\"Harry P. Otter\\\\\\"}, \\\\\\"related_before\\\\\\": '
        '{\\\\\\"species.name\\\\\\": \\\\\\"O\\\\\\"}, \\\\\\"related_after\\\\\\": '
        '{\\\\\\"species.name\\\\\\": \\\\\\"Otter\\\\\\"}, \\\\\\"table\\\\\\": '
        '\\\\\\"animals\\\\\\", \\\\\\"type\\\\\\": 2, '
        '\\\\\\"statement_consent_reqs\\\\\\": '
        '[\\\\\\"{\\\\\\\\\\\\\\"consent_tag\\\\\\\\\\\\\\": '
        '\\\\\\\\\\\\\\"animal_personal\\\\\\\\\\\\\\", '
        '\\\\\\\\\\\\\\"consenting_individuals\\\\\\\\\\\\\\": '
        '[\\\\\\\\\\\\\\"3TC1\\\\\\\\\\\\\\"], '
        '\\\\\\\\\\\\\\"consent_owner_type\\\\\\\\\\\\\\": '
        '\\\\\\\\\\\\\\"ANIMAL\\\\\\\\\\\\\\"}\\\\\\"], '
        '\\\\\\"beforeHash\\\\\\": \\\\\\"43c3079e44b0bd9d483c48ebac8aaadf\\\\\\", '
        '\\\\\\"afterHash\\\\\\": \\\\\\"52682077142b690568ed9c4ec544cfce\\\\\\"}\\"], '
        '\\"tables\\": [\\"animals\\"], \\"timestamp\\": 1584724573, \\"locations\\": '
        '[\\"node1\\", \\"node2\\"]}"}')

        msg2 = Message()
        msg2.deserialise('{"id": "8c9d55f9-00e9-4fca-8627-f09a354af013", "originNode": '
        '"node2", "destination": "node1", "receivedBy": {}, "signatureInner": null, '
        '"signatureOuter": null, "encryptedKey": null, "nonce": null, "tag": null, '
        '"originTimestamp": 1584724574, "payload": "{\\"id\\": '
        '\\"1f4be3c5-40b4-4308-8799-14756e012c3a\\", \\"statements\\": '
        '[\\"{\\\\\\"before\\\\\\": {\\\\\\"id\\\\\\": \\\\\\"3TC1\\\\\\", '
        '\\\\\\"species\\\\\\": 1, \\\\\\"name\\\\\\": \\\\\\"Harry P. Otter\\\\\\"}, '
        '\\\\\\"after\\\\\\": {\\\\\\"id\\\\\\": \\\\\\"3TC1\\\\\\", '
        '\\\\\\"species\\\\\\": 1, \\\\\\"name\\\\\\": \\\\\\"Harry Potter\\\\\\"}, '
        '\\\\\\"related_before\\\\\\": {\\\\\\"species.name\\\\\\": \\\\\\"O\\\\\\"}, '
        '\\\\\\"related_after\\\\\\": {\\\\\\"species.name\\\\\\": \\\\\\"Otter\\\\\\"}, '
        '\\\\\\"table\\\\\\": \\\\\\"animals\\\\\\", \\\\\\"type\\\\\\": 1, '
        '\\\\\\"statement_consent_reqs\\\\\\": '
        '[\\\\\\"{\\\\\\\\\\\\\\"consent_tag\\\\\\\\\\\\\\": '
        '\\\\\\\\\\\\\\"animal_personal\\\\\\\\\\\\\\", '
        '\\\\\\\\\\\\\\"consenting_individuals\\\\\\\\\\\\\\": '
        '[\\\\\\\\\\\\\\"3TC1\\\\\\\\\\\\\\"], '
        '\\\\\\\\\\\\\\"consent_owner_type\\\\\\\\\\\\\\": '
        '\\\\\\\\\\\\\\"ANIMAL\\\\\\\\\\\\\\"}\\\\\\"], \\\\\\"beforeHash\\\\\\": '
        '\\\\\\"6c6c9be1d1495e11d408dcbd988ef51a\\\\\\", \\\\\\"afterHash\\\\\\": '
        '\\\\\\"ca5c709915e604e3567cb8bc068109e9\\\\\\"}\\"], \\"tables\\": '
        '[\\"animals\\"], \\"timestamp\\": 1584724574, \\"locations\\": '
        '[\\"node1\\", \\"node2\\"]}"}')

        # can't have encrypted messages here as the keys to encrypt them would
        # - either need to be stored locally
        #   -> can't do that as they're foreign private keys
        # - or the serialised message would have to be encrypted already
        #   -> can't do that as the keys used would be different every time the
        #      integration containers are spun up
        # workaround: just use unencrypted messages - the encryption is tested separately
        self.__bb.receivedMessages([msg1, msg2])

        # check the messages are executed and the data added to the local db
        result = self.__mysql.execute(f"SELECT * FROM animals;")
        self.assertEqual(result, [(1, 1, 'Harry Potter')])


if __name__ == "__main__":
    unittest.main()

