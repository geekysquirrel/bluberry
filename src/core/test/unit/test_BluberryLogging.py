"""
BluberryConf test file
"""

__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import unittest
import logging

import core.BluberryLogging


class test_BluberryLogging(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")

    def tearDown(self):
        self.__logger.info(f"Finished test {self._testMethodName}")

    def test_getLogger(self):
        logger = core.getLogger('foo')
        self.assertEqual(logger.getEffectiveLevel(), logging.DEBUG)

    def test_setLogLevel(self):
        logger = core.getLogger('foo')

        with self.assertLogs('foo', level='DEBUG') as cm:
            core.setLogLevel('foo', 'debug')
            logger.debug('debug')
            logger.info('info')
            logger.warning('warning')
            logger.error('error')
            logger.critical('critical')
        self.assertEqual(cm.output, ['DEBUG:foo:debug',
                                     'INFO:foo:info',
                                     'WARNING:foo:warning',
                                     'ERROR:foo:error',
                                     'CRITICAL:foo:critical'])

        with self.assertLogs('foo', level='INFO') as cm:
            core.setLogLevel('foo', 'info')
            logger.debug('debug')
            logger.info('info')
            logger.warning('warning')
            logger.error('error')
            logger.critical('critical')
        self.assertEqual(cm.output, ['INFO:foo:info',
                                     'WARNING:foo:warning',
                                     'ERROR:foo:error',
                                     'CRITICAL:foo:critical'])

        with self.assertLogs('foo', level='WARNING') as cm:
            core.setLogLevel('foo', 'warning')
            logger.debug('debug')
            logger.info('info')
            logger.warning('warning')
            logger.error('error')
            logger.critical('critical')
        self.assertEqual(cm.output, ['WARNING:foo:warning',
                                     'ERROR:foo:error',
                                     'CRITICAL:foo:critical'])

        # using shorthand for warning
        with self.assertLogs('foo', level='WARNING') as cm:
            core.setLogLevel('foo', 'warn')
            logger.debug('debug')
            logger.info('info')
            logger.warning('warning')
            logger.error('error')
            logger.critical('critical')
        self.assertEqual(cm.output, ['WARNING:foo:warning',
                                     'ERROR:foo:error',
                                     'CRITICAL:foo:critical'])

        with self.assertLogs('foo', level='ERROR') as cm:
            core.setLogLevel('foo', 'error')
            logger.debug('debug')
            logger.info('info')
            logger.warning('warning')
            logger.error('error')
            logger.critical('critical')
        self.assertEqual(cm.output, ['ERROR:foo:error',
                                     'CRITICAL:foo:critical'])

        with self.assertLogs('foo', level='CRITICAL') as cm:
            core.setLogLevel('foo', 'critical')
            logger.debug('debug')
            logger.info('info')
            logger.warning('warning')
            logger.error('error')
            logger.critical('critical')
        self.assertEqual(cm.output, ['CRITICAL:foo:critical'])

        with self.assertLogs('foo', level='CRITICAL') as cm:
            core.setLogLevel('foo', 'cRITiCaL')
            logger.debug('debug-mixedcase')
            logger.info('info-mixedcase')
            logger.warning('warning-mixedcase')
            logger.error('error-mixedcase')
            logger.critical('critical-mixedcase')
        self.assertEqual(cm.output, ['CRITICAL:foo:critical-mixedcase'])

        # using "correct" integer value
        with self.assertLogs('foo', level='ERROR') as cm:
            core.setLogLevel('foo', 40)
            logger.debug('debug-integer')
            logger.info('info-integer')
            logger.warning('warning-integer')
            logger.error('error-integer')
            logger.critical('critical-integer')
        self.assertEqual(cm.output, ['ERROR:foo:error-integer',
                                     'CRITICAL:foo:critical-integer'])

        # apparently any value is rounded up to the next logging level
        with self.assertLogs('foo', level='ERROR') as cm:
            core.setLogLevel('foo', 31)
            logger.debug('debug-integer')
            logger.info('info-integer')
            logger.warning('warning-integer')
            logger.error('error-integer')
            logger.critical('critical-integer')
        self.assertEqual(cm.output, ['ERROR:foo:error-integer',
                                     'CRITICAL:foo:critical-integer'])

        self.assertRaises(ValueError, core.setLogLevel, 'foo', 'i-dont-exist')


if __name__ == "__main__":
    unittest.main()

