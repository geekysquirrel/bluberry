__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2019, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import unittest
import json
import rsa

import core.BluberryLogging
from core.BluberryMessage import Message
from datetime import datetime

DTFORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'


class test_BluberryMessage(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")

        # generate test RSA keys
        (test_BluberryMessage.public1, test_BluberryMessage.private1) =\
            rsa.newkeys(2048, poolsize=8)
        (test_BluberryMessage.public2, test_BluberryMessage.private2) =\
            rsa.newkeys(2048, poolsize=8)
        (test_BluberryMessage.public3, test_BluberryMessage.private3) =\
            rsa.newkeys(2048, poolsize=8)

        with open('core/test/resources/test-message.json', 'r') as f:
            test_BluberryMessage.msg = json.loads(f.read())

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")

    def tearDown(self):
        self.__logger.info(f"Finished test {self._testMethodName}")

    def getMessage(self, key: object) -> Message:
        msg = Message()
        msg.deserialise(json.dumps(test_BluberryMessage.msg[str(key)]))
        return msg

    def test_sort(self):
        # chronological order: 1, 2, 14
        msg1 = self.getMessage(1)
        msg2 = self.getMessage(2)
        msg3 = self.getMessage(14)
        messages = [msg2, msg3, msg1]
        self.assertEqual(messages[0], msg2)
        self.assertEqual(messages[1], msg3)
        self.assertEqual(messages[2], msg1)
        messages.sort()
        self.assertEqual(messages[0], msg1)
        self.assertEqual(messages[1], msg2)
        self.assertEqual(messages[2], msg3)

    def test_create(self):
        msg = Message()
        self.assertIsNotNone(msg.getId())
        self.assertIsNone(msg.getPayload())
        self.assertIsNone(msg.getOriginNode())
        self.assertIsNone(msg.getDestination())
        self.assertEqual(msg.getReceivedBy(), dict())
        self.assertFalse(msg.isEncrypted())
        self.assertIsNone(msg.getOriginTimestamp())
        # UUID is random, cannot be tested

    def test_serialise_deserialise_round_trip(self):
        # unencrypted message
        msg = Message()
        msg.setOriginNode('node1')
        msg.setDestination('node2')
        msg.setOriginTimestamp('123')
        msg.setPayload('jdhfjhsdfsdjhfjkhsakfhksdhfjskl')

        msgAfter = msg.deserialise(msg.serialise())
        self.assertEqual(msg, msgAfter)

        # encrypted message
        msg.encrypt(test_BluberryMessage.public2, test_BluberryMessage.private2)

        msgAfter = msg.deserialise(msg.serialise())
        self.assertEqual(msg, msgAfter)

    def test_serialise_unencrypted(self):
        msg = Message()
        msg.setOriginNode('node1')
        msg.setDestination('node2')
        msg.setOriginTimestamp('1970-01-01T00:00:00.000000Z')
        msg.setPayload('This is a standard valid message')

        msg.serialise()
        self.assertIsNotNone(msg.getId())
        self.assertEqual(msg.getOriginNode(), 'node1')
        self.assertEqual(msg.getDestination(), 'node2')
        self.assertEqual(msg.getReceivedBy(), dict())
        self.assertFalse(msg.isEncrypted())
        self.assertEqual(msg.getOriginTimestamp(), '1970-01-01T00:00:00.000000Z')
        self.assertEqual(msg.getPayload(), 'This is a standard valid message')

    def test_serialise_wrong_origin_datatype(self):
        msg = Message()
        msg.setOriginNode(float("nan"))
        self.assertRaises(RuntimeError, msg.serialise)

    def test_deserialise_happy_case(self):
        msg = self.getMessage(1)
        self.assertEqual(msg.getId(), '1')
        self.assertEqual(msg.getOriginNode(), 'node2')
        self.assertEqual(msg.getDestination(), 'node1')
        self.assertEqual(msg.getReceivedBy(), dict())
        self.assertFalse(msg.isEncrypted())
        self.assertEqual(msg.getOriginTimestamp(), '1970-01-01T00:00:00.000000Z')
        self.assertEqual(msg.getPayload(), 'This is a standard valid message')

    def test_deserialise_fake_encrypted(self):
        # message that looks encrypted but hasn't actually gone through the process
        self.assertFalse(self.getMessage(2).isEncrypted())

    def test_deserialise_encrypted(self):
        self.assertTrue(self.getMessage(0).isEncrypted())

    def test_deserialise_missing_id(self):
        msg = Message()
        self.assertRaises(RuntimeError, msg.deserialise,
                          json.dumps(test_BluberryMessage.msg["3"]))

    def test_deserialise_missing_origin(self):
        msg = Message()
        self.assertRaises(RuntimeError, msg.deserialise,
                          json.dumps(test_BluberryMessage.msg["4"]))

    def test_deserialise_missing_destination(self):
        msg = Message()
        self.assertRaises(RuntimeError, msg.deserialise,
                          json.dumps(test_BluberryMessage.msg["5"]))

    def test_deserialise_missing_recipients(self):
        msg = Message()
        self.assertRaises(RuntimeError, msg.deserialise,
                          json.dumps(test_BluberryMessage.msg["6"]))

    def test_deserialise_missing_encrypted_key(self):
        self.assertEqual(self.getMessage(7).isEncrypted(), False)

    def test_deserialise_missing_origin_timestamp(self):
        msg = Message()
        self.assertRaises(RuntimeError, msg.deserialise,
                          json.dumps(test_BluberryMessage.msg["11"]))

    def test_deserialise_missing_payload(self):
        msg = Message()
        self.assertRaises(RuntimeError, msg.deserialise,
                          json.dumps(test_BluberryMessage.msg["12"]))

    def test_deserialise_empty_id(self):
        msg = Message()
        self.assertRaises(RuntimeError, msg.deserialise,
                          json.dumps(test_BluberryMessage.msg["24"]))

    def test_deserialise_null_id(self):
        msg = Message()
        self.assertRaises(RuntimeError, msg.deserialise,
                          json.dumps(test_BluberryMessage.msg["25"]))

    def test_deserialise_received_by(self):
        self.assertEqual(self.getMessage(13).getReceivedBy(), {})
        self.assertEqual(len(self.getMessage(13).getReceivedBy()), 0)
        self.assertEqual(len(self.getMessage(14).getReceivedBy()), 1)
        self.assertEqual(len(self.getMessage(15).getReceivedBy()), 2)
        self.assertEqual(len(self.getMessage(17).getReceivedBy()), 3)

    def test_deserialise_unordered_json(self):
        msg = self.getMessage(20)
        self.assertEqual(msg.getId(), '20')
        self.assertEqual(msg.getOriginNode(), 'node2')
        self.assertEqual(msg.getDestination(), 'node1')
        self.assertEqual(msg.getReceivedBy(), dict())
        self.assertFalse(msg.isEncrypted())
        self.assertEqual(msg.getOriginTimestamp(), '2019-01-02T11:37:45.098333Z')
        self.assertEqual(msg.getPayload(), 'This message\'s JSON is in the wrong order')

    def test_deserialise_wrong_datatype_encrypted(self):
        # wrong datatype for encrypted field
        self.assertFalse(self.getMessage(21).isEncrypted())
        self.assertFalse(self.getMessage('21a').isEncrypted())
        self.assertFalse(self.getMessage('21b').isEncrypted())
        self.assertFalse(self.getMessage('21c').isEncrypted())
        self.assertFalse(self.getMessage('21d').isEncrypted())

    def test_deserialise_wrong_datatype_string(self):
        self.assertEqual(self.getMessage(22).getId(), '22')

    def test_deserialise_wrong_datatype_dict(self):
        msg = Message()
        self.assertRaises(RuntimeError, msg.deserialise,
                          json.dumps(test_BluberryMessage.msg["23"]))

    def test_deserialise_very_old_timestamp(self):
        msg = self.getMessage(26)
        self.assertEqual(msg.getOriginTimestamp(), '1919-01-02T11:37:45.098338Z')
        dt = datetime.strptime(msg.getOriginTimestamp(), DTFORMAT)
        self.assertEqual(dt.timestamp(), -1609330934.901662)

    def test_deserialise_future_timestamp(self):
        msg = self.getMessage(28)
        self.assertEqual(msg.getOriginTimestamp(), '2919-01-02T11:37:45.098339Z')
        dt = datetime.strptime(msg.getOriginTimestamp(), DTFORMAT)
        self.assertEqual(dt.timestamp(), 29947664265.09834)

    def test_deserialise_long_message(self):
        self.assertEqual(len(self.getMessage(99).getPayload()), 11770)

    def test_receive(self):
        msg = self.getMessage(1)
        self.assertEqual(len(msg.getReceivedBy()), 0)
        msg.receive('node1', test_BluberryMessage.private1)
        self.assertEqual(len(msg.getReceivedBy()), 1)

        # check if the signature matches
        self.assertTrue(msg.isRecipientValid('node1', test_BluberryMessage.public1))
        self.assertFalse(msg.isRecipientValid('node1', test_BluberryMessage.public2))

        # that should have removed the invalid signature
        self.assertEqual(len(msg.getReceivedBy()), 1)

    def test_receive_again(self):
        msg = self.getMessage(1)
        self.assertEqual(len(msg.getReceivedBy()), 0)
        msg.receive('node1', test_BluberryMessage.private1)
        msg.receive('node1', test_BluberryMessage.private1)
        self.assertEqual(len(msg.getReceivedBy()), 1)

    def test_isRecipientValid_wrong_recipient(self):
        self.assertFalse(self.getMessage(8)
                         .isRecipientValid('node1', test_BluberryMessage.public1))

    def test_isRecipientValid_no_signature(self):
        self.assertFalse(self.getMessage(8).isRecipientValid('bob', None))

    def test_isRecipientValid_wrong_signature(self):
        self.assertFalse(self.getMessage(8).isRecipientValid('bob', test_BluberryMessage.public1))

    def test_isRecipientValid_invalid_key(self):
        msg = self.getMessage(8)
        msg.receive('node1', test_BluberryMessage.private1)
        self.assertFalse(msg.isRecipientValid('node1', None))

    def test_reachedDestination_not_received(self):
        self.assertFalse(self.getMessage(1).reachedDestination(test_BluberryMessage.public1))

    def test_reachedDestination_wrong_recipient_correct_key(self):
        msg = self.getMessage(1)
        msg.receive('node3', test_BluberryMessage.private3)
        self.assertFalse(msg.reachedDestination(test_BluberryMessage.public1))

    def test_reachedDestination_correct_recipient_wrong_key(self):
        msg = self.getMessage(1)
        msg.receive('node1', test_BluberryMessage.private2)
        self.assertFalse(msg.reachedDestination(test_BluberryMessage.public1))

    def test_reachedDestination_correct_recipient_correct_key(self):
        msg = self.getMessage(1)
        msg.receive('node1', test_BluberryMessage.private1)
        self.assertTrue(msg.reachedDestination(test_BluberryMessage.public1))

    def test_encrypt(self):
        msg = self.getMessage(1)
        self.assertFalse(msg.isEncrypted())
        self.assertEqual(msg.getPayload(), 'This is a standard valid message')
        msg.encrypt(test_BluberryMessage.public1, test_BluberryMessage.private1)
        self.assertTrue(msg.isEncrypted())
        self.assertRaises(RuntimeError, msg.encrypt,
                          test_BluberryMessage.public1,
                          test_BluberryMessage.private1)

    def test_decrypt_happycase(self):
        # use a message from node2 to node1
        msg = self.getMessage(1)
        msg.encrypt(test_BluberryMessage.public1, test_BluberryMessage.private2)
        msg.decrypt(test_BluberryMessage.private1, test_BluberryMessage.public2)
        self.assertFalse(msg.isEncrypted())
        self.assertEqual(msg.getPayload(), 'This is a standard valid message')

    def test_decrypt_unencrypted(self):
        msg = self.getMessage(1)
        self.assertRaises(RuntimeError, msg.decrypt,
                          test_BluberryMessage.private1,
                          test_BluberryMessage.public2)

    def test_decrypt_wrong_private_key(self):
        msg = self.getMessage(1)
        msg.encrypt(test_BluberryMessage.public1, test_BluberryMessage.private2)
        self.assertRaises(RuntimeError, msg.decrypt,
                          test_BluberryMessage.private1,
                          test_BluberryMessage.public1)

    def test_decrypt_wrong_public_key(self):
        msg = self.getMessage(1)
        msg.encrypt(test_BluberryMessage.public1, test_BluberryMessage.private2)
        self.assertRaises(RuntimeError, msg.decrypt,
                          test_BluberryMessage.private2,
                          test_BluberryMessage.public2)

    def test_decrypt_tampered_with_encrypted_message(self):
        msg = self.getMessage(1)
        msg.encrypt(test_BluberryMessage.public1, test_BluberryMessage.private2)
        msg.setPayload(msg.getPayload() + " <- I'm with stupid")
        self.assertRaises(RuntimeError, msg.decrypt,
                          test_BluberryMessage.private1,
                          test_BluberryMessage.public2)

    def test_decrypt_tampered_with_tag(self):
        msg = self.getMessage(1)
        msg.encrypt(test_BluberryMessage.public1, test_BluberryMessage.private2)
        msg1 = json.loads(msg.serialise())
        tag = msg1["tag"]
        if tag[0] == "x":
            msg1["tag"] = "X" + tag[1:]
        else:
            msg1["tag"] = "x" + tag[1:]
        msg = Message()
        msg.deserialise(json.dumps(msg1))
        self.assertRaises(RuntimeError, msg.decrypt,
                          test_BluberryMessage.private1,
                          test_BluberryMessage.public2)

    def test_decrypt_invalid_outer_signature(self):
        msg = self.getMessage(1)
        msg.encrypt(test_BluberryMessage.public1,
                    test_BluberryMessage.private2)
        msg1 = json.loads(msg.serialise())
        sigOuter = msg1["signatureOuter"]
        if sigOuter[0] == "x":
            msg1["signatureOuter"] = "X" + sigOuter[1:]
        else:
            msg1["signatureOuter"] = "x" + sigOuter[1:]
        msg = Message()
        msg.deserialise(json.dumps(msg1))
        self.assertRaises(RuntimeError, msg.decrypt,
                          test_BluberryMessage.private1,
                          test_BluberryMessage.public2)

    def test_decrypt_invalid_inner_signature(self):
        msg = self.getMessage(1)
        msg.encrypt(test_BluberryMessage.public1,
                    test_BluberryMessage.private2)
        msg1 = json.loads(msg.serialise())
        sigInner = msg1["signatureInner"]
        if sigInner[0] == "x":
            msg1["signatureInner"] = "X" + sigInner[1:]
        else:
            msg1["signatureInner"] = "x" + sigInner[1:]
        msg = Message()
        msg.deserialise(json.dumps(msg1))
        self.assertRaises(RuntimeError, msg.decrypt,
                          test_BluberryMessage.private1,
                          test_BluberryMessage.public2)

    def test_isValid(self):
        msg = Message()
        self.assertFalse(msg.isValid())
        msg.deserialise(json.dumps(test_BluberryMessage.msg["1"]))
        self.assertTrue(msg.isValid())
        msg.setOriginNode(7)
        self.assertFalse(msg.isValid())

    def test_getStr(self):
        self.assertEqual(str(self.getMessage(1)), "Message 1 from node2 to node1 sent at "
        "1970-01-01T00:00:00.000000Z, encrypted? False. It has  been received by {}. "
        "Payload:\nThis is a standard valid message")

    def test_get_B64(self):
        self.assertIsNone(Message().getSignatureInner())


if __name__ == "__main__":
    unittest.main()

