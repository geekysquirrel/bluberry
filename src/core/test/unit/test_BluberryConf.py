"""
BluberryConf test file
"""

__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2018, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import unittest
import json

import core.BluberryLogging
from core.BluberryConf import Conf


class test_BluberryConf(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")

    def tearDown(self):
        self.__logger.info(f"Finished test {self._testMethodName}")

    def test_init(self):

        # Invalid files
        self.assertRaises(RuntimeError, Conf, None)

        self.assertRaises(json.decoder.JSONDecodeError, Conf,
                          'core/test/resources/empty-file')

        self.assertRaises(FileNotFoundError, Conf,
                          'core/test/resources/conf1.json')

        # Required parts missing
        self.assertRaises(RuntimeError, Conf,
                          'core/test/resources/conf-invalid1.json')

        self.assertRaises(RuntimeError, Conf,
                          'core/test/resources/conf-invalid2.json')

        self.assertRaises(RuntimeError, Conf,
                          'core/test/resources/conf-invalid2.json')

        # Optional parts missing
        conf = Conf('core/test/resources/conf-nodb.json')
        self.assertFalse(conf.getConf()['db'])
        conf = Conf('core/test/resources/conf-nodb2.json')
        self.assertFalse(conf.getConf()['db'])
        conf = Conf('core/test/resources/conf-nomsg.json')
        self.assertFalse(conf.getConf()['msg'])
        conf = Conf('core/test/resources/conf-nomsg2.json')
        self.assertFalse(conf.getConf()['msg'])

        # Fully functional config file
        conf = Conf('core/test/resources/conf.json')
        # TODO: is this OK? Should it not come from a variable?
        conf = Conf('/bluberry/core/test/resources/conf.json')

    def test_getConf(self):
        conf = Conf('core/test/resources/conf.json')
        self.assertIsNotNone(conf.getConf())

    def test_getDbConf(self):
        conf = Conf('core/test/resources/conf.json')
        self.assertTrue(conf.getConf()['db'])

        self.assertEqual(conf.getDbConf(),
                         {'port': 3306, 'user': 'root', 'password': '1234'})

    def test_getMsgConf(self):
        conf = Conf('core/test/resources/conf.json')
        self.assertTrue(conf.getConf()['msg'])

        self.assertEqual(conf.getMsgConf(),
                         {'port': 5672, 'user': 'pika', 'password': 'pika'})

        conf.setConf({})
        self.assertFalse(conf.getConf())

    def test_get(self):
        conf = Conf('core/test/resources/conf.json')

        self.assertEqual(conf.get('db', 'port'), 3306)

        self.assertIsNone(conf.get('node', 'node1', 'publicKey'))
        self.assertIsNone(conf.get('nodes', 'node5', 'publicKey'))
        self.assertIsNone(conf.get('nodes', 'node2', 'privateKey'))
        self.assertEqual(conf.get('nodes', 'node1', 'publicKey'),
                         '/path/to/publicKeys/node1.pem')

    def test_set(self):
        conf = Conf('core/test/resources/conf.json')
        self.assertEqual(conf.get('db', 'port'), 3306)
        conf.set(['db', 'port'], 3307)
        self.assertEqual(conf.get('db', 'port'), 3307)


if __name__ == "__main__":
    unittest.main()

