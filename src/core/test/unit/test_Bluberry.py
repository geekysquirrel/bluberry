"""
Bluberry integration test file
"""

__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import unittest
import time
import os
import copy
from datetime import datetime
from typing import List
from unittest.mock import MagicMock, patch, PropertyMock

import core.BluberryLogging
from core.Bluberry import Bluberry, BluberryDBManagerService, BluberryMessageManagerService
from core.IBluberryDBReader import IBluberryDBReader
from core.IBluberryMessageReader import IBluberryMessageReader
from core.BluberryMessage import Message
from core.BluberryConf import Conf


class test_Bluberry(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        core.setLogLevel('Bluberry', 'debug')
        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")
        cls.__conf = Conf('core/conf/seedConf.json')
        cls.__logger.debug(cls.__conf.getConf())

    @classmethod
    def tearDownClass(cls):
        # make sure the threads have finished
        time.sleep(cls.__conf.get('local', 'interval') * 1.5)

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")
        self.__bb = None

    def tearDown(self):
        if self.__bb is not None:
            while self.__bb.isRunning():
                self.__bb.stop()
            self.__bb = None
        self.__logger.info(f"Finished test {self._testMethodName}")

    def setConfField(self, conf: Conf, keys: List[str], value: object) -> Conf:
        newconf = copy.deepcopy(conf)
        newconf.set(keys, value)
        return newconf

    def test_init_no_modules(self):
        conf = self.setConfField(self.__conf, ['local', 'db'], None)
        conf = self.setConfField(conf, ['local', 'msg'], None)
        self.__bb = Bluberry(conf)

        with self.assertLogs('Bluberry', level='WARNING') as cm:
            self.__bb.invokeDB()
        with self.assertLogs('Bluberry', level='WARNING') as cm:
            self.__bb.invokeMsg()


if __name__ == "__main__":
    unittest.main()

