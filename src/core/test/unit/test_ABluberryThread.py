"""
ABluberryThread test file
"""

__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2018, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import unittest
import time

import core.BluberryLogging
from core.ABluberryThread import ABluberryThread


class test_ABluberryThread(unittest.TestCase):

    INTERVAL = 0.01

    # dummy class inheriting from abstract class
    class BluberryThread(ABluberryThread):

        def conditionalAction(self):
            super().conditionalAction()

        def unconditionalAction(self):
            super().unconditionalAction()

    # alternative dummy class inheriting from abstract class
    class BluberrySlowThread(ABluberryThread):

        def __init__(self, name: str, interval: float):
            super(self.__class__, self).__init__(name, interval)
            self.__logger = core.getLogger(self.__class__.__name__)

        def conditionalAction(self):
            super().conditionalAction()
            self.__logger.debug('Doing something else...')

        def unconditionalAction(self):
            super().unconditionalAction()
            self.__logger.debug('Doing something...')
            time.sleep(2 * test_ABluberryThread.INTERVAL)
            self.__logger.debug('Done.')

    @classmethod
    def setUpClass(cls):
        core.setLogLevel('Bluberry', 'debug')
        cls.__logger = core.getLogger(cls.__name__)
        cls.__logger.info(f"Running test class {cls.__name__}")

    def setUp(self):
        self.__logger.info(f"Running test {self._testMethodName}")
        self.t = self.BluberryThread('BluberryThread', self.INTERVAL)

    def tearDown(self):
        if self.t.isRunning:
            self.t.stop()
            # give it some time - otherwise it may fail
            time.sleep(self.INTERVAL)
        self.t = None
        self.__logger.info(f"Finished test {self._testMethodName}")

    def test_getSet(self):
        self.assertEqual(self.t.getName(), 'BluberryThread')
        self.assertEqual(self.t.getInterval(), self.INTERVAL)
        self.t.setInterval(0.3)
        self.assertEqual(self.t.getInterval(), 0.3)

    def test_stopBetweenActions(self):
        self.t = self.BluberrySlowThread('BluberrySlowThread', 2 * self.INTERVAL)
        self.t.start()
        self.__logger.debug('sleep')
        time.sleep(self.INTERVAL)
        self.__logger.debug('wakey wakey')
        self.t.stop()
        self.assertFalse(self.t.isRunning())
        # TODO: not sure what to test here. it's getting the escape route
        # out of the run() loop before conditional action is executed.

    def test_start(self):
        self.assertFalse(self.t.isRunning())
        self.t.start()
        # give it some time - otherwise it may fail
        time.sleep(0.5 * self.INTERVAL)
        self.assertTrue(self.t.isRunning())

    def test_stop(self):
        self.t.start()
        # give it some time - otherwise it may fail
        time.sleep(0.5 * self.INTERVAL)
        self.assertTrue(self.t.isRunning())
        self.t.stop()
        self.assertFalse(self.t.isRunning())

    def test_pause(self):
        self.assertFalse(self.t.isPaused())
        self.t.pause()
        self.assertTrue(self.t.isPaused())

    def test_resume(self):
        self.t.pause()
        self.assertTrue(self.t.isPaused())
        self.t.resume()
        self.assertFalse(self.t.isPaused())

    def test_conditionalAction(self):
        lastConditionalAction = self.t.getLastConditionalAction()
        self.assertIsNone(lastConditionalAction)
        self.t.start()
        # give it some time - otherwise it may fail
        time.sleep(0.5 * self.INTERVAL)
        # should be executed while the thread is running
        lastConditionalAction = self.t.getLastConditionalAction()
        self.assertIsNotNone(lastConditionalAction)
        time.sleep(2 * self.INTERVAL)
        self.assertNotEqual(lastConditionalAction,
                            self.t.getLastConditionalAction())
        # should stop running while thread is paused
        self.t.pause()
        lastConditionalAction = self.t.getLastConditionalAction()
        time.sleep(2 * self.INTERVAL)
        self.assertEqual(lastConditionalAction,
                         self.t.getLastConditionalAction())

    def test_unconditionalAction(self):
        lastUnconditionalAction = self.t.getLastUnconditionalAction()
        self.assertIsNone(lastUnconditionalAction)
        self.t.start()
        # give it some time - otherwise it may fail
        time.sleep(0.5 * self.INTERVAL)
        # should be executed while the thread is running
        lastUnconditionalAction = self.t.getLastUnconditionalAction()
        self.assertIsNotNone(lastUnconditionalAction)
        time.sleep(2 * self.INTERVAL)
        self.assertNotEqual(lastUnconditionalAction,
                            self.t.getLastUnconditionalAction())
        # should *not* stop running while thread is paused
        self.t.pause()
        lastUnconditionalAction = self.t.getLastUnconditionalAction()
        time.sleep(2 * self.INTERVAL)
        self.assertNotEqual(lastUnconditionalAction,
                            self.t.getLastUnconditionalAction())


if __name__ == "__main__":
    unittest.main()

