__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2018, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import abc
from typing import List
from core.BluberryMessage import Message


class IBluberryMessageWriter(abc.ABC):
    """
    Bluberry message writer interface:
    This defines an interface for collecting local updates as messages and
    providing them to a local place, i.e. on this node.
    How this works is dependent on the implementation and all the interface
    needs to know is that a message was "published", i.e. is ready to be read
    by the other nodes' message reader.
    """

    @abc.abstractmethod
    def publish(self, messages: List[Message] = []):
        """
        This method publishes the messages it receives to a place where they
        can be "picked up" by message readers. This can be through a message
        queue, database, (temporary) file, REST interface or any other means.

        Args:
            messages : List[Message]
                The updates to be published locally for other nodes to pick up
        """

    @abc.abstractmethod
    def discard(self, messages: List[Message] = []):
        """
        Discards the given messages from the local place.

        Args:
            messages : List[Message]
                The local messages to be discarded
        """

    @abc.abstractmethod
    def notify(self, node: str, messages: List[Message] = []):
        """
        This method notifies a node that the given messages have been received
        by the designated recipient and are safe to delete from their place.

        Args:
            node : str
                The node to notify

            messages : List[Message]
                The messages the node should remove from its queue
        """
