from __future__ import annotations

__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2018, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"

import json
import uuid
import rsa
import base64
import binascii
from Crypto.Cipher import AES

import core.BluberryLogging


class Message:
    """
    Module for all things related to messages. This class represents a single
    message transmitted over Bluberry.

    Attributes:
        __id : str
            A random, static ID that is generated when the object is created. It  must not be
            changed afterwards, except when a message is deserialised and even then only
            internally by the `deserialise()` method
        __originNode : str
            The identifier of the node at which the message was created.
        __destination : str
            The identifier of the node for which the message is destined.
            Note that this can be a node identifier as well as a "group identifier" as long as
            the config file provides a mapping between groups and nodes. The reason this field is
            a single identifier rather than a set is that a message is most likely encrypted and
            can only be decrypted by the designated recipient by design. Unless the private key
            required for decrypting the message is shared (e.g. by a group), this can only be a
            single node.
        __receivedBy : dict
            A log, showing a record of nodes that received the message (both intermediate
            and designated recipients) and their signature as proof of receiving the message.
            The keys are the node IDs and the values are the node's signatures.
        __signatureInner : str
            A signature by the origin node, signing the unencrypted (i.e. original) message. It
            should be obtained as a very first step during the encryption process.
        __signatureOuter : str
            A signature by the origin node, signing the encrypted message.
            Its purpose is to be verifyable by anybody who has the sender's public key in
            order to verify is actually comes from the sender.
            This is needed as the inner signature won't be available for verification
            while the message is encrypted.
        __encryptedKey : str
            The encrypted random one-time symmetric key. Note that there must not be a
            public setter for this as it is be automatically (un-)set when calling the `encrypt()`
            and `decrypt()` methods. If this field is null (or None or equivalent), then the
            message is not encrypted. However, the existence of a value for this field alone does
            not automatically mean that the message is encrypted: it also needs a tag and nonce to
            be not null and the fields to be of the correct type. You may implement other extra
            checks, such as checking for the correct length of the fields and whether they contain
            any disallowed characters.
        __nonce : str
            The nonce used internally by the encryption/decryption algorithm.
        __tag : str
            The tag used internally by the encryption/decryption algorithm.
        __originTimestamp : str
            This is a string, representing a UTC datetime, so it's sortable.
            The format is '1970-01-01T00:00:00.000000Z'.
        __payload : str
            The payload of the message as set and used by the database module, in plain text or
            encrypted. Note that encrypted messages must be encoded, so that backslashes don't
            render the JSON invalid.
    """

    def __init__(self):
        """
        Creates a new message object, containing nothing but a newly generated ID.
        """
        self.__logger = core.getLogger(self.__class__.__name__)

        self.__id = str(uuid.uuid4())
        self.__originNode = None
        self.__destination = None
        self.__receivedBy = dict()
        self.__signatureInner = None
        self.__signatureOuter = None
        self.__encryptedKey = None
        self.__nonce = None
        self.__tag = None
        self.__originTimestamp = None
        self.__payload = None

    def __str__(self) -> str:
        """
        Create a human-readable string representation of the Message object for
        printing.

        Returns:
            str: A human-readable string representation of the message.
        """
        return (f'Message {self.__id} from {self.__originNode} to '
                f'{self.__destination} sent at '
                f'{self.__originTimestamp}, encrypted? {self.isEncrypted()}. '
                f'It has  been received by {self.__receivedBy}. '
                f'Payload:\n{self.__payload}')

    def __lt__(self, other: Message) -> bool:
        """
        Sort messages by date.

        Returns:
            str: True if the message is earlier than the "other" message.
        """
        return self.getOriginTimestamp() < other.getOriginTimestamp()

    def _asdict(self) -> dict:
        """
        Helper function to translate a more complex object for the purpose of
        serialisation.

        Returns:
            dict: A dictionary of this object's attributes.
        """
        return {
            'id': self.__id,
            'originNode': self.__originNode,
            'destination': self.__destination,
            'receivedBy': dict(self.__receivedBy),
            'signatureInner': self.__signatureInner,
            'signatureOuter': self.__signatureOuter,
            'encryptedKey': self.__encryptedKey,
            'nonce': self.__nonce,
            'tag': self.__tag,
            'originTimestamp': self.__originTimestamp,
            'payload': self.__payload
        }

    def serialise(self, sortKeys: bool = False, indentSpaces: int = None) -> str:
        """
        Transform the python object into JSON.

        Args:
            sortKeys : bool
                Whether to sort the resulting JSON object alphabetically by key.

            indentSpaces : int
                The number of spaces each new level should be indented.

        Returns:
            str: A JSON representation of the message object.

        Raises:
            RuntimeError: If the object could not be serialised.
        """
        serialised = ''
        try:
            serialised = json.dumps(
                self,
                default=lambda o: o._asdict(),
                allow_nan=False,
                sort_keys=sortKeys,
                indent=indentSpaces
            )
        except (TypeError, OverflowError, ValueError) as e:
            self.__logger.error(f"Could not serialise message:\n{self}")
            raise RuntimeError(f"Could not serialise {self}") from e

        return serialised

    def deserialise(self, jsonstring: str) -> Message:
        """
        Transform a JSON message back into a python object.

        Args:
            jsonstring : str
                A JSON string representing a message object.

        Returns:
            Message: The deserialised object

        Raises:
            RuntimeError: If the object could not be deserialised because
                          - the JSON is incomplete
                          - a field has the wrong datatype
        """

        jstr = json.loads(jsonstring)

        requiredFields = ['id', 'originNode', 'destination', 'receivedBy',
                          'payload', 'originTimestamp']
        missingFields = []
        for field in requiredFields:
            if field not in jstr:
                missingFields.append(field)
        if len(missingFields) > 0:
            raise(RuntimeError('Incomplete JSON message, missing '
                               f'{missingFields}: {jsonstring}'))

        # No need to check for required fields - we already made sure they're there
        self.__setId(jstr["id"])
        self.setOriginNode(jstr["originNode"])
        self.setDestination(jstr["destination"])
        self.setPayload(jstr["payload"])
        self.setOriginTimestamp(jstr["originTimestamp"])

        # extra checks
        if not isinstance(jstr["receivedBy"], dict):
            raise(RuntimeError('Wrong datatype for receivedBy field: '
                               f'{jstr["receivedBy"]}'))
        self.__setReceivedBy(dict(jstr["receivedBy"]))
        if 'signatureInner' in jstr:
            self.__setSignatureInner(jstr["signatureInner"])
        if 'signatureOuter' in jstr:
            self.__setSignatureOuter(jstr["signatureOuter"])
        if 'encryptedKey' in jstr:
            self.__setEncryptedKey(jstr["encryptedKey"])
        if 'nonce' in jstr:
            self.__setNonce(jstr["nonce"])
        if 'tag' in jstr:
            self.__setTag(jstr["tag"])

        return self

    def receive(self, nodeId: str, privateKey: str):
        """
        "Receive" the message, i.e. add the node ID to the list of recipients,
        signing the message as proof it has been received by this node.

        Args:
            nodeId : str
                The ID of the node receiving the message

            privateKey : str
                The private key, required to sign the message upon receiving
        """
        if nodeId in self.__receivedBy:
            self.__logger.warning('This message has already been received by node {nodeId}')

        sig = rsa.sign(self.getPayload().encode('utf8'), privateKey, 'SHA-512')
        self.__receivedBy[nodeId] = str(base64.b64encode(sig), 'utf8')

    def isRecipientValid(self, nodeId: str, publicKey: str) -> bool:
        """
        Check whether a recipient is valid. Leave false recipients intact in case a
        check is executed using the wrong key for a correct recipient.

        Args:
            nodeId : str
                The ID of the node to check

            publicKey : str
                The public key of that node, required to verify their signature

        Returns:
            bool: Whether the signature matches the node.
        """

        if nodeId not in self.__receivedBy:
            self.__logger.warning(f"{nodeId} is not a recipient")
            return False

        try:
            rsa.verify(self.getPayload().encode('utf8'),
                       Message.fromB64(self.__receivedBy[nodeId]), publicKey)
        except rsa.pkcs1.VerificationError:
            self.__logger.warning(f"{nodeId}\'s signature is invalid")
            return False
        except binascii.Error:
            self.__logger.warning(f"{nodeId}\'s signature could not be decoded")
            return False
        except AttributeError:
            self.__logger.warning(f"{nodeId}\'s public key is invalid")
            return False
        return True

    def reachedDestination(self, publicKey: str) -> bool:
        """
        Check whether the message has been received by the designated recipient.
        Make sure the signatures are correct.

        Args:
            publicKey : str
                The public key of the destination node, required to verify their signature

        Returns:
            bool: Whether the signature matches the node and ultimately whether the
                  message has been received by its final destination.
        """
        # The correct recipient is in the list and their signature is correct
        if set([self.__destination]).issubset(set(self.__receivedBy.keys()))\
           and self.isRecipientValid(self.__destination, publicKey):
            return True
        return False

    def encrypt(self, publicKey: str, privateKey: str) -> Message:
        """
        Encrypt the message payload for a designated recipient, using their public
        key. The message will also be signed using their private key.

        Args:
            publicKey : str
                The public key of the destination node, required to encrypt the message

            privateKey : str
                The private key of the origin node, required to sign the message

        Returns:
            Message: The encrypted message object

        Raises:
            RuntimeError: If the message is already encrypted
        """
        if self.isEncrypted():
            raise RuntimeError(f"This message is already encrypted")

        # sign unencrypted message
        sigInn = rsa.sign(self.getPayload().encode('utf8'),
                          privateKey, 'SHA-512')
        self.__setSignatureInner(str(base64.b64encode(sigInn), 'utf8'))

        # generate a random AES key
        aes_key = rsa.randnum.read_random_bits(256)
        cipher = AES.new(aes_key, AES.MODE_EAX)
        self.__setNonce(cipher.nonce)

        # encrypt the payload with the random key
        ciphertext, tag =\
            cipher.encrypt_and_digest(self.getPayload().encode('utf8'))
        self.__setTag(tag)
        # use base64 for binary data
        self.setPayload(str(base64.b64encode(ciphertext), 'utf8'))

        # encrypt the AES key with the RSA key
        aes_key_encrypted = rsa.encrypt(aes_key, publicKey)
        self.__setEncryptedKey(aes_key_encrypted)

        # sign encrypted message
        sigOut = rsa.sign(self.getPayload().encode('utf8'),
                          privateKey, 'SHA-512')
        self.__setSignatureOuter(str(base64.b64encode(sigOut), 'utf8'))

        return self

    def decrypt(self, privateKey: str, publicKey: str) -> Message:
        """
        Decrypt the message using the appropriate private key. Also check whether
        the sender's signature is correct using their public key.

        Args:
            privateKey : str
                The private key of the recipient node, required to decrypt the message

            publicKey : str
                The public key of the origin node, required to verify the signatures

        Returns:
            Message: The decrypted message object

        Raises:
            RuntimeError: In the following cases:
                          - the message is (already) unencrypted
                          - either of the signatures are invalid
                          - the random key could not be decrypted
                          - the message is otherwise corrupt
        """
        if not self.isEncrypted():
            raise(RuntimeError(f"Trying to decrypt unencrypted message"))

        # check outer signature
        try:
            rsa.verify(self.getPayload().encode('utf8'),
                       self.getSignatureOuter(), publicKey)
        except rsa.pkcs1.VerificationError as e:
            raise RuntimeError("The outer signature was invalid") from e

        # decrypt AES key
        try:
            aes_key = rsa.decrypt(self.__getEncryptedKey(), privateKey)
        except rsa.pkcs1.DecryptionError as e:
            raise RuntimeError("Could not decrypt the random key") from e

        # decrypt payload with decrypted key
        cipher = AES.new(aes_key, AES.MODE_EAX, nonce=self.__getNonce())
        decoded = base64.b64decode(self.getPayload())
        plaintext = cipher.decrypt(decoded).decode('utf8')

        # check inner signature
        try:
            rsa.verify(plaintext.encode('utf8'),
                       self.getSignatureInner(), publicKey)
        except rsa.pkcs1.VerificationError as e:
            raise RuntimeError("The inner signature was invalid") from e

        # happy now - use message
        try:
            cipher.verify(self.__getTag())
            self.setPayload(plaintext)
            self.__setNonce(None)
            self.__setTag(None)
        except ValueError as e:
            raise RuntimeError("The message was corrupted") from e

        self.__setEncryptedKey(None)

        return self

    def isValid(self) -> bool:
        """
        Check if the message is valid.

        Returns:
            bool: Whether the message is valid, i.e. all required fields are present and the
                  datatypes are correct.
        """
        # check if the values of fields are of the correct datatype
        noneType = type(None)
        if not type(self.__id) == str or \
           type(self.__originNode) not in [str, noneType] or\
           type(self.__destination) not in [str, noneType] or\
           type(self.__receivedBy) not in [dict, noneType] or\
           type(self.__signatureInner) not in [str, noneType] or\
           type(self.__signatureOuter) not in [str, noneType] or\
           type(self.__encryptedKey) not in [str, noneType] or\
           type(self.__nonce) not in [str, noneType] or\
           type(self.__tag) not in [str, noneType] or\
           type(self.__payload) not in [str, noneType]:
            return False

        # check if all required fields are contained
        if (
            self.__id is None
            or self.__originNode is None
            or self.__destination is None
            or self.__receivedBy is None
            or self.__payload is None
            or self.__originTimestamp is None
        ):
            return False

        return True

    def isEncrypted(self) -> bool:
        """
        Check if the message is encrypted.

        Returns:
            bool: Whether the message is encrypted, i.e. has all fields set that are set
                  during the encryption and required by the decryption.
        """
        return (self.__encryptedKey is not None
                and type(self.__encryptedKey) == str
                and self.__tag is not None
                and self.__nonce is not None)

    # Static methods ##########################################################

    @staticmethod
    def toB64(thing: bytes) -> str:
        """
        Helper method to get an encoded string version of a binary object

        Args:
            thing : bytes
                A binary object

        Returns:
            str: The base64 encoded string representing that object
        """
        if thing in ['', True, False, None]:
            return None
        else:
            if type(thing) == bytes:
                return str(base64.b64encode(thing), 'utf8')
            else:
                return thing

    @staticmethod
    def fromB64(thing: str) -> bytes:
        """
        Helper method to get a binary object from an encoded string

        Args:
            thing : str
                A base64 encoded representation of a binary object

        Returns:
            bytes: The binary object
        """
        if thing is None:
            return thing
        return base64.b64decode(thing)

    # Getters/Setters #########################################################

    def getId(self) -> str:
        return self.__id

    # This should not be used externally - only the deserialisation method can
    # use this
    def __setId(self, newid: str):
        if newid in ['', True, False, None]:
            raise RuntimeError(f"Invalid ID: {newid}")
        self.__id = str(newid)

    # These should not be used externally - only the encryption/decryption
    # methods can use this
    def __getEncryptedKey(self) -> str:
        return Message.fromB64(self.__encryptedKey)

    def __setEncryptedKey(self, key: str):
        self.__encryptedKey = Message.toB64(key)

    def __getNonce(self) -> str:
        return Message.fromB64(self.__nonce)

    def __setNonce(self, nonce: str):
        self.__nonce = Message.toB64(nonce)

    def __getTag(self) -> str:
        return Message.fromB64(self.__tag)

    def __setTag(self, tag: str):
        self.__tag = Message.toB64(tag)

    def getOriginNode(self) -> str:
        return self.__originNode

    def setOriginNode(self, originNode: str):
        self.__originNode = originNode

    def getDestination(self) -> str:
        return self.__destination

    def setDestination(self, destination: str):
        self.__destination = destination

    def getReceivedBy(self) -> dict:
        return self.__receivedBy

    # A node cannot receive a message on behalf of another node.
    def __setReceivedBy(self, receivedBy: dict):
        self.__receivedBy = receivedBy

    def getSignatureInner(self) -> {}:
        return Message.fromB64(self.__signatureInner)

    def __setSignatureInner(self, signatureInner: str):
        self.__signatureInner = Message.toB64(signatureInner)

    def getSignatureOuter(self) -> {}:
        return Message.fromB64(self.__signatureOuter)

    def __setSignatureOuter(self, signatureOuter: str):
        self.__signatureOuter = Message.toB64(signatureOuter)

    def getOriginTimestamp(self) -> str:
        return self.__originTimestamp

    def setOriginTimestamp(self, originTimestamp: str):
        self.__originTimestamp = originTimestamp

    def getPayload(self) -> str:
        return self.__payload

    def setPayload(self, payload: str):
        self.__payload = payload

