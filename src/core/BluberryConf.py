__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"


import json
import os
import copy
from pathlib import Path
from typing import List

import core.BluberryLogging


class Conf:
    """
    A class for reading Bluberry distributed configuration files.

    Attributes:
        __conf : dict
            The internal representation of the configuration object, being the
            result of `json.loads()`.
    """

    def __init__(self, path: str):
        """
        The constructor reads the configuration file(s) from disk into memory.

        Args:
            path : str
                The path of the core configuration file relative to the main directory.

        Raises:
            RuntimeError: if no config file could be found at the path.
        """
        self.__logger = core.getLogger(self.__class__.__name__)
        self.__conf = None

        if path is None:
            raise RuntimeError("No configuration file supplied.")
        else:
            self.__logger.debug(f"Trying to load config file from {path}")

        # convert relative path to absolute path
        if not path.startswith('/'):
            path = Path(f"{os.environ['BB_WORKDIR']}{os.sep}{path}")
        else:
            path = Path(path)

        if not path.exists():
            self.__logger.error(f"Configuration file {path} doesn't exist!")
            raise FileNotFoundError(f"Configuration file {path} doesn't exist!")

        with open(path, 'r') as f:
            self.__conf = json.loads(f.read())
            self.__logger.info(f"Loaded config file from {path}")

            # main configuration file - needs to be present!
            if ('local' not in self.__conf or self.__conf['local'] is None
                    or len(self.__conf['local']) == 0
                    or 'nodeId' not in self.__conf['local']
                    or 'network' not in self.__conf['local']
                    or 'db' not in self.__conf['local']
                    or 'msg' not in self.__conf['local']):
                raise RuntimeError(f"Invalid config file '{path}'")

            # database config - optional
            if ('dbconf' in self.__conf['local']
                    and self.__conf['local']['dbconf'] is not None
                    and self.__conf['local']['dbconf']):

                # TODO: replace Path with os.path
                dbconfpath = Path(f"{os.environ['BB_WORKDIR']}{os.sep}"
                                  f"{self.__conf['local']['dbconf']}")

                if os.path.isfile(dbconfpath):
                    with open(dbconfpath, 'r') as dbc:
                        self.__conf['db'] = json.loads(dbc.read())
                else:
                    self.__logger.warning("Could not find database "
                                          f"configuration file {dbconfpath}.")
            else:
                self.__logger.warning("No database configuration found.")

            # messaging config - optional
            if ('msgconf' in self.__conf['local']
                    and self.__conf['local']['msgconf'] is not None
                    and self.__conf['local']['msgconf']):

                # TODO: replace Path with os.path
                msgconfpath = Path(f"{os.environ['BB_WORKDIR']}{os.sep}"
                                  f"{self.__conf['local']['msgconf']}")

                if os.path.isfile(msgconfpath):
                    with open(msgconfpath, 'r') as msgc:
                        self.__conf['msg'] = json.loads(msgc.read())
                else:
                    self.__logger.warning("Could not find messaging "
                                       f"configuration file {msgconfpath}.")
            else:
                self.__logger.warning("No messaging configuration found.")

    def get(self, *args) -> object:
        """
        Get a value from a config file using the keys as a path to it.
        Note that this happens "by value" so changing the returned object does not change
        the internal config of this class.

        Args:
            *args : str
                A variable number of strings, so to get `__config['foo']['bar']['baz']` you
                should call this method passing in `get('foo', 'bar', 'baz')`.

        Returns:
            object: The value found for the given key(s) or None if it wasn't found.

        Examples:
            The arguments passed to this method describe a path through the `self.__conf` dict:

            >>> my_conf_object.get('db', 'connection', 'port')
            1234
        """
        current = copy.deepcopy(self.__conf)
        found = []
        for arg in args:
            if arg in current:
                current = current[arg]
                found.append(arg)
            else:
                errormsg = f"Could not find {arg} in config"
                if len(found) > 0:
                    errormsg = f"{errormsg}/{'/'.join(found)}."
                else:
                    errormsg = f"{errormsg}."
                self.__logger.error(errormsg)
                return None
        return current

    def set(self, keys: List[str], value: object):
        """
        Set a value in a config file using the keys as a path to it.

        Args:
            keys : List[str]
                A List of strings, so to set `__config['foo']['bar']['baz']` to 'newvalue' you
                should call this method passing in `set(['foo', 'bar', 'baz'], 'newvalue')`
            value : object
                The new value to set for the key

        Examples:
            The list passed to this method describes a path through the `self.__conf` dict:

            >>> my_conf_object.set(['db', 'connection', 'port'], 1234)
        """
        # build a "path" of partial config
        parts = []
        previousPart = self.getConf()
        for key in keys:
            # prepend the partial config
            parts.insert(0, previousPart)
            previousPart = previousPart[key]
        # can safely ignore the last part - it's the old value we want to replace
        self.__logger.debug(f"Replacing {previousPart} with {value} in conf/{'/'.join(keys)}")
        # replace the value, going backwards from inner to outer part
        newvalue = value
        for part in parts:
            # go backwards through the keys
            part[keys.pop()] = newvalue
            newvalue = part
        # the last "newvalue" will be the full conf object with the value replaced.
        self.setConf(newvalue)

    def getConf(self) -> object:
        return copy.deepcopy(self.__conf)

    def setConf(self, conf: object):
        self.__conf = copy.deepcopy(conf)

    def getDbConf(self) -> object:
        return copy.deepcopy(self.__conf['db'])

    def getMsgConf(self) -> object:
        return copy.deepcopy(self.__conf['msg'])

