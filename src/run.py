"""
Entrypoint for Bluberry
"""

__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"

import time
import os
import sys
from datetime import datetime

import core.BluberryLogging
from core.Bluberry import Bluberry
from core.BluberryConf import Conf
from core.BluberryMessage import Message


def createMessage(origin: str, destination: str, payload: str) -> Message:
    msg = Message()
    msg.setOriginNode(origin)
    msg.setDestination(destination)
    msg.setOriginTimestamp(datetime.now()
                           .strftime('%Y-%m-%dT%H:%M:%S.%fZ'))
    msg.setPayload(payload)
    return msg


if __name__ == '__main__':

    logger = core.getLogger("Bluberry-entrypoint")

    logger.debug(os.environ)

    try:
        confname = 'core/conf/conf.json'
        while not os.path.isfile(confname):
            time.sleep(1)
            logger.debug('Waiting for configuration file...')
        conf = Conf(confname)

        # dynamically import modules here. This is needed for the reflection,
        # which only considers classes that have already been loaded
        importModules = ['msg', 'db']
        for mod in importModules:
            moddir = f"/bluberry/{mod}/{conf.get('local', mod)}"
            logger.debug(f"Checking directory {moddir}")
            for f in os.listdir(moddir):
                fname = f[:-3]
                fullfile = f"{moddir}/{f}"
                if os.path.isfile(fullfile) and not f.startswith('_')\
                   and fullfile.endswith('.py'):
                    logger.debug(f"Trying to import {fname} from {moddir}")
                    # corresponds to "from msg.xyz.MyClass import MyClass"
                    __import__(f"{mod}.{conf.get('local', mod)}.{fname}",
                               fromlist=[fname])

        bb = Bluberry(conf)
        bb.startServices()

        messages = [createMessage('node1', 'node2', 'update1'),
                    createMessage('node1', 'node2', 'update2'),
                    createMessage('node1', 'node3', 'update1')]
        bb.receivedDBUpdates(messages)

        while True:
            logger.debug('Bluberry is still running')
            time.sleep(bb.getConf().get('local', 'interval') * 10)

    except KeyboardInterrupt:
        logger.info("Exiting Bluberry")
        bb.stop()
        # wait for services to finish
        while bb.isRunning():
            time.sleep(0.1)
        logger.info("Services stopped. Goodbye!")
        sys.exit(0)

