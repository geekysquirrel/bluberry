# This is to be exeuted from within the docker container.
# The working directory is defined in the dockerfile and we're now in it.

# Time to wait before automatically running the integration tests.
# This is required as the test stacks need time to start up their services.
TEST_WAIT?=10

# This means test does not generate any artifacts, meaning it will always be run as it's never
# "up to date" due to no existing test reports.
.PHONY: setup unittest integrationtest

.SILENT: setup update-config unittest integrationtest test-run doc run

# This target generates configuration files and keys for a given number of nodes.
# It does not find out the addresses/host names/ports for each of the nodes because
# at the time this is executed, they may not be running.
# The results are saved in a directory structure on the container and need to be
# retrieved and copied onto the host machine.
setup:
	$(info ***** INSIDE DOCKER CONTAINER $(CONTAINER_NAME): Running setup script *****)
	# Available env vars: NUM_NODES, SETUP_DIR, NODE_PREFIX, DB, MSG
	python3 setup.py

# This is used while a container is already running to update the configuration file on the fly.
# TODO: check if bluberry services restart is required (probably!).
update-config:
	$(info ***** INSIDE DOCKER CONTAINER $(CONTAINER_NAME): Updating run-time config *****)
	python3 update-config.py

# The - in front of the commands means that make will keep going despite errors (i.e. non-zero
# return codes).
unittest:
	$(info ***** INSIDE DOCKER CONTAINER $(CONTAINER_NAME): Running unit test target *****)
	echo "***** Running static code analysis *****"
	-pycodestyle $(MODULE) --show-source --statistics --count \
	  --ignore=W391,W503,E121,E126,E127,E128 --max-line-length=98
	echo "Done."
	echo "***** Finding unused imports and variables *****"
	- export PCREGREP_COLOUR="38;5;208"; python3 -m pyflakes $(MODULE) \
	  | pcregrep --colour=always " .*used.*" | grep -v "__init__.py"
	echo "Done."
	echo "Finding TODOs"
	# Colour codes are ANSI for up to 256 colours,
	# see https://en.wikipedia.org/wiki/ANSI_escape_code
	- export PCREGREP_COLOUR="38;5;177"; pcregrep --colour=always -MHnr \
	  "\n\s*(--|//|%|/\*)?\s*#\s?(TODO|FIXME|XXX|NOTE|BUG|HACK).*(\n\s*#.*)*" $(MODULE)
	echo "***** Running unit tests *****"
	# Usage:
	# make unittest
	# make unittest MODULE=.
	# make unittest MODULE=core
	# make unittest MODULE=db
	# make unittest MODULE=msg
	# make unittest MODULE=msg.rabbitmq.test.unit.test_SQLiteConnector
	-mod=$(MODULE); \
	command='.'; \
	case $$mod in \
	core) command="discover -p test_*.py -t . -s ./core"; \
	   ;; \
	msg) command="discover -p test_*.py -t . -s ./msg/"$(MSG); \
	   ;; \
	db) command="discover -p test_*.py -t . -s ./db/"$(DB); \
	   ;; \
	.) command="discover -p test_*.py"; \
	   ;; \
	*) \
	    command=$(MODULE); \
	;; \
	esac; \
	coverage run --branch --append --omit="*/test/unit/*","*/__init__.py" \
	  -m unittest $$command
	echo "***** Testing unit test coverage *****"
	#-coverage report -m
	-coverage html
	-coverage-badge -o coverage-badge.svg

integrationtest:
	$(info ***** INSIDE DOCKER CONTAINER $(CONTAINER_NAME): Running integration tests *****)
	# This is to wait for all containers to spin up and services to start
	sleep $(TEST_WAIT)
	echo "***** Running integration tests *****"
	# Usage:
	# make integration-run
	# make integration-run MODULE=.
	# make integration-run MODULE=core
	# make integration-run MODULE=db
	# make integration-run MODULE=msg
	# make integration-run MODULE=msg.rabbitmq.test.integration.itest_BluberryRabbitMQMessageReader
	-mod=$(MODULE); \
	command='.'; \
	case $$mod in \
	core) command="discover -p itest_*.py -t . -s core"; \
	   ;; \
	msg) command="discover -p itest_*.py -t . -s msg."$(MSG); \
	   ;; \
	db) command="discover -p itest_*.py -t . -s db."$(DB); \
	   ;; \
	.) command="discover -p itest_*.py"; \
	   ;; \
	*) \
	    command=$(MODULE); \
	;; \
	esac; \
	coverage run --branch --append --omit="*/test/integration/*","*/__init__.py" \
	  -m unittest $$command
	echo "***** Testing integration test coverage *****"
	#-coverage report -m
	-coverage html
	-coverage-badge -o coverage-badge.svg

test-run:
	$(info ***** INSIDE DOCKER CONTAINER $(CONTAINER_NAME): Running Bluberry test *****)
	python3 hello.py

doc:
	$(info ***** INSIDE DOCKER CONTAINER $(CONTAINER_NAME): Generating docs *****)
	pdoc3 --html -o pdoc core db.$(DB) msg.$(MSG)
	python3 generate-docs.py

run:
	$(info ***** INSIDE DOCKER CONTAINER $(CONTAINER_NAME): Running Bluberry *****)
	python3 run.py
	
