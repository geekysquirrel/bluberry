"""
Generates documentation for all modules using pdoc

Nicked some of the code from https://github.com/pdoc3/pdoc/blob/master/pdoc/cli.py
"""

__author__ = "Stefanie Wiegand"
__copyright__ = "Copyright 2020, University of Southampton"
__email__ = "s.wiegand@soton.ac.uk"

import os

import core.BluberryLogging

DIR = f"{os.environ['BB_WORKDIR']}{os.sep}pdoc"

logger = core.getLogger("Pdoc")
logger.info("Generating documentation overview")

with open(f"{DIR}/index.html", 'w+') as html_file:
    html_file.write("<!DOCTYPE html><html>"
    "<head><title>Bluberry code documentation</title></head>"
    "<body>"
    "<h1>Bluberry code documentation</h1>"
    "<ul>"
    "<li><a href='./core/index.html'>core</a><br />The core module</li>"
    f"<li><a href='./db/{os.environ['DB']}/index.html'>db</a><br />"
    f"  The database module, using {os.environ['DB']}</li>"
    f"<li><a href='./msg/{os.environ['MSG']}/index.html'>msg</a><br />"
    f"  The messaging module, using {os.environ['MSG']}</li>"
    "</ul>"
    "</body>"
    "</html>")

logger.info("Done.")
