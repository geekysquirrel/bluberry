# This is the outer makefile, i.e. the file used from the host machine in order to use the
# bluberry application in a containerised way.

# Configure your application here #############################################

BUILD:=latest
DB:= mysql
MSG:=rabbitmq

# Number of nodes to be set up/spun up. Can either be set here as a default or
# passed to the make target: `make target NUM_NODES=x`.
# The names will start with 1 and go up to and including this number.
NUM_NODES?=3

# The prefix for the node ID used for Bluberry nodes. It will have a number
# appended to make it unique when multiple containers run simultaneously.
NODE_PREFIX:=node

# Prefix for containers. The names of containers will look like this:
#   prefixstacknumber_servicename_1
CONTAINER_PREFIX:=bb

# The working directory on the Bluberry core container
BB_WORKDIR:=/bluberry

# Test config #################################################################

# Which container number should be the "home" node, i.e. where to run the
# integration tests. It also defines the node on which to run the application.
HOME_NODE?=1

# When attaching to a node, you need to give the name of which node you'd like to attach to.
# Defaults to the home node.
ATTACH_NODE?=$(HOME_NODE)

# Name of the test network connecting all test containers (effectively
# simulating the internet). No hyphens please!
ITEST_NET:=itest_network

# Time to wait before automatically running the integration tests.
# This is required as the test stacks need time to start up their services.
TEST_WAIT?=10

# Which module(s) to test (. (=all), core, db, msg, app)
MODULE?=.

# Internal config - don't touch ###############################################

# Stop the script if the number of nodes is not set
ifeq ($(NUM_NODES),)
  $(error NUM_NODES is not set)
endif

SHELL:=/bin/bash # Use bash syntax

# Core config
BB_NAME = bluberry_core
BB_TAG = bluberry:$(BUILD)
BB_DIR = ./src/core
BB_DF = $(BB_DIR)/conf/Dockerfile

# Database module
DB_NAME = bluberry_db
DB_TAG = $(DB_NAME):$(BUILD)
DB_DIR = ./src/db/$(DB)
DB_DF = $(DB_DIR)/conf/Dockerfile

# Messaging module
MSG_NAME = bluberry_msg
MSG_TAG = $(MSG_NAME):$(BUILD)
MSG_DIR = ./src/msg/$(MSG)
MSG_DF = $(MSG_DIR)/conf/Dockerfile

# Utility container names - need to be unique as there's only ever one running
# simultaneously.
UPDATE_CONTAINER:=bluberry_update
SETUP_CONTAINER:=bluberry_setup
RUN_CONTAINER:=bluberry_run
TEST_CONTAINER:=bluberry_test
DOC_CONTAINER:=bluberry_doc

# This defines a directory on a container and thus already is inside the /src dir,
# or /bluberry as it is called on the container.
SETUP_DIR:=setup

# Don't print verbose output from make target - only print output of commands.
.SILENT: all build --update-image setup --create-test-network --spin-up-nodes --update-config unittest up --live-copy attach integration-run integration-down integrationtest test run test-run doc clean

# Targets #####################################################################

# This first target is the default target.
all:
	bold=$(tput bold); \
	normal=$(tput sgr0); \
	echo -e "No target given. Please use one of these:\n"; \
	echo -e "- \033[1mbuild\033[0m \t\t\tBuild the docker images for Bluberry"; \
	echo -e "- \033[1msetup [NUM_NODES=n]\033[0m \t\tGenerate configuration files for n nodes"; \
	echo -e "- \033[1mup [NUM_NODES=n]\033[0m \t\tStart n nodes"; \
	echo -e "- \033[1mattach [ATTACH_NODE=n]\033[0m:\tAttach the terminal to this node to view"; \
	echo -e "\t\t\t\tits output as it runs."; \
	echo -e "- \033[1mtest\033[0m \t\t\t\trun unit- and integration tests"; \
	echo -e "- \033[1munittest\033[0m \t\t\trun unit tests (will run in dedicated container)"; \
	echo -e "- \033[1mintegrationtest [TEST_WAIT=n]\033[0m run integration tests (will kill "; \
	echo -e "\t\t\t\tall currently running nodes)."; \
	echo -e "\t\t\t\tWait n seconds before running the tests"; \
	echo -e "- \033[1mrun [HOME_NODE=n]\033[0m \t\tRun the application on node number n"; \
	echo -e "- \033[1mtest-run\033[0m \t\t\truns a \"Hello, world!\" like test in python"; \
	echo -e "- \033[1mdoc\033[0m \t\t\t\tgenerates the documentation"; \
	echo -e "- \033[1mclean\033[0m \t\t\tstop and delete all containers, networks,"; \
	echo -e "\t\t\t\tvolumes, and remove all generated files."; \
	echo -e "\t\t\t\t\033[1mUse with caution!"

build:
	$(info ***** ON HOST MACHINE: Building Bluberry & modules *****)
	
	if test -d $$DB_DF; then \
		docker build -f $(DB_DF) -t $(DB_TAG) $(DB_DIR) ; \
	else \
		echo ERROR: Could not build Bluberry database module - dockerfile is missing! ; \
		return 1 ; \
	fi
	if test -d $$MSG_DF; then \
		docker build -f $(MSG_DF) -t $(MSG_TAG) $(MSG_DIR) ; \
	else \
		echo ERROR: Could not build Bluberry messaging module - dockerfile is missing! ; \
		return 1 ; \
	fi
	# Build core last as it requires all dependencies and thus takes a long time.
	if test -d $$BB_DF; then \
		docker build --build-arg DB=$(DB) --build-arg MSG=$(MSG) -f $(BB_DF) -t $(BB_TAG) . ; \
	else \
		echo ERROR: Could not build Bluberry core - dockerfile is missing! ; \
		return 1 ; \
	fi

# This fires up a new container based on the latest bluberry image, which is kept running.
# It removes the old files and then copies the (updated) source files from the host onto
# the container. The changes from the container are then committed back to the image,
# overriding the old version.
--update-image:
	$(info ***** ON HOST MACHINE: Updating Bluberry source files in docker image *****)
	-docker run -it -d --name $(UPDATE_CONTAINER) $(BB_TAG)
	-docker exec $(UPDATE_CONTAINER) rm -rf $(BB_WORKDIR)
	-docker cp src/. $(UPDATE_CONTAINER):$(BB_WORKDIR)
	# TODO: do we want this to be copied back to the host?
#	-docker exec $(UPDATE_CONTAINER) autopep8 $(BB_WORKDIR)/ --recursive --in-place
	# note: docker commit doesn't remove old files due to layering!
	-docker commit $(UPDATE_CONTAINER) $(BB_TAG)
	-docker kill $(UPDATE_CONTAINER)
	docker rm $(UPDATE_CONTAINER)

# Generate setup (config and keys) in separate container and copy the resulting
# directory structure onto the host.
setup: --update-image
	$(info ***** ON HOST MACHINE: Setting up $(NUM_NODES) nodes *****)
	test "$(NUM_NODES)" -gt 0
	-docker run -it -d -e CONTAINER_NAME=$(SETUP_CONTAINER) --name $(SETUP_CONTAINER) $(BB_TAG)
	-docker exec -e NUM_NODES=$(NUM_NODES) -e SETUP_DIR=$(SETUP_DIR) \
	  -e NODE_PREFIX=$(NODE_PREFIX) -e DB=$(DB) -e MSG=$(MSG) -e BB_WORKDIR=$(BB_WORKDIR) \
	  $(SETUP_CONTAINER) make setup
	-docker cp $(SETUP_CONTAINER):$(BB_WORKDIR)/$(SETUP_DIR) $(SETUP_DIR)
	-docker kill $(SETUP_CONTAINER)
	docker rm $(SETUP_CONTAINER)

--create-test-network:
	docker network create $(ITEST_NET)

--spin-up-nodes: setup
	$(info ***** ON HOST MACHINE: Spinning up $(NUM_NODES) nodes *****)
	test "$(NUM_NODES)" -gt 0
	n=1 ; while [[ $$n -le $(NUM_NODES) ]] ; do \
	    echo "Spinning up stack "$(NODE_PREFIX)$$n; \
	    BB_BUILD=$(BUILD) NUM_NODES=$(NUM_NODES) NODE_PREFIX=$(NODE_PREFIX) \
	    THIS_NODE=$(NODE_PREFIX)$$n BB_WORKDIR=$(BB_WORKDIR) \
	    docker-compose -p $(CONTAINER_PREFIX)$$n \
	      -f $(BB_DIR)/conf/docker-compose.yml \
	      -f $(DB_DIR)/conf/docker-compose.yml \
	      -f $(MSG_DIR)/conf/docker-compose.yml \
	    up -d; \
	    ((n = n + 1)) ; \
	done

# "Private" target - should only be called from within other targets as it depends
# on $(NUM_NODES) having been spun up and $(ITEST_NET) having been created.
--update-config: --create-test-network --spin-up-nodes
	$(info ***** ON HOST MACHINE: Updating config for all running containers *****)
	# Read container details and pass them on to the inner makefile as env vars.
	# This needs to be all in one bash line as each make line runs in a separate subshell
	# and bash variable definitions are lost between separate commands.
	# As a consequence, this cannot have "real" comments in between the bash code.
	# Only externally available containers will be connected to the shared network.
	# Internally available containers will use the IP they receive from their internal network.
	DB_PORT=`cat src/db/$(DB)/conf/db-conf.json | jq -r '..|.port?' | grep -v null` \
	MSG_PORT=`cat src/msg/$(MSG)/conf/msg-conf.json | jq -r '..|.port?' | grep -v null` \
	DB_IPS=""; DB_PORTS=""; MSG_IPS=""; MSG_PORTS=""; \
	HOST_IP=$$(ip -4 addr show docker0 | grep -Po 'inet \K[\d.]+'); \
	echo "Host IP: $$HOST_IP"; \
	echo "Services running at:"; \
	n=1 ; while [[ $$n -le $(NUM_NODES) ]] ; do \
	  `# generate container names`; \
	    BBN=$(CONTAINER_PREFIX)$$n"_"$(BB_NAME)"_1"; \
	    DBN=$(CONTAINER_PREFIX)$$n"_"$(DB_NAME)"_1"; \
	    MSGN=$(CONTAINER_PREFIX)$$n"_"$(MSG_NAME)"_1"; \
	  `# generate virtual network names`; \
	    DBNW=$(CONTAINER_PREFIX)$$n"_bb_db_network"; \
	    MSGNW=$(CONTAINER_PREFIX)$$n"_bb_msg_network"; \
	  `# attach containers to virtual networks`; \
	    docker network connect $(ITEST_NET) $$BBN; \
	    docker network connect $(ITEST_NET) $$MSGN; \
	  `# copy node config files to containers`; \
	    docker cp $(SETUP_DIR)/$(NODE_PREFIX)$$n/. $$BBN:$(BB_WORKDIR)/core/conf; \
	  `# get container IPs and ports`; \
	    DB_IP_INT=$$(docker inspect -f \
	    "{{.NetworkSettings.Networks.$$DBNW.IPAddress}}" $$DBN); \
	    DB_IPS_INT="$$DB_IPS -e DB_IP_INT$$n=$$DB_IP_INT"; \
	    MSG_IP_INT=$$(docker inspect -f \
	    "{{.NetworkSettings.Networks.$$MSGNW.IPAddress}}" $$MSGN); \
	    MSG_IPS_INT="$$MSG_IPS_INT -e MSG_IP_INT$$n=$$MSG_IP_INT"; \
	    MSG_IP_EXT=$$(docker inspect -f \
	    '{{.NetworkSettings.Networks.$(ITEST_NET).IPAddress}}' $$MSGN); \
	    MSG_IPS_EXT="$$MSG_IPS_EXT -e MSG_IP_EXT$$n=$$MSG_IP_EXT"; \
	    MSG_P=$$(docker inspect -f \
	    "{{ (index (index .NetworkSettings.Ports \"$$MSG_PORT/tcp\") 0).HostPort }}" $$MSGN); \
	    MSG_PORTS="$$MSG_PORTS -e MSG_PORT$$n=$$MSG_P"; \
	  `# print current stack network config`; \
	    echo "* $(DB) $$DBN:"; \
	    echo "   - $$DB_IP_INT:$$DB_PORT (stack-internal on $$DBNW)"; \
	    echo "* $(MSG) $$MSGN:"; \
	    echo "   - $$MSG_IP_INT:$$MSG_PORT (stack-internal on $$MSGNW)"; \
	    echo "   - $$MSG_IP_EXT:$$MSG_PORT (docker-internal on $(ITEST_NET))"; \
	    echo "   - $$HOST_IP:$$MSG_P (via docker host)"; \
	    ((n = n + 1)) ; \
	done; \
	`# remove setup files`; \
	rm -rf $(SETUP_DIR); \
	`# update config file for each container`; \
	n=1 ; while [[ $$n -le $(NUM_NODES) ]] ; do \
	    BBN=$(CONTAINER_PREFIX)$$n"_"$(BB_NAME)"_1"; \
	    docker exec -e DB=$(DB) -e MSG=$(MSG) -e HOST_IP=$$HOST_IP \
	      -e NUM_NODES=$(NUM_NODES) -e CONTAINER_NAME=$$BBN \
	      -e DB_PORT=$$DB_PORT -e MSG_PORT=$$MSG_PORT -e NODE_PREFIX=$(NODE_PREFIX) \
	      $$DB_IPS_INT $$MSG_IPS_INT $$MSG_IPS_EXT $$MSG_PORTS $$BBN make update-config; \
	    ((n = n + 1)) ; \
	done;

# Calling the makefile in the working directory of the docker container.
# This is executed in that directory after the build.
unittest: --update-image
	$(info ***** ON HOST MACHINE: Running Bluberry unit tests *****)
	-docker run -e DB=$(DB) -e MSG=$(MSG) -e "TERM=xterm-256color" \
	  -e CONTAINER_NAME=$(TEST_CONTAINER) --name $(TEST_CONTAINER) -it -d $(BB_TAG)
	# use previous integration test coverage if it exists
	if test -f "./.coverage"; then \
	    docker cp ./.coverage $(TEST_CONTAINER):$(BB_WORKDIR); \
	fi
	-docker exec -e DB=$(DB) -e MSG=$(MSG) -e BB_WORKDIR=$(BB_WORKDIR) \
	  -e CONTAINER_NAME=$(TEST_CONTAINER) $(TEST_CONTAINER) make unittest MODULE=$(MODULE)
	# Copy result of unit test coverage to host so it can be combined with
	# integration test results
	-docker cp $(TEST_CONTAINER):$(BB_WORKDIR)/.coverage ./.coverage
	-docker cp $(TEST_CONTAINER):$(BB_WORKDIR)/htmlcov ./doc
	-docker cp $(TEST_CONTAINER):$(BB_WORKDIR)/coverage-badge.svg .
	-docker kill $(TEST_CONTAINER)
	-docker rm $(TEST_CONTAINER)

# Target for manually starting up the integration test stack
up: --update-image setup --create-test-network --spin-up-nodes --update-config

--live-copy:
	$(info ***** ON HOST MACHINE: Live-copying new files to container *****)
	-docker exec $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1 \
	  rm -rf /tmp/coreconf/ /tmp/dbconf/ /tmp/msgconf/
	-docker exec $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1 \
	  mv $(BB_WORKDIR)/core/conf /tmp/coreconf
	-docker exec $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1 \
	  mv $(BB_WORKDIR)/db/$(DB)/conf /tmp/dbconf
	-docker exec $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1 \
	  mv $(BB_WORKDIR)/msg/$(MSG)/conf /tmp/msgconf
	-docker exec $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1 rm -rf $(BB_WORKDIR)
	-docker cp src/. $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1:$(BB_WORKDIR)
	# use previous unit test coverage if it exists
	if test -f "./.coverage"; then \
	    docker cp ./.coverage $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1:$(BB_WORKDIR); \
	fi
	-docker exec $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1 \
	  rm -rf $(BB_WORKDIR)/core/conf $(BB_WORKDIR)/db/$(DB)/conf $(BB_WORKDIR)/msg/$(MSG)/conf
	-docker exec $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1 \
	  cp -rf /tmp/coreconf/ $(BB_WORKDIR)/core/conf
	-docker exec $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1 \
	  cp -rf /tmp/dbconf/ $(BB_WORKDIR)/db/$(DB)/conf
	-docker exec $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1 \
	  cp -rf /tmp/msgconf/ $(BB_WORKDIR)/msg/$(MSG)/conf

attach:
	$(info ***** ON HOST MACHINE: Attaching to Bluberry node $(NODE_PREFIX)$(ATTACH_NODE) *****)
	BBN=$(CONTAINER_PREFIX)$(ATTACH_NODE)"_"$(BB_NAME)"_1"; \
	DBN=$(CONTAINER_PREFIX)$(ATTACH_NODE)"_"$(DB_NAME)"_1"; \
	MSGN=$(CONTAINER_PREFIX)$(ATTACH_NODE)"_"$(MSG_NAME)"_1"; \
	docker-compose -p $(CONTAINER_PREFIX)$(ATTACH_NODE) \
	  -f $(BB_DIR)/conf/docker-compose.yml \
	  -f $(DB_DIR)/conf/docker-compose.yml \
	  -f $(MSG_DIR)/conf/docker-compose.yml \
	  logs --follow;

# This tagret manually runs the integration tests, but assumes integration-up has been
# executed previously so all containers are available.
# Backs up the container config before updating the code from the host and restoring the config.
integration-run: --live-copy
	$(info ***** ON HOST MACHINE: Running Bluberry integration tests *****)
	-docker exec -e CONTAINER_NAME=$(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1 \
	  $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1 make integrationtest \
	  DB=$(DB) MSG=$(MSG) MODULE=$(MODULE) BB_WORKDIR=$(BB_WORKDIR) TEST_WAIT=0
	-docker cp $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1:$(BB_WORKDIR)/.coverage ./.coverage
	-docker cp $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1:$(BB_WORKDIR)/htmlcov ./doc
	-docker cp $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1:$(BB_WORKDIR)/coverage-badge.svg .

# This shuts down any running integration test stacks.
integration-down:
	$(info ***** ON HOST MACHINE: Stopping Bluberry integration test stacks *****)
	$(call shut_down,$(NUM_NODES), $(CONTAINER_PREFIX))
	docker network rm $(ITEST_NET)

# Runs the integration tests automatically, shutting down and cleaning up after itself.
integrationtest: up
	$(info ***** ON HOST MACHINE: Running Bluberry integration tests *****)
	docker exec -e CONTAINER_NAME=$(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1 \
	  $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1 make integrationtest \
	  DB=$(DB) MSG=$(MSG) MODULE=$(MODULE) BB_WORKDIR=$(BB_WORKDIR) TEST_WAIT=$(TEST_WAIT)
	-docker cp $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1:$(BB_WORKDIR)/.coverage \
	  ./.coverage.integration
	-docker cp $(CONTAINER_PREFIX)$(HOME_NODE)_$(BB_NAME)_1:$(BB_WORKDIR)/coverage-badge.svg .
	$(call shut_down,$(NUM_NODES), $(CONTAINER_PREFIX))
	docker network rm $(ITEST_NET)

test: unittest integrationtest

run: --live-copy
	$(info ***** ON HOST MACHINE: Running Bluberry *****)
	BBN=$(CONTAINER_PREFIX)$(HOME_NODE)"_"$(BB_NAME)"_1"; \
	docker cp $(SETUP_DIR)/$(NODE_PREFIX)1/. $$BBN:$(BB_WORKDIR)/core/conf; \
	docker exec -e CONTAINER_NAME=$$BBN -e BB_BUILD=$(BUILD) -e DB=$(DB) -e MSG=$(MSG) \
	  -e NUM_NODES=$(NUM_NODES) -e NODE_PREFIX=$(NODE_PREFIX) -e THIS_NODE=$(NODE_PREFIX)1 \
	  -e BB_WORKDIR=$(BB_WORKDIR) \
	  $$BBN make run
#	docker exec $$BBN ls -la msg/$(MSG); \

test-run: --update-image
	$(info ***** ON HOST MACHINE: Test-running Bluberry *****)
	docker run -e CONTAINER_NAME=$(RUN_CONTAINER) --name=$(RUN_CONTAINER) \
	-it $(BB_TAG) make test-run

doc: --update-image
	$(info ***** ON HOST MACHINE: Test-running Bluberry *****)
	-rm -r ./doc/pdoc
	-docker run --name=$(DOC_CONTAINER) -e CONTAINER_NAME=$(DOC_CONTAINER) \
	  -e DB=$(DB) -e MSG=$(MSG) -e BB_WORKDIR=$(BB_WORKDIR) -it $(BB_TAG) make doc
	-docker cp $(DOC_CONTAINER):$(BB_WORKDIR)/pdoc ./doc/pdoc
	-docker kill $(DOC_CONTAINER)
	docker rm $(DOC_CONTAINER)

clean:
	$(info ***** ON HOST MACHINE: Cleaning Bluberry containers and images *****)
	if [ $$(docker ps -q | wc -l) -ne 0 ]; then docker kill $$(docker ps -q); fi
	-docker container prune -f
	# use with caution!
	-docker volume prune -f
	-docker network prune -f
	-docker image prune -f
	-docker system prune -f
	-find . -name "__pycache__" -type d -exec rm -r {} \;
	-rm -rf $(SETUP_DIR)
	-rm -f ./.coverage*

# Functions ###################################################################

# Shuts down (headless) containers
# $(1) = the number of containers
# $(2) = the prefix for the containers; the container number will be appended
define shut_down
	test "$(1)" -gt 0
	n=1 ; while [[ $$n -le $(1) ]] ; do \
	    echo "Shutting down stack"$(2)$$n; \
	    BB_BUILD=$(BUILD) NUM_NODES=$(1) NODE_PREFIX=$(NODE_PREFIX) \
	    THIS_NODE=$(NODE_PREFIX)$$n \
	    docker-compose -p $(2)$$n \
	    -f $(BB_DIR)/conf/docker-compose.yml \
	    -f $(DB_DIR)/conf/docker-compose.yml \
	    -f $(MSG_DIR)/conf/docker-compose.yml \
	    down --remove-orphans; \
	    ((n = n + 1)) ; \
	done
endef

