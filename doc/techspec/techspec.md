# Introduction

## Document outline
This is the accompanying documentation for Bluberry, a consent-based database synchronisation layer. It contains background information about the requirements analysis and outlines the architecture of the system as a whole and the different components.
It is designed to help anybody who wants to implement this.

For the reference implementation and consequently the examples in this document, we used scenarios from the project [A Step Change in LMIC Prosthetics Provision through Computer Aided Design, Actimetry and Database Technologies](https://gow.epsrc.ukri.org/NGBOViewGrant.aspx?GrantRef=EP/R014213/1) (EP/R014213/1). 

## Definitions, conventions and terminology

| Term                  | Definition for the purpose of this document |
|-----------------------|---------------------------------------------|
| **ACID**              | **A**tomicity, **C**onsistency, **I**solation, **D**urability. This describes properties, that database transactions in traditional DBMS typically have. |
| **Asynchronous**      | Not happening at the same time. |
| **Consent**           | Permission/agreement by the data subject. This is not sufficient, unless the consent is *informed* and thus meaningful. |
| **CRUD**              | **C**reate, **R**ead, **U**pdate, **D**elete. Describes operations that can be done to data. |
| **Data**              | Recorded information, which can be analog (e.g. on paper) or digital. |
| **Database**          | This term is often used synecdochically when people would really like to say "Database Schema", "Data", "Database Server" or "DBMS". In this document, it stands for the logical container of data according to a schema, which is managed by a DBMS. |
| **Database server**   | The physical machine on which the database service is hosted. |
| **Database service**  | Also database application. A process that provides an interface to the DBMS, running on a database server. A service could be a database client, that can be run on an operating system, often as a daemon, and provide an interface for the user to send commands to the DBMS. If the interface is not intended to be used by a user directly, but through an API by another program, it would be called "Database API" instead but essentially provide the same functions. |
| **Database schema**   | The model that formally describes the structure of the data and how it is organised in a database. |
| **Data controller**   | The party who initiated the collecting and/or processing of data. They usually benefit from the data they control and require the consent from the data subjects. This term is used in the GDPR and shall be used . |
| **Data owner**        | This term should be avoided. While legal ownership of the data always remains with the data subject, the data controller may assign the role of data owner to a dedicated team or individual in charge of maintaining data quality and integrity. |
| **Data processor**    | Any party that processes the data on behalf of another party. Processing can include collecting, storing or altering; the important detail is that the processor does not make decisions about how, when, where and why to process the data and is not interested in the result. |finally
| **Data recipient**    | Any party to which the data is disclosed with the exception of public authorities during a particular enquiry in accordance with the EU or state law. |
| **Data subject**      | The person or organisation to which the data relates. This can be more than one and in this case, it is most likely the patient. |
| **DBMS**              | **D**ata**B**ase **M**anagement **S**ystem. The entire system that comprises of the source code and/or binaries for the database service and indirectly its specification. A running DBMS is referred to as database service. |
| **Digital signature** | A digital code, generated using the signatory's private key and the data being signed to verify the authenticity of the signatory's identity. |
| **Encryption**        | A mechanism to make information unreadable by anybody except the designated recipient who holds the key to decrypt the information. Encryption can refer to simply encoding the information using a symmetric key, for example to encrypt data at rest on a hard disk. It can also refer to public-key cryptography, or asymmetric cryptography, where data in transit is encrypted by the sender using the recipient's public key and subsequently decrypted by the recipient using their private key. |
| **GDPR**              | General Data Protection Regulation. A European [set of rules](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN) describing rules for the collection, processing and storing of personal data. |
| **Private Key**       | One part of a key pair, a private key is only known to the owner. It can be used to sign data or to decrypt data that has been encrypted using the corresponding public key. |
| **Public Key**        | The other part of a key pair, a public key is publicly known, although it may have to be distributed. It can be used to verify a digital signature or to encrypt data for a known recipient. |
| **Message**           | Data being exchanged between two entities, containing the actual information that constitutes the payload as well as other properties, such as message status and some metadata, e.g. the recipient ID and address, date/timestamps, etc. |
| **Personal data**     | Any data that relates to a natural, identifiable person. |
| **Real-time**         | A system is considered to respond in real-time if it satisfies the real-time constraint imposed by the use case. For computer systems, this is usually expected to be in the order of milliseconds. |

The following terms will be used to describe the functionality of this systems and have been defined in [RFC2119](http://www.ietf.org/rfc/rfc2119.txt) as follows:

1. **MUST**   This word, or the terms "REQUIRED" or "SHALL", mean that the
   definition is an absolute requirement of the specification.

2. **MUST NOT**   This phrase, or the phrase "SHALL NOT", mean that the
   definition is an absolute prohibition of the specification.

3. **SHOULD**   This word, or the adjective "RECOMMENDED", mean that there
   may exist valid reasons in particular circumstances to ignore a
   particular item, but the full implications must be understood and
   carefully weighed before choosing a different course.

4. **SHOULD NOT**   This phrase, or the phrase "NOT RECOMMENDED" mean that
   there may exist valid reasons in particular circumstances when the
   particular behavior is acceptable or even useful, but the full
   implications should be understood and the case carefully weighed
   before implementing any behavior described with this label.

5. **MAY**   This word, or the adjective "OPTIONAL", mean that an item is
   truly optional.  One vendor may choose to include the item because a
   particular marketplace requires it or because the vendor feels that
   it enhances the product while another vendor may omit the same item.
   An implementation which does not include a particular option MUST be
   prepared to interoperate with another implementation which does
   include the option, though perhaps with reduced functionality. In the
   same vein an implementation which does include a particular option
   MUST be prepared to interoperate with another implementation which
   does not include the option (except, of course, for the feature the
   option provides.)

# Scope
This specification defines a layer for **consent-based**, **asynchronous** (on-demand), **not necessarily consistent** database synchronisation. It describes the interfaces it needs to provide in order to integrate with various technologies such as DBMS and messaging middleware. It describes how these should be implemented in order to satisfy the requirements and be compliant with this specification. However it does not explain *how* this should be implemented, nor does it impose specific technologies or tools.

The synchronisation described is asynchronous in that it does not necessarily synchronise automatically. While it may be automated, there needs to be the option that it is triggered exclusively manually. Synchronisation may happen irregularly and not necessarily in the correct order, meaning updates can be received that precede other, already received updates. These updates can be exchanged directly if the two nodes involved are connected but may also happen while one node is not connected. In that case, messages can be passed to another ("proxy") node by the originator and either directly or through more proxies, reach the destination when the originator may already be offline.

The database is also most likely not consistent, which means that the data held on different nodes within the synchronisation group may be different: some data may be shared whereas other data may exist only on a subset of the group or a single node. This is due to consent, which needs to be given in order to allow data to be synchronised. If consent is obtained from all data subject for synchronisation across the entire group, the synchronisation would eventually leave the data consistent, provided all synchronisation took place before new changes were made.

## Technical assumptions and prerequisites

### General architectural assumptions

* Any kind of data could in theory be stored in the database, including CSV or PDF files,
  images and binary data
* A network of nodes will only be set up once. As this system will handle highly sensitive
  personal data, adding, removing and modifying nodes in the network remotely would either require
  a central trusted authority or introduce a security risk. For this reason, we decided that setup
  happens only once when the system is set up. Subsequent changes are possible although need to be
  done manually for each of the nodes in the network.

### Specific hardware assumptions for the reference implementation

* The device has 128GB of disk space
* Typical connection speeds should be above 10MBit

# Scenario and use cases
![The use cases and stakeholders](img/usecasesV4.png)

## Stakeholders
This section describes user stories, i.e. small situations, in which the respective stakeholders expect the system to function a certain way.

In the following subsections, requirements that relate to the system rather than just the synchronisation layer are shown in *italic* text. This means they do not have to be implemented directly by the synchronisation layer, but they do need to be supported.

### Patient/family member
I would like
* my personal data to be used where required to support my treatment.
* my personal data to be safe at all times, i.e. encrypted in transit and at rest.
* control where my data is held physically.
* *access control in place to prevent unauthorised access to my personal data.*
* *to understand how, when and where my personal data is processed and to what end.*
* *to understand and have control over who is allowed to access and receive my personal data.* 
* *control over my personal data, i.e. to be able to withdraw consent at any time and have my data deleted upon request.*
* *to be able to view my complete data record.*

### Clinical staff (Prosthetist, Physiotherapist, Receptionist, Community Worker)
I would like
* *to perform local CRUD operations on the database schema.*
* access to all local data at any time, even when the node I'm using to access the data is not
  connected to any other node.
* to review and subsequently accept or roll back updates that have been obtained from other nodes.
* a way of triggering synchronisation of incoming and/or outgoing data.

### Government
I would like
* *to view anonymised statistics on healthcare provision.*

### NGO (Manager, IT Engineer)
I would like
* to ensure that the database does not get corrupted.
* to have control over which data leaves each node and QA that data before synchronising.
* *to be able to aggregate anonymised statistics from data on all nodes in a group.*

### Hardware provider (Accountant, Software Engineer)
I would like
* a way to configure new applications to use the database synchronisation.

## Scenario
Exceed Worldwide currently treats ~11k patients in hundreds of locations across Cambodia.
* The maximum number of patients does not exceed 50k in the forseeable future.
* The maximum number of nodes does not exceed 1000.
* No more than 5000 patients visit one single clinic.
* A patient typically sees their doctor about once a year.

## Data protection, Privacy and Consent
The synchronisation layer will support detailed user consent by design. Although it is outside the scope to enforce access control for patient data or parts thereof, consent is still applicable to the decision whether data should be synchronised to a given node at all.

The full list of consent-related statements obtained through stakeholder interviews in Cambodia (ERGO/FPSE/46271) is as follows:

| Comment                                                      | Number|
|------------------------------------------------------------------|---|
| I'm not worried about my data                                    | 2 |
| I'm very worried about my data                                   | 3 |
| I have resigned over privacy                                     | 1 |
| I fundamentally distrust large organisations                     | 3 |
| I value convenience over privacy                                 | 1 |
| Synchronisation to me is a means to reduce work                  | 2 |
| I would like to sell access to my data in return for benefits    | 1 |
| I fear data sharing might lead to me losing access to services   | 1 |
| I fear losing my medical data                                    | 1 |
| I want consent digitised                                         | 1 |
| I would like to have a choice when it comes to my data           | 4 |
| An "accept all" option for consent is a bad idea                 | 1 |
| I want more information on the consequences of data sharing      | 3 |
| I would like a more visual explanation of consent                | 1 |
| I want higher granularity for consent                            | 1 |
| I would like to be able acess/review all my data at any time     | 1 |
| I want to limit the time in which my data can be stored/accessed | 1 |

According to these comments, there are four dimensions of consent:

* the stakeholders accessing the data
* the types of data stored
* the geolocation of the data
* the duration for which data is stored

While we need to make consent for each of these dimensions configurable for the user, an excessively high granularity would overcomplicate things and lead to the users being overwhelmed with the choice.

To provide some "real-world" use-cases for the synchronisation, the following personas were created, each one reflecting a particular view without being linked to an actual interview participant.

### Persona 1: Sokha
<img alt="Sokha" src="img/consent/sokha.png"  width="200">

Sokha is 42 and works as a farmer.
He's a transtibial amputee due to a landmine when he was 12 years old.

He's not concerned about his data and prefers convenience over privacy - if his data is synchronised across all clinics, he won't have to remember his details every time he visits.
However, he likes the prospect of having better information on where his data is stored, who can access it and what it's used for. He'd be happy to provide his data to research in the hope this can lead to better prosthetic services becoming available. He would also allow access to his data for market research - but he would like to get some benefits in return, such as discounts on new products.

### Persona 2: Mony
<img alt="Mony" src="img/consent/mony.png"  width="200">

Mony is 25 and works as a systems administrator in a small accountancy firm.
She's a transfemoral amputee following a road traffic accident on a motorbike 6 years ago.

She's very aware of data issues and extremely protective of her own data. She uses pseudonyms on social media and holds a general distrust of large organisations in general and their data protection practices in particular. She supports better consent methods and would like to manage her own consent at a much higher granularity. She also wants to be able to access all her data at any point directly.

### Persona 3: Kanya
<img alt="Kanya" src="img/consent/kanya.png"  width="200">

Kanya is 64 and a retired seamstress, who still does sewing work to earn extra money a few hours a week.
She's a transtibial amputee following complications caused by her diabetes about 15 years ago.

Not a tech-savvy person, for Kanya, access to prosthetics services are the top priority. She fears that oversharing of data might lead to her losing access to these services. This partially stems from her general mistrust of large organisations but also from her lack of knowledge of potential risks and benefits. She would like to have this addressed through better consent descriptions.

## Use cases
These use-cases are designed to help test the implementation once it's in place. The system supports all of them by design, although it does rely on the implementation of the components in order to correctly synchronise data end-to-end.

### Simple synchronisation
This is the initial test that can be used to show that the reference implementation works. The story is as follows:

A new patient, Sokha, registers at the Phnom Penh Exceed clinic reception PC. He agrees to have his data synchronised anywhere for as long as possible and wants all actors to be able to access it.
After the registration, the data is synchronised to another node (the Physiotherapists') in the clinic. The record can now be brought up by accessing that node using a tablet.
Meanwhile, the social workers are out and about with their mobile node. As they are not currently connected to the internet, Sokha's data does not appear when accessing the system through their node.

### Complex consent
In this use-case, the focus is on consent and how it affects what is synchronised where. We can compare synchronisation for all three personas on three different nodes:

* 3 patients, 1 consents to everything, one to as little as possible and another one to a subset,
  (e.g. 2 locations out of 3, no mobile devices, only appointments, no medical data included).
* All patients' data is added to one node and the synchronisation is triggered
* We now confirm data is only synchronised where it is expected to be.

### Proxy sync/Encryption
This use-case shows how data can only be read by designated recipients. The setup is as follows:

* One patient is added to one node (1) with two more nodes (2, 3) currently disconnected.
  The data is to be shared with node 3 only.
* Synchronisation happens between nodes 1 and 2 while node 3 is offline
* The data is received by node 2 but remains encrypted
* Node 1 goes offline and afterwards node 3 comes online. At this point, synchronisation happens
  between nodes 2 and 3.
* The data should now be on node 1 and 3 but not on 2.

For an extended version of this test, we can check how long the encrypted message is retained on one node before it has been received by *all* designated recipients but this is optional.

### Conflict handling
While conflict handling is the job of the actual (patient management) system, the synchronisation layer still needs to support detecting and escalating these conflicts, and finally provide a mechanism to execute the selected action on the database.
This use case describes what happens when updates are made while nodes are not connected and how conflicts arising in those situations can be reconciled. The playbook is:

* The records for a patient is synced to two other nodes.
* All nodes then go offline.
* The record is updated on both other nodes, changing the spelling of the first name respectively
  but differently from the other node's update.
* All nodes come online again and the changes from both updated nodes are synchronised back to the
  original node (and also the other nodes).
* The conflict should be detected and an action taken to escalate the conflict in a way that it
  could be picked up by the system that uses the synchronisation layer.

### Changing consent
As shown in some of the other use cases, this synchronisation layer operates using a consent by design philosophy. Not only does this mean that synchronisation is potentially restricted based on individual patient consent, it also means that patients have the right to modify or withdraw consent they have already given. This would work as follows:

* A patient has their data stored across all three nodes.
* The patient revokes consent for on patient revokes consent for one node.
* After a synchronisation, that data should be removed from that one node but still exist on other
  nodes.
* The patient restores consent again.
* After synchronisation, the data should be returned to all nodes.

# Configuration
The synchronisation layer is flexible and can be configured to work with a multitude of possible combinations of database and messaging technologies. This section explains how it needs to be configured in order to work properly.

## General configuration
The fields explained:

* **nodeId**
  Must be unique within the system and immutable.
* **TODO**

setup.py:
- uses a seed config to generate config files for all nodes in a group, example below.
- also generates cryptographic key pairs for each node and distributes the public keys to all other 
  nodes in the group.
- the generated files need to be copied onto the nodes.

```js
{
    "local": {
        "nodeId": "node0",
        "network": "ExceedCambodia",
        "db": {
            "type": "mysql",
            "host": "localhost",
            "user": "root",
            "pw": "root",
            "db": "test",
            "logfile": "/var/log/mysql/flora.log"
        },
        "sync": {
            "filtering": "blacklist/whitelist",
            "tables": [ "users", "user_property" ],
            "conflictResolutionStrategy": "escalate/firstwins/lastwins/originatorwins"
        },
        "encryption": {
            "publicKey": "/path/to/publicKey",
            "privateKey": "/path/to/privateKey"
        },
        "consent": {
            "key": {
                "TableName": "patient",
                "FieldName": "patient_id",
                "FieldType": "integer"
            },
            "value": {
                "TableName": "patient",
                "FieldName": "consent",
                "FieldType": "boolean/json"
            },
            "ledger": {
                "9": "full",
                "3": "local",
                "14": [ "node1", "node3" ]
            }
        }
    },
    "nodes": {
        "node1": {
            "url": "https://1.2.3.4/bluberry",
            "publicKey": "/path/to/publicKeys/node1.pem"
        },
        "node2": {
            "url": "https://1.2.3.5/bluberry",
            "publicKey": "/path/to/publicKeys/node2.pem"
        },
        "node3": {
            "url": "https://1.2.3.6/bluberry",
            "publicKey": "/path/to/publicKeys/node3.pem"
        }
    }
}
```

## Consent configuration
The consent configuration is done in two layers to make it more flexible and able to adapt to the specific needs of different organisations. These two layers are described below:

### Consent mapping
In the first step, the organisation needs to define its consent definitions and the mapping to the database. This is done by defining a set of labels that are meaningful for the patients and can be linked to items in a consent form. Labels need to be defined for

* data
* locations and
* stakeholders

If the organisation decides not to give its users, it should be noted that all consent is GDPR-compliant opt-in, i.e. operated as a whitelist. No blanket consent can be given using this consent configuration schema.

A sample consent mapping is given below:

```js
{
    dataType: {
        personal: "Identifiable data, such as name, date of birth, address or social media accounts",
        non-medical: "Data such as appointments, but using a patient ID instead of a name",
        medical: "Data relating to the medical record of the patient"
        // add more definitions here
    },
    dataTypeMapping: {
        // data type mapped to tables/columns here
        // if there are two categories for a piece of data, both will apply
        personal: {
            person: [ person_name, person_dob,... ]
        },
        medical: {
            allergies: [ allergy_id, ... ]
        },
        ...
    },
    locationType: {
        global: "Anywhere in the network",
        static: "All nodes that stay in one place",
        mobile: "All nodes without a static location"
        // add more definitions here, such as "cambodia_only" or "hospitals_only"
    },
    // This data needs to map to the nodes in the config file
    locationMapping: {
        global: [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ],
        static: [ 1, 3, 4, 6, 8, 9 ],
        mobile: [ 2, 5, 7 ]
    },
    stakeholderType: {
        medicalProfessional: "A doctor, nurse or other practitioner",
        socialWorker: "Clinical staff who see patients in the community",
        clinicAdminStaff: "Clinic managers, secretaries, receptionists,...",
        itStaff: "Non-medical staff who manage the computer systems",
        // add more stakeholder types here
    },
    // This data needs to map to actual accounts
    stakeholderMapping: {
        medicalProfessional: {
            // it supports both individual users...
            individualIds: [ 1, 34, 567 ],
            individualTable: "users",
            individualColumn: "user_id",
            // ...as well as group-based permissions, where individual permissions have precedence
            groupIds: [ 2, 3 ],
            groupTable: "groups",
            groupColumn: "group_id"
        }
    }
}
```

### Consent record
The consent record uses the labels defined by the consent mapping. Not only does this make the consent record much tidier, but it also remains human readable.

A sample consent record might look like this:

```js
{
    consentId: abc123,
    patient_identifier: 345676868,
    location: {
        // where the data may be stored
        personal: global,
        medical: static,
        ...
    },
    time: {
        // how long the data may be stored for
        // note that again this is a whitelist, so only data listed here can be stored at all
        personal: "1970-01-01_00-00-01Z",   // until the time given
        medical: true,                      // can be stored indefinitely
    },
    stakeholders: {
        // who can access the data
        // each data type needs to be named here, otherwise nobody can access it
        personal: [ "medicalProfessional", "adminStaff", ... ],
        medical: [ "medicalProfessional"
        ...
    }
}
```

# Architectural overview
The architecture of the synchronisation layer is modular in order to give implementers maximum flexibility. Not only is this specification independent of the implementation language; it is also agnostic of the type of database used or the messaging layer. The following figure shows how interfaces are defined between the core implementation and the outward-facing modules.

![The definitive architecture diagram](img/archv3.png)

As long as data is passed between the different modules in a well-defined format, the core module is indifferent as to how that data was obtained from the database. Similarly, as long as the message format is correct and the recipient is able to verify the identity of the sender, the technology used for the implementation is not important.

As a result, a full implementation could use different languages for the core, messaging and database modules.

## Core module
The reference implementation represents a good entry point for what is required in an implementation for a different platform. While the full API documentation has all low-level information, this document explains on a higher level what the components are and how they work together.

Not all methods are covered in this document as many of them are obvious, such as getters and setters. More information can be found in the API docs.

### `Bluberry` main service
The heart of the implementation is the `Bluberry` main class.
It's not a service itself, but a wrapper which runs two separate services:

- a message manager service and
- a database manager service

As shown in the architectural diagram, these are technology-independent and use only interfaces to communicate with the actual implementations.

This component is best described in the API docs.

### The `BluberryMessage` class
This class describes the data structure that is used for transporting information between the different modules. It needs to fulfil the criteria defined here in order to be compatible with the rest of the system.

Apart from the Bluberry main class itself, this class contains most of the implementation as it covers the full encryption/decryption part.

#### Human-readable string representation
There should be a method that creates a human-readable representation of the `Message` object, showing all metadata fields, content and recipients. This method does not need to show verbose, encrypted information but may choose to do so. In python, such a method would be `\_\_str\_\_(self)` while in Java it would be `toString()`. It returns a human-readable string for debugging purposes.

#### (De)serialisation methods
Unsurprisingly, these methods (de)serialises a Message object, translating all its fields from/into a JSON representation. It may choose to sort and indent the resulting JSON but does not need to.

Conversely, the `deserialise()` method translates a JSON object into a Message. It should check all required fields are present, otherwise it should throw an exception.
Furthermore it should make sure the datatype of the fields matches the datatype of the JSON property.

#### receive(nodeId, privateKey)
This method marks a message as received by the node with the given ID, using their privateKey to sign the message as a proof.
If a signature already exists for the ID, it may choose to

 a) do nothing
 b) override the existing signature with the new signature if they differ
 c) raise an exception
 
In any case, it should log the event.

#### isValid()
This method checks whether a message object is valid, i.e. whether it contains all required fields and checks they are all the correct datatype. These checks may be extended to check for specific content, length etc. but that's up to the implementation to decide.

#### Getters and setters
Apart from the methods described above, this class may define internal helper methods and needs to provide public getters and setters. However, some of these methods should be private as they are only used internally and should never be invoked otherwise. These are:

 * setId
 * getEncryptedKey
 * setEncryptedKey
 * getNonce
 * setNonce
 * getTag
 * setTag
 * setReceivedBy
 * setSignatureInner
 * setSignatureOuter

Making these methods private (or not defining them at all) prevents unauthorised updating of fields that should only be updated in certain circumstances or by authorised users providing the correct key(s).

#### Encryption/decryption methods
The encryption and decryption methods are typically called by a management component, such as the MessageManagerService and require a key to encrypt, decrypt, sign and verify the messages. It goes without saying that these methods must not store these keys under any circumstances, nor must they print the keys or any part thereof or a stack trace from exceptions in the encryption libraries in logging messages.

The encryption concept is relatively simple and involves both symmetric and asymmetric encryption. The following pictures illustrate how it works:

![Encryption part 0](img/encr/0.png)
Two parties are about to exchange a message. Both have their private key and the other party's public key. It starts with a message being created on Bob's end.

---
![Encryption part 1](img/encr/1.png)
Bob uses his private key to sign the message. He does this as a very first step to minimise the time a malicious party has to tamper with it.

---
![Encryption part 2](img/encr/2.png)
In the next step, Bob generates a random symmetric key, which also generates a nonce and a tag related to the key. The reason Bob doesn't use his private key is that an asymmetric key can only encrypt a message that is slightly shorter than the key itself (as it requires some padding). This means there is an upper limit on the message size which in most cases does not cover the required size for this application. Simply using a longer key would make encryption very slow as it's a calculation-intensive process. A symmetric key can encrypt a longer message but, due to its shorter length, is easier to guess. The solution is to use a random key, which is generated during the encryption process.

---
![Encryption part 3](img/encr/3.png)
The random key is now used to encrypt the message.

---
![Encryption part 4](img/encr/4.png)
The random key is now encrypted using Bob's private key.

---
![Encryption part 5](img/encr/5.png)
As the encrypted message, nonce and tag are represented as binary large objects, or "blobs", they need to be encoded as strings if we want them to be serialisable.

---
![Encryption part 6](img/encr/6.png)
Now the encryption is done, Bob signs the encrypted, encoded message again using his private key. The reason Bob needs to sign twice is that the first signature can now no longer be verified as the message is encrypted. Only the recipient will be able to decrypt and verify it, but for this system, anybody needs to be able to tell who the sender of a message is.

---
![Encryption part 7](img/encr/7.png)
The signatures, which are also blobs, need to be encoded before they can be serialised.

---
![Encryption part 8](img/encr/8.png)
The parts of the "finished" message are now all serialised.

---
![Encryption part 9](img/encr/9.png)
Bob sends the message by passing it to the messaging module.

---
![Encryption part 10](img/encr/10.png)
On Alice's side, the messaging module receives the message.

---
![Encryption part 11](img/encr/11.png)
The message is de-serialised, recovering all the required parts.

---
![Encryption part 12](img/encr/12.png)
All parts of the message are now decoded into their original, binary form in order to proceed with the decryption.

---
![Encryption part 13](img/encr/13.png)
Before anything else happens, Alice checks Bob's signature using his public key. If that check fails, the message should be discarded. An exception may be raised or logged but the system must not continue to process it in any other way. If the signature is valid, the message is further processed, meaning it's passed on if Alice is not the recipient and decrypted if she is.

---
![Encryption part 14](img/encr/14.png)
The random key is now decrypted using Alice's private key.

---
![Encryption part 15](img/encr/15.png)
The symmetric key, tag and nonce are used to decrypt the message.

---
![Encryption part 16](img/encr/16.png)
Alice now verifies Bob's signature on the decrypted message to make sure it has not been tampered with on Bob's side during the encryption process.

---
![Encryption part 17](img/encr/17.png)
Alice can now use the message, which in this context means pass it on to the core module and subsequently the database module.

## Database module
This module is responsible for reading changes that were made to a database schema. Only DBMS of the same type can be synchronised. There are two possible ways of reading the changes:

1) Direct DBMS access
   This requires access to the database directly and the implementation needs to manage issues
   related to concurrent access, duplication and transactions.
2) Log access
   This removes the need to connect directly to the DBMS (if a log file is used rather than a log
   table) and avoids duplication issues but it can potentially be slow.

The reference implementation uses a MySQL database, as that is the technology on which the popular EHCR system OpenMRS is built.

### Consent
Apart from methods to support CRUD operations on the database, this module also needs to implement consent mapping. The reason is that consent is captured in a modular way, independently from the actual database implementation: a patient doesn't care which database table the information is stored in; they only want to be sure personal information only gets synchronised to where they have consented to.

How consent mapping is implemented is completely up to the software developer as long as the configuration file format is honoured. This means if an organisation replaces their EHCR system, they don't need to go back to all patients to ask for their consent; all they need to do is re-implement the mapping for the new EHCR/DBMS to map to the correct data based on already existing consent.

The reference implementation contains some ideas on how that can be done.

## Messaging module
Messaging is divided into two parts: local and remote messaging.
The idea is that there are two sets of queues, separating between the local messages that are specific to the node it runs on and remote messages, which are aggregated from all other nodes in the network. Only the remote module needs exposure to the network.

The following pictures describe the messaging process:

![Messaging part 0](img/msg/00.png)
---
![Messaging part 1](img/msg/01.png)
---
![Messaging part 2](img/msg/02.png)
---
![Messaging part 3](img/msg/03.png)
---
![Messaging part 4](img/msg/04.png)
---
![Messaging part 5](img/msg/05.png)
---
![Messaging part 6](img/msg/06.png)
---
![Messaging part 7](img/msg/07.png)
---
![Messaging part 8](img/msg/08.png)
---
![Messaging part 9](img/msg/09.png)
---
![Messaging part 10](img/msg/10.png)
---
![Messaging part 11](img/msg/11.png)
---
![Messaging part 12](img/msg/12.png)

The reference implementation uses a AMQP/RabbitMQ messaging setup.

# Developer guide

## Code structure

Each module should be located in its own subdirectory, i.e. `./src/msg/your-messaging-module` or `./src/db/your-db-module`.

All modules depend on `core` but are otherwise independent.
This means they bring their own dockerfile for any third-party tools they depend on.
Dockerfile(s) and docker-compose.yml are located at `./src/module-type/your-module/conf/`.
Other configuration files for building/configuring the container, such as `requirements.txt` should also be located in the same directory or subdirectories.

Each module **must** define its configuration file, which **must** be named `./src/module-type/your-module/conf/module-type-conf.json`, e.g. `./src/db/mysql/conf/msg-conf.json`. Furthermore, this file **must** define a "port" field in its main object. This should refer to the main port on which other containers connect to the module's container's services. A service may well run multiple endpoints on multiple ports, but this is the one on which Bluberry will attempt to contact it.

It's worth noting that while there is no restriction on the number of implementations for the reader/writer classes, the algorithm that loads them into the main Bluberry module is greedy, i.e. it will load the first available class it can find. Ideally, each module only contains one implementation so this should not apply to most cases.

### In-code documentation
For Python docstrings a combination of [Google-type docstrings](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html) and [numpydoc](https://numpydoc.readthedocs.io/en/latest/example.html) guidelines should be followed.
Google-type docstrings are more convenient to give arguments (`Args:`) while numpydoc has a better way of giving details for type hints.

Please use type hints for all arguments and document all public or inheritable methods. If a class inherits a method from an abstract base class, it does not need to document the inherited method unless it modifies it.

## Docker configuration

When executing dockerfiles for modules, the working directory will always be the module's main directory. For the core dockerfile, the execution directory is the main directory.

Modules themselves will be running on the same container as the core module. However, they connect to their docker containers via virtual networks:

![](./doc/img/containers.png "The Bluberry container-based architecture")

Note that all arrows in the picture are directional, i.e. communication is initiated one way, adding extra layers of separation between the different parts of the system. Containers (purple) expose ports (yellow) that can be made accessible externally by the host machine.

The core module is built upon the latest python3 dockerfile. It contains all code located in bluberry's `/src` directory, including all modules' code. It also defines two environment variables, `$DB` and `$MSG`, which contain the names of the two modules as the system is running.

Each module can define its own Dockerfile, where existing images can be adapted by copying configuration files onto the image adn executing initialisation scripts to set up the image for use in the Bluberry system.

A `docker-compose.yml` file is mandatory for each module. This single file is appended to the `/core/conf/docker-compose.yml` file, which defines the core container and the networks between core and messaging/database modules respectively. The modules' `docker-compose.yml` file needs to attach the service it defines to its network (`bb_msg_network` or `bb_msg_network`) but not the other network in order to ensure a division between the components.
Please not that within the .yml file, no hyphens should be used in keys (e.g. service names) as they are characters with special semantics and denote sequences, see <https://yaml.org/spec/1.2/spec.html#id2772075>.
When exposing ports, use the `expose` statement if you do not want the ports to be accessible from the host system (i.e. for the database, which should be only read by the database module and teh application, both of which run in the same docker stack). For debugging purposes or for services that need to be visible externally (i.e. the messaging service), use the `ports` statement instead. However, when running multiple stacks simultaneously (i.e. during integration testing), explicitly mapping to a port on the host system would result in port clash. For this reason, never define the host port, and only give the container port which will be valid within the docker-compose stack system only.

## Testing

### Unit tests
Tests should be located in each module's subdirectory at `/test`, split into `/test/unit` and `/test/integration`. Unit tests should not depend on anything other than the module and core code and not require external containers to be running. External methods (such as db and msg methods) should be mocked. All resources required for a unit test should always be located at `/src/module-type/your-module/test/resources` and nothing should be loaded from elsewhere. If your module unit test depends on something else, this is an indication that it is not actually a unit test but an integration test, see below.

### Integration tests
Integration tests should test the module's connectivity with the container the external service (i.e. database or message middleware) is running on. To do this, the required containers are spun up using the external makefile. You can configure in that makefile how many containers you need. The details of the externally available containers (i.e. messaging) will all be available in the integration test using the environment variables `$MSG_IPx` where `x` stands for the number of the test stack and logically the node it represents.

![](./doc/img/integrationtesting.png "Connectivity during integration testing with docker-compose stacks")

Internally available services (i.e. the database container) should only be accessed from within its own stack, so ports should not be mapped to the host machine and used as they are instead.

