# Bluberry

![coverage report](coverage-badge.svg)

![bluberry logo](doc/img/bluberry.png)

©2018 University of Southampton

This project contains all code for the asynchronous, consent-based distributed database synchronisation layer for the research project [A Step Change in LMIC Prosthetics Provision through Computer Aided Design, Actimetry and Database Technologies](https://gow.epsrc.ukri.org/NGBOViewGrant.aspx?GrantRef=EP/R014213/1) (EP/R014213/1).

You can find more information about the project or our wider activities at [https://peoplepoweredprosthetics.com/](https://peoplepoweredprosthetics.com/).

## License
In theory, each module within the `/src/db` and `/src/msg` directories can have its own author(s) and licensing terms.
If none are specified on a lower level, the global license applies.

Bluberry is licensed under a dual model:

- if you're an individual, non-profit organisation or educational institution, you can use this software under [Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0)
- for all government or for profit organisations you need to get in touch with us to discuss your licensing options.

## Limitations
This code is the result of our research on a GCRF-funded project. While we came up with a concept that allows consent-based synchronisation and designed an API which provides all functionality to achieve that, this code is only a first **proof-of-concept** and not ready to be used "off the shelf" or integrated into existing systems for production. However, we encourage you to build your own implementation based on our open specification or even to re-use some of our code. These are the steps you need to take:

* Hire a software engineer if you don't already employ one. They should be familiar with the database you're trying to synchronise as well as the general concept of messaging/protocols and security.
* Decide what programming language you would like to use. You may be restricted by already existing infrastructure, but bear in mind that it needs to support
  - accessing the database you want to synchronise and
  - sending messages across networks
  We have decided to use Python but you can implement the same concept in other languages.
* Implement the API as defined by the interfaces you find in this repository. The [technical specification](doc/techspec/techspec.md) should help you with this. Where something is not specified, you're free to implement it in your own way.
* Make sure your implementation is secure by having it assessed by a security professional. If you're synchronising real patient data, your jurisdiction might impose a process for auditing software before it can be used in production. Check with your local government if you're not sure. We will not be held responsible for any data loss or leak if you fail to make sure your implementation is secure.
* If you haven't already done it, you can now acquire more fine-grained consent from the patients and set up the consent-levels you want your system to support. This involves configuring the application so that the consent levels are mapped to your existing data. You can find more information on how to do this in the [technical specification](doc/techspec/techspec.md).

If you are interested in advancing this research in collaboration with us please get in touch using *contact @ peoplepoweredprosthetics . com*.

## Why call it "Bluberry"?
Like many other plants, bluberries form [clonal colonies](https://en.wikipedia.org/wiki/Clonal_colony).
This means they are part of a group which is genetically identical but consists of individual plants that are connected.

Synchronised databases store the same data in principle but then they can diverge, depending on external factors such as consent and connectivity.

We think Bluberry describes all this neatly. 

## Acknowledgement
We would like to thank Dr. Martin Hall-May for the many conversations in which he provided valuable input to this proof of concept.

# Usage

## Requirements
You need to have [make](https://www.gnu.org/software/make/), [jq](https://stedolan.github.io/jq/), [docker](https://docs.docker.com/engine/install/) and [docker-compose](https://docs.docker.com/compose/install/) installed and to be connected to the internet.

While in theory, you can do all that on Windows, it has not been tested and it is recommended to use Linux.

## Building
Build the system using
```
make build
```

## Testing
You can test the system using
```
make test
```

For other options type `make` (without any options) or refer to the [Makefile](./Makefile).

The tests require IPv4 port forwarding so if you haven't enabled it on your machine, this is how to do it:

```bash
sudo sysctl net.ipv4.conf.all.forwarding=1
```

## Documentation
The API documentation can be built by running
```
make doc
```
and a general documentation/specification can be found [here](doc/techspec/techspec.md).

You can also implement your own version of Bluberry in another language using other technologies. The [technical specification](doc/techspec/techspec.md) will help you to do that.

## Configuration
To enable running Bluberry in production, i.e. one stack per physical machine, you need to make sure that the port(s) exposed by the messaging module are not blocked by your firewall, i.e. you need to add rules for those ports.

